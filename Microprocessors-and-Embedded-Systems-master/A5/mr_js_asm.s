@==============================================================================================
@  FILE          : mr_jsasm.s
@  PROJECT       : Assignment #5 PROG2010 - Microprocessors and Emebbeded System
@  PROGRAMMER    : Manthan Rami &  Jayson Ovishek Biswas
@  Date          : 2019-11-27
@  DESCRIPTION   : This file containt declaraton of many function are as follow:
@                  mr_jsGame, LED_OFF, POINT_TO_START, Signal_Check, BLINK_ALL, and End_Program
@                  Which are used to fullfil all the requirments of the game this function.
@===============================================================================================
.data
LEDaddress:             .word 0x48001014                                          
                        .equ LED_ON, 0xFF00
                        .equ LED_OFF,0x00FF
                        .equ LED_0,0x0200
                        .equ LED_1,0x0100
                        .equ LED_2,0x0400
                        .equ LED_3,0x8000
                        .equ LED_4,0x0800
                        .equ LED_5,0x4000
                        .equ LED_6,0x1000
                        .equ LED_7,0x2000
                        .equ DEFAULT_DELAY, 500

@ Test code for my own new function called from C
    .code   16              @ This directive selects the instruction set being generated.
                            @ The value 16 selects Thumb, with the value 32 selecting ARM.
    .text                   @ Tell the assemBLer that the upcoming section is to be considered
                            @ assemBLy language instructions - Code section (text -> ROM
@@ Function Header BLock
    .align  2               @ Code alignment - 2^n alignment (n=2)
                            @ This causes the assemBLer to use 4 byte alignment
    .syntax unified         @ Sets the instruction set to the new unified ARM + THUM
                            @ instructions. The default is divided (separate instruction sets)
    .global mr_jsGame          @ Make the symbol name for the function visiBLe to the linker
    .code   16              @ 16bit THUMB code (BOTH .code and .thumb_func are required)
    .thumb_func             @ Specifies that the following symbol is the name of a THUMB
                            @ encoded function. Necessary for interlinking between ARM and THUMB code.
    .type   mr_jsGame, %function   @ Declares that the symbol is a function (not strictly required)
@=============================================================================================================
@ Function Declaration : int mr_jsGame(int delay, char* pattern, int target)
@ Input: R0, r1 (i.e. R0 holds delay value, r1 holds pattern of light , r2 holds Target led)
@ Returns: R0
@ Here is the actual function
@=============================================================================================================
mr_jsGame:        
PUSH {LR}
MOV R4,R0                           @  DELAY
MOV R5,R1                           @  PATTERN
MOV R6,R2                           @  TargetB
MOV R8,R5                           @  saving address of array's base
MOV R7,#10                          @  setting value r7 to 10
FOREVER_LOOP:            
              LDRB R0,[R5]          @   GRAB THE CONTENTS POINTED TO
              SUB  R0,R0,#48        @   Substracting 48 from to get actuall number
              CMP  R0,#0            @   Comparing if the loop reach to the end of the array
              BLT  POINT_TO_START   @   If reach then turn off all led and reset array pointer
              MOV R10,R0            @   setting value of r10 to r0
              CMP  R0,R7            @   comparing if the value of ro is equalto r7
              BEQ   MAKE_DELAY      @   if value of r0 == r7 then call make delay
              MOV  R7,R10           @   setting value of r7 to r10
              BLINK:  MOV R0,R10    @   setting value of ro to r10
              mov  r10,#0
              BL   LOAD_LED   @   Calling BSP_LED_Toggle function tu turn on target led
              MOV  R0,R4            @   setting value of r0 t r4
              BL  HAL_Delay         @   calling Hal_delay for getting delay in milliseconds
              MOV  R0,#0            @   setting value of r0 to 0
              BL   BSP_PB_GetState  @   call BSP C function using Branch with Link (BL)
              CMP  R0, #1           @   CHECK IF THE USER PRESS BUTTON OR NOT
              BEQ  Signal_Check     @   if user press button then if call signal_check
              MOV R0,R10            @   setting value of r 0 to r10
              mov R10,#1
              BL  LOAD_LED    @   Calling BSP_LED_Toggle function tu turn off target led
              ADD R5,#1             @   increasing pointer by #1 to point next bit
BAL FOREVER_LOOP                    @   always loop bcak to forever_loo

@=============================================================================================================
@ Lable : POINT_TO_START
@ Input    : Nothing
@ Returns  : Nothing
@ Purpose  : This Function will reset the array Pointer to Starting loaction by substracting R8 from R5
@============================================================================================================
POINT_TO_START:
            MOV R5,R8              @   Reseting the counter of the Array
            BAL FOREVER_LOOP        @   Looping back to Forever_Loop
@=============================================================================================================
@ Lable : Signal_Check
@ Input    : Nothing
@ Returns  : Nothing
@ Purpose  : This function will check if the user push button on target light or missed it and based on it
@            call another function
@=============================================================================================================
Signal_Check:         
          CMP R6,R7                 @   Comparing If the user stop the cycle at target light or not
          BEQ BLINK_ALL             @   Calling BLink_ALL function if user Wins the game
          BNE LIGHT_TARGET          @   Calling Light_target if the user Lose the Game
@=============================================================================================================
@ Lable : BLINK_ALL
@ Input    : Nothing
@ Returns  : Nothing
@ Purpose  : This function will be called in Signal_Check function when user get the target light. also thisM
@            function will BLink all led Twice
@============================================================================================================
BLINK_ALL:


        ldr r1,=LEDaddress @ Load the GPIO address we need
        ldr r1, [r1] @ Dereference r1 to get the value we want
        ldrh r0,[r1] @ Get the current state of that GPIO (half word only)
        orr r0,r0,#LED_ON @ Use bitwise OR (ORR) to set the bit at 0XFF00
        strh r0,[r1] @ Write the half word back to the memory address for the GPIO
        MOV R0,#DEFAULT_DELAY
        BL HAL_Delay
        ldr r1,=LEDaddress @ Load the GPIO address we need
        ldr r1, [r1] @ Dereference r1 to get the value we want
        ldrh r0,[r1] @ Get the current state of that GPIO (half word only)
        and r0,r0,#LED_OFF @ Use bitwise OR (ORR) to set the bit at 0XFF00
        strh r0,[r1] @ Write the half word back to the memory address for the GPIO
        
        MOV R0,#DEFAULT_DELAY
        BL HAL_Delay
        
        ldr r1,=LEDaddress @ Load the GPIO address we need
        ldr r1, [r1] @ Dereference r1 to get the value we want
        ldrh r0,[r1] @ Get the current state of that GPIO (half word only)
        orr r0,r0,#LED_ON @ Use bitwise OR (ORR) to set the bit at 0x00FF
        strh r0,[r1] @ Write the half word back to the memory address for the GPIO

        MOV R0,#DEFAULT_DELAY
        BL HAL_Delay
        
        ldr r1,=LEDaddress @ Load the GPIO address we need
        ldr r1, [r1] @ Dereference r1 to get the value we want
        ldrh r0,[r1] @ Get the current state of that GPIO (half word only)
        and r0,r0,#LED_OFF @ Use bitwise OR (ORR) to set the bit at 0XFF00
        strh r0,[r1] @ Write the half word back to the memory address for the GPIO
        mov r0,#1
        BAL End_Program                                @    calling End_program TO quit asse,mBLy file
@=============================================================================================================
@ Lable    : LIGHT_TARGET
@ Input    : Nothing
@ Returns  : Nothing
@ Purpose  : This funciton will turn off all the Led and BLing only Target led if user missed it during
@            playing game
@============================================================================================================
LIGHT_TARGET:
            MOV R0,R7
            BL LED_TURN_OFF
            MOV R0,R6                             @     Setting value of R6 to R6
            BL LOAD_LED                    @     Calling BSP_LED_Toggle function tu turn on target led
            MOV R0,#0                             @     Setting value of R0 to #0 if user lose the Game
            BAL End_Program                         @     call function to  exit the program
@=============================================================================================================
@ Lable    : MAKE_DELAY
@ Input    : Nothing
@ Returns  : Nothing
@ Purpose  : This funciton will create a delay of milliseconds provided by the user.
@============================================================================================================
MAKE_DELAY:
            MOV R0,R4                           @       setting value of r0 to delay value
            BL HAL_Delay                        @       calling hal_delay function to get delay
            BAL BLINK                           @       Always link back to blink lable
@=============================================================================================================
@ Lable    : LOAD_LED
@ Input    : Nothing
@ Returns  : Nothing
@ Purpose  : This funciton wILL LOAD appropriate led address to the register  according to the user input 
@============================================================================================================
LOAD_LED:
        MOV r11,lr                              
        CMP R0,#0
        BEQ LED0
        CMP R0,#1
        BEQ LED1
        CMP R0,#2
        BEQ LED2
        CMP R0,#3
        BEQ LED3
        CMP R0,#4
        BEQ LED4
        CMP R0,#5
        BEQ LED5
        CMP R0,#6
        BEQ LED6
        CMP R0,#7
        BEQ LED7    
        mov lr,r11    
        BX LR
@====================================================================
@ Lable    : LED0
@ Input    : Nothing
@ Returns  : Nothing
@ Purpose  : This funciton wILL LOAD led 0 address to the register.
@===================================================================
LED0:
    mov r9,lr
    MOV R11 ,#LED_0
    CMP R10,#0
    IT EQ
    BLEQ  LED_TURN_ON
    CMP r10,#0
    IT NE
    BLNE  LED_TURN_OFF
    mov lr,r9
    BX LR
@====================================================================
@ Lable    : LED1
@ Input    : Nothing
@ Returns  : Nothing
@ Purpose  : This funciton wILL LOAD led 0 address to the register.
@===================================================================
LED1:
    mov r9,lr
    MOV R11,#LED_1
    CMP R10,#0
    IT EQ
    BLEQ  LED_TURN_ON
    CMP r10,#0
    IT NE
    BLNE  LED_TURN_OFF
    mov lr,r9
    BX LR
@====================================================================
@ Lable    : LED2
@ Input    : Nothing
@ Returns  : Nothing
@ Purpose  : This funciton wILL LOAD led 0 address to the register.
@===================================================================
LED2:
    mov r9,lr
    MOV R11 ,#LED_2
    CMP R10,#0
    IT EQ
    BLEQ  LED_TURN_ON
    CMP r10,#0
    IT NE
    BLNE  LED_TURN_OFF
    mov lr,r9
    BX LR
@====================================================================
@ Lable    : LED3
@ Input    : Nothing
@ Returns  : Nothing
@ Purpose  : This funciton wILL LOAD led 0 address to the register.
@===================================================================
LED3:
    MOV R11,#LED_3
    mov r9,lr
    CMP R10,#0
    IT EQ
    BLEQ  LED_TURN_ON
    CMP r10,#0
    IT NE
    BLNE  LED_TURN_OFF
    mov lr,r9
    BX LR
@====================================================================
@ Lable    : LED4
@ Input    : Nothing
@ Returns  : Nothing
@ Purpose  : This funciton wILL LOAD led 0 address to the register.
@===================================================================
LED4:
    mov r9,lr
    MOV R11,#LED_4    
    CMP R10,#0
    IT EQ
    BLEQ  LED_TURN_ON
    CMP r10,#0
    IT NE
    BLNE  LED_TURN_OFF
    mov lr,r9
    BX LR
@====================================================================
@ Lable    : LED5
@ Input    : Nothing
@ Returns  : Nothing
@ Purpose  : This funciton wILL LOAD led 0 address to the register.
@===================================================================
LED5:
    MOV R11,#LED_5
    mov r9,lr
    CMP R10,#0
    IT EQ
    BLEQ  LED_TURN_ON
    CMP r10,#0
    IT NE
    BLNE  LED_TURN_OFF
    mov lr,r9
    BX LR
@====================================================================
@ Lable    : LED6
@ Input    : Nothing
@ Returns  : Nothing
@ Purpose  : This funciton wILL LOAD led 0 address to the register.
@===================================================================
LED6:
    mov r9,lr
    MOV R11,#LED_6
    CMP R10,#0
    IT EQ
    BLEQ  LED_TURN_ON
    CMP r10,#0
    IT NE
    BLNE  LED_TURN_OFF
    mov lr,r9
    BX LR
@====================================================================
@ Lable    : LED7
@ Input    : Nothing
@ Returns  : Nothing
@ Purpose  : This funciton wILL LOAD led 0 address to the register.
@===================================================================
LED7:
    mov r9,lr
    MOV R11,#LED_7
    CMP R10,#0
    IT EQ
    BLEQ  LED_TURN_ON
    CMP r10,#0
    IT NE
    BLNE  LED_TURN_OFF
    mov lr,r9
    BX LR
@=============================================================================================================
@ Lable    : LED_TURN_ON
@ Input    : Nothing
@ Returns  : Nothing
@ Purpose  : This funciton wILL LOAD appropriate led address to the register  according to the user input 
@============================================================================================================
LED_TURN_ON:
        ldr r1,=LEDaddress @ Load the GPIO address we need
        ldr r1, [r1] @ Dereference r1 to get the value we want
        ldrh r0,[r1] @ Get the current state of that GPIO (half word only)
        orr r0,r0,r11 @ Use bitwise OR (ORR) to set the bit at 0XFF00
        strh r0,[r1] @ Write the half word back to the memory address for the GPIO    
        bx lr
@=============================================================================================================
@ Lable    : LED_TURN_OFF
@ Input    : Nothing
@ Returns  : Nothing
@ Purpose  : This funciton wILL LOAD appropriate led address to the register  according to the user input 
@============================================================================================================
LED_TURN_OFF:
        ldr r1,=LEDaddress @ Load the GPIO address we need
        ldr r1, [r1] @ Dereference r1 to get the value we want
        ldrh r0,[r1] @ Get the current state of that GPIO (half word only)
        and r0,r0,#LED_OFF @ Use bitwise OR (ORR) to set the bit at 0XFF00
        strh r0,[r1] @ Write the half word back to the memory address for the GPIO
        bx lr
@=============================================================================================================
@ Lable : End_Program
@ Input    : Nothing
@ Returns  : Nothing
@ Purpose  : This function will quit assemBLy file safely by getting back the link register
@============================================================================================================
End_Program:
            POP {LR}                            @       POPing out Lr
            BX LR                               @       returning to the previous location
.size   mr_jsGame, .-mr_jsGame    @@ - symbol size (not req)



@=============================================================================================================
   .code   16              @ This directive selects the instruction set being generated.
                            @ The value 16 selects Thumb, with the value 32 selecting ARM.
    .text                   @ Tell the assemBLer that the upcoming section is to be considered
                            @ assemBLy language instructions - Code section (text -> ROM
@@ Function Header BLock
    .align  2               @ Code alignment - 2^n alignment (n=2)
                            @ This causes the assemBLer to use 4 byte alignment
    .syntax unified         @ Sets the instruction set to the new unified ARM + THUM
                            @ instructions. The default is divided (separate instruction sets)
    .global mr_jsWatch          @ Make the symbol name for the function visiBLe to the linker
    .code   16              @ 16bit THUMB code (BOTH .code and .thumb_func are required)
    .thumb_func             @ Specifies that the following symbol is the name of a THUMB
                            @ encoded function. Necessary for interlinking between ARM and THUMB code.
    .type   mr_jsWatch, %function   @ Declares that the symbol is a function (not strictly required)
@=============================================================================================================

@=================================================================================
@ Lable    : mr_jsWatch
@ Input    : R0 : delay value@           
@ Returns  : Nothing
@ Purpose  : This function will loop all led blink and refresh watchdog every time
@           till the user press button and the it count down watchdog and reset 
@            the  stm32f3 board.
@=================================================================================
mr_jsWatch:
            
            MOV R4,R0
            MOV R5,LR
            PUSH {LR,R0}            
        LOOP_LED:            
            BL mes_IWDGRefresh
            MOV R11,#LED_ON
            BL LED_TURN_ON
            MOV LR,R5
            MOV R0,R4
            BL HAL_Delay
            MOV LR,R5
            MOV R0,#0
            BL BSP_PB_GetState
            CMP R0,#1
            BEQ QUIT
            MOV LR,R5
            MOV R11,#LED_OFF
            BL LED_TURN_OFF
            BAL LOOP_LED
@====================================================================
@ Lable    : QUIT
@ Input    : Nothing           
@ Returns  : Nothing
@ Purpose  : This function will allow watch dog to count down without
@            refreshing it 
@=================================================================== 
QUIT:
            MOV R11,#LED_OFF
            BL LED_TURN_OFF
            MOV LR,R5
            MOV R0,#DEFAULT_DELAY
            BL HAL_Delay   
            MOV LR,R5 
            MOV R11,#LED_ON
            BL LED_TURN_ON
            BAL QUIT
