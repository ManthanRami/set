@==============================================================================================
@  FILE          : mr_asm.s
@  PROJECT       : Assignment #3 PROG2010 - Microprocessors and Emebbeded System
@  PROGRAMMER    : Manthan Rami
@  Date          : 2019-10-30
@  DESCRIPTION   : This file containt declaraton of many function are as follow:
@                  mrGame, LED_OFF, POINT_TO_START, Signal_Check, BLINK_ALL, and End_Program
@                  Which are used to fullfil all the requirments of the game this function.
@===============================================================================================
@ Test code for my own new function called from C
    .code   16              @ This directive selects the instruction set being generated.
                            @ The value 16 selects Thumb, with the value 32 selecting ARM.

    .text                   @ Tell the assemBLer that the upcoming section is to be considered
                            @ assemBLy language instructions - Code section (text -> ROM)

@@ Function Header BLock
    .align  2               @ Code alignment - 2^n alignment (n=2)
                            @ This causes the assemBLer to use 4 byte alignment

    .syntax unified         @ Sets the instruction set to the new unified ARM + THUMB
                            @ instructions. The default is divided (separate instruction sets)

    .global mrGame          @ Make the symbol name for the function visiBLe to the linker

    .code   16              @ 16bit THUMB code (BOTH .code and .thumb_func are required)
    .thumb_func             @ Specifies that the following symbol is the name of a THUMB
                            @ encoded function. Necessary for interlinking between ARM and THUMB code.

    .type   mrGame, %function   @ Declares that the symbol is a function (not strictly required)



@=============================================================================================================
@ Function Declaration : int mrGame(int delay, char* pattern, int target)
@ Input: R0, r1 (i.e. R0 holds delay value, r1 holds pattern of light , r2 holds Target led)
@ Returns: R0
@ Here is the actual function
@=============================================================================================================
mrGame:
PUSH {LR}
MOV R4,R0                           @  DELAY
MOV R5,R1                           @  PATTERN
MOV R6,R2                           @  TargetB
MOV R8,R5                           @  saving address of array's base
MOV R7,#10                          @  setting value r7 to 10
FOREVER_LOOP:
              LDRB R0,[R5]          @   GRAB THE CONTENTS POINTED TO
              SUB  R0,R0,#48        @   Substracting 48 from to get actuall number
              CMP  R0,#0            @   Comparing if the loop reach to the end of the array
              BLT  POINT_TO_START   @   If reach then turn off all led and reset array pointer
              MOV R10,R0            @   setting value of r10 to r0
              CMP  R0,R7            @   comparing if the value of ro is equalto r7
              BEQ   MAKE_DELAY      @   if value of r0 == r7 then call make delay
              MOV  R7,R10           @   setting value of r7 to r10
              BLINK:  MOV R0,R10    @   setting value of ro to r10
              BL   BSP_LED_Toggle   @   Calling BSP_LED_Toggle function tu turn on target led
              MOV  R0,R4            @   setting value of r0 t r4
              BL  HAL_Delay         @   calling Hal_delay for getting delay in milliseconds
              MOV  R0,#0            @   setting value of r0 to 0
              BL   BSP_PB_GetState  @   call BSP C function using Branch with Link (BL)
              CMP  R0, #1           @   CHECK IF THE USER PRESS BUTTON OR NOT
              BEQ  Signal_Check     @   if user press button then if call signal_check
              MOV R0,R10            @   setting value of r 0 to r10
              BL  BSP_LED_Toggle    @   Calling BSP_LED_Toggle function tu turn off target led
              ADD R5,#1             @   increasing pointer by #1 to point next bit
BAL FOREVER_LOOP                    @   always loop bcak to forever_loop
@=============================================================================================================
@ Lable : POINT_TO_START
@ Input    : Nothing
@ Returns  : Nothing
@ Purpose  : This Function will reset the array Pointer to Starting loaction by substracting R8 from R5
@============================================================================================================
POINT_TO_START:
            MOV R5,R8              @   Reseting the counter of the Array
            BAL FOREVER_LOOP        @   Looping back to Forever_Loop
@=============================================================================================================
@ Lable : Signal_Check
@ Input    : Nothing
@ Returns  : Nothing
@ Purpose  : This function will check if the user push button on target light or missed it and based on it
@            call another function
@=============================================================================================================
Signal_Check:
         @BKPT
          CMP R6,R7                 @   Comparing If the user stop the cycle at target light or not
          BEQ BLINK_ALL             @   Calling BLink_ALL function if user Wins the game
          BNE LIGHT_TARGET          @   Calling Light_target if the user Lose the Game
@=============================================================================================================
@ Lable : BLINK_ALL
@ Input    : Nothing
@ Returns  : Nothing
@ Purpose  : This function will be called in Signal_Check function when user get the target light. also thisM
@            function will BLink all led Twice
@============================================================================================================
BLINK_ALL:
        MOV R0,R7
        BL BSP_LED_Off
        MOV R5,#7                                  @    Setting value of R5 to #7
        MOV R6,#3
        LOOP_WIN:

                LOOP_ON:
                        MOV R0,R5                          @    Setting value of R0 to R5
                        BL BSP_LED_Toggle                  @    Calling BSP_LED_OFF to turn off all LED
                        SUB R5,R5,#1                       @    Substracting #1 from R5 to loop through 7 to 0
                        CMP R5,#0
                BGE LOOP_ON
                MOV R0,#200
                BL HAL_Delay
                MOV R5 ,#7
        SUB R6,R6,#1
        CMP R6,#0
        BGE LOOP_WIN
        MOV R0,#1
        BAL End_Program                                @    calling End_program TO quit asse,mBLy file
@=============================================================================================================
@ Lable : LIGHT_TARGET
@ Input    : Nothing
@ Returns  : Nothing
@ Purpose  : This funciton will turn off all the Led and BLing only Target led if user missed it during
@            playing game
@============================================================================================================
LIGHT_TARGET:
            MOV R0,R7
            BL BSP_LED_Off
            MOV R0,R6                             @     Setting value of R6 to R6
            BL BSP_LED_Toggle                     @     Calling BSP_LED_Toggle function tu turn on target led
            MOV R0,#0                             @     Setting value of R0 to #0 if user lose the Game
            BAL End_Program                         @     call function to  exit the program
@=============================================================================================================
@ Lable : MAKE_DELAY
@ Input    : Nothing
@ Returns  : Nothing
@ Purpose  : This funciton will create a delay of milliseconds provided by the user.
@============================================================================================================
MAKE_DELAY:
            MOV R0,R4                           @       setting value of r0 to delay value
            BL HAL_Delay                        @       calling hal_delay function to get delay
            BAL BLINK                           @       Always link back to blink lable
@=============================================================================================================
@ Lable : End_Program
@ Input    : Nothing
@ Returns  : Nothing
@ Purpose  : This function will quit assemBLy file safely by getting back the link register
@============================================================================================================
End_Program:
            POP {LR}                            @       POPing out Lr
            BX LR                               @       returning to the previous location

.size   mrGame, .-mrGame    @@ - symbol size (not req)
@ AssemBLy file ended by single .end directive on its own line
.end
