/*
*  FILE          : mr_hook.c
*  PROJECT       : Assignment #3 PROG2010 - Microprocessors and Emebbeded System
*  PROGRAMMER    : Manthan Rami
*  Date          : 2019-10-30
*  DESCRIPTION   : This Program will take three user input 1st Delay in millisecond 2nd Pattern of light to blink
                   and 3rd Targeted light, Incase of not providing inputs it will works with default value for all 
                   inputs. It will start a game using this inputs.
*/

//INCLUDE
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>
#include "common.h"
#include "stm32f3xx_hal.h"

//CONSTANT DECLARATION
#define DEFAULT_DELAY 500
#define DEFAULT_TARGET 1


//PROTOTYPE DECLARATION
int mrGame(int delay, char* pattern,int target);

/*
* FUNCTION    : mrA3
* DESCRIPTION : PURPOSE OF THIS FUNCTION IS TO ADD AN FUNCTION CALL TO
                THE MINICOM MENU SO THAT WE CAN INTERACT WITH IT
* PARAMETERS  : action -INT
* RETURNS     : NOTHING
*/
void mrA3(int action)
{

    char *dest;
    uint32_t delay;
    uint32_t target;
    int fetch_status;
    int redcode;
    if(action==CMD_SHORT_HELP)
    {
      return;
    }

    if(action==CMD_LONG_HELP)
    {
      printf("Addition Test\n\n""This command tests new addition function\n");
      return;
    }

    fetch_status = fetch_uint32_arg(&delay);
    if(fetch_status)
    {
      //Use a default delay value
      delay = DEFAULT_DELAY;
    }
    fetch_status = fetch_string_arg(&dest);
    if(fetch_status)
     {
       dest="01357642";
     }
    fetch_status = fetch_uint32_arg(&target);
    if(fetch_status)
    {
    	// Use a default delay value
      target = DEFAULT_TARGET;
    }
    redcode=mrGame(delay,dest,target );
        if(redcode==1)
        {
          printf("You Won the Game !!\n");
        }
        else
        {
           printf("You Lose the Game !!\n");
        }
}

//ADDING FUNCTION NAME IN MINCOM MENU
ADD_CMD("mrA3", mrA3,"<Delay> <Pattern> <Target> ")
