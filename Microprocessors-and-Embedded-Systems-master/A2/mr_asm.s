@  FILE          : mr_asm.s
@  PROJECT       : Assignment #2 PROG2010 - Microprocessors and Emebbeded System
@  PROGRAMMER    : Manthan Rami
@  Date          : 2019-10-09
@  DESCRIPTION   : This file containt declaraton of two function 1) mr_led_demo and 2)busy_delay both are used in this assignment 2 
@                  this program will loop throught blinking led using the value of count given by user and busy_delay will make a 
@                  delay between led blinks.
@  

@ Test code for my own new function called from C

@ This is a comment. Anything after an @ symbol is ignored.
@@ This is also a comment. Some people use double @@ symbols.


    .code   16              @ This directive selects the instruction set being generated.
                            @ The value 16 selects Thumb, with the value 32 selecting ARM.

    .text                   @ Tell the assembler that the upcoming section is to be considered
                            @ assembly language instructions - Code section (text -> ROM)

@@ Function Header Block
    .align  2               @ Code alignment - 2^n alignment (n=2)
                            @ This causes the assembler to use 4 byte alignment

    .syntax unified         @ Sets the instruction set to the new unified ARM + THUMB
                            @ instructions. The default is divided (separate instruction sets)

    .global mr_led_demo        @ Make the symbol name for the function visible to the linker

    .code   16              @ 16bit THUMB code (BOTH .code and .thumb_func are required)
    .thumb_func             @ Specifies that the following symbol is the name of a THUMB
                            @ encoded function. Necessary for interlinking between ARM and THUMB code.

    .type   mr_led_demo, %function   @ Declares that the symbol is a function (not strictly required)




@ Function Declaration : int mr_led_demo(int count, int delay)
@ Input: r0, r1 (i.e. r0 holds count value, r1 holds delay value)
@ Returns: r0
@ Here is the actual function
mr_led_demo:

  MOV R10 ,R0    @COUNT
  MOV R5 ,R1     @DELAY

  MOV R6,#1     @LED ON OFF LOOP
  MOV R7,#0     @COUNTER FOR IT

  MOV R8, #7    @NUMBER OF LED
  MOV R9, #0    @COUNTER FOR LED BLINK
  MOV R4,#1

  PUSH {LR,R0}                                                  @Pusing value of all variables to stack
    COUNT_LOOP:                                                 @Counter used for managing count provided by user 
                MOV R7,#0                                       @setting R7 to #0
                LED_ONOFF:                                      @LOOP FOR LED ON AND OFF
                            MOV R9,#0                             @setting R9 to #0
                            LED_BLINK:                            @Loop for blinking led on and off
                                    MOV R0,R5                   @SETTING VALUECOF r0 TO DELAY VALUE PROVIDED BY USER 
                                    BL busy_delay               @CALLING BUSY DELAY FUNCTION
                                    MOV R0,R9                   @SETTING VALUE OF r0 to r9
                                    BL   BSP_LED_Toggle         @ call BSP C function using Branch with Link (bl)
                                    ADD R9, R9, #1              @ ADDING 1 TO R9
                            CMP R9, #7                            @COMPARING R9 WITH 7
                            BLE LED_BLINK                         @LOOP IT AGAIN TO LED_BLINK
                ADD R7,R7, #1                                     @ADDING 1 TO R7
                CMP R7,R6                                         @COMPARING R7,WITH R6
                BLE LED_ONOFF                                     @lOOPING BACK
    ADD R4,R4, #1                                             @ADDING 1 TO R4
    CMP R4,R10                                                @COMPARING R4 WITH R10
    BLE COUNT_LOOP                                            @LOOPING BACK   
  POP {LR,R0}                                          @POPING OUT THE VALUE 
  BX LR                                                         
.size   mr_led_demo, .-mr_led_demo    @@ - symbol size (not req)

@ Function Declaration : int busy_delay(int cycles)
@
@ Input: r0 (i.e. r0 holds number of cycles to delay)
@ Returns: r0
@ REFRENCE : GIVEN BY PROFESSOR E.D 

@ Here is the actual function
busy_delay:

    push {r4}

    mov r4, r0

delay_loop:
    subs r4, r4, #1

    bgt delay_loop

    mov r0, #0                      @ Return zero (always successful)

    pop {r4}

    bx lr                           @ Return (Branch eXchange) to the address in the link register (lr)

@ Assembly file ended by single .end directive on its own line
.end