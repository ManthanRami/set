/*
*  FILE          : mr_hook.c
*  PROJECT       : Assignment #2 PROG2010 - Microprocessors and Emebbeded System
*  PROGRAMMER    : Manthan Rami
*  Date          : 2019-10-09
*  DESCRIPTION   : This Program will take two user input 1 for count and 2 for delay using it,
                   it will call mr_led_demo function and blink led using value provided by user
                   if user not provide any value for count and delay it will set to deafault values.
*/

//INCLUDE 
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>
#include "common.h"


//CONSTANT DECLARATION
#define DEFAULT_DELAY 0XFFFFF
#define DEFAULT_COUNT 3

//PROTOTYPE DECLARATION 
int mr_led_demo(int count, int delay);

/*
* FUNCTION    : mrA2
* DESCRIPTION : THIS FUNCTION WILL TAKE INPUT FROM THE USER FOR COUNT AND DELAY
                AND CHECK IF THE USER HAS PROVID INPUT OR NOT IF NOT THEN IT 
                WILL PROVID DEFAULT VALUE TO THE COUNT AND DELAY AND CALL mr_led_demo.
* PARAMETERS  : action -INT
* RETURNS     : NOTHING
*/
void mrA2(int action)
{

    uint32_t delay;
    uint32_t count;
    int fetch_status;

    if(action==CMD_SHORT_HELP) 
    {
      return;
    }    
    if(action==CMD_LONG_HELP) 
    {
      printf("Addition Test\n\n""This command tests new addition function\n");
      return;
    }
    fetch_status = fetch_uint32_arg(&count);
    if(fetch_status)
    {
    	// Use a default delay value
      count = DEFAULT_COUNT;
    }

    fetch_status = fetch_uint32_arg(&delay);
    if(fetch_status)
    {
      // Use a default delay value
      delay = DEFAULT_DELAY;
    }
  printf("mrA2 returned number of loop: %d\n", mr_led_demo(count,delay) );
}

//ADDING FUNCTION NAME IN MINCOM MENU
ADD_CMD("mrA2", mrA2," <count> <delay> will loop through blinking led")
