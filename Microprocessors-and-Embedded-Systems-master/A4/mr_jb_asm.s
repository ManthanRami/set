@==============================================================================================
@  FILE          : mr_asm.s
@  PROJECT       : Assignment #4 PROG2010 - Microprocessors and Emebbeded System
@  PROGRAMMER    : Manthan Rami, Jayson Ovishek Biswas 
@  Date          : 2019-11-14
@  DESCRIPTION   : This file containt declaraton of many function are as follow:
@                  mr_JbTilt, whichLed and End_Program
@                  Which are used to fullfil all the requirments of the game this function.
@===============================================================================================
@ Test code for my own new function called from C
    .code   16              @ This directive selects the instruction set being generated.
                            @ The value 16 selects Thumb, with the value 32 selecting ARM.

    .text                   @ Tell the assemBLer that the upcoming section is to be considered
                            @ assemBLy language instructions - Code section (text -> ROM)

@@ Function Header BLock
    .align  2               @ Code alignment - 2^n alignment (n=2)
                            @ This causes the assemBLer to use 4 byte alignment

    .syntax unified         @ Sets the instruction set to the new unified ARM + THUMB
                            @ instructions. The default is divided (separate instruction sets)

    .global mr_jbA4          @ Make the symbol name for the function visiBLe to the linker

    .code   16              @ 16bit THUMB code (BOTH .code and .thumb_func are required)
    .thumb_func             @ Specifies that the following symbol is the name of a THUMB
                            @ encoded function. Necessary for interlinking between ARM and THUMB code.

    .type   mr_jbA4, %function   @ Declares that the symbol is a function (not strictly required)
@=============================================================================================================
@ Function Declaration : int mr_jbA4(int stayTime, int Target, int gameTime )
@ Input: R0, r1 (i.e. R0 holds stayTime value, r1 holds Target led , r2 holds gameTime led)
@ Returns: R0
@ Here is the actual function
@=============================================================================================================
mr_jbA4:
PUSH {LR}                           @  pushing Lr to stack
MOV R4,R0                           @  stayTime
MOV R5,R1                           @  Target
MOV R6,R2                           @  gameTime

        MOV R0, #0x32
        MOV R1, #0x29
        BL COMPASSACCELERO_IO_Read        
        sxtb r0, r0                 @sxtb it
        mov r3, #32                 @store 32 for dividing
        sdiv r4, r0, r3             @divide by 32
        MOV R0,#0x32
        MOV R1,#0x2B
        BL COMPASSACCELERO_IO_Read
        sxtb r0, r0                 @sxtb it
        mov r3, #32                 @store 32 for dividing
        sdiv r5, r0, r3             @divide by 32
        bl WHICH_LED
        BAL End_Program


@=============================================================================================================
@ Lable Declaration :  WHICH_LED
@ Input: R4 and r5
@ Returns: Nothing
@ Description: This function turn off all led if any led is on and then on basis of x and y value it turn on 
@              led.
@=============================================================================================================
WHICH_LED:    
        MOV R6,#7                                  @    Setting value of R5 to #7
        LOOP_OFF:
        MOV R0,R6                          @    Setting value of R0 to R5
        BL BSP_LED_Off                     @    Calling BSP_LED_OFF to turn off all LED
        SUB R6,R6,#1                       @    Substracting #1 from R5 to loop through 7 to 0
        CMP R6,#0                          @    Comparing if the Value of R5 is equal to 0
        BGE LOOP_OFF
          
        CMP R4,#0
        BNE NEXT
        CMP R5,#0
        BLE NEXT
        MOV R0,#3
        BL BSP_LED_Toggle
    NEXT:
        CMP R4,#0
        BLE NEXT1
        CMP R5,#0
        BNE NEXT1
        MOV R0,#0
        BL BSP_LED_Toggle
    NEXT1:
        CMP R4,#0
        BGE NEXT2
        CMP R5,#0
        BNE NEXT2
        MOV R0,#7
        BL BSP_LED_Toggle
    NEXT2:
        CMP R4,#0
        BNE NEXT3
        CMP R5,#0
        BGE NEXT3
        MOV R0,#4
        BL BSP_LED_Toggle
    NEXT3:
        CMP R4,#0
        BLE NEXT4
        CMP R5,#0
        BLE NEXT4
        MOV R0,#1
        BL BSP_LED_Toggle
    NEXT4:
        CMP R4,#0
        BGE NEXT5
        CMP R5,#0
        BLE NEXT5
        MOV R0,#5
        BL BSP_LED_Toggle
    NEXT5:
        CMP R4,#0
        BGE NEXT6
        CMP R5,#0
        BGE NEXT6
        MOV R0,#6
        BL BSP_LED_Toggle
    NEXT6:
        CMP R4,#0
        BLE End_Program
        CMP R5,#0
        BGE End_Program
        MOV R0,#2
        BL BSP_LED_Toggle
        bal End_Program
@=============================================================================================================
@ Function : End_Program
@ Input    : Nothing
@ Returns  : Nothing
@ Purpose  : This function will quit assemBLy file safely by getting back the link register
@============================================================================================================
End_Program:
            POP {LR}                            @       POPing out Lr
            BX LR                               @       returning to the previous location

.size   mr_jbA4, .-mr_jbA4    @@ - symbol size (not req)
@ AssemBLy file ended by single .end directive on its own line
.end
