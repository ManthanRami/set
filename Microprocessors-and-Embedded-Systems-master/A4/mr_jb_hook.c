/*
*  FILE          : mr_hook.c
*  PROJECT       : Assignment #4 PROG2010 - Microprocessors and Emebbeded System
*  PROGRAMMER    : Manthan Rami, Jayson Ovishek Biswas 
*  Date          : 2019-11-14
*  DESCRIPTION   : This Program will take three user input 1st stayTime 2nd target led 3rd GameTime
                    using this it starts game which read accelorometer and on base of x and y value
                    it blinks up led.
*/
//INCLUDE

#include <stdio.h>
#include<stdbool.h>
#include <stdint.h>
#include <ctype.h>
#include "common.h"
#include "stm32f3xx_hal.h"
#include "stm32f3_discovery.h"

//CONSTANT DECLARATION
#define DEFAULT_TARGET 5
#define DEFAULT_GAME_TIME 30
#define DEFAULT_STAY_TIME 500

//global declaration
volatile bool isRun = false;
uint32_t runSecond=0;
uint32_t gapTime=0;
uint32_t counter=0;
uint32_t gameTime;
uint32_t target;
uint32_t stayTime;

//PROTOTYPE DECLARATION
int mr_jbA4(int stayTime,int target, int gameTime);

void mrA3(int action)
{
  int fetch_status;
  if(action==CMD_SHORT_HELP)
  {
    return;
  }
  if(action==CMD_LONG_HELP)
  {
    printf("Addition Test\n\n""This command tests new addition function\n");
    return;
  }
  fetch_status = fetch_uint32_arg(&stayTime);
  if(fetch_status)
  {
    //Use a default stayTime value
    stayTime = DEFAULT_STAY_TIME;
  }
  fetch_status = fetch_uint32_arg(&target);
  if(fetch_status)
  {
    // Use a default TARGET value
    target = DEFAULT_TARGET;
  }
  fetch_status = fetch_uint32_arg(&gameTime);
  if(fetch_status)
  {
    //Use a default gameTime value
    gameTime = DEFAULT_GAME_TIME;
  }
  isRun=true;
}

//ADDING FUNCTION NAME IN MINCOM MENU
ADD_CMD("mrA3", mrA3,"<StayTime> <Target Led> <GameTime> ")

void  mr_jb_Tick(void)
{
  if(isRun)
  {
    if(runSecond!=gameTime)
    {
      gapTime++;
      counter++;
      if(gapTime==1000)
      {
        runSecond++;
        gapTime=0;
      }
      if(counter==200)
      {
        counter=0;
        mr_jbA4(25,2,5);
      }
    }
    else
    {
      isRun=false;
    }
  }
}









