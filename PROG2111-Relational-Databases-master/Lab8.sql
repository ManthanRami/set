/*==============================================================
* Student Name:   Manthan Rami
* Student Number: 8628901
* Date of Submit: 12-11-2019
* Description:    This file contains some Structured Query 
                  answers of Lab7 Questions.
==================================================================*/

-- Selecting NorthWind Database -- 
    USE Northwind;
/*==============================================================
                            Question 1
===============================================================*/
    select CustomerID,ContactName,Country,City from Customers;
/*==============================================================
                            Question 2
===============================================================*/
    select distinct country from Customers order by country;
/*==============================================================
                            Question 3
===============================================================*/
    select CompanyName,City from customers where Country='Germany';
/*==============================================================
                            Question 4
===============================================================*/
    select CustomerID,ContactName from customers where Fax is null;
/*==============================================================
                            Question 5
===============================================================*/
    select count(ProductID) from products;
/*==============================================================
                            Question 6
===============================================================*/
    select ProductID,ProductName,UnitPrice from products;
/*==============================================================
                            Question 7
===============================================================*/
    select ProductName,UnitsInStock,UnitPrice from products where 
    unitprice>20 order by UnitPrice DESC ;
/*==============================================================
                            Question 8
===============================================================*/
    elect count(ProductId) from products where Discontinued=-1;
/*==============================================================
                            Question 9
===============================================================*/
    select CategoryName,ProductName from categories,products where 
    categories.categoryID=products.categoryID;
/*==============================================================
                            Question 10
===============================================================*/
    SELECT concat(Title, " " , FirstName , " " , LastName) as 
    Salutation FROM employees;