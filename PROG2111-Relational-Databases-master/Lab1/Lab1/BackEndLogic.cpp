/*
* FILE : BackEndLogic.cs
* PROJECT : PROG2111 - LAB #1
* PROGRAMMER : Manthan Rami
* FIRST VERSION : 2019-09-17
* DESCRIPTION :
 This file contains all the function defination whioch is used in project1. 
 moreover this file contains the backend logic of the web server how server is managing the data.
*/

#include"Header.h"
/*
 * Function: startStop()
 * Description: This function start program and load all data into a list and at the end it empty list and turn off
 * Parameters:  bool &state:
				list<node>&myList:
				node mydata:
 * Returns: 0 if the insertion failed, 1 otherwise
*/
int startStop(bool& state, list<node>& myList, node mydata)
{
	fstream pFile("test.txt");
	if (!state)
	{
		state = true;
		//=================================================================
		if (pFile.is_open())
		{
			string line = "";
			int i = 0;
			while (getline(pFile, line))
			{
				char char_array[1000];
				string data_seperate[5] = {};
				strcpy(char_array, line.c_str());
				char* token = strtok(char_array, ",");
				i = 0;
				while (token)
				{
					data_seperate[i] = token;
					i++;
					token = strtok(NULL, ",");
				}
				//=================================================================
				mydata.spsid = data_seperate[0];
				mydata.fieldID = atoi(data_seperate[1].c_str());
				mydata.iFuel = atoi(data_seperate[2].c_str());
				mydata.iproduct = atoi(data_seperate[3].c_str());
				mydata.productID = data_seperate[4];
				myList.push_back(mydata);
			}
			pFile.close();

		}
		else
		{
			cout << "File Doesn't exist" << endl;
			return -1;
		}
	}
	else
	{
		state = false;
		myList.clear();
	}
	return 0;
}


/*
 * Function: insertData()
 * Description: This function inserts an element in the correct location in the list, sorted rating.
 * Parameters: list<node>&myList
			   node mydata
 * Returns: 0 if the insertion failed, 1 otherwise
*/
int insertData(list<node>& myList, node mydata)
{
	ofstream pWriteFile("test.txt");
	string whatToDO = "y";
	int counter = 0;
	bool judge = true;
	std::chrono::time_point<std::chrono::system_clock> start, end;		//time varaible initializing

	if (pWriteFile.is_open())
	{
		while (whatToDO == "y")
		{
			char spsid[24] = { NULL };
			char productID[24] = { NULL };

			//=================================================================
			cout << "\nPlease Enter Information:" << endl;		//prompting for user input
			cout << "SPSID: ";
			cin >> spsid;		
			while (judge)
			{
				CHECK:size_t len = strlen(spsid);
				counter = 0;
				while ( counter < len)
				{
					while(!isalnum(spsid[counter]))
					{
						cout << "\nPlease Enter Valid Input!!" << endl;
						cout << "SPSID: ";
						cin >> spsid;	
						goto CHECK;
					}
					counter++;
					judge = false;
				}				
			}
			//=================================================================
			cout << "FieldID(1-255) : ";			//prompting for user input
			mydata.fieldID = getNum();
			CHECK_NUM1:while(mydata.fieldID>255|| mydata.fieldID == -1|| mydata.fieldID == 0)
			{
				cout << "\nPlease Enter Valid Input!!" << endl;
				cout << "FieldID(1-255) : ";
				mydata.fieldID = getNum();
				goto CHECK_NUM1;
			}
		
			//=================================================================
			cout << "IFuel(1-350) : ";
			mydata.iFuel = getNum();							//prompting for user input
			CHECK_NUM2:while (mydata.iFuel > 350 || mydata.iFuel == -1|| mydata.iFuel == 0)
			{
				cout << "\nPlease Enter Valid Input!!" << endl;
				cout << "IFuel(1-350) : ";
				mydata.iFuel = getNum();
				goto CHECK_NUM2;
			}
			//=================================================================
			cout << "IProduct(1-1200): ";						//prompting for user input
			mydata.iproduct = getNum();
			CHECK_NUM3:while (mydata.iproduct > 1200 || mydata.iproduct==-1|| mydata.iproduct==0)
			{
				cout << "\nPlease Enter Valid Input!!" << endl;
				cout << "IProduct(1-1200): ";
				mydata.iproduct = getNum();
				goto CHECK_NUM3;
			}
			//=================================================================
			cout << "ProductID: ";
			cin >> productID;						//prompting for user input
			judge = true;
			while (judge)
			{
				CHECK1:size_t len = strlen(productID);
				counter = 0;
				while (counter < len)
				{
					while (!isalnum(productID[counter]))		//validification
					{
						cout << "\nPlease Enter Valid Input!!" << endl;
						cout << "ProductID: ";
						cin >> productID;
						goto CHECK1;
					}
					counter++;
					judge = false;
				}
			}
			//=================================================================
			mydata.productID = productID;
			mydata.spsid = spsid;
			myList.push_back(mydata);
			//=================================================================
			cout << "Do you want to add more entries?(y\\n): ";
			cin >> whatToDO;
			CHECK_ANS:if (whatToDO != "n")		//validification 
			{
				if (whatToDO != "y")
				{
					while (true)
					{
						cout << "\nPlease Answer in (y\\n): ";
						cin >> whatToDO;
						goto CHECK_ANS;
					}
				}				
			}	
			//=================================================================
			if (whatToDO == "n")
			{
				start = std::chrono::system_clock::now();		//timer start
				list <node> ::iterator itr;
				for (itr = myList.begin(); itr != myList.end(); ++itr)
				{
					pWriteFile << itr->spsid << DELI << itr->fieldID << DELI << itr->iFuel << DELI << itr->iproduct << DELI << itr->productID << "\n";		//writing to the file
				}
				end = std::chrono::system_clock::now();			//timer stop
				std::chrono::duration<double> elapsed_seconds = end - start;		//time calculation			//calculating second
				cout << "Time taken to insert record: " << elapsed_seconds.count() * 1000 << " ms.";
				pWriteFile.close();
				getch();
				break;
			}
		}
	}
	else
	{
		cout << "File Doesn't exist" << endl;
		return -1;
	}
	return 0;
}


/*
 * Function: deleteData()
 * Description: This function inserts an element in the correct location in the list, sorted rating.
 * Parameters:  list<node>&myList:
 * Returns: 0 if the insertion failed, 1 otherwise
*/
int deleteData(list<node>& myList)
{
	ofstream pWriteFile1("test.txt");
	string temp = "";
	int record = 0;
	std::chrono::time_point<std::chrono::system_clock> start, end;

	//=================================================================
	if (pWriteFile1.is_open())
	{
		cout << "Enter SPSID(To delete): ";
		cin >> temp;
		list <node> ::iterator it;
		start = std::chrono::system_clock::now();
		it = myList.begin();
		LOOP2:while(it != myList.end())
		{ 
			if (it->spsid == temp)
			{
				myList.erase(it++);
				record++;
				it = myList.begin();
				goto LOOP2;
			}
			++it;
		}
		//=================================================================
		list <node> ::iterator itr1;
		for (itr1 = myList.begin(); itr1 != myList.end(); ++itr1)
		{
			pWriteFile1 << itr1->spsid << DELI << itr1->fieldID << DELI << itr1->iFuel << DELI << itr1->iproduct << DELI << itr1->productID << "\n";		//writing to the file
		}
		end = std::chrono::system_clock::now();
		std::chrono::duration<double> elapsed_seconds = end - start;			//calculating second
		cout << record << " record deleted" << endl;
		cout << "Time taken to delete record: " << elapsed_seconds.count() * 1000 << " ms.";
		pWriteFile1.close();
		getch();
	}
	else
	{
		cout << "File Doesn't exist" << endl;
		return -1;
	}
	return 0;
}


/*
 * Function: find()
 * Description: This function inserts an element in the correct location in the list, sorted rating.
 * Parameters:  list<node>myList:
 * Returns:		Nothing
*/
void find(list<node>myList)
{
	char buf1[1000] = { NULL };
	string whatToDo1 = "";
	int counter = 0;
	bool judge = true;
	string temp1 = "";
	cout << "Enter SPSID(To Find) OR \"All\" to Show all: ";
	cin >> temp1;
	while (judge)
	{
		CHECK1:size_t len = strlen(temp1.c_str());
		counter = 0;
		while (counter < len)
		{
			while (!isalnum(temp1[counter]))
			{
				cout << "\nPlease Enter Valid Input!!" << endl;
				cout << "Enter SPSID(To Find) OR \"All\" to Show all: ";
				cin >> temp1;
				goto CHECK1;
			}
			counter++;
			judge = false;
		}
	}
	std::chrono::time_point<std::chrono::system_clock> start, end;
	//=================================================================
	list <node> ::iterator it1;
	if (temp1 == "All")
	{
		start = std::chrono::system_clock::now();
		for (it1 = myList.begin(); it1 != myList.end(); ++it1)
		{		
			snprintf(buf1, 1000, "%-15s%-15d%-15d%-15d%-15s\n", it1->spsid.c_str(), it1->iproduct, it1->fieldID, it1->iFuel, it1->productID.c_str());
			cout << buf1 << endl;
		}
		end = std::chrono::system_clock::now();
		std::chrono::duration<double> elapsed_seconds = end - start;			//calculating second
		cout << "Time taken for All record: " << elapsed_seconds.count() * 1000 << " ms" << endl;		//converting into milisecond
		getch();
	}
	else
	{
		counter = 0;
		//=================================================================
		start = std::chrono::system_clock::now();
		for (it1 = myList.begin(); it1 != myList.end(); ++it1)
		{			
			if (it1->spsid == temp1)
			{
				counter++;
				snprintf(buf1, 1000, "%-15s%-15d%-15d%-15d%-15s\n", it1->spsid.c_str(), it1->iproduct, it1->fieldID, it1->iFuel, it1->productID.c_str());	//printing out to console
				cout << buf1 << endl;
			}
		}
		//=================================================================
		if (counter != 0)
		{
			end = std::chrono::system_clock::now();
			std::chrono::duration<double> elapsed_seconds = end - start;			//calculating second
			cout << counter << " Record has been found with " << "\"" << temp1 << "\"" << " SPSID" << endl;
			cout << "Time taken to Find records " << elapsed_seconds.count() * 1000 << "ms" << endl;		//converting into milisecond
		}
		//=================================================================
		else
		{
			end = std::chrono::system_clock::now();
			std::chrono::duration<double> elapsed_seconds = end - start;		//converting into second
			cout << "\nRecord NOT Found !!" << endl;
			cout << "Time taken to Find records " << elapsed_seconds.count() * 1000 << "ms" << endl;		//converting into milisecond
			
		}
		getch();
	}
}


/*
 * Function: random()
 * Description: This function inserts random genrated data into list and write it to txt file.
 * Parameters:  list<node>&myList: list which is used to store all data 
				node mydata: structure which is used to store random generated data and used to push in list
 * Returns:		0 if there is no issue else -1 if there is issue in opening file
*/
int random(list<node>& myList, node mydata)
{
	int howMany = 0;
	int count1 = 0;
	int count2 = 0;
	char buffer[1000] = { NULL };
	ofstream pWriteFile2("test.txt");	//opening file for writing

	//=================================================================
	if (pWriteFile2.is_open())		//checking if the file is open or not is open it procedee
	{
		string spsID[] = { "JDR0423","NHSP310F","STARA3100AR","HAGIESTS16","CASE4430" };	//storing given value of SPSID into an array
		string proID[] = { "DUPCurzate","BAYScala","SYNMaxim" };		//storing given value of ProductID into an array
		cout << "how many Random Entries you want to insert?  " << endl;
		cin >> howMany;
		std::chrono::time_point<std::chrono::system_clock> start, end;
		start = std::chrono::system_clock::now();
		//=================================================================
		for (int i = 0; i < howMany; i++)
		{
			int pID = (0 + rand() % 3);
			int sID = (0 + rand() % 5);
			mydata.spsid = spsID[sID];
			mydata.productID = proID[pID];
			mydata.fieldID = ((rand() % 255) + 1);		//genrating a random number between 1 to 255
			mydata.iFuel = ((rand() % 355) + 1);		//genrating a random number between 1 to 355
			mydata.iproduct = (rand() % 1200) + 1;		//genrating a random number between 1 to 1200
			myList.push_back(mydata);
			snprintf(buffer, 1000, "%-15s%-15d%-15d%-15d%-15s\n", mydata.spsid.c_str(), mydata.iproduct, mydata.fieldID, mydata.iFuel, mydata.productID.c_str());
			cout << buffer << endl;
		}
		//=================================================================
		list <node> ::iterator it3;
		for (it3 = myList.begin(); it3 != myList.end(); ++it3)
		{
			pWriteFile2 << it3->spsid << DELI << it3->fieldID << DELI << it3->iFuel << DELI << it3->iproduct << DELI << it3->productID << "\n";		//writing to the file
		}
		//=================================================================
		end = std::chrono::system_clock::now();
		std::chrono::duration<double> elapsed_seconds = end - start;
		cout << "Time taken to insert records "<<howMany<<": " << elapsed_seconds.count() * 1000 << "ms" << endl;
		pWriteFile2.close();	//closing open file 
	}
	else
	{
		cout << "File Doesn't exist" << endl;
		return -1;
	}
	return 0;
}
/*
 * Function: getNum()
 * Description: This function will get input from the user and check whether the given input is number or not.
 * Parameter:	Nothing				
 * Returns:		actual number if there is no issue else -1 if there is issue in  file
*/
int getNum(void) 
{ 
	char record[121] = { 0 }; /* record stores the string from the user*/   
	int number = 0;   /* fgets() - a function that can be called in order to read user input from the keyboard */   
	cin >> record;
	if (sscanf(record, "%d", &number) != 1) 
	{      /* if this line of code is executed � it means that the user didn�t enter  number */      
		number = -1; 
	}
	return number; 
}