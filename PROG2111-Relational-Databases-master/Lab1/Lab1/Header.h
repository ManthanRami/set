/*
* FILE : Header.cpp
* PROJECT : PROG2111 - Lab #1
* PROGRAMMER : Manthan Rami
* FIRST VERSION : 2019-09-12
* DESCRIPTION :
 This file contains all the declaration of struct and prototype of the all function which are goimg to used in project 1.
*/

#pragma once
#include<conio.h>
#include<ctype.h>
#include<stdio.h>
#include<iomanip>
#include <iostream>
#include <chrono>
#include <string>
#include <fstream>
#include<list>

#pragma warning(disable : 4996)
#pragma warning(disable : 6031)
using namespace std;

typedef struct node
{
	string spsid = "";
	int fieldID = 0;
	int iFuel = 0;
	int iproduct = 0;
	string productID = "";
}node;

#define DELI ","

int insertData(list<node>& myList, node mydata);
int random(list<node>& myList, node mydata);
int deleteData(list<node>& myList);
void find(list<node>myList);
int getNum(void);
int startStop(bool& state, list<node>& myList, node mydata);
