/*
* FILE : UserInterface.cpp
* PROJECT : PROG2111 - Lab #1
* PROGRAMMER : Manthan Rami
* FIRST VERSION : 2019-09-12
* DESCRIPTION :
* The program is an backend logic of the web server howthe web server is managing the data. 
  This program has an console based UI which allow user to interract with webserver.
*/

#include"Header.h"


int main()
{
	char input		 = NULL;
	bool state		 = false;
	int retcode		 = 0;
	list<node>myList;
	node mydata;
	
	
	while (true)
	{
		while (true)
		{
			system("cls");
			cout << "1\tStart/Stop" << endl;
			while (state)
			{
				cout << "2\tINSERT" << endl;
				cout << "3\tDELETE" << endl;
				cout << "4\tFIND" << endl;
				cout << "5\tRANDOM" << endl;
				cout << "6\tQuit" << endl;
				break;
			}
			cout << "\nEnter Choice: ";
			cin >> input;
			if (input == '1')
			{
				retcode= startStop(state,myList,mydata);
				if (retcode == -1)
				{
					return 0;
				}
			}
			if (state)
			{
				if (input == '2')
				{
					retcode = insertData(myList, mydata);
					if (retcode == -1)
					{
						return 0;
					}
				}
				else if (input == '3')
				{
					retcode = deleteData(myList);
					if (retcode == -1)
					{
						return 0;
					}
				}
				else if (input == '4')
				{
					find(myList);
					if (retcode == -1)
					{
						return 0;
					}
				}
				else if (input == '5')
				{
					
					retcode = random(myList, mydata);
					if (retcode == -1)
					{
						return 0;
					}		
					getch();
				}
				else if(input=='6')
				{
					return 0;
				}
			}			

		}
	}

}
