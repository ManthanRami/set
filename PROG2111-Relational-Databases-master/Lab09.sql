/*=======================================================================================
* Student Name:   Manthan Rami
* Student Number: 8628901
* Date of Submit: 20-11-2019
* Description:    This file contains some Structured Query answers of Lab9 Questions.
==========================================================================================*/

/*===========================================================================================
                            Selecting NorthWind Database
============================================================================================*/
    USE Northwind;
/*===========================================================================================
                            Question 11
============================================================================================*/
	select TerritoryDescription,RegionDescription from territories 
    inner join region on territories.regionID=region.regionId;
/*===========================================================================================
                            Question 12
=============================================================================================*/
	select orderdetails.orderID,customerid,productid,quantity from 
	orderdetails inner join orders on orderdetails.OrderID=orders.OrderID;
/*===========================================================================================
                            Question 13
=============================================================================================*/
	select orderdetails.orderID,customerid,productid,quantity,
	(UnitPrice*orderdetails.quantity) as Extended_Price from 
    orderdetails inner join orders on orderdetails.OrderID=orders.OrderID;
/*===========================================================================================
                            Question 14
=============================================================================================*/
    select OrderID,OrderDate,CompanyName,concat(employees.Firstname," ", employees.Lastname) 
    as Employee_Name from orders ;
/*===========================================================================================
                            Question 15
=============================================================================================*/
    select distinct orders.CustomerID,customers.CompanyName from orders
    inner join customers on orders.CustomerID=customers.CustomerID ;
/*===========================================================================================
                            Question 16
=============================================================================================*/
    select Customers.CustomerID, Customers.companyName from Customers left join Orders 
    on Orders.CustomerID = Customers.CustomerID where Orders.from CustomerID is null;
/*===========================================================================================
                            Question 17
=============================================================================================*/
    insert into  region (region.regionid, region.regionDescription) 
    values (5,"Europe") ;
/*===========================================================================================
                            Question 18
=============================================================================================*/
    delete from region  where regiondescription = "Europe";    
/*===========================================================================================
                            Question 19
=============================================================================================*/
    update customers set ContactName='Hans Schmidt' where companyName="Ernst Handel";   
/*===========================================================================================
                            Question 20
=============================================================================================*/
    update products set UnitPrice =(unitPrice+1);
/*===========================================================================================
                            Question 21
=============================================================================================*/
    insert into categories(CategoryID,CategoryName,Description,Picture )
    values (9,"Discontinued","products which are Discontinued", null );
/*===========================================================================================
                            Question 22
=============================================================================================*/
    update products set CategoryID=9 where discontinued=-1;