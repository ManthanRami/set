/*===============================================================================================================
*  FILE          : A3.sql
*  PROJECT       : PROG2111 - Relational Databases A-3
*  PROGRAMMER    : Manthan Rami
*  Date          : 2019-12-06
*  DESCRIPTION   : This is file contains database design for the assignment 3 Revenge of Wally                             
*================================================================================================================*/
-- ---------------------------------------------------
-- Schema mrwally
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS mrwally ;
-- ----------------------------------------------------
-- Schema mrwally
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS mrwally DEFAULT CHARACTER SET utf8 ;
USE mrwally ;
-- -----------------------------------------------------
-- Table `mrwally`.`Customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS Customer;
CREATE TABLE Customers 
(
  CustomerID INT UNSIGNED NOT NULL auto_increment,
  FirstName VARCHAR(45) NOT NULL,
  LastName VARCHAR(45) NOT NULL,
  Telephone VARCHAR(45) NOT NULL,   
  primary key(CustomerID)  
  );
  					-- Customer--
-- ==========================================================================
insert into customers( FirstName,LastName,Telephone)
values	('Carlo', 'Sgro', '519-555-0000'),
		('Norbert', 'Mika','416-555-1111'),
		('Russell', 'Foubert','519-555-2222'),
		('Sean', 'Clarke','519-555-3333'),
		('Manthan', 'Rami','519-555-8888');     
        
					
-- -----------------------------------------------------
-- Table `mrwally`.`Branch`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Branch
 (
  BranchID INT unsigned NOT NULL auto_increment,
  BranchName VARCHAR(45) NULL,
  PRIMARY KEY (BranchID)
  );
						-- Branch--
-- ==========================================================================
insert into branch( BranchName)
values	("Sports World"),
		("Waterloo"),
		("Cambridge"),
		("St. Jacobs");        
-- -----------------------------------------------------
-- Table `mrwally`.`Order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Orders (
  OrderID INT unsigned NOT NULL auto_increment,
  CustomerID INT unsigned NULL,
  OrderDate DATE NOT NULL,
  BranchID INT unsigned NULL,
  OrderStatus varchar(25) not null,
  OrderTotal varchar(10) not null,
  PRIMARY KEY (OrderID),
  FOREIGN KEY (CustomerID) REFERENCES Customers(CustomerID),
  FOREIGN KEY (BranchID) REFERENCES Branch(BranchID)
);
      				-- Orders--
-- ==========================================================================
insert into orders(CustomerID,BranchID,OrderDate,OrderStatus,orderTotal)
values(4,1,'2019-09-20',"PAID","232.5"),
	  (3,3,'2019-10-06',"PAID","25.3"),
	  (2,4,'2019-11-02',"PAID","154.5"),
	  (2,1,'2019-11-04',"RFND","134.5");
      
-- -----------------------------------------------------
-- Table `mrwally`.`Products`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Products
 (
  ProductID INT unsigned NOT NULL auto_increment,
  ProductName VARCHAR(45) NULL,
  ProductPrice double  NOT NULL,
  ProductStock INT  NOT NULL,
  PRIMARY KEY (ProductID)
  );
  					-- Products--
-- ==========================================================================
insert into products(ProductName,ProductPrice,ProductStock)
values	("Disco Queen Wallpaper(roll)",12.95,56),
		("Countryside Wallpaper(roll)",11.95,24),
		("Victorian Lance Wallpaper(roll)",14.95,44),
		("Drywall Tape (roll)",3.95,120),
		("Drywall Tape (pkg 10)",36.95,30),
		("Drywall Repair (tube)",6.95,90);
        
-- -----------------------------------------------------
-- Table `mrwally`.`OrderLine`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS OrderLine
(
  OrderLineID INT NOT NULL auto_increment,
  OrderID INT unsigned NOT NULL,
  ProductID INT unsigned NOT NULL,
  Quantity INT unsigned NOT NULL,
  FOREIGN KEY (OrderID) REFERENCES Orders(OrderID),
  FOREIGN KEY (ProductID) REFERENCES Products(ProductID),
  PRIMARY KEY (OrderLineID)
);

  					-- OrderLine--
-- ==========================================================================
insert into OrderLine(OrderID,ProductID,Quantity)
values	(1,3,4),
		(1,6,1),
		(1,4,2),
		(2,2,10),
		(3,1,12),
		(3,4,3),
		(4,1,12),
		(4,4,3);
