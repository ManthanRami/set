﻿/*===============================================================================================================
*  FILE          : DatabaseCommunicator.cs
*  PROJECT       : PROG2111 - Relational Databases
*  PROGRAMMER    : Manthan Rami
*  Date          : 2019-12-06
*  DESCRIPTION   : This is file containt all logic to communicate to the data base of MRWally                                                
*================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using MySql.Data;
using System.Windows;
using System.Data;
using System.Windows.Controls;
using System.Configuration;
using System.Data.SqlClient;

namespace RevengeOfWally
{
    class DatabaseCommunicator
    {
        private DataSet data;
        private Customer                customer;
        static  MySqlCommand        command;
        private MySqlDataReader     reader;
        private MySqlConnection     connect;
        private MySqlDataAdapter    adapter;
        private List<Customer>      myList = new List<Customer>();

        public DatabaseCommunicator()
        {
            try
            {
                connect = new MySqlConnection("Server = 127.0.0.1; Uid = root; Password = Conestoga1 ;Database = mrwally; Port = 3306");

                command = new MySqlCommand();
            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e);
            }
            try
            {
                connect.Open();
                command.Connection = connect;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex);
                connect.Close();
            }
        }
        /*================================================================================================
        *  Function    : IsExistInMRWally
        *  Description : This function will check is the given query srting data is exist or not 
                         and retrive it if found
        *  Parameters  : string query: sql command
        *  Returns     : string found : data 
        ================================================================================================*/        
        public string IsExistInMRWally(string query)
        {
            string found= "";
            command.CommandText = query;
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                found = found + " " + reader[0];
            }
            reader.Close();
            return found;
        }
        /*================================================================================================
        *  Function    : GetProductsDetails
        *  Description : This function will get all the product info
        *  Parameters  : string id : product id
        *  Returns     : Product object
        ================================================================================================*/        
        public Product GetProductsDetails(string id)
        {
            Product p = new Product();
            string query = "Select * from Products where Products.ProductID=" + id + ";";
            command.CommandText = query;
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                p = new Product() {  ProductID = Convert.ToInt32( reader["ProductID"].ToString()),  ProductName = reader["ProductName"].ToString(),  ProductPrice = Convert.ToDouble(reader["ProductPrice"].ToString()),  ProductStock = Convert.ToInt32(reader["ProductStock"].ToString() )};
            }
            reader.Close();
            return p;
        }
        /*================================================================================================
        *  Function    : GetCustomerFromMrWally
        *  Description : This function will get all the customer info
        *  Parameters  : string id : customer id
        *  Returns     : Customer object
        ================================================================================================*/ 
        public Customer GetCustomerFromMrWally(string id)
        {
            customer = new Customer();
            string query = "Select * from customers where customers.customerId="+id+";";
            command.CommandText = query;
            reader = command.ExecuteReader();
            while (reader.Read())
            {               
                customer =new Customer() { id=reader["customerid"].ToString(), firstName = reader["FirstName"].ToString(), lastName = reader["LastName"].ToString(), Telephone = reader["Telephone"].ToString() };
            }
            reader.Close();
            
            return customer;
        }
        /*================================================================================================
        *  Function    : ProductsOfMRWally
        *  Description : This function will get all the poduct  list present in the customer order
        *  Parameters  : Nothing
        *  Returns     : List<Product> p list of all item in order
        ================================================================================================*/      
        public List<Product> ProductsOfMRWally()
        {
            Product product = new Product();
            var p = new List<Product>();
            string query = "Select * from products;";
            command.CommandText = query;
            reader.Close();
            reader = command.ExecuteReader();
            while(reader.Read())
            {
                p.Add(new Product() { ProductID = Convert.ToInt32(reader["ProductID"].ToString()), ProductName = reader["ProductName"].ToString(), ProductPrice = Convert.ToDouble(reader["ProductPrice"].ToString()), ProductStock = Convert.ToInt32(reader["ProductStock"].ToString() )});
            }
            reader.Close();
            return p;
        }
        /*================================================================================================
        *  Function    : BranchesOFMRWally
        *  Description : This function will get all the branch from the database
        *  Parameters  : Nothing
        *  Returns     : List<Branch> b name of all branch
        ================================================================================================*/      
        public List<Branch> BranchesOFMRWally()
        {
            Branch branch = new Branch();
            var b = new List<Branch>();
            string query = "Select * from Branch;";
            command.CommandText = query;
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                b.Add(new Branch() { branchID = reader["branchID"].ToString(), branchName = reader["branchName"].ToString()});
            }
            return b;
        }
        /*================================================================================================
        *  Function    : GetDataSet
        *  Description : This function will get a dataset to load on grid according to the query
        *  Parameters  : string query : table to get
        *  Returns     : dataset 
        ================================================================================================*/ 
        public DataSet GetDataSet(string query)
        {
            try
            { 
                adapter = new MySqlDataAdapter(query, connect);
                data = new DataSet();
                adapter.Fill(data);
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /*================================================================================================
        *  Function    : CUDFromMRWally
        *  Description : This function will allow  application to create update and insert any data into
                         the database
        *  Parameters  : string query : sql command
        *  Returns     : Nothing as return type is void
        ================================================================================================*/ 
        public void CUDFromMRWally(string query)
        {
            command.CommandText = query; 
            reader = command.ExecuteReader();
            reader.Close();
        }
    }
}
