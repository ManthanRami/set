﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevengeOfWally
{
    class OrderItem
    {
        public int Product_ID { get; set; }
        public string Product_Name { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
        public double Total_Amount { get; set; }
    }
}
