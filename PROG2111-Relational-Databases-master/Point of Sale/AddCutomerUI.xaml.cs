﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RevengeOfWally
{
    /// <summary>
    /// Interaction logic for AddCutomerUI.xaml
    /// </summary>
    public partial class AddCutomerUI : Window
    {
        public AddCutomerUI()
        {
            InitializeComponent();
        }
         
        DatabaseCommunicator dc = new DatabaseCommunicator();
        
        private void Add_Click(object sender, RoutedEventArgs e)
        {
            Regex regex = new Regex("[0-9][0-9][0-9][-][0-9][0-9][0-9][-][0-9][0-9][0-9][0-9]");
            int redcode = 0;
            string fname = Fname.Text;
            if (fname == "")
            {
                //error
            }
            string lname = Lname.Text;
            if (lname == "")
            {
                //error
            }
            string number = Tnumber.Text;
            if (number == "")
            {
                //error
            }
            if (!regex.IsMatch(number))
            {
                Error.Content = "Please Enter Telephone Number in XXX-XXX-XXXX pattern";
                return;
            }
            string found = "";
            if (redcode == 0)
            {
                string searchQuery = "SELECT customerid FROM customers where Telephone='" + number + "';";
                dc.IsExistInMRWally(searchQuery);
            }
            if (found == "")
            {
                string insertQuery = "insert into customers(FirstName, LastName, Telephone)values('" + fname + "', '" + lname + "','" + number + "' );";
                dc.CUDFromMRWally(insertQuery);
                MessageBox.Show("Customer Has been added !!", "Done", MessageBoxButton.OK, MessageBoxImage.Information);
                Fname.Text = "";
                Lname.Text = "";
                Tnumber.Text = "";
            }
            else
            {
                //error
                Error.Content = "Customer Already Exist in Database !!";
            }

        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            MainWindow home = new MainWindow();
            home.Show();
            this.Close();
        }

        private void Fname_Error(object sender, ValidationErrorEventArgs e)
        {

        }
    }
}
