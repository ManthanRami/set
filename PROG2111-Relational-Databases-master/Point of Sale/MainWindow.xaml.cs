﻿/*===============================================================================================================
*  FILE          : MainWindow.xaml.cs
*  PROJECT       : PROG2111 - Relational Databases A-3
*  PROGRAMMER    : Manthan Rami
*  Date          : 2019-12-06
*  DESCRIPTION   : This is Revenge of Wally A sales point application for the sales agent to create order                               
*================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

namespace RevengeOfWally
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        struct Invoice
        {
            public string        branchName;
            public string        customerName;
            public string        date;
            public string        orderID;
            public string        orderStatus;
            
            public string        subTotal;
            public double        hst;
            public double       saleTotal;
            public List<string> productsPurchased;
        }
        bool             isCustomerSelected = false;
        bool             allowToSelectForOrderLine = false;
        bool             allowToSelectForCustomer = false;
        DataSet          ds;
        Product          pDetails;
        DataTable        dataT;
        OrderItem        orItem;
        Regex            regexTelephone = new Regex("[0-9][0-9][0-9][-][0-9][0-9][0-9][-][0-9][0-9][0-9][0-9]");
        Regex            regexName = new Regex(@"^[a-zA-Z]+$");
        Regex            regexID = new Regex("^[0-9][0-9]*[0-9]*$");
        Invoice         invoiceString = new Invoice();
        List<Product>   productList = new List<Product>();
        List<Branch>    branchList = new List<Branch>();
        
        DatabaseCommunicator    dc = new DatabaseCommunicator();
        static List<OrderItem>  line = new List<OrderItem>();
        public MainWindow()
        {
            InitializeComponent();
            //LoadStatus();
            //LoadBranchs();
            //LoadProducts();
        }
        /*================================================================================================
        *  Function    : CustSearch_Click
        *  Description : This function will search for customer in the database.
        *  Parameters  : object sender:
                         RoutedEventArgs e:
        *  Returns     : Nothing as return type is void
`       ================================================================================================*/
        private void CustSearch_Click(object sender, RoutedEventArgs e)
        {
            int     customerId = 0;
            bool    signal = true;
            string  id = CustID.Text;
            string  number = ACode.Text + "-" + num1.Text + "-" + num2.Text;
            string  query = "";            

            if (!string.IsNullOrEmpty(id))
            {
                if (signal = checkIfNumber(id, out customerId))
                {
                    query = "Select customerID from customers where customers.customerId=" + customerId.ToString() + ";";
                    id = dc.IsExistInMRWally(query);
                    if (id == "")
                    {
                        MessageBox.Show("No Customer Exist with this ID !!", "Empty Fields", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else
                    {
                        query = "Select * from customers where customers.customerId=" + customerId.ToString() + ";";
                        Customer cs = dc.GetCustomerFromMrWally(id);
                        FillFields(cs);
                        ds = dc.GetDataSet(query);
                        showData.ItemsSource = ds.Tables[0].DefaultView;
                        isCustomerSelected = true;
                    }
                }
            }
            else if (number.Length != 2)
            {
                if (CheckTelephone(number))
                {
                    query = "Select customerId from customers where customers.telephone ='" + number + "';";
                    id = dc.IsExistInMRWally(query);
                }
                if (id == "")
                {
                    MessageBox.Show("No Customer Exist with this Telephone number !!", "Empty Fields", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    query = "Select * from customers where customers.telephone ='" + number + "';";
                    Customer cs = dc.GetCustomerFromMrWally(id);
                    FillFields(cs);
                    ds = dc.GetDataSet(query);
                    showData.ItemsSource = ds.Tables[0].DefaultView;
                    isCustomerSelected = true;
                }
            }
            else
            {
                MessageBox.Show("ID OR Telephone must be filled to search customer !!", "Unique Fields Empty", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
        /*================================================================================================
        *  Function    : LoadBranchs
        *  Description : This function will search for branches available on the 
        *  Parameters  : Nothing
        *  Returns     : Nothing as return type is void
`       ================================================================================================*/
        private void LoadBranchs()
        {
            BranchCombo.Items.Add("Select Branch");
            branchList = dc.BranchesOFMRWally();
            foreach (Branch b in branchList)
            {
                BranchCombo.Items.Add(b.branchName);
            }
            BranchCombo.SelectedIndex = 0;
        }
        /*================================================================================================
        *  Function    : LoadProducts
        *  Description : This function will search for products available on the databse and load it 
                         to the combo
        *  Parameters  : Nothing
        *  Returns     : Nothing as return type is void
`       ================================================================================================*/
        private void LoadProducts()
        {
            Productcombo.Items.Add("Select Product");
            productList = dc.ProductsOfMRWally();
            foreach (Product pd in productList)
            {
                Productcombo.Items.Add(pd.ProductName);
            }
            Productcombo.SelectedIndex = 0;
        }
        /*================================================================================================
        *  Function    : FillFields
        *  Description : This function will take customer and select it as current customer.
        *  Parameters  : Customer cs : customer object having all details
        *  Returns     : Nothing as return type is void
`       ================================================================================================*/
        private void FillFields(Customer cs)
        {
            CustFname.Text = cs.firstName;
            CustLname.Text = cs.lastName;
            CustID.Text =    cs.id;
            string[] contact = cs.Telephone.Split('-');
            ACode.Text = contact[0];
            num1.Text = contact[1];
            num2.Text = contact[2];
        }
        /*================================================================================================
        *  Function    : MakeFieldEmpty
        *  Description : This function will make all the customer field empty
        *  Parameters  : Nothing
        *  Returns     : Nothing as return type is void
`       ================================================================================================*/
        private void MakeFieldEmpty()
        {
            CustFname.Text = "";
            CustLname.Text = "";
            CustID.Text = "";
            ACode.Text = "";
            num1.Text = "";
            num2.Text = "";
        }
        /*================================================================================================
        *  Function    : CheckTelephone
        *  Description : This function will make that customer telephone is in proper format
        *  Parameters  : Nothing
        *  Returns     : Nothing as return type is void
        ================================================================================================*/        
        private bool CheckTelephone(string t)
        {
            if (regexTelephone.IsMatch(t))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /*================================================================================================
        *  Function    : checkIfNumber
        *  Description : This function will make sure that customer id is more than 0 and check for number
        *  Parameters  : string s: user input string 
                         out int id: int to store converted value
        *  Returns     : Nothing as return type is void
        ================================================================================================*/         
        private bool checkIfNumber(string s, out int id)
        {
            if (Int32.TryParse(s, out id))
            {
                if (id < 1)
                {
                    MessageBox.Show("Customer Id must be greater than ZERO !!");
                    return false;
                }
                return true;
            }
            else
            {
                MessageBox.Show("Customer Id must be A Postivie Integer !!");
                return false;
            }

        }
        /*================================================================================================
        *  Function    : LoadCustomerTable_Click
        *  Description : This function will get all the customer details from the databse and load to grid
        *  Parameters  : object sender:
                         RoutedEventArgs e:
        *  Returns     : Nothing as return type is void
        ================================================================================================*/       
        private void LoadCustomerTable_Click(object sender, RoutedEventArgs e)
        {
            allowToSelectForOrderLine = false ;
            allowToSelectForCustomer = true;
            string query = "Select * from customers;";
            ds = dc.GetDataSet(query);
            showData.ItemsSource = ds.Tables[0].DefaultView;
        }
        /*================================================================================================
        *  Function    : LoadStatus
        *  Description : This function will load all the status to the stauts combo
        *  Parameters  : Nothing
        *  Returns     : Nothing as return type is void
        ================================================================================================*/       
        private void LoadStatus()
        {
            StatusCombo.Items.Add("Select status");
            StatusCombo.Items.Add("PAID");
            StatusCombo.Items.Add("RFND");
            StatusCombo.Items.Add("PEND");
            StatusCombo.SelectedIndex = 0;
        }
        /*================================================================================================
        *  Function    : InventoryLevel_Click
        *  Description : This function will get all the product name and product stock from the database 
                        and load it to the grid
        *  Parameters  : object sender:
                         RoutedEventArgs e:
        *  Returns     : Nothing as return type is void
        ================================================================================================*/       
        private void InventoryLevel_Click(object sender, RoutedEventArgs e)
        {
            allowToSelectForOrderLine = false;
            allowToSelectForOrderLine = false;
            if (BranchCombo.SelectedIndex > 0)
            {
                string query = "Select ProductName,ProductStock from Products;";
                ds = dc.GetDataSet(query);
                showData.ItemsSource = ds.Tables[0].DefaultView;
            }
            else
            {
                MessageBox.Show("Branch Not selected !!", "Selection Empty", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
        /*================================================================================================
        *  Function    : CustAdd_Click
        *  Description : This function will add the customer to the database
        *  Parameters  : object sender:
                         RoutedEventArgs e:
        *  Returns     : Nothing as return type is void
        ================================================================================================*/           
        private void CustAdd_Click(object sender, RoutedEventArgs e)
        {
            string check = "";
            if (checkCustomerFields())
            {
                string query = "Select customerId from customers where customers.customerID=" + CustID.Text + ";";
                check = dc.IsExistInMRWally(query);
                if (check == "")
                {
                    string number = ACode.Text + "-" + num1.Text + "-" + num2.Text;
                    query = "Select customerId from customers where customers.Telephone='" + number + "';";
                    check = dc.IsExistInMRWally(query);
                    if (check == "")
                    {
                        query = "Insert into Customers(customerID,FirstName,LastName,Telephone)values(" + CustID.Text + ",'" + CustFname.Text + "','" + CustLname.Text + "','" + number + "');";
                        dc.CUDFromMRWally(query);
                        MessageBox.Show("Customer Added Succesfully !", "Job Done", MessageBoxButton.OK, MessageBoxImage.Information);
                        MakeFieldEmpty();
                        LoadCustomerTable_Click (sender, e);   
                    }
                    else
                    {
                        MessageBox.Show("Customer Exist with this Telephone number!!", "Entry Exists", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Customer Exist with this ID!!", "Entry Exists", MessageBoxButton.OK, MessageBoxImage.Error);
                    MakeFieldEmpty();
                }
            }

        }
       /*================================================================================================
        *  Function    : CheckProductFields
        *  Description : This function will make sure all the fields of products are filled.
        *  Parameters  : Nothing
        *  Returns     : Nothing as return type is void
        ================================================================================================*/ 
        private bool CheckProductFields()
        {
            int index = Productcombo.SelectedIndex;
            int brIndex = BranchCombo.SelectedIndex;
            bool redcode = false;
            string itemNumber = ItemQuantity.Text;
            if (index > 0)
            {
                if (brIndex > 0)
                {
                    if (regexID.IsMatch(itemNumber))
                    {
                        if (Convert.ToInt32(itemNumber) > 0)
                        {
                            redcode = true;
                        }
                        else
                        {
                            MessageBox.Show("Item Quantity must be more than ZERO !!", "Selection Empty", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Quantity must be Positive integer !!", "Selection Empty", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Please Select Branch First !!", "Selection Empty", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Please Select Product First !!", "Selection Empty", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return redcode;
        }
        /*================================================================================================
        *  Function    : AddToCart_Click
        *  Description : This function will add the products to the cart.
        *  Parameters  : object sender:
                         RoutedEventArgs e:
        *  Returns     : Nothing as return type is void
        ================================================================================================*/ 
        private void AddToCart_Click(object sender, RoutedEventArgs e)
        {
            bool redcode = false;
            allowToSelectForOrderLine = false;
            allowToSelectForCustomer = false;
            orItem = new OrderItem();
            pDetails = new Product();
            string query = "";
            int quantity = 0;
            redcode = checkCustomerFields();
            if (redcode)
            {
                if (CheckProductFields())
                {
                    quantity = Convert.ToInt32(ItemQuantity.Text);
                    if (line.Count != 0)
                    {
                        
                        foreach (OrderItem i in line)
                        {
                            if (i.Product_ID == Productcombo.SelectedIndex)
                            {
                                quantity += i.Quantity;
                                query = "Select productStock from products where products.productID=" + Productcombo.SelectedIndex + ";";
                                int actualStockc = Convert.ToInt32(dc.IsExistInMRWally(query));
                                if (Convert.ToInt32(ItemQuantity.Text) <= actualStockc)
                                {
                                    line.Remove(i);
                                }
                                else
                                {
                                    MessageBox.Show("Product: " + i.Product_Name + "\nAvailable Stock :" + actualStockc, "Quantity Exceed ", MessageBoxButton.OK, MessageBoxImage.Error);
                                    return;
                                }
                                break;
                            }
                        }
                    }               
                    orItem.Product_ID = Productcombo.SelectedIndex;
                    orItem.Product_Name = Productcombo.SelectedItem.ToString();
                    query = "Select ProductPrice from products where products.productID=" + orItem.Product_ID + "; ";
                    orItem.Price = Convert.ToDouble(dc.IsExistInMRWally(query))*1.4;
                    orItem.Quantity = quantity;
                    orItem.Total_Amount = orItem.Price * orItem.Quantity;
                    query = "Select productStock from products where products.productID=" + orItem.Product_ID + ";";
                    int stock = (Convert.ToInt32(dc.IsExistInMRWally(query)));
                    query = "update products set productStock=" +( stock - Convert.ToInt32(ItemQuantity.Text)) + " where productID=" + orItem.Product_ID + ";";
                    dc.CUDFromMRWally(query);
                    line.Add(orItem);
                    InventoryLevel_Click(sender, e);                                        
                    RefreshCustomerCart(line);
                }
            }
            else
            {
                MessageBox.Show("Please Select Customer First !!  ", "Selection Empty", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
        /*================================================================================================
        *  Function    : RefreshCustomerCart
        *  Description : This function will refresh the shopping cart.
        *  Parameters  : List<OrderItem> orderline:
        *  Returns     : Nothing as return type is void
        ================================================================================================*/ 
        private void RefreshCustomerCart(List<OrderItem> orderline)
        {
            List<string> shopcart = new List<string>();
            dataT = new DataTable("CustomerCart");
            DataRow dRow;
            double orderTotal = 0.0;
            dataT.Columns.Add("Product Name", typeof(string));
            dataT.Columns.Add("Price", typeof(string));
            dataT.Columns.Add("Quantity", typeof(string));
            dataT.Columns.Add("Total Price", typeof(string));
            if (line.Count != 0)
            {                
                foreach (OrderItem item in line)
                {
                    item.Total_Amount = item.Price * item.Quantity;
                    dRow = dataT.NewRow();
                    dRow["Product Name"] = item.Product_Name;
                    dRow["Price"] = item.Price.ToString();
                    dRow["Quantity"] = item.Quantity.ToString();
                    dRow["Total Price"] = item.Total_Amount.ToString();
                    dataT.Rows.Add(dRow);
                }
                DataView view = new DataView(dataT);
                ShoppingCart.ItemsSource = view;
            }
            else
            {
                dataT.Clear();
                DataView view = new DataView(dataT);
                ShoppingCart.ItemsSource = view;
            }
            if(line.Count>0)
            {
                foreach (OrderItem item in line)
                {
                    orderTotal += item.Total_Amount;
                }
                TotalOrderCost.Content = "$ " + string.Format("{0:N2}", orderTotal);
                invoiceString.subTotal = string.Format("{0:N2}", orderTotal);
            }           
            else
            {
                TotalOrderCost.Content = ""; 
            }
        }
        /*================================================================================================
        *  Function    : checkCustomerFields
        *  Description : This function will make sure that all the fields of customer is filled.
        *  Parameters  : Nothing
        *  Returns     : Nothing as return type is void
        ================================================================================================*/ 
        private bool checkCustomerFields()
        {
            bool redcode = false;
            string number = ACode.Text + "-" + num1.Text + "-" + num2.Text;
            if (regexID.IsMatch(CustID.Text))
            {
                if (regexName.IsMatch(CustFname.Text) && !string.IsNullOrEmpty(CustFname.Text))
                {
                    if (regexName.IsMatch(CustLname.Text) && !string.IsNullOrEmpty(CustLname.Text))
                    {
                        if (regexTelephone.IsMatch(number) && !string.IsNullOrEmpty(number))
                        {
                            redcode = true;
                            return redcode;
                        }
                        else
                        {
                            MessageBox.Show("Invalid Telephone Number !!", "Entry Invalid ", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Invalid Last Name !!", "Entry Invalid ", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Invalid First Name !!", "Entry Invalid ", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Invalid ID !!", "Entry Invalid ", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return redcode;
        }
        /*================================================================================================
        *  Function    : orderHistory_Click
        *  Description : This function will list down all the order of the customers.
        *  Parameters  : object sender:
                         RoutedEventArgs e:
        *  Returns     : Nothing as return type is void
        ================================================================================================*/         
        private void orderHistory_Click(object sender, RoutedEventArgs e)
        {           
            if (string.IsNullOrEmpty(CustID.Text) && string.IsNullOrEmpty(CustFname.Text) && string.IsNullOrEmpty(CustLname.Text)&&string.IsNullOrEmpty(ACode.Text) && string.IsNullOrEmpty(num1.Text) && string.IsNullOrEmpty(num2.Text))
            {                
                string query = "select OrderID,CONCAT(customers.FirstName,\" \",customers.LastName) AS CustomerName,OrderDate,OrderStatus,BranchName from orders inner join customers on customers.customerID = orders.customerID inner join branch on branch.branchID=orders.branchID order by orderID;";
                ds = dc.GetDataSet(query);
                showData.ItemsSource = ds.Tables[0].DefaultView;
            }
            else
            if (VerifyCustomer(CustID.Text))
            {
                string query = "select orderID,CONCAT(customers.FirstName,\" \",customers.LastName) AS CustomerName,OrderDate,OrderStatus,branchName from orders inner join customers on customers.customerID = orders.customerID inner join branch on branch.branchID=orders.branchID  where customers.customerID = "+CustID.Text+" order by orderID;";
                ds = dc.GetDataSet(query);
                allowToSelectForOrderLine = true;
                showData.ItemsSource = ds.Tables[0].DefaultView;
            }
            else
            {
                MessageBox.Show("No Customer found with this details !!", "Entry Invalid ", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        /*================================================================================================
        *  Function    : VerifyCustomer
        *  Description : This function will check if the customer is exists in database or not.
        *  Parameters  :string id: customer id
        *  Returns     : return true if  otherwise false
        ================================================================================================*/         
        private bool VerifyCustomer(string id)
        {
            string query = "Select customerID from customers where customerID =" + id + "";
            if(dc.IsExistInMRWally(query)=="")
            {
                return false;
            }
            return true;
        }
        /*================================================================================================
        *  Function    : Delete_Click
        *  Description : This function will delete products from the shopping cart 
        *  Parameters  : object sender:
                         RoutedEventArgs e:
        *  Returns     : Nothing as return type is void
        ================================================================================================*/         
        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            int redcode = 0;
            allowToSelectForOrderLine = false;
            allowToSelectForCustomer = false;
            string query="";
            int pID = Productcombo.SelectedIndex;
            if (checkCustomerFields() && CheckProductFields())
            {
                foreach(OrderItem oi in line)
                {
                    if(pID==oi.Product_ID)
                    {
                        if(oi.Quantity>= Convert.ToInt32(ItemQuantity.Text))
                        {
                            oi.Quantity -= Convert.ToInt32(ItemQuantity.Text);
                            if (oi.Quantity < 1)
                            {
                                query = "Select productStock from products where products.productID=" + oi.Product_ID + ";";
                                query = "update products set productStock=" + (Convert.ToInt32(dc.IsExistInMRWally(query)) + Convert.ToInt32(ItemQuantity.Text)) + " where productID=" + oi.Product_ID + ";";
                                dc.CUDFromMRWally(query);
                                line.Remove(oi);
                                redcode = 1;
                                RefreshCustomerCart(line);
                                InventoryLevel_Click(sender, e);
                                break;
                            }
                            else
                            {
                                query = "Select productStock from products where products.productID=" + oi.Product_ID + ";";
                                query = "update products set productStock=" + (Convert.ToInt32(dc.IsExistInMRWally(query)) + Convert.ToInt32(ItemQuantity.Text)) + " where productID=" + oi.Product_ID + ";";
                                dc.CUDFromMRWally(query);
                                redcode = 1;
                                RefreshCustomerCart(line);
                                InventoryLevel_Click(sender, e);
                                break;
                            }
                        }
                        else
                        {
                            redcode = 1;
                            MessageBox.Show("Product Quantity is less to delete!!", "Invalid Entry ", MessageBoxButton.OK, MessageBoxImage.Error);
                            break;
                        }
                    }
                }
                if(redcode==0)
                {
                    MessageBox.Show("No such Product Found in Cart !!", "Invalid Entry ", MessageBoxButton.OK, MessageBoxImage.Error);
                }

            }
            else
            {
                MessageBox.Show("Customer and Product Details Must be filled !!", "Empty Fields ", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        /*================================================================================================
        *  Function    : CheckOut_Click
        *  Description : This function will create entry on the database of order and orderline.
        *  Parameters  : object sender:
                         RoutedEventArgs e:
        *  Returns     : Nothing as return type is void
        ================================================================================================*/ 
        private void CheckOut_Click(object sender, RoutedEventArgs e)
        {
            int orderStatus = StatusCombo.SelectedIndex;
            DateTime now = DateTime.Now;
            string query; 
            int newOrderID = 0;                                 
            if (line.Count>0)
            {
                if(orderStatus>0)
                {
                    if (orderDate.SelectedDate.ToString() != "")
                    {
                        invoiceString.date = DateTime.Now.ToString("D");
                        invoiceString.customerName = CustFname.Text + " " + CustLname.Text;
                        invoiceString.hst = (13 * Convert.ToDouble(invoiceString.subTotal)) / 100;
                        invoiceString.saleTotal = invoiceString.hst + Convert.ToDouble(invoiceString.subTotal);
                        invoiceString.branchName = BranchCombo.Text;
                        invoiceString.orderStatus = StatusCombo.Text;
                        invoiceString.productsPurchased = new List<string>();
                        string[] date = orderDate.SelectedDate.ToString().Replace("/", "-").Split(' ');
                        query = "select max(OrderID) from orders ;";
                        query = dc.IsExistInMRWally(query);
                        if (query==" ")
                        {
                            newOrderID += 1;
                        }
                        else
                        {
                            newOrderID = Convert.ToInt32(query) + 1;
                        }                        
                        query = "insert into orders(OrderID,CustomerID,BranchID,OrderDate,orderStatus,orderTotal)values(" + newOrderID + "," + CustID.Text + ", " + BranchCombo.SelectedIndex + ", '" +date[0] + "','" + StatusCombo.Text + "'," + invoiceString.subTotal + ");";
                        dc.CUDFromMRWally(query);
                        invoiceString.orderID = newOrderID.ToString();
                        if (orderStatus == 1)
                        {
                            foreach (OrderItem item in line)
                            {
                                query = "insert into OrderLine(OrderID,ProductID,Quantity)values(" + newOrderID + ", " + item.Product_ID + ", " + item.Quantity + ");";
                                dc.CUDFromMRWally(query);
                                invoiceString.productsPurchased.Add("" + item.Product_Name + " " + item.Quantity.ToString() + " x " + "$" + string.Format("{0:N2}", item.Price) + " = $" + string.Format("{0:N2}", item.Total_Amount) + "\n");
                            }
                            GenrateInvoice();
                            line.Clear();
                            Reset();
                            invoiceString.productsPurchased = null;
                        }
                        if (orderStatus == 2)
                        {
                            foreach (OrderItem item in line)
                            {
                                query = "Select productStock from products where products.productID=" + item.Product_ID + ";";
                                query = "update products set productStock=" + (Convert.ToInt32(dc.IsExistInMRWally(query)) + item.Quantity) + " where productID=" + item.Product_ID + ";";
                                dc.CUDFromMRWally(query);
                                query = "insert into OrderLine(OrderID,ProductID,Quantity)values(" + newOrderID + ", " + item.Product_ID + ", " + item.Quantity + ");";
                                dc.CUDFromMRWally(query);
                                invoiceString.productsPurchased.Add("" + item.Product_Name + " " + item.Quantity.ToString() + " x " + "$" + string.Format("{0:N2}", item.Price) + " = $" + string.Format("{0:N2}", item.Total_Amount) + "\n");
                            }
                            GenrateInvoice();
                            Reset();
                            line.Clear();
                            invoiceString.productsPurchased = null;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please select order Date!!", "Empty Fields ", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Please select Order Status !!", "Empty Fields ", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            else
            {
                MessageBox.Show("Cart is Empty!!", "Empty Fields ", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

        }
        /*================================================================================================
        *  Function    : Reset
        *  Description : This function will make all the  field empty
        *  Parameters  : Nothing
        *  Returns     : Nothing as return type is void
        ================================================================================================*/         
        public void Reset()
        {
            MakeFieldEmpty();
            ProductFieldEmpty();
            showData.ItemsSource = "";
            ShoppingCart.ItemsSource = "";
            StatusCombo.SelectedIndex = 0;
            orderDate.Text = "";
            TotalOrderCost.Content = "";
        }
        /*================================================================================================
        *  Function    : ProductFieldEmpty
        *  Description : This function will make all the product field empty
        *  Parameters  : Nothing
        *  Returns     : Nothing as return type is void
        ================================================================================================*/         
        public void ProductFieldEmpty()
        {
            Productcombo.SelectedIndex = 0;
            ItemQuantity.Text = "";
            BranchCombo.SelectedIndex = 0;
        }
        /*================================================================================================
        *  Function    : GenrateInvoice
        *  Description : This function will genrate recipite for the customer
        *  Parameters  : Nothing
        *  Returns     : Nothing as return type is void
        ================================================================================================*/         
        public void GenrateInvoice()
        {
           string bill = "*******************************\nThank you for Shopping at\n" + "Wally's " + invoiceString.branchName + "\n" + "On" + invoiceString.date + ", " + invoiceString.customerName + "!\n\n" + "Order ID : " + invoiceString.orderID + "\n\n";
           foreach(string s in invoiceString.productsPurchased)
           {
                bill += s;
           }
           bill += "\nSubtotal= $ " + string.Format("{0:N2}", invoiceString.subTotal) + "\n";
           bill += "HST (13) = $ " + string.Format("{0:N2}", invoiceString.hst) + "\n";           
            if(invoiceString.orderStatus=="PAID")
            {
                bill += "Sale Total = $ " + string.Format("{0:N2}", invoiceString.saleTotal) + "\n";
                bill +="Paid - Thank you !\"";
            }
            else
            {
                bill += "Sale Total = -$ " + string.Format("{0:N2}", invoiceString.saleTotal) + "\n";
                bill += "Refund - Thank you !\"";
            }
           
           MessageBox.Show(bill, "Invoice Receipt", MessageBoxButton.OK, MessageBoxImage.Information);            
        }
        /*================================================================================================
        *  Function    : showData_SelectionChanged
        *  Parameters  : object sender:
                         System.Windows.Controls.SelectionChangedEventArgs  e:
        *  Returns     : Nothing as return type is void
        ================================================================================================*/ 
        private void showData_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            string query = "";
           if(allowToSelectForOrderLine)
           {
                DataGrid gd = (DataGrid)sender;
                dynamic rowView = gd.SelectedItem;
                if (rowView != null)
                {
                    query = "select products.ProductName, format((products.productPrice*1.4),2) as Product_Price , orderline.Quantity, format(((products.Productprice*1.4) * orderline.Quantity),2) as TotalPrice from orderline  inner join products on orderline.ProductID = products.ProductID where OrderID =" + rowView[0] + " ;";
                    ds=dc.GetDataSet(query);
                    ShoppingCart.ItemsSource = ds.Tables[0].DefaultView;
                    query = "select orderTotal from orders where orderID=" + rowView[0]+";";
                    query = dc.IsExistInMRWally(query);
                    TotalOrderCost.Content = "$ " + query;
                    Cartbox.Content = "Order Detials";
                }                
            }
           else if(allowToSelectForCustomer)
           {
                DataGrid gd = (DataGrid)sender;
                dynamic rowView = gd.SelectedItem;
                if (rowView != null)
                {
                    query = "select * from customers where customerID="+ rowView[0]+";";
                    ds = dc.GetDataSet(query);

                    DataRow row = ds.Tables[0].Rows[0];
                    CustFname.Text = row["FirstName"].ToString();
                    CustLname.Text = row["LastName"].ToString();
                    CustID.Text = row["CustomerID"].ToString();
                    string[] num= row["Telephone"].ToString().Split('-');
                    ACode.Text = num[0];
                    num1.Text = num[1];
                    num2.Text = num[2];
                }
            }
        }
    }

}

