﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevengeOfWally
{
    class Customer
    {

        public string id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string Telephone { get; set; }
    }
}
