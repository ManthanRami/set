;*******************************************************************
;* This stationery serves as the framework for a user application. *
;* For a more comprehensive program that demonstrates the more     *
;* advanced functionality of this processor, please see the        *
;* demonstration applications, located in the examples             *
;* subdirectory of the "Freescale CodeWarrior for HC08" program    *
;* directory.                                                      *
;*******************************************************************

; Include derivative-specific definitions
            INCLUDE 'derivative.inc'
            

; export symbols
            XDEF _Startup, main
            ; we export both '_Startup' and 'main' as symbols. Either can
            ; be referenced in the linker .prm file or from C/C++ later on
            
            
            
            XREF __SEG_END_SSTACK   ; symbol defined by the linker for the end of the stack


; variable/data section
FIRSTOPERAND:	EQU 	$80 	; value of firstoperand is at address $80 
SECONDOPERAND:	EQU 	$81 	; value of secondoperand is at address $81 
SUM: 			EQU		$84		; where to store sum 
DIFFERENCE: 	EQU		$86 	; where to store difference 

MY_ZEROPAGE: SECTION  SHORT     ; Insert here your data definition

; code section

MyCode:     SECTION
main:
	

_Startup:
            LDHX   #__SEG_END_SSTACK ; initialize the stack pointer
            TXS
			CLI			; enable interrupts
		    JMP mainLoop
calculateSum:
	PSHH				; preserve the H:X and A register values upon being called
	PSHX
	PSHA
    LDX	6, SP			; load the X register with the firstoperand  
    CLRH				; need to clear out the H register
    LDA	7, SP			; load the secondoperand into the accumulator
    ADC X					; perform the summation
    STA	6, SP			; store the answer into the place on the stack where
						; the firstoperand  was
    PULA				; pop the saved H:X and A values off the stack
    PULX
	PULH
    RTS					; return to where we came from

calculateDiffrence:
	
	PSHH				; preserve the H:X and A register values upon being called
    PSHX
    PSHA
    LDX	6, SP			; load the X register with the firstoperand  
    CLRH				; need to clear out the H register
    LDA	7, SP			; load the secondoperand into the accumulator
	NEGA					; it inverse value
    ADC	X				; perform the summation
    STA	6, SP			; store the answer into the place on the stack where
						; the firstoperand  was
	JSR	calculateSum	; call function	
    PULA				; pop the saved H:X and A values off the stack
    PULX

mainLoop:
            
  ; Insert your code here
    LDA		FIRSTOPERAND 		; firstoperand  value
	PSHA						; push firstoperand  onto the stack -will get it later
	LDA		SECONDOPERAND 		; secondoperand value
	PSHA						; push secondoperand onto the stack -will get it later
	JSR		calculateSum		; call the function 
	PULA						; pop the answer of the stack
	STA		SUM			
	AIS		#1					; clean up stack- there is one byte remaining- get rid of it
	LDA		FIRSTOPERAND 		; firstoperand  value
	PSHA						; push firstoperand  onto the stack -will get it later
	LDA		SECONDOPERAND 		; secondoperand value
	PSHA						; push secondoperand onto the stack -will get it later
	JSR		calculateDiffrence	; call the function
	PULA						; pop the answer of the stack
	STA		DIFFERENCE 		
	AIS		#1					; clean up stack- there is one byte remaining- get rid of it
          
               NOP

            feed_watchdog
            BRA    mainLoop


