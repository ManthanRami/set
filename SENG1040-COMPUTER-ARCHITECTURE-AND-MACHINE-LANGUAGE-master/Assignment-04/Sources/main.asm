;*******************************************************************
;* This stationery serves as the framework for a user application. *
;* For a more comprehensive program that demonstrates the more     *
;* advanced functionality of this processor, please see the        *
;* demonstration applications, located in the examples             *
;* subdirectory of the "Freescale CodeWarrior for HC08" program    *
;* directory.                                                      *
;*******************************************************************

; Include derivative-specific definitions
            INCLUDE 'derivative.inc'
            

; export symbols
            XDEF _Startup, main
            ; we export both '_Startup' and 'main' as symbols. Either can
            ; be referenced in the linker .prm file or from C/C++ later on
            
            
            
            XREF __SEG_END_SSTACK   ; symbol defined by the linker for the end of the stack


; variable/data section
VAR1:			EQU		$100
VAR2:			EQU		$101
VAR3:			EQU		$102
FINAL:			EQU		$10A

MY_ZEROPAGE: SECTION  SHORT         ; Insert here your data definition

; code section
MyCode:     SECTION
main:
_Startup:
            LDHX   #__SEG_END_SSTACK ; initialize the stack pointer
            TXS
			CLI			; enable interrupts

mainLoop:
            LDA #48			    ; LINE 01 - This instruction takes a value 0x30 and save it to an accumulator.
			STA VAR1			; LINE 02 - This instruction save a copy of value from the accumulator and pass to var1.			
			LDA #%10011001		; LINE 03 - This instruction takes value of 0x99 and save it to an accumulator.			
			STA VAR2			; LINE 04 - This instruction save copy of value from an accumulator and pass to var2.		
			LDA #$A3			; LINE 05 - This instruction takes value of 0xA3 and save it to an accumulator.			
			STA VAR3			; LINE 06 - This instruction save copy of value from an accumulator and pass to the var3.			
								; LINE 07
			LDA VAR1			; LINE 08 - This instruction takes vaule from memory address of var1 and stores it in accumulator.
												
			ADD VAR2			; LINE 09 - This instruction add function containing value of 0x30 and 0x99, and pass a copy of it to an accumulator.
			NEGA				; LINE 10 - In This instruction, accumulator will store value of 0x37a.
			ADD VAR3			; LINE 11
			STA FINAL			; LINE 12 - This instruction takes copy of value from an accumulator and pass to memory location.
													
								; LINE 13
			endOfProg:			; LINE 14
			BRA endOfProg		; LINE 15
									
									
            NOP

            feed_watchdog
            STA SRS              ; feed the watchdog
            BRA    mainLoop

