/*
* FILE : mini2.cpp
* PROJECT : SENG1000 - mini2
* PROGRAMMER : Manthan Rami
* FIRST VERSION : 2019 - 1 - 02
* DESCRIPTION : This program will calculate the sum of 1 to 100 integers.
*/
#include<stdio.h>
int main()
{
	int initial_number = 0;
	int sum = 0;
	while (initial_number <=  100) 
	{
		sum = sum + initial_number;
		initial_number++; 
	}
	printf("The sum of the numbers from 1 to 100 is %d", sum);
	return 0;
}