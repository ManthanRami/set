/*
*FILE: mini6.c
*PROJECT: mini6
*PROGRAMMER: MANTHAN RAMI
*Date:30/03/2019
*DESCRIPTION: this program takes string input from the user and split it into cityname, flayover time and layover time.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// warinig disable
#pragma warning(disable:4996)


//declaring constant
#define ksizeOfarray 81


int main()
{
	
	while (1)
	{

		int count = 0;
		int coma = 0;
		int colon = 0;
		char userInput[ksizeOfarray] = { 0 };
		printf("Please enter a string:");
		fgets(userInput, ksizeOfarray, stdin);

		char cityName[81] = { 0 };
		char flightTime[81] = { 0 };
		char layoverTime[81] = { 0 };

		int flightHour = 0;
		int flightMin = 0;
		int layoverHour = 0;
		int layoverMin = 0;

		char *position = NULL;
		if (userInput == '.')
		{
			break;
		}
		
		else
		{
			//int length = 0;
			
			for (count = 0; count <= strlen(userInput); count++)
			{
				if (userInput[count] == ',' )
				{
					coma++;
					
				}
				else if (userInput[count] == ':')
				{
					colon++;
				}
				else
				{

				}
			}
			if (coma == 2 && colon==2)
			{

				// getting city name
				position = strchr(userInput, ',');
				(*position) = '\0';
				strcpy(cityName, userInput);



				// getting flight time
				position++;
				strcpy(flightTime, position); // string copy to flightTime
				(*(strchr(flightTime, ','))) = '\0';


				position = strchr(position, ',');
				(*position) = '\0';


				// getting layover time
				position++;
				strcpy(layoverTime, position);// string copy to layoverTime



				flightHour = atoi(flightTime);
				layoverHour = atoi(layoverTime);


				position = strchr(flightTime, ':');  //seperating from ":"
				flightMin = atoi(++position);

				position = strchr(layoverTime, ':'); //seperating from ":"

				layoverMin = atoi(++position);

				layoverMin = (layoverMin * 100) / 60;
				flightMin = (flightMin * 100) / 60;
				printf("===============================================================");
				printf("\n%s has flight time %d.%d hours and layover time %d.%d hours\n", cityName, flightHour, flightMin, layoverHour, layoverMin);
				printf("===============================================================\n");
			}
			else
			{
				printf("Invalid Input\n");
			}
		}

			

	}
		
	
	return 0;
}

 