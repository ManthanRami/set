
// Description: This file contains the prototypes, constant and include files for the mini7  project.
// These are all of the prototypes for the various functions found within the source files.

#pragma once
#include<stdio.h>
#include<string.h>
#include<stdlib.h>

//warning disable
#pragma warning (disable:4996)
#pragma warning (disable:4018)
#pragma warning (disable:4047)

//prototypes
int parseTime(char inputTime[], unsigned int *pHours, unsigned int *pMinutes);
int parseLine(char inputline[], struct CityInfo *pCityInfo);

//constant
#define MAX_SIZE 81
#define WRONG_NUM 0
#define SUCCESS 1

//struct declaration
struct  MyTime
{
	unsigned int hours;
	unsigned int minutes;

};

struct CityInfo
{
	char cityName[MAX_SIZE];
	struct MyTime flightTime;
	struct MyTime layoverTime;

};

