/*
*FILE: mini7.c
*PROJECT: mini7
*PROGRAMMER: MANTHAN RAMI
*Date: 13/04/2019
*DESCRIPTION: this program takes string input from the user and seperate it into diffrent components and fill it to struct.
*/
#include"mini7.h"
int main()
{
	struct  CityInfo x;
	int errorNum = 0;
	char userInput[81] = { 0 };

	printf("Please enter a string:");
	fgets(userInput, 81, stdin);

	errorNum=parseLine(userInput, &x);
	if (errorNum == 0)
	{
		printf("Error with input data format");
		return WRONG_NUM;
	}
	return SUCCESS;
}
