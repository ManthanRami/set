#include"mini7.h"
//This file include two major used function for mini7 project.

/*Description: Separates a string into its component parts, as described by the parameters.The component 
			   parts are used to fill a struct that is pointed to by the second parameter.
Parameters :
			char inputline[] : input line containing city name and time information(originating from the user input)
							   that should be in the format required in Mini - 06
			struct CityInfo *pCityInfo : pointer to a struct that is filled in with the city name and time information
										 from the input line
Returns : 1 if OK, 0 if there are problems with the data
*/

int parseLine(char inputline[], struct CityInfo *pCityInfo)
{
	int count = 0;
	int comma = 0;
	int hour = 0;
	int minutes = 0;
	int return_num = 0;


	char *address = NULL;
	char fTime[81] = { 0 };
	char lTime[81] = { 0 };

	for (count = 0; count <= strlen(inputline); count++) //checking comma
	{
		if (inputline[count] == ',')
		{
			comma++;

		}
		else
		{
		}
	}
	if(comma == 2)
	{
		// getting city name
		address = strchr(inputline, ',');
		(*address) = '\0';
		strcpy(pCityInfo->cityName, inputline);

		//getting flight time
		address++;
		strcpy(fTime, address); // string copy to flightTime
		(*(strchr(fTime, ','))) = '\0';
		address = strchr(address, ',');
		(*address) = '\0';

		// getting layover time
		address++;
		strcpy(lTime, address);// string copy to layoverTime
	}
	else
	{
		return WRONG_NUM;
	}

	//seprating hours and minutes
	return_num= parseTime(fTime, &hour, &minutes);
	if (return_num == NULL) 
	{
		return WRONG_NUM;
	}
	// initialize struct
	struct MyTime flightTime;
	flightTime.hours = hour;        //fill array by flight hours
	flightTime.minutes = minutes;  //fill array by flight minutes

	//seprating hours and minutes
	return_num=parseTime(lTime, &hour, &minutes);
	if (return_num == NULL)
	{
		return WRONG_NUM;
	}
	// initialize struct
	struct MyTime layoverTime;
	layoverTime.hours = hour;     //fill array by layover hours
	layoverTime.minutes = minutes;  //fill array by layover minutes

	return SUCCESS;
}


/*
Description: Separates a time string into hours and minutes
 Parameters:
			 char inputTime[]: string containing a time that should be in the format required in Mini-06
			 unsigned int *pHours: pointer to hours portion of the time
			 unsigned int *pMinutes: pointer to minutes portion of the time
 Returns: 1 if OK, 0 if there are problems with the data
 */
int parseTime(char inputTime[], unsigned int *pHours, unsigned int *pMinutes)
{
	int count = 0;
	int colon = 0;

	char *location = NULL;
	char hours[3] = { 0 };
	char minutes[3] = { 0 };

	for (count = 0; count <= strlen(inputTime); count++) // checking for ':'
	{
		if (inputTime[count] == ':')
		{
			colon++;

		}
		else
		{
		}
	}
	if (colon == 1)
	{
		location = strchr(inputTime, ':');//seperating from ":"
		(*location) = '\0';
		strcpy(hours, inputTime);
		*pHours = atoi(hours);

		location++;
		strncpy(minutes, location, 2);
		*pMinutes = atoi(minutes);
		return SUCCESS;
	}
	else
	{
		return WRONG_NUM;
	}
	
}