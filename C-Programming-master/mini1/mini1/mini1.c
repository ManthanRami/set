/*
* FILE : mini1.c
* PROJECT : SENG1000 - Mini1
* PROGRAMMER : Manthan Rami
* FIRST VERSION : 2019 - 19 - 01
* DESCRIPTION : This program nothing except displaying few sentence by printf("***");
*/
#include <stdio.h> 

int main(void)
{
	int number = 9;

	/* print the number upon startup */
	printf("Here be number! ");
		printf("The number is %d\n", number);

	/* change the value of the number and print it */
	number = 8;
	printf("The number is now %d, not %d\n", number, 9);

	return 0;
}
