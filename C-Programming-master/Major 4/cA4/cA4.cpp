/*
*FILE: cA4.cpp
*PROJECT: cA4
*PROGRAMMER: MANTHAN RAMI
*Date:15/04/19
*DESCRIPTION: This program will take input from the "input.txt" and split it in to diffrent files named theCities.txt for cities name and theTimes.dat for flighr time and layover time.
*/

//include file 
#include <stdio.h>                              
#include <stdlib.h>                            
#include <string.h>

//declaring constant
#define MAX_SIZE 81
#define READ_TEXT "r"
#define WRITE_TXT "w"
#define WRITE_BINARY "wb"

// output file names
#define CITY_OUTPUT "theCities.txt"
#define TIME_OUTPUT "theTimes.dat"

//return values 
#define WRONG_THING -1
#define ZERO 0
#define OKAY 0

//disabale warrning 
#pragma warning(disable: 4996)
#pragma warning(disable: 4313)
#pragma warning(disable: 4477)


int main(int argc, char *argv[])
{
	FILE *finput = NULL;		
	FILE *fcity = NULL;			
	FILE *ftime = NULL;		

	char record[MAX_SIZE] = { 0 };
	char flightTime[MAX_SIZE] = { 0 };
	char layoverTime[MAX_SIZE] = { 0 };
	char *location = NULL;
	
	int flightHour = 0;
	int flightMin = 0;
	int layoverHour = 0;
	int layoverMin = 0;
	int done = OKAY;

	char flighth[MAX_SIZE] = { 0 };
	char flightm[MAX_SIZE] = { 0 };
	char layourh[MAX_SIZE] = { 0 };
	char LayoverM[MAX_SIZE] = { 0 };

	if (argc != 2) //checking file 
	{
		printf("Usage: cA4 <inputFile>\n");
		return WRONG_THING;
	}

	// Open the input file from command line
	finput = fopen(argv[1], READ_TEXT);

	// check if provided file exist, exit if it doesn't
	if (finput == ZERO)
	{
		printf("Error reading [%s]", argv[1]);
		return WRONG_THING;
	}

	fcity = fopen(CITY_OUTPUT, WRITE_TXT);
	if (fcity == ZERO) // checking file 
	{
		printf("Error opening [%s]", CITY_OUTPUT);
		return WRONG_THING;
	}

	// create binary file to write
	ftime = fopen(TIME_OUTPUT, WRITE_BINARY);
	if (ftime == ZERO) // checking file 
	{
		printf("Error opening[%s]", TIME_OUTPUT);
		return WRONG_THING;
	}

	while (fgets(record, MAX_SIZE, finput))
	{
		// if the line does not contain comma (,) as a delimeter, skip over it
		if (!strchr(record, ','))
			continue;

		location = strchr(record, ',');
		(*location) = '\0';
		if (fprintf(fcity, "%s\n", record) < ZERO) //writting in text file
		{
			printf("Error writing file [%s]\n", CITY_OUTPUT);
			return WRONG_THING;
		}

		//getting flightTime
		location++;
		strcpy(flightTime, location); // string copy to flightTime
		(*(strchr(flightTime, ','))) = '\0';

		location = strchr(location, ',');
		(*location) = '\0';

		// getting layover time
		location++;
		strcpy(layoverTime, location);// string copy to layoverTime

		flightHour = atoi(flightTime);		 //converting string into intger
		layoverHour = atoi(layoverTime);	//converting string into intger

		location = strchr(flightTime, ':');  //seperating from ":"
		flightMin = atoi(++location);		//converting string into intger

		location = strchr(layoverTime, ':'); //seperfating from ":"
		layoverMin = atoi(++location); //converting string into intger

		char flighth = flightHour;
		char flightm = flightMin;
		char layourh = layoverHour;
		char LayoverM = layoverMin;

		if (fprintf(ftime, "%c%c", flighth, flightm) < ZERO) //writting flight time in binary file
		{
			printf("Error writing  file [%s]\n", TIME_OUTPUT);
			return WRONG_THING;
		}
		
		if (fprintf(ftime, "%c%c", layourh, LayoverM) < ZERO) //writting layovertime in binary file
		{
			printf("Error writing file [%s]\n", TIME_OUTPUT);
			return WRONG_THING;
		}
	}
		// close the file we are writing cities names to
		if (fclose(fcity) == EOF)
		{
			// can't close it 
			printf("Error closing [%s]\n", CITY_OUTPUT);
			return WRONG_THING;
		}

		// close the file we are writing time to
		if (fclose(ftime) == EOF)
		{
			// can't close it 
			printf("Error closing [%s]\n", TIME_OUTPUT);
			return WRONG_THING;
		}

		// close the file we are reading from 
		if (fclose(finput) == EOF)
		{
			// can't close it 
			printf("Error closing [%s]\n", argv[1]);
			return WRONG_THING;
		}

		return done;
}

