/*============================================================================================================================

  FILE : ca2.cpp
  PROJECT : Ca2
  PROGRAMMER : Manthan Rami
  FIRST VERSION : 2019 - 21-02
  DESCRIPTION : This program will disply Power Menu as :
				Power Menu
				1. Change base
				2. Change exponent
				3. Display base raised to exponent
				4. Exit program
				Option?
				and it will take input from the user for
				base and exponent and display result on
				selecting option 3.

============================================================================================================================*/

#include<stdio.h>


// Function Prototype

int getNum(void);
int calculatePower(int base, int exponent);
int checkRange(int number, int minimumNumber, int maximumNumber);


int main(void)
{
	int base_Num = 1;
	int number = 0;
	int exponent_Num = 1;
	int power = 1;
	int temp = 1;
	int a = 0;
	while (number != 4)
	{

		printf("\nPower Menu\n 1. Change base\n 2. Change exponent\n 3. Display base raised to exponent\n 4. Exit program\n Option?");
		number = getNum();
		switch (number)
		{
		case 1:
			printf("Please Enter any Base ");
			temp = base_Num;
			base_Num = getNum();
			a = checkRange(base_Num, 1, 25);
			if (a == 0)
			{
				base_Num = temp;
			}
			break;
		case 2:
			printf("Please Enter any Exponent ");
			exponent_Num = getNum();
			checkRange(exponent_Num, 1, 5);
			break;
		case 3:
			power = calculatePower(base_Num, exponent_Num);
			printf("%d\n", power);
			break;
		case 4:
			break;
		default:
			printf("Please Enter a Valid option");
		}

	}
}

/*============================================================================================================================

  Function:       calculatePower
  Parameter:      (1) int base : Value will be filled from main function
				  (2) int exponent : Value will be filled from main function
  Return Value:   Return 1 if the value of base is equal to 0,
				  result otherwise.
  Description:    This function will take value of base and exponent from the main
				  function and calculate it and send result back to main function.

============================================================================================================================*/
int calculatePower(int base, int exponent)
{
	int i = 1;
	int result = 1;

	if (base == 0)
	{
		return 1;
	}
	else
	{
		for (i = exponent; i > 0; i--)
		{
			result = result * base;
		}
		return result;
	}
}





/*===========================================================================================================================

Function:       checkRange
Parameter:      (1) int number
Return Value:   Return 1 if the conditon is true,
				return 0 otherwise.
Description:    This function will take value of base or exponent from the main function and check if they are in range.
				If they are in range , the condition is true.

============================================================================================================================*/

int checkRange(int number, int minimumNumber, int maximumNumber)
{

	if (minimumNumber <= number && number <= maximumNumber)
	{
		return 1;
	}
	else
	{
		printf("Value is out of Range");
		return 0;
	}
}

/*==========================================================================================================================

Function: getNum()
Parameter: void
Return Value: Return number
Description: This function will take input from the user.

============================================================================================================================*/

#pragma warning(disable: 4996)
int getNum(void)
{
	/* the array is 121 bytes in size; we'll see in a later lecture how we can improve this code */
	char record[121] = { 0 }; /* record stores the string */
	int number = 0;
	/* NOTE to student: indent and brace this function consistent with your others */
	/* use fgets() to get a string from the keyboard */
	fgets(record, 121, stdin);
	/* extract the number from the string; sscanf() returns a number
	 * corresponding with the number of items it found in the string */
	if (sscanf(record, "%d", &number) != 1)
	{
		/* if the user did not enter a number recognizable by
		 * the system, set number to -1 */
		number = -1;
	}
	return number;
}