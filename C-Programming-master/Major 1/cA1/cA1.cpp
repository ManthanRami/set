/*
* FILE : cA1.cpp
* PROJECT : cA1
* PROGRAMMER : Manthan Rami
* FIRST VERSION : 2019 - 03 - 02
* DESCRIPTION : This program is printing a to z on LHS (Left Hand Side) and average value in integer of
				my inital Letter as per ASCII Table on RHS (Right hand side).
*/



#include<stdio.h>
#define Initial_letter 'm'   // my name's initial Letter.		
int main(void)
{


	char alphabets = 'a';
	int counter = 1;
	int total = 0;
	int avg = 0;



	while (alphabets < 'z') // loop that will loop whole process until the value of alphabets became "z".
	{

		printf("%c\t", alphabets); // 't' is used for leaving space between tow coulomb 

		total = total + alphabets; // To calculate average.
		avg = total / counter;


		if (alphabets == Initial_letter) // if condition to check weather the given condition is true or false.
		{

			printf("%d\n", avg); //  This will print my average value in integer of my inital Letter as per ASCII Table.
		
		}
		else
		{

			printf("%c\n", avg);// This will print a to z in character. 
		
		}


		counter++; // by using "+" I have increase value of counter.
		alphabets++; // by using "+" I have increase value of alphabets.


	}

	return 0;

}
