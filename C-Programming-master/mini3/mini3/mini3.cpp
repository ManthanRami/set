/*
*FILE: mini3.cpp
*PROJECT: mini3
*PROGRAMMER: MANTHAN RAMI
*Date:6/02/19
*DESCRIPTION: this program takes input from the user and verifies whether number is even or odd.
*/
#include<stdio.h>
int getNum(void);
int isOdd(int number);
int main()
{
	int n = 0;
	int isOddReturn = 0;
	int number = 0;

	printf("please input number :");

	// changing the value of number variable to that of input from user 
	number = getNum();
	//giving the value of return from isOdd functon to variable isOddReturn
	isOddReturn = isOdd(number);

	if (isOddReturn == 0)
	{
		printf("number is Even");
	}
	else
	{
		printf("number is Odd");
	}
}
/*
*FUNCTION: isOdd
*DESCRIPTION:= This function takes value from taken from user through getNum and checks wheter the number is even or odd.
*PARAMETERS: int number : the value of number is taken from the user in main function.
*Return:  0 if number is odd or 1 if number is odd:
*/

int isOdd(int number)
{
	// check whether input is even and return 0
	if (number % 2 == 0)
	{
		return 0;
	}
	// returns 1 if input is not even
	else
	{
		return 1;
	}
}
// getNum() function taken form the sample given by Sean in sample 3.
#pragma warning(disable: 4996) 
int getNum(void)   
{
	/* the array is 121 bytes in size; we'll see in a later lecture how we can improve this code */
	char record[121] = { 0 }; /* record stores the string */
	int number = 0;
	/* NOTE to student: indent and brace this function consistent with your others */
	/* use fgets() to get a string from the keyboard */
	fgets(record, 121, stdin);
	/* extract the number from the string; sscanf() returns a number
	 * corresponding with the number of items it found in the string */
	if (sscanf(record, "%d", &number) != 1)
	{
		/* if the user did not enter a number recognizable by
		 * the system, set number to -1 */
		number = -1;
	}
	return number;
}