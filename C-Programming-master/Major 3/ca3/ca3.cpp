/*
* FILE : ca3.cpp
* PROJECT : ca3
* PROGRAMMER : Manthan Rami
* FIRST VERSION : 2019-03-23
* DESCRIPTION : This program will take input from the user for home city and destination city and calaculate total travelling time and return in HH:MM format.
*/

#include<stdio.h>
#include<stdlib.h>
#include<iostream>
using namespace std;

//constant declaration 
const int size_of_Array = 5;

//Prototypes
int getNum(void);
int calculateTime(int home, int destination);

int main()
{
	//Variable declaration 
	int starting_city = 0;
	int ending_city = 0;
	int totalHours = 0;
	int minutes = 0;
	int timeTaken = 0;
	
	while (true)
	{
		// List of countries names
		cout << "\n 1.| Toronto\n";
		cout << " 2.| Atlanta\n";
		cout << " 3.| Austin\n";
		cout << " 4.| Denver\n";
		cout << " 5.| Chicago\n";
		cout << " 6.| Buffalo\n";

		cout << "\nPlease Enter Starting City: ";
		starting_city = getNum();

		//checking user input for starting city
		if (starting_city == 0 || starting_city == -1)
		{
			return 0;
		}

		cout << "\nPlease Enter Ending City: ";
		ending_city = getNum();
		//checking user input for ending city
		if (ending_city == 0 || ending_city == -1)
		{
			return 0;
		}

		//checking user input range and displaying error incase of out of range
		if (starting_city > 6 || ending_city > 6 || starting_city<-1 || ending_city<-1)
		{
			cout << "\nPlease Enter A Valid Input" << endl;
			continue;
		}

		timeTaken = calculateTime(starting_city - 1, ending_city - 1);
		totalHours = timeTaken / 60;
		minutes = timeTaken % 60;
		cout << "=================================================="<<endl;
		cout <<"Total time taken to travell "<< totalHours<<":" <<minutes<< endl;
		cout << "==================================================" << endl;
	}
	return 0;
}

/*
  *	 FUNCTION:    calculateTime()
  *	 DESCRIPTION: This function will take values of home and destination from main function and calculate total time taken for travelling. 
  *	 PARAMETERS:  int home- starting city given by user.
				  int destination- Ending city given by user.
  *	 RETURNS :    Total traveling time between two cities in hours and minutes.
*/

int calculateTime(int home, int destination)
{

	int minutes = 0;
	const int flyoverTime[size_of_Array] = { 255,238, 235, 134,207 };
	const int layoverTime[size_of_Array] = { 80, 46,689, 53,0 };

	if (home < destination)//forward travel
	{
		for (home; home < destination; home++)
		{
			// if  destination city is next city to home city
			if (home == destination - 1)
			{
				minutes += flyoverTime[home];
			}
			else   	//if destination city is not next city to home city
			{
				minutes += flyoverTime[home] + layoverTime[home];
			}
		}
	}
	   //reverse travel
	if (home > destination)
	{
		for (home; home > destination; home--)
		{
			//if destination city is next city to home city
			if (home == destination + 1)
			{
				minutes += flyoverTime[home - 1];
			}
			else    //if destination city is not next city to home city
			{
				minutes += flyoverTime[home - 1] + layoverTime[home - 2];
			}

		}

	}

	return minutes;
}

#pragma warning(disable: 4996)
int getNum(void)
{
	/* the array is 121 bytes in size; we'll see in a later lecture how we can improve this code */
	char record[121] = { 0 }; /* record stores the string */
	int number = 0;
	/* NOTE to student: indent and brace this function consistent with your others */
	/* use fgets() to get a string from the keyboard */
	fgets(record, 121, stdin);
	/* extract the number from the string; sscanf() returns a number
	 * corresponding with the number of items it found in the string */
	if (sscanf(record, "%d", &number) != 1)
	{
		/* if the user did not enter a number recognizable by
		 * the system, set number to -1 */
		number = -1;
	}
	return number;
}
