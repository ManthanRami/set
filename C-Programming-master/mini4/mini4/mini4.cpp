/*
* FILE : mini4.cpp
* PROJECT : SENG1000 - mini4
* PROGRAMMER : Manthan Rami
* FIRST VERSION : 2019 - 20 - 02
* DESCRIPTION : This program will take 10 integer from the user and display the minimum value of element and index number.
*/
#include<stdio.h>
#include <iostream> 

using namespace std;
int main(void)
{
	int myArray[10] = { 0 };
	int index = 0;
	int minimum = 0;
	int low_index_val = 0;


	//Print to prompt use to enter 10 data 
	cout << "Please Enter any 10 digit\n";

	//Get value from user and store it in array "myArray"
	while (index < 10)
	{
		cin >> myArray[index];
		index++;

	}
	// initializing 1st eleent of "myArray" array as minimum 
	minimum = myArray[0];

	// Finding the lowest value in the Array with the refrence of the first element.
	// "low_inde_val" is the index pf the lowest element in myArray.
	// 
	for (index = 0; index < 10; index++)
	{

		if (minimum > myArray[index])
		{
			minimum = myArray[index];
			low_index_val = index;
		}

	}
	// printing the lowst value in the myArray with its element number.
	cout << "Minimum array element is " << myArray[low_index_val] << " and index is " << low_index_val;

}