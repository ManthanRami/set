/*
  FILE : mini5.cpp
  PROJECT : mini5
  PROGRAMMER : Manthan Rami
  FIRST VERSION : 15-03-2019
  DESCRIPTION :This program will take input from user and give its average and sum.
*/

#include<stdio.h>
#include<iostream>
#include<ctype.h>

using namespace std;

//const variables declaration 
#define k 121
#define sizeOfArray 5
#define finalValue 20


// prototype of function which are used in this program.
bool getDouble(double *pNumber);
void generateState3(double d1, double d2, double d3, double *pAverage, double *pSum);
void generateStatsFromArray(double values[], int numArray, double *pAverage, double *pSum);
int fillArray(double values[], int numArray, double fillValue);


int main(void)
{
	double one = 0;
	double two = 0;
	double three = 0;
	double average_value = 0;
	double sum_value = 0;
	double my_array[5] = { 0 };
	int loop = 0;

	printf("Please Enter three Value:\n");

	//for getting user input for variable. 
	if (!getDouble(&one)) return -1;
	if (!getDouble(&two)) return -1;
	if (!getDouble(&three)) return -1;

	generateState3(one, two, three, &average_value, &sum_value);

	printf(" \n The average and sum of the three variables is: %lf and %lf\n",average_value, sum_value);
	printf("\n**********************************************************************");

	printf( "\n\nPlease Enter any five digit: \n");
	while (loop < 5)
	{
		if (!getDouble(&my_array[loop]))
		{
			return -1;
		}
		loop++;
	}
	//Calling generateStatsFromArray() function to calculate average and sum value of all elements.
	generateStatsFromArray(my_array, sizeOfArray, &average_value, &sum_value);

	printf("\nThe average and sum of the elements of array is: %lf,%lf\n", average_value, sum_value);
	printf("\n*********************************************************************\n");

	//Calling fillArray() function to fill array elements
	fillArray(my_array, sizeOfArray, finalValue);

	printf("\n%lf,%lf,%lf,%lf,%lf", my_array[0], my_array[1], my_array[2], my_array[3], my_array[4]);
	printf("\n======================================================================\n");
	
	return 0;

}


/*
================================================================================================================================================================
================================================================================================================================================================

Function: getDouble()
Parameter: double *pNumber: pointer to a variable that is filled in by the user input, if valid
Return Value:  int: 1 if the user entered a valid floating-point number, 0 otherwise
Description: This function gets a floating-point number from the user. If the user enters a valid
			 floating-point number, the value is put into the variable pointed to by the parameter
			 and 1 is returned.If the user-entered value is not valid, 0 is returned.

================================================================================================================================================================
================================================================================================================================================================
*/

#pragma warning(disable: 4996)

bool getDouble(double *pNumber)
{

	char record[k] = { 0 };
	fgets(record, 121, stdin);

	if (sscanf(record, "%lf", pNumber) != 1)
	{

		return false;//if the user input any invalid input for the required varaibles or elements.
	}
	return true;
}


/*
================================================================================================================================================================
================================================================================================================================================================

Function: generateStats3()
Parameters: double d1, d2, d3: three floating-point numbers
			uble *pAverage: pointer to a variable that is filled in by this function with the average of
			d1, d2, and d3 double *pSum: pointer to a variable that is filled in by this function with the
			sum of d1,d2, and d3
Return Value:  none
Description: This function takes three floating-point numbers passed as doubles and calculates the average
			 and sum of the numbers.  Once calculated, the average gets put into the variable pointed to by
			 the pAverage parameter and the sum gets put into the variable pointed to by the pSum parameter.

================================================================================================================================================================
================================================================================================================================================================
*/


void generateState3(double d1, double d2, double d3, double *pAverage, double *pSum)
{
	// Logic for calculating sum and average of variables.
	*pSum = (d1 + d2 + d3); // sum of variables.
	*pAverage = *pSum / 3; // average varuables.

}


/*
================================================================================================================================================================
================================================================================================================================================================

Function: generateStatsFromArray()
Parameters: double values[]: floating-point numbers int numArray: number of array elements.
			double *pAverage: pointer to a variable that is filled in by this function with the average of all elements in the array.
			double *pSum: pointer to a variable that is filled in by this function with the sum of all elements in the array.
Return Value:  none
Description: This function takes an array of floating-point (double) numbers, given the number of elements in the array as a parameter,
			and calculates the average and sum of the numbers.  Once calculated, the average gets put into the variable pointed to by
			the pAverage parameter and the sum gets put into the variable pointed to by the pSum parameter.

================================================================================================================================================================
================================================================================================================================================================
*/


void generateStatsFromArray(double values[], int numArray, double *pAverage, double *pSum)
{
	int count = 0;
	*pSum = 0;

	// Logic for calculating sum and average of elements of my_Array.
	while (count < numArray)
	{
		*pSum = *pSum + values[count];//Sum of elements of array.
		count++;
	}
	*pAverage = *pSum / 5; //Average of elements of array.

}



/*
================================================================================================================================================================
================================================================================================================================================================

Function: fillArray()
Parameters: double values[]: floating-point numbers
			int numArray: number of array elements
			double fillValue: value to put into array elements
Return Value:  none
Description: This function takes an array of floating-point (double) numbers, given the number of elements in the array as a parameter,
			and puts the fillValue into each element of the array.

================================================================================================================================================================
================================================================================================================================================================
*/

int fillArray(double values[], int numArray, double fillValue)
{
	//for loop for filling the elements of array.
	for (int counter = 0; counter < numArray; counter++)
	{
		values[counter] = fillValue;
	}

	return 0;
}