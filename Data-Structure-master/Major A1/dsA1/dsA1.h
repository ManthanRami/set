#pragma once
/*

* Description: This file contains the prototypes, constant and include files for the dsA1 project.
			   These are all of the prototypes for the various functions found within the source files.
*/
#pragma once

#include <stdio.h>
#include <stdlib.h>
#include<string.h>

//warning disable 
#pragma warning(disable : 4996)

//constant declaration 
#define ERROR 0
#define SUCCESS 1
#define kInputLength 30


// data types
typedef struct songInfo
{
	char *artist;
	char *title;
	int rate;
	struct songInfo *prev;
	struct songInfo *next;
} songInfo;

// prototypes
int ratingSortedList(char *artistName, char* titleName, int rate, songInfo **head, songInfo **tail);
int alphaSortedList(char *artistName, char *titleName, int rate, songInfo **head, songInfo **tail);
int getNum(void);
void printSongInfo(songInfo *arr);
void terminaTor(char *input);
void memoryFree(songInfo *arr);

songInfo *findSong(songInfo*node, char *artist, char*title);
songInfo*deleteNode(songInfo*node, songInfo**head, songInfo**tail);
songInfo *updateList(songInfo*node, char *artistName, char*titleName, int rate);
