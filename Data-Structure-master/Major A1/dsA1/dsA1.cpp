/*
*  Projectname:	dsA1
 * Filename:	dsA1.cpp
 * Author:		Manthan Rami
 * Date:		06/13/2019
 * Description:	This program that takes in information about songs and stores the artist and title
				in a linked list to be displayed. It will take unlimited pair of c style string of song
				from user until user gives an invalid input for artist and print it out in rating sorted 
				list and alphabetic sorted list.
 */
#include"dsA1.h"

int main(int argc, char *argv[])
{
	songInfo *firstHead = NULL, *firstTail = NULL;
	songInfo *secondHead = NULL, *secondTail = NULL;
	songInfo *blockTodelete = NULL;
	char artist[kInputLength] = { 0 };
	char title[kInputLength] = { 0 };
	int rate = 0;
	int i = 0;
	int check = 0;
	// enter info into list
	while (1)
	{
		// prompt user for a first name
		printf("\nEnter a song- %d Info: ", i + 1);
		
		//artist  name
		printf("\nEnter a artist name : ");
		fgets(artist, kInputLength, stdin);
		if (strcmp(artist, ".\n") == 0)
		{
			break;
		}

		//Title name
		printf("Enter a title name : ");
		fgets(title, kInputLength, stdin);
		
		//rating
		printf("Please rate song (1 to 5) :- ");
		rate = getNum();
		
		//Removing '\n'
		terminaTor(artist);
		terminaTor(title);
		
		//creating doubly sorted Lnkedlist by rating  
		check=ratingSortedList(artist, title, rate, &firstHead, &firstTail);
		if (check == ERROR)
		{
			printf("Error! No more memory!\n");
			return 0;
		}

		//creating doubly sorted Lnkedlist by alphabetical order
		check=alphaSortedList(artist, title, rate, &secondHead, &secondTail);
		if (check == ERROR)
		{
			printf("Error! No more memory!\n");
			return 0;
		}
		i++;
	}

	// printing rating sorted list
	printSongInfo(firstHead);

	//printing Alphabetical order list
	printf("\nAlphabetical order List: \n\n");
	printSongInfo(secondHead);

	//promting user for additional song input
	printf("\nEnter a song Info: ");

	//artist name
	printf("\nEnter a artist name : ");
	fgets(artist, kInputLength, stdin);
	
	//Title name
	printf("Enter a title name : ");
	fgets(title, kInputLength, stdin);

	//rating 
	printf("Please rate song (1 to 5) :- ");
	rate = getNum();

	//removing '\n'
	terminaTor(artist);
	terminaTor(title);

	// finding song in artist list
	blockTodelete = findSong(secondHead, artist, title);
	if (blockTodelete == NULL)
	{
		printf("Song Not found");

		// printing rating sorted list
		printSongInfo(firstHead);

		// printing Alphabetical order sorted list
		printf("\nAlphabetical order List: \n\n");
		printSongInfo(secondHead);
	}
	else
	{
		if (blockTodelete->rate == rate)//checking for new rating
		{
			printf("Please enter rating for this song to be update: ");
			rate = getNum();
			if (rate != blockTodelete->rate)// if it found new rating it will update 
			{
				//will update artist list with new rating 
				updateList(secondHead, artist, title, rate);
				
				//finding song in rating sorted list
				blockTodelete = findSong(firstHead, artist, title);

				//deleting from rating sorted
				deleteNode(blockTodelete, &firstHead, &firstTail);

				// re-inserting in rating sorted
				check = ratingSortedList(artist, title, rate, &firstHead, &firstTail);

				//checking for meomory allocation 
				if (check == ERROR)
				{
					printf("Error! No more memory!\n");
					return 0;
				}

				// printing rating sorted list
				printSongInfo(firstHead);

				// printing Alphabetical order sorted list
				printf("\nAlphabetical order List: \n\n");
				printSongInfo(secondHead);
			}
			else
			{
				printf("\n\nSong Not Found");

				// printing rating sorted list
				printSongInfo(firstHead);

				// printing Alphabetical order sorted list
				printf("\nAlphabetical order List: \n\n");
				printSongInfo(secondHead);

			}

		}

	}
	
	
	//free rating Sorted linkedList
	memoryFree(firstHead);

	//free artistname Sorted linkedList
	memoryFree(secondHead);
	
	// initializing head and tail of rating sorted list to NULL
	firstHead = NULL;
	firstTail = NULL;

	// initializing head and tail of  Alphabetical order sorted list to NULL
	secondHead = NULL;
	secondTail = NULL;

	return 0;
}

//===============================================================================================================================
/*
 * Function:	terminaTor:
 *
 * Description:	This function will search character '\n' in string and repalce it with '\0'.
 *
 * Parameter: char *input
 * Return Value: Nothing as data type is void
 */
void terminaTor(char *input)
{
	(*(strchr(input, '\n')) = '\0');
}

//===============================================================================================================================
/*
 * Function:
 *
 * Description:	This function will copy all data from destination to source.
 * Parameter:	char *destination: array containing information
				char *source: where is to be copied.
 * Return Value: Nothing as data type is void
 */
void dataCopy(char *destination, char *source)
{
	strcpy(destination, source);
}

//================================================================================================================================
/*
 * Function:	memoryChecking
 *
 * Description:	This function will check weather memory allocated to array is appropriate or having some issue with it.
 *
 * Parameter:	char *arr: array to be checked for.
 * Return Value: return value 1 if there is any error in memory allocation.
 */
int memoryChecking(char *arr)
{
	if (arr == NULL)
	{
		printf("Error in allocating memory");
		return ERROR;
	}
	return SUCCESS;
}	 
//===============================================================================================================================
/*
 * Function:	getNum:
 *
 * Description:	This function will take input for integer from the user.
 *
 * Parameter: void

 * Return Value: an integer given by the user. 
 */

int getNum(void)
{
	char record[121] = { 0 }; /* record stores the string from the user*/
	int number = 0;   /* fgets() - a function that can be called in order to read user input from the keyboard */
	fgets(record, 121, stdin);   /* sscanf() � a function that is able to �extract� something from the user input */
	if (sscanf(record, "%d", &number) != 1) {      /* if this line of code is executed � it means that the user didn�t enter  number */
		number = -1;
	}
	return number;
}