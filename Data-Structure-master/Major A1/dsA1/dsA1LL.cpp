
#include"dsA1.h"
/*
 * Function: ratingSortedList()
 * Description: This function inserts an element in the correct location in the list, sorted rating.
 * Parameters:  char *artistName: string containing the artist name
				char *titleName: string containing the title name
 *				songInfo **head, **tail: POINTERS TO head and tail of the list
 * Returns: 0 if the insertion failed, 1 otherwise
*/
int ratingSortedList(char *artistName, char *titleName, int rate, songInfo **head, songInfo **tail)
{
	songInfo *newBlock = NULL;
	songInfo *beforeElement = NULL;
	songInfo *afterElement = NULL;

	//dynamically memory allocatiion of newBlock
	newBlock = (songInfo *)malloc(sizeof(songInfo));

	//checking for memory allocation
	if (newBlock == NULL)
	{
		return 0;
	}

	//dynamically memory allocatiion of artis00name
	newBlock->artist = (char*)malloc((strlen(artistName) + 1) * sizeof(char));

	//copying data of artistname to newBlock
	strcpy(newBlock->artist, artistName);
	
	//dynamically memory allocatiion of Title name
	newBlock->title = (char*)malloc((strlen(titleName) + 1) * sizeof(char));
	
	//copying data of titleName to newBlock
	strcpy(newBlock->title, titleName);

	//assignning value of rating 
	newBlock->rate = rate;
	
	newBlock->prev = NULL;
	newBlock->next = NULL;


	if (*head == NULL)
	{
		*head = *tail = newBlock;
		return 1;
	}
	else if ((*head)->rate > rate)// special case
	{
		newBlock->next = *head;
		(*head)->prev = newBlock;
		*head = newBlock;
	}
	else
	{
		beforeElement = *head;		   	// first song in list
		afterElement = (*head)->next;  // second song in list 
		while (afterElement != NULL)
		{
			if (afterElement->rate >= rate)
			{
				break;
			}
			beforeElement = afterElement;
			afterElement = afterElement->next;
		}
		newBlock->prev = beforeElement;
		newBlock->next = afterElement;
		beforeElement->next = newBlock;
		if (afterElement == NULL)
		{
			*tail = newBlock;
		}
		else
		{
			afterElement->prev = newBlock;
		}

	}

	return 1;
}

//==============================================================================================================================/
/*
 * Function: alphaSortedList()
 * Description: This function inserts an element in the correct location in the list, sorted alphabetical order list
 * Parameters:  char *artistName: string containing the artist name
				char *titleName: string containing the title name
 *				songInfo **head, **tail: POINTERS TO head and tail of the list
 * Returns: 0 if the insertion failed, 1 otherwise
*/
int alphaSortedList(char *artistName, char *titleName, int rate, songInfo **head, songInfo **tail)
{
	songInfo *newBlock = NULL;
	songInfo *beforeElement = NULL;
	songInfo *afterElement = NULL;

	//dynamically memory allocatiion of newBlock
	newBlock = (songInfo *)malloc(sizeof(songInfo));

	//checking for memory allocation
	if (newBlock == NULL)
	{
		printf("Error! No more memory!\n");
		return 0;
	}

	//dynamically memory allocatiion of artis00name
	newBlock->artist = (char*)malloc((strlen(artistName) + 1) * sizeof(char));

	//copying data of artistname to newBlock
	strcpy(newBlock->artist, artistName);

	//dynamically memory allocatiion of Title name
	newBlock->title = (char*)malloc((strlen(titleName) + 1) * sizeof(char));

	//copying data of titleName to newBlock
	strcpy(newBlock->title, titleName);

	//assignning value of rating 
	newBlock->rate = rate;

	newBlock->prev = NULL;
	newBlock->next = NULL;

	if (*head == NULL)
	{
		*head = *tail = newBlock;
		return 1;
	}
	else if (strcmp((*head)->artist, artistName) > 0)// special case!
	{
		newBlock->next = *head;
		(*head)->prev = newBlock;
		*head = newBlock;
	}
	else
	{
		beforeElement = *head;		// first item in list
		afterElement = (*head)->next;	// second item in list 
		while (afterElement != NULL)
		{
			if (strcmp((*head)->artist, artistName) >= 0)
			{
				break;
			}
			beforeElement = afterElement;
			afterElement = afterElement->next;
		}
		newBlock->prev = beforeElement;
		newBlock->next = afterElement;
		beforeElement->next = newBlock;
		if (afterElement == NULL)
		{
			*tail = newBlock;
		}
		else
		{
			afterElement->prev = newBlock;
		}

	}

	return 1;
} 

//==============================================================================================================================/
/*
 * Function: findSong()
 * Description: This function will find a node from linkedlist containing same song information 
 * Parameters: 	songInfo *head: POINTERS TO head and tail of the list
				char *artistName: string containing the artist name
				char *titleName: string containing the title name
 * Returns: NULL if the song not found, pointer to node containing song otherwise
*/
songInfo *findSong(songInfo*node, char *artistName, char*titleName)
{
	songInfo *ptr = NULL;
	ptr = node;
	while (ptr->next != NULL)
	{
		if (strcmp(ptr->artist, artistName) == 0 && strcmp(ptr->title, titleName) == 0)
		{
			return ptr;
		}
		ptr = ptr->next;
	}
	if (strcmp(ptr->artist, artistName) == 0 && strcmp(ptr->title, titleName) == 0)
	{
		return ptr;
	}
	return NULL;
}

//==============================================================================================================================/
/*
 * Function: deleteNode()
 * Description: This function will delete the given node and relink pointer and set list.
 * Parameters: 	songNode *node: node to delete
				songNode **head: pointer to head of list
				songNode **tail: pointer to tail of list
 * Returns: Nothing
*/
songInfo*deleteNode(songInfo*node, songInfo**head, songInfo**tail)
{
	songInfo *curr = NULL;
	songInfo *next = NULL;

	curr = *head;

	while (curr != NULL)
	{
		if (strcmp(node->artist, curr->artist) == 0 && strcmp(node->title, curr->title) == 0, node->rate == curr->rate)
		{
			if ((curr == *head) && (curr == *tail))
			{
				*head = *tail = NULL;
				free(curr);
				break;
			}
			if (curr == *head)
			{
				songInfo *secondElement = curr->next;
				*head = curr->next;
				secondElement->prev = NULL;
			}
			else
			{
				if (curr == *tail)
				{
					songInfo *secondlastElement = curr->prev;
					*tail = curr->prev;
					secondlastElement->next = NULL;
				}
				else
				{
					songInfo *precedingElement = curr->prev;
					songInfo *followingElement = curr->next;
					followingElement->prev = precedingElement;
					precedingElement->next = followingElement;
				}
			}
			free(curr);
			break;
		}
		curr = curr->next;
	}
	return 0;
}

//==============================================================================================================================/
/*
 * Function: printSongInfo()
 *
 * Description: This function prints all of the information contained within the array in a nicely-formatted fashion,
				one song per line. 

 * Parameter: songInfo *arr: address pointing to a node

 * Return Value: new value of the head parameter pointing to the start of the linked list
 */
void printSongInfo(songInfo *arr)
{
	songInfo *ptr = arr;
	while (ptr != NULL) // Loop runs till ptr is null.
	{
		printf("\n");
		printf("%-35s%-35s%-10d\n", ptr->artist, ptr->title, ptr->rate); // printing list of song provided by the user
		ptr = ptr->next;
	}	
	printf("\n\n<End of list>\n\n");
}

//==============================================================================================================================/
/*
 * Function:	memoryFree()

 * Description:	This function will relase memory which allocated by the malloc in getSongInfo function.

 * Parameter: songInfo *arr: array of struct

 * Return Value: Nothing as data type is void.
 */
void memoryFree(songInfo *arr)
{
	while (arr != NULL)
	{
		free(arr->artist);
		free(arr->title);
		arr = arr->next;
	}
}
//==============================================================================================================================/
/*
 * Function: updateList()
 * Description: This function will find a node from linkedlist containing same song information and update song rating 
 * Parameters: 	songInfo *head: POINTERS TO head and tail of the list
				char *artistName: string containing the artist name
				char *titleName: string containing the title name
				int rate: cointaining rating for song
 * Returns: NULL if the song not found
*/
songInfo *updateList(songInfo*node, char *artistName, char*titleName,int rate)
{
	songInfo *ptr = NULL;
	ptr = node;
	while (ptr->next != NULL)
	{
		if (strcmp(ptr->artist, artistName) == 0 && strcmp(ptr->title, titleName) == 0)
		{
			ptr->rate =rate;
		}
		ptr = ptr->next;
	}
	if (strcmp(ptr->artist, artistName) == 0 && strcmp(ptr->title, titleName) == 0)
	{
		ptr->rate = rate;
	}
	return NULL;
}