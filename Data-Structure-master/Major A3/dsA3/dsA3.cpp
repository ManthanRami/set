/*
*  FILE          : dsA3.cpp
*  PROJECT       : dsA3
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 08-08-2019
*  DESCRIPTION   : This program will take input from the user for song as name of artist,title and rating,
				   store it into two diffrent map one is sorted by artist name and one by ratingand print it out.
				   Also take one additional input for searching in both map and if the song is find then it will 
				   prompt user for new rating for that song and if user enters new rating which is same as old 
				   rating then it will do nothing and print out both map else it will update map with new rating  song.
*/
#include"dsA3.h"

int main()
{
	multimap<string, SongInfo>artistSorted;
	multimap<int, SongInfo>ratingSorted;
	SongInfo songDetails;

	int rating = 0;
	int printArtistSorted = 1;
	int printRatingSorted = 2;

	getInputForMap(songDetails,artistSorted,ratingSorted);	//Filling the map with song information	
	printMap(artistSorted, ratingSorted, printArtistSorted);	//printing out artist sorted Map
	printMap(artistSorted, ratingSorted, printRatingSorted);	//printing out rating sorted Map
	GetSongUpdateInfo(songDetails);		//Get input from the user for song to search in both map.

	if (findSongArtistSorted(artistSorted, songDetails) != artistSorted.end())	//checking if song is found or not if found then it is going to take a new input for rating and reinsert it in to artist sorted map.
	{
		cout << "Please Enter new Rating for this Song: ";
		cin >> rating;
		reinsertToArtistMap(songDetails, artistSorted, findSongArtistSorted(artistSorted, songDetails),rating);	//re-inserting the song with new rating info
	}
	if (findSongRatingSorted(ratingSorted, songDetails) != ratingSorted.end())	//checking if song is found or not if found then it is going to take a new input for rating and reinsert it in to rating sorted map.
	{
		reinsertToRatingMap(songDetails, ratingSorted, findSongRatingSorted(ratingSorted, songDetails), rating); //re-inserting the song with new rating info
	}

	printMap(artistSorted, ratingSorted, printArtistSorted);	//printing out artist sorted Map
	printMap(artistSorted, ratingSorted, printRatingSorted);	//printing out rating sorted Map
	return 0;
}

/**********************************************************************************************************************
 * Function: findSongArtistSorted()
 * Description: This function will search a song given by the user,in artist sorted map
 * Parameters:  multimap<string, SongInfo>: map1 artist sorted map in which song informations are contained
				SongInfo: songDetails - containing information of song which is needed to be search in the map.
 * Returns:		returns itr1 pointing to the sing if the song is found in map1 else return pointer to the end of map1 
/*********************************************************************************************************************/
multimap<string, SongInfo>::iterator findSongArtistSorted(multimap<string, SongInfo>&map1, SongInfo songDetails)
{
	multimap<string, SongInfo>::iterator itr1;
	itr1 = map1.begin();
	while (itr1 != map1.end())
	{
		if (itr1->second.artistName == songDetails.artistName && itr1->second.rating == songDetails.rating && itr1->second.songName == songDetails.songName)	//checking if the song with proper information found or not
		{
			return itr1;
		}
		itr1++;
	}
	return map1.end();
}

/**********************************************************************************************************************
 * Function: findSongRatingSorted()
 * Description: This function will search a song given by the user,in artist sorted map
 * Parameters:  multimap<int, SongInfo>: map2 artist sorted map in which song informations are contained
				SongInfo: songDetails - containing information of song which is needed to be search in the map.
 * Returns:		returns itr2 pointing to the sing if the song is found in map2 else return pointer to the end of map1
/*********************************************************************************************************************/
multimap<int, SongInfo>::iterator findSongRatingSorted(multimap<int, SongInfo>&map2, SongInfo songDetails)
{
	multimap<int, SongInfo>::iterator itr2;
	itr2 = map2.begin();
	while (itr2 != map2.end())
	{
		if (itr2->second.artistName == songDetails.artistName && itr2->second.rating == songDetails.rating  && itr2->second.songName == songDetails.songName)
		{
			return itr2;
		}
		itr2++;
	}
	return map2.end();
}

/*************************************************************************************************************
 * Function: printMap()
 * Description: This function will print out both the map.
 * Parameters:  multimap<string, SongInfo>map1 - artist sorted map.
				multimap<int, SongInfo>map2 - rating sorted map.
				int whichMap - which map to print out.
 * Returns:		Nothing
/*************************************************************************************************************/
void printMap(multimap<string, SongInfo>map1, multimap<int, SongInfo>map2, int whichMap)
{
	if (whichMap == PRINT_ARTIST_MAP)
	{
		multimap<string, SongInfo>::iterator itr3;
		cout << endl;
		cout << "Song Information(Artist Sorted) : " << endl;
		itr3 = map1.begin();	//assigning itr1 to the begining element of artistsortted map 
		while (itr3 != map1.end())
		{
			cout << setw(35) << itr3->second.artistName << setw(35) << itr3->second.songName << setw(35) << itr3->second.rating << endl;
			itr3++;
		}

	}
	else if (whichMap == PRINT_RATING_MAP)
	{
		multimap<int, SongInfo>::iterator itr4;
		cout << "Song Information(Rating Sorted): " << endl;
		itr4 = map2.begin();	//assigning itr2 to the begining element of ratingsorted map 
		while (itr4 != map2.end())
		{
			cout << setw(35) << itr4->second.artistName << setw(35) << itr4->second.songName << setw(35) << itr4->second.rating << endl;
			itr4++;
		}
	}
}

/*********************************************************************************************************************************
 * Function: getInputForMap()
 * Description: This function will take input from the user for song and store it into both map by diffrent way of sorting.
 * Parameters:  multimap<string, SongInfo>map1 - artist sorted map.
				multimap<int, SongInfo>map2 - rating sorted map.
 * Returns:		Nothing
/*********************************************************************************************************************************/
void getInputForMap(SongInfo song, multimap<string, SongInfo>&map1, multimap<int, SongInfo>&map2)
{
	while (true)
	{
		cout << "Please enter Song Information:" << endl;
		cout << "Artist Name: ";
		cin.getline( song.artistName);
		if (song.artistName == ".")
		{
			break;
		}
		cout << "Song name: ";
		cin >> song.songName;
		cout << "Rating: ";
		cin >> song.rating;
		cout << endl;
		map1.insert(make_pair(song.artistName, song));	//inserting into the artist sorted map
		map2.insert(make_pair(song.rating, song));		//inserted into the rating sorted map
	}
}

/*********************************************************************************************************************************************************
 * Function: reinsertToArtistMap()
 * Description: This function inserts an element in the correct location in the artist sorterd map
 * Parameters:  SongInfo song - struct containing user input for updating song info 
				multimap<string, SongInfo>map1 - artist sorted map.
				multimap<string, SongInfo>::iterator itr5 - pointer pointing to the song which is needed to be deleted and reinsert it with new info 
				int rating - new rating for the song to be changed
 * Returns:		Nothing
/**********************************************************************************************************************************************************/
void reinsertToArtistMap(SongInfo song, multimap<string, SongInfo>&map1, multimap<string, SongInfo>::iterator itr5,int rating)
{
	if (rating >= MIN_RATING_VALUE && rating <= MAX_RATING_VALUE)
	{
		if (rating != itr5->second.rating)
		{
			map1.erase(itr5);
			song.rating = rating;
			map1.insert(make_pair(song.artistName, song));
		}
	}
}

/*************************************************************************************************************
 * Function: reinsertToRatingMap()
 * Description: This function inserts an element in the correct location in the rating sorterd map
 * Parameters:  SongInfo song - struct containing user input for updating song info
				multimap<int, SongInfo>map1 - rating sorted map.
				multimap<int, SongInfo>::iterator itr5 - pointer pointing to the song which is needed to be deleted and reinsert it with new info
				int rating - new rating for the song to be changed
 * Returns:		Nothing
/*************************************************************************************************************/
void reinsertToRatingMap(SongInfo song, multimap<int, SongInfo>&map2, multimap<int, SongInfo>::iterator itr6, int rating)
{
	if (rating >= MIN_RATING_VALUE && rating <= MAX_RATING_VALUE)
	{
		if (rating != itr6->second.rating)
		{
			map2.erase(itr6);
			song.rating = rating;
			map2.insert(make_pair(rating, song));
		}
	}
}

/*************************************************************************************************************
 * Function: GetSongUpdateInfo()
 * Description: This function will take strcut as paramter and fill it by taking input from the user for searching a song into both map
 * Parameters:  SongInfo song - struct containing user input for updating song information
 * Returns:		Nothing
/*************************************************************************************************************/
void GetSongUpdateInfo(SongInfo &song)
{
	cout << "Please enter song to find:" << endl;
	cout << "Artist Name :" << endl;
	cin >> song.artistName;
	cout << "Title Name: " << endl;
	cin >> song.songName;
	cout << "Please enter song rating:" << endl;
	cin >> song.rating;
}