#pragma once
#include<stdio.h>
/*
*  FILE          : dsA3.h
*  PROJECT       : dsA3
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 08-08-2019
*  DESCRIPTION   : This is a header file containing all the inculde file,constant,prototype and structure declartion which is going to use in project dsA3
*/

#include<string>
#include<iostream>
#include<map>
#include<iomanip>
using namespace std;

//constant declaration
#define PRINT_ARTIST_MAP	1	
#define PRINT_RATING_MAP	2	
#define MAX_RATING_VALUE	5	
#define MIN_RATING_VALUE	1

//structure declaration
typedef struct SongInfo
{
	string songName = "";
	string artistName = "";
	int rating = 0;
}SongInfo;

//prototype
void GetSongUpdateInfo(SongInfo &song);
void printMap(multimap<string, SongInfo>map1, multimap<int, SongInfo>map2, int whichMap);
void getInputForMap(SongInfo song, multimap<string, SongInfo>&map1, multimap<int, SongInfo>&map2);
multimap<string, SongInfo>::iterator findSongArtistSorted(multimap<string, SongInfo>&map1, SongInfo songDetails);
multimap<int, SongInfo>::iterator findSongRatingSorted(multimap<int, SongInfo>&map2, SongInfo songDetails);
void reinsertToArtistMap(SongInfo song, multimap<string, SongInfo>&map1, multimap<string, SongInfo>::iterator itr5, int rating);
void reinsertToRatingMap(SongInfo song, multimap<int, SongInfo>&map1, multimap<int, SongInfo>::iterator itr6, int rating);

