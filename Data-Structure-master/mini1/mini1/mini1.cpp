/*
*  Projectname:	mini1
 * Filename:	mini1.cpp
 * Author:		Manthan Rami
 * Date:		05/16/2019
 * Description:	This program that takes in information about songs and stores the artist and title
				in an array of structs to be displayed. It will take 10 pair of c style string of song
				from user and print it out in proper right & left justify format.
 */

#include <stdio.h>
#include <stdlib.h>
#include<string.h>

 //warning disable 
#pragma warning(disable : 4996)

//constant declaration 
#define ERROR 1
#define SUCCESS 0


//structure defination
struct songInfo
{
	char *artist = NULL;
	char *title = NULL;
};

//prototypes
//===========================================================================================
int getSongInfo(songInfo *arr, char artist[], char title[]);
void printSongInfo(songInfo *arr);
void memoryFree(songInfo *arr);
void terminaTor(char *input);
void dataCopy(char *destination, char *source);
int memoryChecking(char *arr);
//============================================================================================

int main(void)
{
	int redcode = 1;
	int i = 0;
	char artist[30] = { 0 };
	char title[30] = { 0 };
	songInfo myArray[10];

	//for (i = 0; i < 10; i++)
	do
	{
		printf("\nEnter Song %d Info :\n", i + 1);
		printf("Artist name : ");

		fgets(artist, 100, stdin);
		printf("Title name : ");
		fgets(title, 100, stdin);

		terminaTor(artist);  //replacing charater '\n' to '\0' using terminaTor function.
		terminaTor(title); //replacing charater '\n' to '\0' using terminaTor function.

		redcode = getSongInfo(&myArray[i], artist, title); //filling fileds of struct with appropriate memory using getSongInfo. 
		if (redcode == ERROR)
		{
			return ERROR;
		}
		i++;
	} while (i < 10);
	printf("\nSong Info:\n\n");

	//printing Song info
	printSongInfo(myArray); // printing containts of myArray using printDongInfo function.

	//memory free
	memoryFree(myArray); //release meomry of myArray using memoryFree function.
	return SUCCESS;
}

// ===================================================================================================================
/*
 * Function: getSongInfo()

 * Description:	This function will allocate memory to
				artist & title string in struct. Also
				fills the field of structs

 * Parameter:	songInfo *arr:	 a pointer to an element of the array.
				char artist[]:	string containing information of artist information
				char title[]:	string containing information of title information

 * Return Value: returns 0 if any Error occurs
 */

int getSongInfo(songInfo *arr, char artist[], char title[])
{
	int signal = 0;
	//memory allocation for artitst
	arr->artist = (char*)malloc((strlen(artist) + 1) * sizeof(char));
	signal = memoryChecking(arr->artist);
	if (signal == ERROR)
	{
		return ERROR;
	}

	//memory allocation for title 
	arr->title = (char*)malloc((strlen(title) + 1) * sizeof(char));
	signal = memoryChecking(arr->title);
	if (signal == ERROR)
	{
		return ERROR;
	}
	//Copy data to struct
	dataCopy((arr)->artist, artist); //copying data using dataCopy function.
	dataCopy((arr)->title, title);	//copying data using dataCopy function.
	return SUCCESS;
}

//==============================================================================================================================
/*
 * Function: printSongInfo()
 *
 * Description: This function prints all of the information contained within the array in a nicely-formatted fashion,
				one song per line. 

 * Parameter: songInfo *arr: array of structs

 * Return Value: new value of the head parameter pointing to the start of the linked list
 */
void printSongInfo(songInfo *arr)
{
	int i = 0;
	do
	{
		printf("%-35s%-35s\n", arr[i].artist, arr[i].title);
		i++;
	} while (i < 10);
}

//==============================================================================================================================
/*
 * Function:	memoryFree()

 * Description:	This function will relase memory which allocated by the malloc in getSongInfo function.

 * Parameter: songInfo *arr: array of struct

 * Return Value: Nothing as data type is void.
 */
void memoryFree(songInfo *arr)
{
	int i = 0;
	do
	{
		free(arr[i].artist);
		free(arr[i].title);
		i++;
	} while (i < 10);
}
//===============================================================================================================================
/*
 * Function:	terminaTor:
 *
 * Description:	This function will search character '\n' in string and repalce it with '\0'.
 *
 * Parameter: char *input
 * Return Value: Nothing as data type is void
 */
void terminaTor(char *input)
{
	(*(strchr(input, '\n')) = '\0');
}

//===============================================================================================================================
/*
 * Function:
 *
 * Description:	This function will copy all data from destination to source.
 * Parameter:	char *destination: array containing information
				char *source: where is to be copied.
 * Return Value: Nothing as data type is void
 */
void dataCopy(char *destination, char *source)
{
	strcpy(destination, source);
}

//================================================================================================================================
/*
 * Function:	memoryChecking
 *
 * Description:	This function will check weather memory allocated to array is appropriate or having some issue with it.
 *
 * Parameter:	char *arr: array to be checked for.
 * Return Value: return value 1 if there is any error in memory allocation.
 */
int memoryChecking(char *arr)
{
	if (arr == NULL)
	{
		printf("Error in allocating memory");
		return ERROR;
	}
	return SUCCESS;
}
