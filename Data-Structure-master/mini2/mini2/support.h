/*

* Description: This file contains the prototypes, constant and include files for the mini2 project.
			   These are all of the prototypes for the various functions found within the source files.
*/
#pragma once

#include <stdio.h>
#include <stdlib.h>
#include<string.h>

//warning disable 
#pragma warning(disable : 4996)

//constant declaration 
#define ERROR 1
#define SUCCESS 0


//structure defination
struct songInfo
{
	char *artist = NULL;
	char *title = NULL;
	songInfo *link;
};

//prototypes
songInfo*createLinkedLisr(songInfo*head);
int getSongInfo(songInfo *arr, char artist[], char title[]);
int memoryChecking(char *arr);

void printSongInfo(songInfo *arr);
void terminaTor(char *input);
void dataCopy(char *destination, char *source);
void memoryFree(songInfo *arr);

