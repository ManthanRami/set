/*
*  Projectname:	mini2
 * Filename:	mini2.cpp
 * Author:		Manthan Rami
 * Date:		05/30/2019
 * Description:	This program that takes in information about songs and stores the artist and title
				in an array of structs to be displayed. It will take unlimited pair of c style string of song
				from user and print it out in proper right & left justify format.
 */
#include"support.h"

int main()
{
	songInfo *head = NULL;
	char artiist[] = "Artist Name";
	char title[] = "Title Name";
	head = createLinkedLisr(head);
	printf("\n\nSong Infomation:\n\n");
	printf("%-35s%-35s\n", artiist, title);
	printSongInfo(head);
	memoryFree(head);
}

// ===================================================================================================================
/*
 * Function: getSongInfo()

 * Description:	This function will allocate memory to
				artist & title string in struct. Also
				fills the field of structs

 * Parameter:	songInfo *arr:	 address pointing to a node
				char artist[]:	string containing information of artist information
				char title[]:	string containing information of title information

 * Return Value: returns 0 if any Error occurs
 */
int getSongInfo(songInfo *arr, char artist[], char title[])
{
	int signal = 0;
	//memory allocation for artitst
	arr->artist = (char*)malloc((strlen(artist) + 1) * sizeof(char));
	signal = memoryChecking(arr->artist);
	if (signal == ERROR)
	{
		return ERROR;
	}

	//memory allocation for title 
	arr->title = (char*)malloc((strlen(title) + 1) * sizeof(char));
	signal = memoryChecking(arr->title);
	if (signal == ERROR)
	{
		return ERROR;
	}
	//Copy data to struct
	dataCopy((arr)->artist, artist); //copying data using dataCopy function.
	dataCopy((arr)->title, title);	//copying data using dataCopy function.
	return SUCCESS;
}

//===============================================================================================================================
/*
 * Function:	terminaTor:
 *
 * Description:	This function will search character '\n' in string and repalce it with '\0'.
 *
 * Parameter: char *input
 * Return Value: Nothing as data type is void
 */
void terminaTor(char *input)
{
	(*(strchr(input, '\n')) = '\0');
}

//===============================================================================================================================
/*
 * Function:
 *
 * Description:	This function will copy all data from destination to source.
 * Parameter:	char *destination: array containing information
				char *source: where is to be copied.
 * Return Value: Nothing as data type is void
 */
void dataCopy(char *destination, char *source)
{
	strcpy(destination, source);
}

//================================================================================================================================
/*
 * Function:	memoryChecking
 *
 * Description:	This function will check weather memory allocated to array is appropriate or having some issue with it.
 *
 * Parameter:	char *arr: array to be checked for.
 * Return Value: return value 1 if there is any error in memory allocation.
 */
int memoryChecking(char *arr)
{
	if (arr == NULL)
	{
		printf("Error in allocating memory");
		return ERROR;
	}
	return SUCCESS;
}
