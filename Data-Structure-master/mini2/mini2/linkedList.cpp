
#include"support.h"
/*
 * Function: phoneInfo * createNewNode(phoneInfo *newHead)
 *
 * Description:	This function will allocate a new entry in the
 * 		linked list, allow data entry, and append this
 * 		entry to the end of the chain.
 *
 * Parameter: phoneInfo *head: start of the linked list
 * Return Value: new value of the head parameter pointing to the start of the linked list
 */
songInfo*createLinkedLisr(songInfo*head)
{
	int i = 0;
	while (1)
	{
		songInfo * temp = NULL;
		songInfo * p = NULL;
		int redcode = 1;
		char artist[30] = { 0 };
		char title[30] = { 0 };

		temp = (songInfo*)malloc(sizeof(songInfo)); // dyanmicakly allocating memory 
		printf("\nEnter Song %d Info :\n", i + 1);
		printf("Artist name : ");
		fgets(artist, 100, stdin);
		if (strchr(artist, '.') != NULL) //searching foir preidos for exiing loop
		{
			break;
		}
		printf("Title name : ");
		fgets(title, 100, stdin);

		terminaTor(artist);  //replacing charater '\n' to '\0' using terminaTor function.
		terminaTor(title); //replacing charater '\n' to '\0' using terminaTor function.

		redcode = getSongInfo(temp, artist, title); //filling fileds of struct with appropriate memory using getSongInfo. 
		if (redcode == ERROR)
		{
			printf("\n");
			break;
		}
		i++;
		temp->link = NULL;

		if (head == NULL) //if list is empty then, make temp as first node
		{
			head = temp;
		}
		else
		{
			p = head;
			while (p->link != 0)
			{
				p = p->link;
			}

			p->link = temp;
		}

	}
	return head;
}


//==============================================================================================================================
/*
 * Function:	memoryFree()

 * Description:	This function will relase memory which allocated by the malloc in getSongInfo function.

 * Parameter: songInfo *arr: array of struct

 * Return Value: Nothing as data type is void.
 */
void memoryFree(songInfo *arr)
{
	while (arr != NULL)
	{
		free(arr->artist);
		free(arr->title);
		arr = arr->link;
	}	
}


//==============================================================================================================================
/*
 * Function: printSongInfo()
 *
 * Description: This function prints all of the information contained within the array in a nicely-formatted fashion,
				one song per line. 

 * Parameter: songInfo *arr: address pointing to a node

 * Return Value: new value of the head parameter pointing to the start of the linked list
 */
void printSongInfo(songInfo *arr)
{
	songInfo *ptr = arr;
	while (ptr != NULL) // Loop runs till ptr is null.
	{
		printf("\n");
		printf("%-35s%-35s\n", ptr->artist, ptr->title); // printing list of song provided by the user
		ptr = ptr->link;
	}	/* end while */

	printf("\n\n<End of list>\n");
}