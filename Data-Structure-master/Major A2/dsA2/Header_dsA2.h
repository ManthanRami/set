/*
*  FILE          : Header_dsA2.h
*  PROJECT       : dsA2
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 07-25-2019
*  DESCRIPTION   : This file contains declaration of all prototype, constant declaration and structure  which is used in project dsA2.
*/
#include <iostream>
#include <stdio.h>
#include <string>
#include <fstream>
#include <stdlib.h>
#include <string.h>

#pragma warning(disable : 4996)
using namespace std;

//constant declaration
#define MAX_BUCKETS			127
#define CHECKING_CONDITION	  0	

//struct declaration
typedef struct WordNode
{
	char*	  words;
	WordNode* next;
}WordNode;

//prototype
unsigned int djb2(string str);
int hashFunction(string str);
void freeAll(WordNode *head);
WordNode* alphaSortedList(WordNode *head, string buff);
WordNode *searchLinktedList(WordNode *head, char* searchWord, int* comparisons);
void searchForWordTwice(char* searchWord, WordNode* linkedList, WordNode* hashTable[], int count[2]);