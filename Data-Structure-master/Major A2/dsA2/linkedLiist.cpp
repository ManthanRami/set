/*
*  FILE          : linkedLsit.h
*  PROJECT       : dsA2
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 07-25-2019
*  DESCRIPTION   : This file contains defination of alphaSortedList, searchLinktedList and searchForWordTwice which is used in project dsA2.
*/
#include"Header_dsA2.h"
//==============================================================================================================================/
/*
 * Function: alphaSortedList()
 * Description: This function inserts an element in the correct location in the list, sorted alphabetical order list
 * Parameters:  WordNode *head: Pointer to the head of the list.
				string buff	  :	string containing input given by the user.
 * Returns:		head		  : new value of the head parameter pointing to the start of the linked list
*/
WordNode* alphaSortedList(WordNode *head,string buff)
{
	char		buffer[100] = "";
	WordNode*	newBlock = NULL;
	WordNode*	temp1 = NULL;
	WordNode*	temp2 = NULL;
	WordNode *	ptr = NULL;

	//dynamically memory allocatiion of newBlock
	newBlock = (WordNode *)malloc(sizeof(WordNode));
	//checking for memory allocation
	if (newBlock == NULL)
	{
		printf("Error! No more memory!\n");
		return 0;
	}
	newBlock->words = (char*)malloc(sizeof(buff));
	strcpy(newBlock->words, buff.c_str());
	newBlock->next = NULL;
	if (head == NULL)
	{
		head = newBlock;
	}
	else
	{
		// non empty chain, so use newHead ptr to follow links until we reach end of
		// list, where the next field is NULL

		ptr = head;
		while (ptr->next != NULL)
		{
			ptr = ptr->next;
		}	/* end while */

		// we are at end since ptr->next is NULL

		// append the new record into ptr->next
		ptr->next = newBlock;
	}	/* endif */
	temp1 = head;
	temp2 = head;
	while (temp1 != NULL)
	{
		temp2 = temp1->next;
		while (temp2 != NULL)
		{
			if (strcmp(temp1->words, temp2->words) > CHECKING_CONDITION)
			{
				strcpy(buffer, temp1->words);
				strcpy(temp1->words, temp2->words);
				strcpy(temp2->words, buffer);

			}
			temp2 = temp2->next;
		}
		temp1 = temp1->next;
	}
	// at this point, the new record linked!
	return head;
}
//==============================================================================================================================/
/*
 * Function: searchLinktedList()
 * Description: This function will search for a word (given by user) in the linedlist.
 * Parameters:  char * searchWord: word to search for (entered by the user)
				struct linkedList * linkedList: linked list to search (in your
				program, you can call the linked list node struct anything that makes sense)
				int * comparisonCount: pointer to int filled in by the function with the count 
				of strcmp comparisonCount done in searching the linked list 
 * Returns:		a pointer to the node containing the word (if found) or NULL.
*/
WordNode* searchLinktedList(WordNode *head, char* searchWord, int* comparisonCount)
{
	int i = 0;
	WordNode *ptr = NULL;
	ptr = head; 
	if (ptr == NULL)
	{
		return 0;
	}
	while (ptr->next != NULL)
	{
		i++;
		if (strcmp(ptr->words, searchWord) == CHECKING_CONDITION)
		{
			*comparisonCount = i;
			return ptr;
		}
		if (strcmp(ptr->words, searchWord) > CHECKING_CONDITION)	//exiting 
		{
			break;
		}
		ptr = ptr->next;
	}
	if (strcmp(ptr->words, searchWord) == CHECKING_CONDITION)
	{
		i++;
		*comparisonCount = i;
		return ptr;
	}
	i++;
	*comparisonCount = i;
	return NULL;
}
//==============================================================================================================================/
/*
 * Function: searchLinktedList()
 * Description: This function will search for a word (given by user) in the linedlist and hash table both.
 * Parameters:  char * searchWord: word to search for (entered by the user)
				WordNode * linkedList: linked list to search
				WordNode * hashTable[]: hash table to search
				int comparisonCount[2]: array containing the count of strcmp
				comparisons done in searching the extremely-long sorted linked
				list (element 0) and in searching the hash table (element 1)
 * Display:		print out if the word is there in linkedlis and hash table and
			    how many comparison done to find the word in both data structure 
 * Returns:		Nothing
*/
void searchForWordTwice(char* searchWord, WordNode* linkedList, WordNode* hashTable[], int comparisonCount[2])
{
	WordNode*	retcode = NULL;
	int			hashValue = 0;
	retcode = searchLinktedList(linkedList, searchWord, &comparisonCount[0]);
	if (retcode == NULL)
	{
		cout <<"\t" <<searchWord << " was NOT found in linked list in " << comparisonCount[0] << " comparisons." << endl;
	}
	else
	{
		cout << "\t" << searchWord << " was found in linked list in " << comparisonCount[0] << " comparisons." << endl;
	}
	hashValue = hashFunction(searchWord);
	retcode = searchLinktedList(hashTable[hashValue], searchWord, &comparisonCount[1]);
	if (retcode == NULL)
	{
		cout << "\t" << searchWord << " was NOT found in hash table bucket in " << comparisonCount[1] << " comparisons." << endl;
	}
	else
	{
		cout << "\t" << searchWord << " was found in hash table bucket in " << comparisonCount[1] << " comparisons." << endl;
	}
}

/*
 * Function: freeAll()
 * Description: This function removes all items from the linked list.
 * Parameters: personInfo *head: start of linked list
 * Returns: nothing
*/
void freeAll(WordNode *head)
{
	WordNode *curr = NULL;
	WordNode *next = NULL;

	curr = head;

	// traverse the list, being careful to not access freed blocks
	while (curr != NULL)
	{
		// keep a pointer to the next block so we can go there after it's freed
		next = curr->next;
		free(curr);
		curr = next;
	}

}
