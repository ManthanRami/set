/*
*  FILE          : dsA2.cpp
*  PROJECT       : dsA2
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 07-25-2019
*  DESCRIPTION   : This program will load 2000 words from a text file present in source folder and
				   load it into a long sorted linked list and in hash table and take user input to 
				   search in noth of them and display how many comparison has benn done in orfer to
				   search in oinkedlist and hash table. Thus we can know which data structure is more 
				   efficient.
*/
#include"Header_dsA2.h"
int main()
{
	int			hashValue = 0;
	int			count[2] = {NULL};
	int			counter = 0;
	int			counter_H = 0;
	int			counter_L = 0;
	WordNode*	longListHead = NULL;
	WordNode*	buckets[MAX_BUCKETS] = { NULL };
	ifstream	pFile("words.txt");
	string		word = "";
	char		input[100] = "";
	if (pFile.is_open())		//check if the file is open or not
	{
		while (pFile >> word)	//take word fromt he input file
		{
			longListHead = alphaSortedList(longListHead, word);		//store in a long sorted list
			hashValue = hashFunction(word);							//get hashValue from the hash function
			buckets[hashValue] = alphaSortedList(buckets[hashValue], word);		//store word into hash table
		}
		while (true)
		{
			counter++;
			cin >> input;
			if (strcmp(input, ".") == 0)	//check exiting loop condition
			{
				counter--;
				break;
			}
			searchForWordTwice(input, longListHead, buckets, count);	//searching in the linked list and hash table.
			counter_H = counter_H + count[1];		//create total number of sum of search done in hash table
			counter_L = counter_L + count[0];		//create total number of sum of search done in Linked list
		}
		cout << "\tTotal Number Of Searches: " << counter << endl;
		cout << "\tTotal Number Of Comparisons in Linked List: " << counter_L << endl;
		cout << "\tTotal Number Of Comparisons in hash table bucket: " << counter_H << endl;

	}
	pFile.close();			//closing the file
	if (pFile.is_open()
		
		)	//Checking if file is open or not ?
	{
		cout << "Unable to close" << endl;
	}
	freeAll(longListHead);	//freeoing the long sorted linked list
	for (int i = 0; i < MAX_BUCKETS; i++) //frreing the hash table
	{
		freeAll(buckets[i]);
	}
	return 0;
}
