/*
*  FILE          : hashing.cpp
*  PROJECT       : dsA2
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 07-25-2019
*  DESCRIPTION   : This file contains defination of djb2 and hashFunction which is used in project dsA2.
*/
#include"Header_dsA2.h"
/*==============================================================================================================================
 * Function: djb2()
 * Description: This function will do some calculation on basis of which kind of data is int double, char,etc...
 * Parameters:  char *str: string containing the word entered by user.
 * Returns: unsigned hash	 result of the calculation
 * Refrence : Taken from the lecture of data strucutre https://conestoga.desire2learn.com/d2l/le/content/269147/viewContent/5416457/View  
			  and it is made by Dan Bernstein
*/
unsigned int djb2(string str)
{
	unsigned int hash = 5381;
	int c = 0;
	int i = 0;
	while ((c = str[i]) != '\0')
	{
		hash = ((hash << 5) + hash) + c;
		i++;
	}
	return hash;
}
/*==============================================================================================================================
 * Function: hashFunction()
 * Description: This function inserts an element in the correct location in the list, sorted alphabetical order list
 * Parameters:  char *str: string containing the word entered by user.
 * Returns: int value between 0 to 10
*/
int hashFunction(string str)
{
	int	value = 0;
	unsigned int	 temp = 0;
	temp = djb2(str.c_str());
	value = temp % MAX_BUCKETS;
	return value;
}