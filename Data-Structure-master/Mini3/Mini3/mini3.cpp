/*
*  FILE          : mini3.cpp
*  PROJECT       : Mini3
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 07-10-2019
*  DESCRIPTION   : This program will take a word as a input and do some mathematically calculation and
				   stores it into a hash table at appropriate index. Also it will take additional input
				   for searching a word in the hash table display SUCCESS! if word found else NOT there!
*/

#include"mini3.h"
int main()
{
	WordNode*	buckets[MAX_BUCKET] = { NULL };
	int			retCode = 0;
	int			hashValue = 0;
	char		buff[MAX_SIZE] = { NULL };

	do
	{
		cout << "\nPlease Enter a word:  ";
		cin >> buff;							//getting user input
		if (strcmp(buff, ".") == 0)
		{
			break;
		}
		hashValue = hashFunction(buff);			// getting hash value
		buckets[hashValue] = alphaSortedList(buckets[hashValue], buff);
	} while (true);

	cout << "\n";
	do
	{

		cout << "Please Enter a word to search: ";
		cin >> buff;									//getting input for searching
		if (strcmp(buff, ".") == 0)
		{
			break;
		}
		hashValue = hashFunction(buff);
		retCode = searchWord(buckets[hashValue], buff); //searching word in hash table
		if (retCode == SUCCESS)
		{
			cout << "Success!\n" << endl;
		}
		else if (retCode == FAIL)
		{
			cout << "Not there!\n" << endl;
		}
	} while (true);

	return 0;
}