/*
*  FILE          : mini3.h
*  PROJECT       : Mini3
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 07-10-2019
*  DESCRIPTION   :
*	This file contains all the prototypes, structure defination and constant declaration which is used in project Mini3.
*/
#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<string>
#include<iostream>

#pragma warning(disable : 4996)
using namespace std;

//Constant declaration
#define MAX_SIZE 21
#define MAX_BUCKET 11

#define SUCCESS 1
#define FAIL 0

typedef struct Wordnode
{
	char words[MAX_SIZE] = { NULL };
	Wordnode *next = NULL;
}WordNode;

//prototypes
int hashFunction(char*str);
unsigned int djb2(char*str);
int searchWord(WordNode *head, char* buff);
Wordnode* alphaSortedList(WordNode *head, char* buff);
