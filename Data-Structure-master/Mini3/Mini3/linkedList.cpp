/*
*  FILE          : linkedLsit.h
*  PROJECT       : Mini3
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 07-10-2019
*  DESCRIPTION   :
*	This file contains defination of alphaSortedList and searchWord which is used in project Mini3.
*/
#include"mini3.h"
//==============================================================================================================================/
/*
 * Function: alphaSortedList()
 * Description: This function inserts an element in the correct location in the list, sorted alphabetical order list
 * Parameters:  WordNode *head: Pointer to the head of the list.
				char* buff	  :	string containing input given by the user.
 * Returns:		head		  : new value of the head parameter pointing to the start of the linked list
*/
Wordnode* alphaSortedList(WordNode *head, char* buff)
{
	char		buffer[MAX_SIZE] = "";
	WordNode*	newBlock = NULL;
	Wordnode*	temp1 = NULL;
	Wordnode*	temp2 = NULL;
	WordNode *	ptr = NULL;

	//dynamically memory allocatiion of newBlock
	newBlock = (WordNode *)malloc(sizeof(WordNode));
	//checking for memory allocation
	if (newBlock == NULL)
	{
		printf("Error! No more memory!\n");
		return 0;
	}
	strcpy(newBlock->words, buff);
	newBlock->next = NULL;
	if (head == NULL)
	{
		head = newBlock;
	}
	else
	{
		// non empty chain, so use newHead ptr to follow links until we reach end of
		// list, where the next field is NULL

		ptr = head;
		while (ptr->next != NULL)
		{
			ptr = ptr->next;
		}	/* end while */

		// we are at end since ptr->next is NULL

		// append the new record into ptr->next
		ptr->next = newBlock;
	}	/* endif */
	temp1 = head;
	temp2 = head;
	while (temp1 != NULL)
	{
		temp2 = temp1->next;
		while (temp2 != NULL)
		{
			if (strcmp(temp1->words, temp2->words) > 0)
			{
				strcpy(buffer, temp1->words);
				strcpy(temp1->words, temp2->words);
				strcpy(temp2->words, buffer);

			}
			temp2 = temp2->next;
		}
		temp1 = temp1->next;
	}
	// at this point, the new record linked!
	return head;
}

//==============================================================================================================================/
/*
 * Function: searchWord()
 * Description: This function will search for a word (given by user) in the linedlist.
 * Parameters:  WordNode *head: Pointer to the head of the list.
				char* buff	  :	string containing input given by the user.
 * Returns:		1 OR 0		  : 1 if word is found in linked list else 0.
*/
int searchWord(WordNode *head, char* buff)
{
	WordNode *ptr = NULL;
	ptr = head;
	if (ptr == NULL)
	{
		return FAIL;
	}
	while (ptr->next != NULL)
	{
		if (strcmp(ptr->words, buff) == 0)
		{
			return SUCCESS;
		}
		ptr = ptr->next;
	}
	if (strcmp(ptr->words, buff) == 0)
	{
		return SUCCESS;
	}
	return FAIL;
}