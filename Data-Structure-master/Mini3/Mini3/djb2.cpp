/*
*  FILE          : djb2.h
*  PROJECT       : Mini3
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 07-10-2019
*  DESCRIPTION   :
*	This file contains defination of djb2 and hashFunction which is used in project Mini3.
*/
#include"mini3.h"
/*==============================================================================================================================
 * Function: djb2()
 * Description: This function will do some calculation on basis of which kind of data is int double, char,etc...
 * Parameters:  char *str: string containing the word entered by user.
 * Returns: unsigned hash	 result of the calculation
*/
unsigned int djb2(char*str)
{
	unsigned int hash = 5381;
	int c = 0;
	while ((c = *str++) != '\0')
	{
		hash = ((hash << 5) + hash) + c;
	}
	return hash;
}
/*==============================================================================================================================
 * Function: hashFunction()
 * Description: This function inserts an element in the correct location in the list, sorted alphabetical order list
 * Parameters:  char *str: string containing the word entered by user.
 * Returns: int value between 0 to 10  
*/
int hashFunction(char* str)
{
	int	value = 0;
	unsigned int	 temp = 0;
	temp = djb2(str);
	value = temp % MAX_BUCKET;
	return value;
}