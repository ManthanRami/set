/*
*  FILE          : mini4.cpp
*  PROJECT       : mini4
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 07-18-2019
*  DESCRIPTION   : This program will prompt user for input a word and store in vector utill user enter ".".
				   Also prompt user for inout a word to search in vector and display whether it is there in vector or not there.
*/
#include <iostream>
#include <vector>
#include <string>

using namespace std;
#define EXIT_LOOP -1

int main(void)
{
	vector<string> myVector; //vector declaration
	string buffer = "";
	vector<string>::iterator iter; //iterator declaration
	int j = 0;
	 
	while(true)
	{
		cout << "Please Enter any word: ";
		cin >> buffer;						//getting user unput
		if (buffer == ".")					//checking for exit 
		{
			break;
		}
		myVector.push_back(buffer);		//pushing word in to vectoe from back
	}
	while(true)
	{
		cout << "Please Enter any word: ";
		cin >> buffer;						//getting user unput
		if (buffer == ".")				//checking for exit 
		{
			break;
		}
		for (iter = myVector.begin(); iter != myVector.end(); iter++) // traversing to myVector using iter
		{
			if (*iter == buffer)			//checking if the word matches any word in the myVector
			{
				break;
			}
		}
		if (j == EXIT_LOOP)	//condition for printing
		{
			cout << "Success!\n";
		}
		else
		{
			cout << "Not there!\n";
		}
	}
	return 0;
}