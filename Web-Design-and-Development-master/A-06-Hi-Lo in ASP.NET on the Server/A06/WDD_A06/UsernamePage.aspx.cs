﻿/*
* FILE           : UsernamePage.aspx.cs
* PROGRAMMERS    : Conor Barr, Manthan Rami
* FIRST VERSIPON : 2019-11-06
* DESCRIPTION    : 
*	This file contains the code-behind for 'UsernamePage.aspx'.
*	The purpose of the page is to get the user's name and check
*	whether it's valid. If the user's name is valid, requests are 
*	redirected to 'NumberPage.aspx'. If it's invalid, an error message
*	is displayed. A valid username can only contain letters (i.e. no letters,
*	symbols, or spaces).
*/

using System;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;

namespace WDD_A06
{
    public partial class UsernamePage : System.Web.UI.Page
    {
        /*
        * NAME	  : Page_Load() 
        * PURPOSE : 
        *   This is loads UsernamePage.aspx. Nothing special is done when 
        *   this page is loaded.
        * INPUTS  : 
        *   object sender: The object that triggered the event
        *   EventArgs e: The arguments that were passed to the event
        * OUTPUTS : 
        *	none
        * RETURNS : 
        *   none
        */
        protected void Page_Load(object sender, EventArgs e)
        {
            // Nothing special is done when the page is loaded
        }


        /*
        * NAME	  : submitBtn_Click() 
        * PURPOSE : 
        *   This is the event handler for the Submit button
        *   on the HiLo_Form.aspx page.
        * INPUTS  : 
        *   object sender: The object that triggered the event
        *   EventArgs e: The arguments that were passed to the event
        * OUTPUTS : 
        *   An error message is displayed if the user's name is invalid.
        *   A valid name only contains letters.
        * RETURNS : 
        *   void
        */
        protected void submitBtn_Click(object sender, EventArgs e)
        {
            Regex regexExpression = new Regex("^[a-zA-Z]+$");
            Match match = regexExpression.Match(nameBox.Text);

            if (match.Success == true)
            {
                Session["UserName"] = nameBox.Text;
                Response.Redirect("NumberPage.aspx");
            }
            else if (match.Success == false)
            {
                // Display error message and clear the text box
                errorMsg.Text = "Name can only contain letters!";

                ClearTextBox(nameBox);
            }            
        }


        /*
        * NAME	  : ClearTextBox()
        * PURPOSE : 
        *   This method clears a text box (i.e. it makes the
        *   text box blank).
        * INPUTS  : 
        *   TextBox box: The Textbox that's being cleared
        * OUTPUTS : 
        *   none
        * RETURNS : 
        *   none
        */
        private void ClearTextBox(TextBox box)
        {
            box.Text = "";
        }
    }
}