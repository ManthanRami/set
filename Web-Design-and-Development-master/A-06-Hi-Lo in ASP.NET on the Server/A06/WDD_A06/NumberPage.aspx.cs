﻿/*
* FILE           : NumberPage.aspx.cs
* PROGRAMMERS    : Conor Barr, Manthan Rami
* FIRST VERSIPON : 2019-11-06
* DESCRIPTION    : 
*	This file contains the code-behind for 'NumberPage.aspx'.
*	The purpose of the page is to get the maximum guessing range
*	from the user and check whether it's valid. If the range is valid, 
*	requests are redirected to 'GameLogic.aspx.'. If it's invalid, an
*	informative error message is displayed. A valid maximum guessing
*	range is any number greater than 1.
*/

using System;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;

namespace WDD_A06
{
    public partial class NumberPage : System.Web.UI.Page
    {
        const int kMinimumRange = 1;

        /*
        * NAME	  : Page_Load()
        * PURPOSE : 
        *   This event handler is called when the page loads initially.
        *   The userName session variable is loaded and then used to
        *   greet the user by name.
        * INPUTS  : 
        *   object sender: The object that triggered the event
        *   EventArgs e: The arguments that were passed to the event
        * OUTPUTS : 
        *   A greeting to the user is displayed.
        * RETURNS : 
        *   void
        */
        protected void Page_Load(object sender, EventArgs e)
        {
            string name = Session["userName"].ToString();
            saluteUser.Text = "Hello " + name;
        }


        /*
        * NAME	  : submitBtn_Click()
        * PURPOSE : 
        *   This is the event handler for 'numSubmitBtn'. It checks whether
        *   the maxRange is a number and within range (it gets the input from
        *   'numberBox'. If input is invalid, different error messages are displayed
        *   based on which error occured. If the input is a valid 
        * INPUTS  : 
        *   object sender: The object that triggered the event
        *   EventArgs e: The arguments that were passed to the event
        * OUTPUTS : 
        *   Error messages are displayed if the maxRange is not a number, or out-of-range.
        * RETURNS : 
        *   none
        */
        protected void submitBtn_Click(object sender, EventArgs e)
        {
            Regex regexExpression = new Regex("^[0-9][0-9]*[0-9]*$");
            Match match = regexExpression.Match(numberBox.Text);

            // Check if the input is a number
            if (match.Success == true)
            {
                int maxNum = Int32.Parse(numberBox.Text);
                if (maxNum > kMinimumRange)
                {
                    Session["firstGuess"] = "true";

                    // Set the minRange and maxRange and then go to the gameLogic page
                    Session["minRange"] = 1;
                    Session["maxRange"] = maxNum;
                    Response.Redirect("gameLogic.aspx");
                }
                else
                {
                    displayErrMessage(numberBox, errorMsg, "Your maximum guess must be greater than 1!");
                }
            }
            else if (match.Success == false)
            {
                displayErrMessage(numberBox, errorMsg, "Field must only contain numbers!");
            }
        }


        /*
        * NAME	  : displayErrMessage()
        * PURPOSE : 
        *   This method clears a textbox and displays an
        *   error message.
        * INPUTS  : 
        *   TextBox inputBox: The Textbox that will be cleared
        *   Label errMsgLabel: The Label where 'string message' will be displayed
        *   string message: The error message that's being displayed
        * OUTPUTS : 
        *   An error message is displayed inside 'Label errMsgLabel'.
        * RETURNS : 
        *   void
        */
        private void displayErrMessage(TextBox inputBox, Label errMsgLabel, string message)
        {
            inputBox.Text = "";
            errMsgLabel.Text = message;
        }
    }
}