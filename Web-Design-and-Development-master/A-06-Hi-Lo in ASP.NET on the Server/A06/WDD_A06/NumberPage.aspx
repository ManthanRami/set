﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NumberPage.aspx.cs" Inherits="WDD_A06.NumberPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" type="text/css" href="CSS/style.css"/>
    <title></title>
</head>
<body style="background-image:url('Hi-lo.jpg')" >
 <form id="numberForm" runat="server">
        <div class="banner"></div>
        <div class="center">
            <div class="userSalute">
                <asp:Label ID="saluteUser" runat="server" class="salute"></asp:Label>
            </div>        
        <table class="table">          

            <tr>                
                <td>                      
                    <div id="userPromptNum" class="inlineNum">Please enter your maximum guess:&nbsp;</div>
                    <asp:TextBox ID="numberBox" runat="server" CssClass="textboxNum"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Button ID="numSubmitBtn" runat="server" CssClass="buttonNum btn_mouseOver" Text="Submit" OnClick="submitBtn_Click" />
                </td>
            </tr>

            <tr>
                <td>
                    <asp:RequiredFieldValidator ID="numberValidator" runat="server" CssClass="errorNum" Display="Dynamic" ControlToValidate="numberBox" ErrorMessage="Field cannot be empty!" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>

            <tr>
                <td>                    
                    <asp:label runat="server" ID="errorMsg" CssClass="errorNum"/>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="infoMessage" class="note">
                        Note: Your guessing range will be between
                        1 and the number you enter above.
                    </div>
                </td>
            </tr>
        </table>
        </div>
    </form>
</body>
</html>


      
