﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UsernamePage.aspx.cs" Inherits="WDD_A06.UsernamePage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" type="text/css" href="CSS/style.css"/>
    <title></title>
    </head>
<body style="background-image:url('Hi-lo.jpg')">
    <form id="usernameForm" runat="server">
        <div class="banner"></div>
        <div class="Greeting">
              <h1>Welcome to the Hi-Lo Game</h1>
        </div>      
        <table class="table">
            <tr>                
                <td>
                    <div id="userPrompt" class="inline">Please enter your name:&nbsp;</div>
                    <asp:TextBox ID="nameBox" runat="server" CssClass="textbox"></asp:TextBox>                                        
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Button ID="submitBtn" runat="server" CssClass="usernameButton btn_mouseOver" Text="Submit" OnClick="submitBtn_Click" />
                </td>
            </tr>

            <tr>
                <td>
                    <asp:RequiredFieldValidator runat="server" CssClass="error" ID="nameValidator" Display="Dynamic" ControlToValidate="nameBox" ErrorMessage="You must enter your name!" ForeColor="Red"/>
                </td>
            </tr>
               
            <tr>
                <td>                    
                    <asp:label runat="server" ID="errorMsg" CssClass="error"/>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
