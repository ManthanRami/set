﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GameLogic.aspx.cs" Inherits="WDD_A06.GameLogic" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" type="text/css" href="CSS/style.css"/>
    <title></title>
</head>
<body runat="server" id="body" style="background-image:url('Hi-lo.jpg')">
    <form id="gameForm" runat="server">
        <div class="banner"></div>
        <div class="center">
            <div class="userSalute">
                <asp:Label ID="saluteUser" runat="server" CssClass="salute" ></asp:Label>
            </div>
        <table class="table">
            <tr>
                <td>
                    <div class="rangeLocation">
                           <asp:Label runat="server" id="infoMsg" CssClass="range"></asp:Label>
                    </div>                 
                </td>
            </tr>
            <tr>                
                <td>
                    <asp:Label ID="guessPrompt" CssClass="inline" runat="server">Please enter your guess:&nbsp;&nbsp;</asp:Label>
                    <asp:TextBox ID="guessBox" CssClass="textbox" runat="server"></asp:TextBox>                                        
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="guessButton" runat="server" CssClass="makeGuessButton btn_mouseOver" Text="Make This Guess" OnClick="MakeGuess_Click" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="playAgainBtn" runat="server" CssClass="playAgainButton btn_mouseOver" Visible="false" Text="Play Again" OnClick="PlayAgain_Click" />

                </td>
            </tr>
            <tr>
                <td>
                    <asp:RequiredFieldValidator ID="emptyValidator" runat="server" CssClass="error" Display="Dynamic" ControlToValidate="guessBox" ErrorMessage="Field cannot be empty!"></asp:RequiredFieldValidator>
                </td>
            </tr>

            <tr>
                <td>
                    <asp:RegularExpressionValidator ID="numValidator" runat="server" CssClass="error" Display="Dynamic" ControlToValidate="guessBox"  ValidationExpression="^[0-9][0-9]*[0-9]*$" ErrorMessage="Your guess must be a number!"></asp:RegularExpressionValidator>
                </td>
            </tr>

            <tr>
                <td>                    
                    <asp:label runat="server" ID="errorMsg" CssClass="error"/>
                </td>
            </tr> 
        </table>
       </div>
    </form>
</body>
</html>
