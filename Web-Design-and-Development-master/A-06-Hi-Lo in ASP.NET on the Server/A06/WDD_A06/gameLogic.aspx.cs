﻿/*
* FILE           : GameLogic.aspx.cs
* PROGRAMMERS    : Conor Barr, Manthan Rami
* FIRST VERSIPON : 2019-11-06
* DESCRIPTION    : 
*	This file contains the code-behind for 'GameLogic.aspx'.
*	The purpose of the page is to play the Hi-Lo game.
*	
*	When the page loads initially, a random number is generated and saved
*	to a Session variable and the user is prompted for a guess. 

*	If the user's guess is correct, they've won the game. The background then 
*	changes, and the user is asked if they want to Play Again. The page is redirected
*	to 'NumberPage.aspx' if the user chooses to Play Again.
*
*	If the guess is incorrect, a new guessing range is calculated based on whether
*	the guess was high or low. The user is then asked to make another guess.
*/

using System;
using System.Web.UI.HtmlControls;

namespace WDD_A06
{
    public partial class GameLogic : System.Web.UI.Page
    {
        private const int kHighGuess = 1;
        private const int kLowGuess = 2;
        private const int kOneBelow = 3; // If the guess is less than the random number by 1
        private const int kOneAbove = 4; // If the guess is greater than the random number by 1

        private string username;
        private int userGuess;
        private int minRange;
        private int maxRange;
        private int randNum;


        /*
        * NAME	  : Page_Load()
        * PURPOSE : 
        *   This event handler is called when the page loads initially.
        *   All Session variables are loaded, the user is greeted, and
        *   the minimum and maximum guessing range is displayed. If it's
        *   the first time that the page loads, a random number is generated.
        * INPUTS  : 
        *   object sender: The object that triggered the event
        *   EventArgs e: The arguments that were passed to the event
        * OUTPUTS : 
        *   A greeting to the user and the minimum/maximum guessing range
        * RETURNS : 
        *   void
        */
        protected void Page_Load(object sender, EventArgs e)
        {
            minRange = Int32.Parse(Session["minRange"].ToString());
            maxRange = Int32.Parse(Session["maxRange"].ToString());

            // Check if it's the first time loading the page
            if (Session["firstGuess"].ToString() == "true")
            {
                // Indicate that it isn't the first guess anymore and then generate a random number
                Session["firstGuess"] = "false";
                Random randNumGenerator = new Random();
                Session["randNum"] = randNumGenerator.Next(minRange, maxRange + 1);
            }

            // Get the username and random number
            username = Session["userName"].ToString();
            randNum = Int32.Parse(Session["randNum"].ToString());

            displayGreeting();
        }


        /*
        * NAME	  : MakeGuess_Click()
        * PURPOSE : 
        *   This method is the event handler for the 'guessButton'.
        *   First, it checks whether the user's guess is within range.
        *   If it's out-of-range, an error message is displayed. If the
        *   guess is within range, it's then checked to see whether it's
        *   is correct.
        *   
        *  When the user guesses incorrectly the guessing range is adjusted
        *  based on whether the guess was high or low. If the guess is correct,
        *  the game is won.
        * INPUTS  : 
        *   object sender: The object that triggered the event
        *   EventArgs e: The arguments that were passed to the event
        * OUTPUTS : 
        * RETURNS : 
        */
        protected void MakeGuess_Click(object sender, EventArgs e)
        {
            // Get the user's guess and the random number
            Session["userGuess"] = guessBox.Text;
            userGuess = Int32.Parse(Session["userGuess"].ToString());

            if (userGuess >= minRange && userGuess <= maxRange)
            {
                // Clear the user input box and the error message
                errorMsg.Text = "";
                guessBox.Text = "";

                // Check whether the user's guess was correct
                if (userGuess == randNum)
                {
                    // The game is won, hide all game elements, congratulate the user, and display the Play Again button
                    HideGuessElements();
                    saluteUser.Text = "You Win !! You guessed the number !!";
                    infoMsg.Text = "";
                    playAgainBtn.Visible = true;                 
                    HtmlGenericControl body = (HtmlGenericControl)FindControl("body");
                    body.Attributes.Add("style", "background:url(win.jpg);");
                }
                else
                {
                    infoMsg.Text += "</br>Your guess was wrong!";
                    findNewGuessRange(isHighOrLow());
                    Response.Redirect("GameLogic.aspx");
                }
            }
            else
            {
                // Clear the textbox and display and error message
                guessBox.Text = "";
                errorMsg.Text = "Your guess must be between " + minRange + " and " + maxRange + ".";
            }
        }


        /*
        * NAME	  : PlayAgain_Click()
        * PURPOSE : 
        *   This is the event handler for the 'playAgainBtn'.
        *   It's purpose is to clear the session variables for the
        *   minRange, maxRange, and randNum. This ensures that the
        *   game can be played again. The page is the redirected to
        *   'NumberPage.aspx'.
        * INPUTS  : 
        * OUTPUTS : 
        * RETURNS : 
        */
        protected void PlayAgain_Click(object sender, EventArgs e)
        {
            // Clear the minRange, maxRange, and ranNum
            Session["minRange"] = "";
            Session["maxRange"] = "";
            Session["randNum"] = "";

            // Get the user's maxRange again
            Response.Redirect("NumberPage.aspx");
        }


        /*
        * NAME	  : displayGreeting()
        * PURPOSE : 
        *   This method displays the greeting to the user when the
        *   page loads initially.
        * INPUTS  : 
        *   none
        * OUTPUTS : 
        *   The user is greeted by name and the allowable guessing range is
        *   stated.
        * RETURNS : 
        *   none
        */
        private void displayGreeting()
        {
            saluteUser.Text = "It's Your Turn " + username;
            infoMsg.Text = "Your allowable guessing range is any value between " + minRange +
                             " and " + maxRange;
        }


        /*
        * FUNCTION    : isHighOrLow()
        * DESCRIPTION : This function checks whether the user's guess
        *   is high or low.
        * PARAMETERS  :
        *   none
        * RETURNS     :
        *   int result: 1 (kLowGuess) if the guess is too low, and 2 (kHighGuess) if the
        *   guess is too high
        */
        private int isHighOrLow()
        {
            int result = 0;

            // Check whether the guess is too high or too low

            if (userGuess == (randNum - 1))
            {
                result = kOneBelow;

            }
            else if (userGuess == (randNum + 1))
            {
                result = kOneAbove;
            }
            else if (userGuess > randNum)
            {
                result = kHighGuess;
            }
            else if (userGuess < randNum)
            {
                result = kLowGuess;
            }

            return result;
        }


        /*
        * FUNCTION    : findNewGuessRange()
        * DESCRIPTION : This function finds the guessing range
        *   based on the user's incorrect guess. The minRange
        *   is found by adding 1 to the user guess and the maxRange
        *   by subtracting 1 from the user.
        *   
        *   If the guess is 1 below (e.g. userGuess == randNum - 1) or
        *   1 above (e.g. userGuess == randNum + 1), the user's guess is
        *   used for the new minRange or maxRange value. This is done so 
        *   that the game will not allow the minRange and maxRange range
        *   to be the same.
        * PARAMETERS  :
        *   userGuess: The guess entered by the user
        *   highOrLow: Whether the guess was high or low
        * RETURNS     : none
        */
        private void findNewGuessRange(int highOrLow)
        {
            // Determine whether the guess was high or low and
            // then adjust the minimum or maximum guess accordingly
            if (highOrLow == kLowGuess)
            {
                minRange = userGuess + 1;
                Session["minRange"] = minRange;
            }
            else if (highOrLow == kHighGuess)
            {
                maxRange = userGuess - 1;
                Session["maxRange"] = maxRange;
            }
            else if (highOrLow == kOneAbove)
            {
                maxRange = userGuess;
                Session["maxRange"] = maxRange;
            }
            else if (highOrLow == kOneBelow)
            {
                minRange = userGuess;
                Session["minRange"] = minRange;
            }
        }


        /*
        * NAME	  : HideGuessElements() 
        * PURPOSE : 
        *   This method hides all elements on the page related to
        *	playing the Hi-Lo game.
        * INPUTS  : 
        *   none
        * OUTPUTS : 
        *	none
        * RETURNS : 
        *   none
        */
        private void HideGuessElements()
        {
            guessPrompt.Visible = false;
            guessBox.Visible = false;
            guessButton.Visible = false;
            emptyValidator.Enabled = false;
            numValidator.Enabled = false;
        }
    }
}