				
      /*
      FILE          : hiloStart.html
      PROJECT       : PROG2001 - Web Development: 
      PROGRAMMER    : Manthan Rami, Jason Kassies
      FIRST VERSION : 2019-11-02
      DESCRIPTION   : This is file contains all the fucntion created in javascript used in hiloStart.html
    */
      /********************************************************************************************
            FUNCTION :validateName
            DESCRIPTION : This function will validate Name of the user if user has enter name or not
                          before proceding to next step.
            PARAMETERS : Nothing
            RETURNS : Nothing
            **********************************************************************************************/
           function validateName()    //function for validating name of user
           {                 
              var userName = document.getElementById("name").value;
              //document.getElementById("Error").innerHTML = "";	
              var lettersNSpaces = /^[A-Za-z\s]+$/;
              if ((userName.trim()).length == 0)     //checking if user has given any input Or not
               {
                  document.getElementById("Error").innerHTML = "Your name <b>cannot</b> be BLANK.";    //Error message if user didn't give any input                                   
                  return false;
                }
              else if(userName.match(lettersNSpaces))  //if user input name then it proceed furthur
              {               
                document.getElementById("Error").innerHTML = "";    //Error message erase                                   
                          
                //hide the divs asking for the name
                document.getElementById("nameDiv").style.display = "None";
                //change the button onclick to the maxGuess function
                document.getElementById("btnName").style.display = "None";
                document.getElementById("btnNum").style.display = "table-row";
                //show the divs asking for the next guess
                document.getElementById("numberDiv").style.display = "Block";
                //add name to hidden form element
                document.getElementById("userName").value = userName;
                document.getElementById("userWelcome").innerHTML="Hello "+userName+", Please Enter Maximum Guess Number : ";													
			        	return true;         
              }
              else
              {
               document.getElementById("Error").innerHTML = "Your name must be made of <b>alpha</b> characters <u>only</u>.";   //Error message if user didn't give any input
               document.getElementById("name").value="";     //reseting value to empty              
               return false;
              }
          }

/********************************************************************************************
FUNCTION :validateNumber
DESCRIPTION : This function will validate Number of the user if user has enter Number or not
          before proceding to next step.
PARAMETERS : Nothing
RETURNS : Nothing
**********************************************************************************************/
	
	function validateNumber()
	{	
		var myNum = document.getElementById("maxGuess").value;
		var regex = new RegExp("^-{0,1}[0-9]*$");		//regular expression that want only a number
        if(myNum.length==0)
        {
			document.getElementById("Error").innerHTML = "Your name <b>cannot</b> be BLANK.";  
			return false;
        }
		else if (regex.test(myNum))	//test for any characters that are not numbers
		{
			document.getElementById("maximum").value = myNum;
			document.forms["firstSubmit"].submit();
		}
		else	//if any character are not a number show error
		{
			document.getElementById("Error").innerHTML = "Your Number must be made of <b>Numbers</b> characters <u>only</u>.";
		}
	}