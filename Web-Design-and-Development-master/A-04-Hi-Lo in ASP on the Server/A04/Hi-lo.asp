<!DOCTYPE html>

<!--
  FILE          : Hi-lo.asp
  PROJECT       : PROG2001 - Web Development: 
  PROGRAMMER    : Manthan Rami, Jason Kassies
  FIRST VERSION : 2019-11-02
  DESCRIPTION   : This is an asp page which has actual game logic which runs using data given html page
-->

<html>
  <head>
    <title>Hi-Lo</title>
    <link rel="stylesheet" href="styleAsp.css">
	<script type="text/javascript">
		function checkInput()	
		{
			var x = document.getElementById("tbGuess").value;
			var regex = new RegExp("^[0-9]*$");
			
			if (x.length > 0)
			{
				if (regex.test(x))
				{
					document.getElementById("GuessID").value = x;
					document.getElementById("Submitbutton").value = x;
					document.forms["formRepost"].submit();
				}
				else
				{
					//incorect input error
					document.getElementById("tbGuess").value="";
					document.getElementById("Error").innerHTML="Field can only Have Positive Intiger !!";
				}
			}
			else
			{
				//missing input error
				document.getElementById("tbGuess").value="";
				document.getElementById("Error").innerHTML="Field cannot be <b>Blank</b> !!";
			}
		}

		function playAgain()
		{
			document.getElementById("userName").value="";
			document.getElementById("maximum").value="";
			document.getElementById("minimum").value="1";
			document.getElementById("MagicNum").value="-1";
			document.getElementById("GuessID").value="";
			document.forms["formRepost"].submit();
		}
		function changeBg()
		{
			document.getElementById("box").style.backgroundColor="red";
		}
	</script>
  </head>
  <body>
	<div class="box" id="box">
	<br />
	<table align="center">
			<tr>
				<td colspan="2">
						<%	
							'declaring all variables needed
							Dim first
							Dim name
							Dim maximumber
							Dim minimumber
							Dim winnumber
							Dim guess
							Dim gameOver
							
							'setting the status flags to false
							first = false
							gameOver = false
							
							'pulling all posted values from the form and checking if its a number
							name = request.form("userName")
							
							maximumber = Request.Form("maximum")
							if IsNumeric(maximumber) then
								maximumber = CInt(maximumber)
							end if

							minimumber = Request.Form("minimum")
							if IsNumeric(minimumber) then
								minimumber = CInt(minimumber)
							end if
									
							winnumber = Request.Form("MagicNum")
							if IsNumeric(winnumber) then
								winnumber = CInt(winnumber)
							end if
							
							guess = Request.Form("GuessN")
							if IsNumeric(guess) then
								guess = CInt(guess)
							end if
							
							'checking if win number is defualt, if it is then we know its the first time running this page
							if winnumber = 0 then
								first = true	'changing that status flag
								Randomize		'generating the random winning number
								winnumber = rnd * maximumber
								winnumber = Round(winnumber, 0)
								winnumber = cint(winnumber)
							end if
						
					%> 

						
					<% 					
					'if it is the first time show welcome message
					'this had the double benifit of not having to compare "...AND NOT first THEN" for any if statements
					%><h1 align="center">Hello <%=name%> </h1><%
					if first then
					%>		
						<h3 style="padding: 25px;">Allowed Range is between <%=minimumber%> to <%=maximumber%> </h3>
					<%
						'if the guess is less than the minimum value run this set of html
					elseif guess < minimumber then
					%>
						<h3 style="padding: 25px;">Allowed Range is between <%=minimumber%> to <%=maximumber%> </h3>
						That number is out of range!		
					<%
					
					elseif guess > maximumber then
					%>
						<h3 style="padding: 25px;">Allowed Range is between <%=minimumber%> to <%=maximumber%> </h3>
						That number is out of range!		
					<%
							'if the guess was less than the winning number and its not the first time running this file
							'also setting the lowest number to what was guessed
					elseif guess < winnumber and guess < (winnumber-1) then
						minimumber = (guess+1)
					%>
						<h3 style="padding: 25px;">Allowed Range is between <%=minimumber%> to <%=maximumber%> </h3>
						Alright <%=name%>, your guess was to Low!
							<!-- a prompt telling you that you were to low -->		
					<%
					elseif guess = (winnumber-1) then					
					%>
						<h3 style="padding: 25px;">Allowed Range is between <%=minimumber%> to <%=maximumber%> </h3>						
							<!-- a prompt telling you that you were to low -->		
					<%
							'if the guess was higher than the winnign number
							'and changing the maximum numberj to what wass guessed
					elseif guess > winnumber then
					maximumber = (guess-1)
					%>
						<p align="center"><h3 style="padding: 25px; ">Allowed Range is between <%=minimumber%> to <%=maximumber%> </h3</p></br>
						Alright <%=name%>, your guess was to High!
							<!-- a prompt telling you that you were to high -->	
					<%
							'if you guessedd the winning number
							'the background colour does not change becuase i dont have time to implement css
						elseif guess = winnumber then
							gameOver = true
					%>
						<h1> YOU WON THE HI-LOW GAME !! </h1>
						<body style="background-color:yellow;">
						</body>
						<tr colspan="3" >  
							<td  align="center">
								<input id="Reset" type="button" value="Play Again" onclick="playAgain()"  class="playAgainasp" />
							</td>
						</tr>
						
					<%
						else 
					%>	<!-- idk how you got here... -->	<%
						end if	
						
					'if the game is not over then display the request message and textbox becuase its not needed when the game does end	
						if NOT gameOver THEN
					%>
				</td>
			</tr>
			<tr>
				<td>
					<p>Try your luck guess the number:</p>
				</td>
				<td>
					<input type="text" id="tbGuess" name="tbGuess">
				</td>
			</tr>
			<tr colspan="3" >  
				<td class="submitRow" >
					<input id="Submitbutton" type="button" value="Submit" onclick="checkInput()" class="submitasp"/>
				</td>
			</tr>
	<%
		end if
	'print out the rest
	%>
			<tr>
				<td  colspan="2">
					<h3 id="Error" style="color: red; padding: 25px;"class="error" ></h3>
				</td>
			</tr>
		</table>				
			<form action="Hi-lo.asp" name="formRepost" method="POST">
				<input type="hidden" name="userName" id="userName" value="<%=name%>">
				<input type="hidden" name="maximum" id="maximum" value="<%=maximumber%>">
				<input type="hidden" name="minimum" id="minimum" value="<%=minimumber%>">
				<input type="hidden" name="MagicNum" id="MagicNum" value="<%=winnumber%>">
				<input type="hidden" name="GuessN" id="GuessID" value="<%=guess%>">
			</form>
	</div>
  </body>
</html>