﻿/*
*  FILE          : program.cs
*  PROJECT       : PROG-2001 Web Design and Development
*  PROGRAMMER    : Manthan Rami, Alex MacCumbe
*  Date          : 2019-10-22
*  DESCRIPTION   : This logic of CGI server which takes QUERY_STRING and parse it and
*                   get user name and animal selection on basis of that it create a 
*                   HTML page which shows animal pitcure and Description about it.
* Refrence       :  Deer
                            https://clipartion.com/wp-content/uploads/2015/11/reindeer-clip-art-cliparts-830x1006.png

                            https://www.britannica.com/animal/deer

                    *Zebra
                            https://cdn4.vectorstock.com/i/1000x1000/98/93/cute-zebra-cartoon-walking-vector-2289893.jpg

                            https://www.britannica.com/animal/zebra

                    *Tiger
                            https://cdn2.vectorstock.com/i/1000x1000/28/96/cute-tiger-cartoon-sitting-vector-1252896.jpg

                            https://www.britannica.com/animal/tiger 

                    *Elephant
                            https://media.istockphoto.com/vectors/cute-elephant-cartoon-vector-id499955826?k=6&m=499955826&s=612x612&w=0&h=9PVTV2-Q26mSnqEm9YeRHd2nk2vETT-NYSY87-hmx_U

                            https://www.britannica.com/animal/elephant-mammal

                    *Kangaroo
                            http://www.clker.com/cliparts/l/V/J/8/b/1/kangaroo-hi.png 

                            https://www.britannica.com/animal/kangaroo

                    *Lion
                            https://www.artranked.com/images/03/033b1b19ad9fa912737712bb18ab258e.jpg 

                            https://www.britannica.com/animal/lion
                    

*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animals
{
    class Program
    {       
        static void Main(string[] args)
        {

            var         value = System.Environment.GetEnvironmentVariable("QUERY_STRING");
            string      qstring = value;       //storing QUERY_STRING to qstring
            char[]      delimiterChars = { '=', '&', '\0' };    //storing all delimeters into array
            string[]    Inputs = qstring.Split(delimiterChars); //spliting into words and saving to input array
            string      textFile = "theZoo/" + Inputs[3] + ".txt";  //setting the path for txt file
            string      image = "theZoo/" + Inputs[3] + ".jpg";  //setting the path for image file
            string      discription = "";                       //use for saving data of text file

            if (Inputs[1] == "" || Inputs[3] == "")    //check if the QUERY_STRING is empty 
            {
                Console.WriteLine("Content - type:text / html\n");
                Console.WriteLine("<!DOCTYPE html>");
                Console.WriteLine("<html lang=\"en\">");
                Console.WriteLine(" <head>");
                Console.WriteLine("<meta charset=\"UTF - 8\">");
                Console.WriteLine("<title>CGI-ZOO</title>"); ;
                Console.WriteLine("</head>");
                Console.WriteLine("<body>");
                Console.WriteLine("<h1 align=\"center\"> Invalid Input URL !!</h1>");      //displaying Error incase of something wrong with URL          
                Console.WriteLine("</BODY>");
                Console.WriteLine("</html>");
            }
            else
            {
                //Now we will read all the data of text file and save it to discription
                using (StreamReader text = new StreamReader(textFile)) 
                {
                    discription = text.ReadToEnd();
                }
                Console.WriteLine("Content - type:text / html\n");
                Console.WriteLine("<!DOCTYPE html>");
                Console.WriteLine("<html lang=\"en\">");
                Console.WriteLine(" <head>");
                Console.WriteLine("<meta charset=\"UTF - 8\">");
                Console.WriteLine("<title>CGI-ZOO</title>"); ;
                Console.WriteLine("</head>");
                Console.WriteLine("<body >");
                //Now that we have gotten the user's name and animal choice We will greet them and provide an image and some information about the animal that they selected
                Console.WriteLine("<h1 align=\"center\"> Thanks for coming to visit our zoo, " + Inputs[1] +". </br> Here is some information about "+Inputs[3]+": </h1>") ;                
                Console.WriteLine("<table align=\"center\">");
                Console.WriteLine("</tr>");
                Console.WriteLine("<tr>");
                Console.WriteLine("<td>");
                //Display the image for the animal they selected on the form page
                Console.WriteLine("<img src=" +image+" width=\"400px\" height=\"400px\" align=\"left\"/>");
                Console.WriteLine("</td>");
                //This is simply used to provide some space between the image and the text
                Console.WriteLine("<td width=\"10px\">");   
                Console.WriteLine("<td width=\"500px\">");
                //Read the contents of the file to the webpage, and place them beside the image of the animal
                Console.WriteLine(discription);
                Console.WriteLine("</td>");
                Console.WriteLine("</tr>");                
                Console.WriteLine(" </table>");
                Console.WriteLine("</BODY>");
                Console.WriteLine("</html>");
            }
        }
    }
    
}
