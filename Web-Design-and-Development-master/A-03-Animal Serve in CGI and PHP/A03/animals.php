<!--
* FILE          : 	animals.php
* PROJECT       : 	PROG2001 - A03: Animal-Serve Website Developed in CGI and PHP
* PROGRAMMER    : 	Alex MacCumber, Mantahn Rami
* FIRST VERSION : 	2019-10-20
* DESCRIPTION   : 	This file contains the PHP server-side logic for populating the Information Page
*					that the user is directed to once they submit valid form information.  It takes
*					their provided information and uses it to display the appropriate image and animal
*					information on the webpage.
-->


<!DOCTYPE HTML>
<HTML>

	<HEAD>
		<TITLE> INFORMATION PAGE</TITLE>
	</HEAD>
	
	<BODY>
	<?PHP
		
		// Read the user provided name and their animal choice
		$nameOfUser = $_POST["secretName"];
		$animalChoice = $_POST["secretAnimal"];
		
		// Generate the file paths for the image and text according to the animalChoice value.
		$imagePath = "theZoo/" . $animalChoice . ".jpg";
		$textPath = "theZoo/" . $animalChoice . ".txt";
		
	?>
	
<!-- Now that we have gotten the user's name and animal choice We will greet them and provide an image and some information about the animal that they selected -->
	<H1 align="center"> <?PHP print "Thanks for coming to visit our zoo, "  . $nameOfUser . ". </br>Here is some information about " . $animalChoice . "s:"; ?> </H1>
	
	<TABLE align="center">
	<DIV id="imageAndText">
		<TR>
			<!-- Display the image for the animal they selected on the form page -->
			<TD><img src= "<?php echo $imagePath;?>" width="400px" height="400px" align="left"/></TD>
			<TD width="10px"><!-- This is simply used to provide some space between the image and the text --></TD>
			<TD width="500px">
			<!-- Read the contents of the file to the webpage, and place them beside the image of the animal -->
			<?PHP echo readfile($textPath) ?>
			</TD>
		</TR>
	</DIV>		
	</TABLE>
	
	</BODY>

</HTML>