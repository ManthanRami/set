﻿/*
*  FILE          : MyServer.cs
*  PROJECT       : PROG2001-Web Development: A-07 : Developing your own Web Server 
*  PROGRAMMER    : Jayson Ovishek Biswas & Manthan Rami
*  FIRST VERSION : November 30 2019
*  DESCRIPTION   : This program will create myOwnWebServer it is only signle threaded server
*                  which take client request and response to it. it take -webRoot, webIP and webPort ad arrgument.
*  Note          : This program will run on default command line arrgument given in A-7 requierment pdf
	
*/
using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text.RegularExpressions;
using System.IO;



namespace MyWebServer
{

    class MyServer
    {
        const int EXIT_CODE = -1;   //exit code to show exception occurs and log created

        //file type supported on the server 
        const string TXT = ".txt"; 
        const string HTML = ".html";
        const string JPEG = ".jpeg";
        const string JPG = ".jpg";
        const string GIF = ".gif";

        //some constat delemeters used to seprate data
        const char DELEMETER = '=';
        const char BLANK = ' ';

        //data types
        const string TXT_M = "text/plain";
        const string HTML_M = "text/html;";
        const string GIF_M = "image/gif";
        const string JPG_M = "image/jpeg";
        const string SET_CHAR = "charset=UTF-8";
        const string Ok = "HTTP/1.1 200 OK";
        const string Not_found = "HTTP/1.1 404 NOT FOUND";
        const string UNKNOWN_CODE = "HTTP/1.1 415 UNSUPPORTED MEDIA TYPE";

        /*======================================================
         * Purpose: TO GET AND GET THE LOCATION OF THE ROOT       
        ======================================================--*/
        string rootLocation 
        { 
            get;
            set; 
        }
        /*======================================================
         * Purpose: TO GET AND GET THE IP ADDRESS OF THE HOST       
        ======================================================--*/
        string hostIp 
        { get;
          set;
        }
        /*=========================================
         * Purpose: TO GET AND GET THE WEB PORT    
        ===========================================*/
        string webPort
        {
            get;
            set;
        }
        /*===========================================
         * Purpose: TO GET AND GET THE  LOCAL FOLDER    
        ============================================*/
        string lFolder
        {
            get;
            set;
        }
        /*==========================================
         * Purpose: TO GET AND GET THE OUTPUTSTRING     
        ===========================================*/
        string outputString
        {
            get;
            set;
        }
        /*======================================
         * Purpose: TO GET AND GET THE DATATYPE     
        =======================================*/
        string dataType
        {
            get;
            set;
        }
        TcpListener myServer; //TCP LISTENER AS SERVER
        TcpClient   myClient; //TCP CLIENT AS CLIENT
        static int Main(string[] args)
        {
            int signal = 0; // USED TO CHECK THE RETURN VALUE FROM THE FUNCTIONS
            do
            {
                MyServer server = new MyServer();   //INITIATES NEW SERVER
                if (server.ParseArguments(args)==EXIT_CODE) //PARSE THE COMMAND LINE ARRGUMENTS
                {
                    return EXIT_CODE;   //RETURNS EXIT_CODE INCASE OF EXCEPTION OCCURS
                }
                if (server.RunListener()==EXIT_CODE)    //START THE LISTNER
                {
                    return EXIT_CODE;   //RETURNS EXIT_CODE INCASE OF EXCEPTION OCCURS
                }
                server.WaitForClient();     //WAIT FOR THE CLIENT TO JOIN THE SERVER
                if (server.ResponseRead()==EXIT_CODE)
                {
                    return EXIT_CODE;   //RETURNS EXIT_CODE INCASE OF EXCEPTION OCCURS
                }
                if (server.RespondRequest()==EXIT_CODE) //RESPOND TO THE REQUEST DONE BY CLIENT
                {
                    return EXIT_CODE;   //RETURNS EXIT_CODE INCASE OF EXCEPTION OCCURS
                }
                signal = server.ShutDownServer();   //SHUTDOWN THE CURRENT SERVER
                if (signal==-EXIT_CODE)
                {
                    return EXIT_CODE;   //RETURNS EXIT_CODE INCASE OF EXCEPTION OCCURS
                }
            }while(true);
        }
        /*================================================================================================
         *  Function    : ParseArguments
         *  Description : This function will parse the command line arrgument into root ip and port
         *  Parameters  : string args : -webRoot, -webIP, -webPort
         *  Returns     : int redcode as -1 if many arguments passed otherwise 0
         ================================================================================================*/
        int ParseArguments(string[] args)
        {
            int redcode = 0;    //USE FOR RETURN THE VALUE AS SIGNAL 
            try
            {
                int i = 0;
                foreach (string arg in args)    //LOOP THROUGH EACH ARGS IN COMMAND LINE ARRGUMENTS
                {
                    string[] pArgs = arg.Split(DELEMETER);    
                    i++;  //INCRESES THE COUNTER TO GET THE WEBROOT, WEBIP & WEBPORT
                    switch (i)
                    {
                        case 1:
                            this.rootLocation = pArgs[1];   //STORE WEBROOT
                            break;
                        case 2:
                            this.hostIp = pArgs[1];         //STORE WEBiP
                            break;
                        case 3:
                            this.webPort = pArgs[1];          //STORE WEBpORT
                            break;
                        default:
                            throw new Exception("too many arguments Passed !!");    //INCASE OD MORE ARGUMENTS PASSED
                    }
                }
            }
            catch (Exception e)     //CATCH EXCEPTION IF ANY OCCURS 
            {
                Logger.CreateLog("Invalid Arguments Passed !!");    //LOG DOWN THE LOG EVENT USING LOGGER CLASS
                redcode = EXIT_CODE;                                //CHANGE THE REDCODE VALUE TO -1
                return redcode;                                     //RETURN REDCODE
            }
            return redcode;     //RETURN REDCODE WITH VALUE 0
        }
        /*================================================================================================
        *  Function    : RunListener
        *  Description : This function will initiates connection of server 
        *  Parameters  : Nothing
        *  Returns     : int redcode as -1 if any exception ocurrs otherwise 0
        ================================================================================================*/
        int RunListener()
        {
            int redcode = 0;     //USE FOR RETURN THE VALUE AS SIGNAL 
            try
            {
                //INITIATES THE SERVER
                this.myServer = new TcpListener(IPAddress.Loopback,Convert.ToInt32(this.webPort));
                myServer.Start();   //START THE SERVER USING START METHOD 
            }
            catch (Exception e) //CATCH EXCEPTION IF ANY OCCURS
            {
                //LOG DOWN THE LOG EVENT USING LOGGER CLASS
                Logger.CreateLog($"Error - Cannot start the server at. IP:{this.hostIp} " +$"Port:{this.webPort} ");
                redcode = EXIT_CODE;                                //CHANGE THE REDCODE VALUE TO -1
                return redcode;                                     //RETURN REDCODE
            }
            return redcode;     //RETURN REDCODE WITH VALUE 0
        }
        /*================================================================================================
        *  Function    : WaitForClient
        *  Description : This function will accept the client connection to the server
        *  Parameters  : Nothing
        *  Returns     : Nothing as return type is void
        ================================================================================================*/
        void WaitForClient()
        {
            this.myClient = this.myServer.AcceptTcpClient();    //WAIT FOR THE CLIENT TO JOIN 
          //     Console.WriteLine("connected  ");      JUST FOR TESTING IF THE CLIENT CONNECTS OR NOT
        }
        /*================================================================================================
        *  Function    : ResponseRead
        *  Description : This function will read the response from the client
        *  Parameters  : Nothing
        *  Returns     : int redcode as -1 if any exception ocurrs otherwise 0
        ================================================================================================*/
        int ResponseRead()
        {
            string inputString = "";            
            int redcode = 0;    //USE FOR RETURN THE VALUE AS SIGNAL 
            try
            {
                NetworkStream ns = myClient.GetStream();
                StreamReader sReader = new StreamReader(ns);
                while (!sReader.EndOfStream)
                {
                    string input = sReader.ReadLine();
                    inputString += input;
                    if (input.Contains("GET"))  // CHECK IF THE INPUT STRING CONTAINTS GET
                    {
                        string[] storeLines = input.Split('/'); //SPLIT THE INPUT STRING FROM '/'
                        this.lFolder = storeLines[1];           // SAVE IT
                        string[] fLines = storeLines[1].Split(BLANK);//SPLIT THE STORELINE FROM BLANK
                        this.lFolder = fLines[0];
                        this.lFolder = this.rootLocation + "\\" + lFolder;
                        string[] fileSplit = this.lFolder.Split('.');
                        this.dataType = "." + fileSplit[1];
                    }
                    if (input.Length==0)    //CHECK IF THE INPUT IS EMPTY OR NOT
                    {
                        break;
                    }
                    this.dataType = this.dataType.ToLower();   //TURN EVERTHING TO LOWERCASE LETTERS
                }
            }
            catch (Exception e)     //CATCH EXCEPTION IF ANY OCCURS
            {
                Logger.CreateLog("Error -Unableto read request from client");
                redcode = EXIT_CODE;                                //CHANGE THE REDCODE VALUE TO -1
                return redcode;                                     //RETURN REDCODE
            }
            return redcode;        //RETURN REDCODE WITH VALUE 0
        }
       /*================================================================================================
       *  Function    : RespondRequest
       *  Description : This function will give respone to the client request
       *  Parameters  : Nothing
       *  Returns     : int redcode as -1 if any exception ocurrs otherwise 0
       ` ================================================================================================*/
        int RespondRequest()
        {
            int redcode = 0;             //USE FOR RETURN THE VALUE AS SIGNAL 
            try
            {
                if (IsSupported()==EXIT_CODE)   //CHECK IF THE FILE IS SUPPORTED TO THE WEBSERVER
                {
                    Code415();  //SENT ERROR CODE 415 
                }
                else if (IsFileExist()==EXIT_CODE)
                {
                    Code404();  //SENT ERROR CODE 404
                }
                else if (this.dataType == HTML)
                {
                    SentMsgHtml();    //SEND RESPONSE WITH A HTML FILE 
                }
                else if (this.dataType == TXT)
                {
                    TxtMessage();     // SEND RESPONSE WITH A PLAIN TEXT MESSAGE FILE
                }
                else if ((this.dataType == JPEG) || (this.dataType == JPG))
                {
                    Jimage();    // SEND RESPONSE WITH A JPEG OR JPG MESSAGE FILE
                }
                else if (this.dataType == GIF)
                {
                    Gimage();    // SEND RESPONSE WITH A GIF MESSAGE FILE
                }
                else
                {
                    Code500();    // SEND ERROR MESSAGE WITH CODE 500 INTERNAL SERVER
                }
            }
            catch (Exception) 
            {
                //LOG DOWN THE EVENT USING LOGGER CLASS INTO LOG FILE
                Logger.CreateLog("Error - Unable to send requested message to the client");
                Code500();
                redcode = EXIT_CODE;                                //CHANGE THE REDCODE VALUE TO -1
                return redcode;                                     //RETURN REDCODE
            }
            return redcode;        //RETURN REDCODE WITH VALUE 0
        }

        /*================================================================================================
       *  Function    : IsFileExist
       *  Description : This function will send a image  with .jpeg extention file
       *  Parameters  : Nothing
       *  Returns     : int redcode as -1 if exits otherwise 0
       ================================================================================================*/
        int IsFileExist()
        {
            int redcode = EXIT_CODE;        //USE FOR RETURN THE VALUE AS SIGNAL 
            switch (this.dataType)          //SWITCH CASE TO DETERMINE THE THE FILE TYPE IS EXISTS OR NOT
            {
                case TXT:   //IF FILE IS TEXT
                case HTML:   //IF FILE IS HTML
                case JPEG:   //IF FILE IS JPEG
                case JPG:   //IF FILE IS JPG
                case GIF:   //IF FILE IS GIF
                    if (File.Exists(this.lFolder))  //CHECK IF THE FILE IS EXISTS IN THE FOLDER
                    {
                        redcode = 0;       // SET THE VALUE OF RETCODE TO 0
                        return redcode;    // RETURN REDCODE INDICATING FILE EXISTS :)
                    }
                    break;
                default:
                    return redcode; //RETURN REDCODE WITH VALUE -1 INDICATING FILE NOT FOUND :(
            }
            return redcode;
        }
        /*================================================================================================
        *  Function    : IsSupported
        *  Description : This function will check the requested file is supported by the server or not
        *  Parameters  : Nothing
        *  Returns     : int redcode as -1 if not suported otherwise 0
        ================================================================================================*/
        int IsSupported()
        {
            int redcode = EXIT_CODE;        //USE FOR RETURN THE VALUE AS SIGNAL 
            switch (this.dataType)          // SWITCH CASE  TO DETERMINE IF THE FIEL IS SUPPORTED TO OUR WEB SERVER
            {
                case TXT:   //IF FILE IS TEXT
                case HTML:   //IF FILE IS HTML
                case JPEG:   //IF FILE IS JPEG
                case JPG:   //IF FILE IS JPG
                case GIF:   //IF FILE IS GIF
                    redcode = 0;       // SET THE VALUE OF RETCODE TO 0
                    return redcode;    // RETURN REDCODE INDICATING FILE IS SUPPORTED :)
                default:
                    return redcode;   //RETURN REDCODE WITH VALUE -1 INDICATING FILE NOT SUPPORTED :(
            }
        }
        /*================================================================================================
        *  Function    : ShutDownServer
        *  Description : This function will shutdown the server 
        *  Parameters  : Nothing
        *  Returns     : int redcode as -1 if any exception ocurrs otherwise 0
        ` ================================================================================================*/
        int ShutDownServer()
        {
            int redcode = 0;        //USE FOR RETURN THE VALUE AS SIGNAL 
            try
            {
                this.myClient.Close();    //DISCONNECT THE CLIENT FROM THE SERVER
                this.myServer.Stop();     // CLOSE THE SERVER PROPERLY
            }
            catch (Exception)
            {
                //LOG DOWN THE EVENT USING LOGGER CLASS INTO LOG FILE
                Logger.CreateLog("Error - Cannot close server appropriate !!");
                redcode = EXIT_CODE;                                //CHANGE THE REDCODE VALUE TO -1
                return redcode;                                     //RETURN REDCODE INDICATIN SERVER NOT CLOSED PROPERLY :( 
            }
            return redcode;       //RETURN REDCODE WITH VALUE 0 INDICATING SERVER CLOSED PROPERLY :)
        }
        /*================================================================================================
        *  Function    : Code404
        *  Description : This function will send error code for 4O4 NOT FOUND 
        *  Parameters  : Nothing
        *  Returns     : Nothing as return type is void
`       ================================================================================================*/
        void Code404()
        {
            string webPage = "";
            NetworkStream ns = myClient.GetStream();            
            using (StreamReader Sr = new StreamReader(@"C:/localWebSite/404.html", Encoding.UTF8))
            {
                webPage = Sr.ReadToEnd();
            }
            this.outputString = Not_found + Environment.NewLine;
            this.outputString = outputString + HTML_M;
            this.outputString = outputString + SET_CHAR + Environment.NewLine;
            this.outputString = outputString + ("Content-Length: " + webPage.Length);
            this.outputString = outputString + webPage;
            StreamWriter sw = new StreamWriter(ns);
            sw.Write(outputString);
            sw.Flush();     //FLUSH THE CURRENT STREAM
            //LOG DOWN THE EVENT USING LOGGER CLASS INTO LOG FILE
            Logger.CreateLog(Environment.NewLine + this.outputString);
        }
        /*================================================================================================
        *  Function    : Code415
        *  Description : This function will send error code for  415 indicating data not supprted.
        *  Parameters  : Nothing
        *  Returns     : Nothing as return type is void
        ================================================================================================*/
        void Code415()
        {
            string webPage = "";
            NetworkStream ns = myClient.GetStream();
            this.outputString = UNKNOWN_CODE + Environment.NewLine;
            this.outputString = outputString + HTML_M;
            this.outputString = outputString + SET_CHAR + Environment.NewLine;
            this.outputString = outputString + ("Content-Length: " + webPage.Length);
            this.outputString = outputString + webPage;
            StreamWriter sw = new StreamWriter(ns);
            //WRITING ON THE CURRENT STREAM
            sw.Write(outputString);
            sw.Flush();     //FLUSH THE CURRENT STREAM
            //LOG DOWN THE EVENT USING LOGGER CLASS INTO LOG FILE
            Logger.CreateLog(Environment.NewLine + this.outputString);    
        }
        /*================================================================================================
        *  Function    : Code500
        *  Description : This function will send error code 500 for Server ERROR
        *  Parameters  : Nothing
        *  Returns     : Nothing as return type is void
        ================================================================================================*/
        void Code500()
        {
            string webPage = "";
            NetworkStream ns = myClient.GetStream();
            this.outputString = outputString  + HTML_M;
            this.outputString = outputString + SET_CHAR + Environment.NewLine;
            this.outputString = outputString + ("Content-Length: " + webPage.Length);
            this.outputString = outputString + webPage;
            StreamWriter sw = new StreamWriter(ns);
            //WRITING ON THE CURRENT STREAM
            sw.Write(outputString);
            sw.Flush();     //FLUSH THE CURRENT STREAM
            //LOG DOWN THE EVENT USING LOGGER CLASS INTO LOG FILE
            Logger.CreateLog(Environment.NewLine + this.outputString);
        }
        /*================================================================================================
        *  Function    : SentMsgHtml
        *  Description : This function will send a message to the client
        *  Parameters  : Nothing
        *  Returns     : Nothing as return type is void
        ================================================================================================*/
        void SentMsgHtml()
        {
            string pageHeader;
            NetworkStream ns = myClient.GetStream();
            using (StreamReader sr = new StreamReader(this.lFolder, Encoding.UTF8))
            {
                this.outputString = sr.ReadToEnd();
            }
            StreamWriter sw = new StreamWriter(ns);

            pageHeader = (Ok);
            pageHeader = pageHeader + (Environment.NewLine);
            pageHeader = pageHeader + (HTML_M + SET_CHAR + Environment.NewLine);
            pageHeader = pageHeader + ("Content-Length: " + this.outputString.Length);
            pageHeader = pageHeader + (Environment.NewLine);
            pageHeader = pageHeader + (Environment.NewLine);
            //WRITING ON THE CURRENT STREAM
            sw.Write(pageHeader);   //SEND HTML PAGE 
            sw.Write(this.outputString);
            sw.Flush();     //FLUSH THE CURRENT STREAM
            //LOG DOWN THE EVENT USING LOGGER CLASS INTO LOG FILE
            Logger.CreateLog(Environment.NewLine + pageHeader); 
        }
        /*================================================================================================
        *  Function    : TxtMessage
        *  Description : This function will send a plain txt msg to the client
        *  Parameters  : Nothing
        *  Returns     : Nothing as return type is void
        ================================================================================================*/
        void TxtMessage()
        {
            string webpage;
            NetworkStream ns = myClient.GetStream();
            using (StreamReader sr = new StreamReader(this.lFolder, Encoding.UTF8))
            {
                this.outputString = sr.ReadToEnd();
            }
            StreamWriter sw = new StreamWriter(ns);

            webpage = (Ok);
            webpage = webpage + (Environment.NewLine);
            webpage = webpage + ("Content-Type: " + TXT_M + SET_CHAR + Environment.NewLine);
            webpage = webpage + ("Content-Length: " + this.outputString.Length);
            webpage = webpage + (Environment.NewLine);
            webpage = webpage + (Environment.NewLine);
            //WRITING ON THE CURRENT STREAM
            sw.Write(webpage); //SEND PALIN TEXT MESSAGE
            sw.Write(this.outputString);
            sw.Flush();     //FLUSH THE CURRENT STREAM
            //LOG DOWN THE EVENT USING LOGGER CLASS INTO LOG FILE
            Logger.CreateLog(webpage);
        }
        /*================================================================================================
        *  Function    : Jimage
        *  Description : This function will send a image  with .jpeg extention file
        *  Parameters  : Nothing
        *  Returns     : Nothing as return type is void
        ================================================================================================*/
        void Jimage()
        {
            string outString;
            NetworkStream ns = myClient.GetStream();
            byte[] image = File.ReadAllBytes(this.lFolder);
            BinaryWriter sw = new BinaryWriter(ns);
            outString = (Ok + Environment.NewLine);
            outString = outString + ("Content-Type: " + JPG_M + Environment.NewLine);
            outString = outString + ("Content-Length: " + image.Length);
            outString = outString + (Environment.NewLine);
            outString = outString + (Environment.NewLine);
            //WRITING ON THE CURRENT STREAM
            sw.Write(outString);
            sw.Write(image); //SEND IMAGE ON THE STREAM
            sw.Flush();     //FLUSH THE CURRENT STREAM
            //LOG DOWN THE EVENT USING LOGGER CLASS INTO LOG FILE
            Logger.CreateLog(outString);
        }
        /*================================================================================================
        *  Function    : Gimage
        *  Description : This function will send a image  with .jpeg extention file
        *  Parameters  : Nothing
        *  Returns     : Nothing as return type is void
        ================================================================================================*/
        void Gimage()
        {
            string outString;
            NetworkStream ns = myClient.GetStream();
            byte[] image = File.ReadAllBytes(this.lFolder);
            BinaryWriter sw = new BinaryWriter(ns);
            outString = (Ok + Environment.NewLine);
            outString = outString + ("Content-Type: " + GIF_M + Environment.NewLine);
            outString = outString + ("Content-Length: " + image.Length);
            outString = outString + (Environment.NewLine);
            outString = outString + (Environment.NewLine);
            //WRITING ON THE CURRENT STREAM
            sw.Write(outString);
            sw.Write(image);// SEND THE GIF ON STREAM
            sw.Flush();     //FLUSH THE CURRENT STREAM
            //LOG DOWN THE EVENT USING LOGGER CLASS INTO LOG FILE
            Logger.CreateLog(outString);
        }
       
    }
}

