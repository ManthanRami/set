﻿/*
*  FILE          : logger.cs
*  PROJECT       : PROG2001-Web Development: A-07 : Developing your own Web Server 
*  PROGRAMMER    : Jayson Ovishek Biswas & Manthan Rami
*  FIRST VERSION : November 30 2019
*  DESCRIPTION   : This file contains the logic of creating proper log file with appropriate format of log details.	
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.CompilerServices;
using System.Diagnostics;



namespace MyWebServer
{
    public static class Logger
    {
        const string FORMAT      = "yyyy-MM-dd hh:mm:ss";   //proper format of date and time string
        const string DATE_FORMAT = "yyyy-MM-dd";    // proper format for the date
        const string FILE_EXTEN  = ".log";          // file extenstion used for log file
        const string FILE_NAME   = "myOwnWebServer";   //name of the file
        const string DELEMETER   = " - ";               //delemeter to seprate data for log string 

        /*================================================================================================
        *  Function    : FileNameGenerate
        *  Description : This function will generate the file name according 
        *                to to the current datae and time
        *  Parameters  : Nothing
        *  Returns     : string fname: as file name
        ================================================================================================*/
        static private string FileNameGenerate()
        {
            string fName = FILE_NAME;   //file name
            fName += DateTime.Today.ToString(DATE_FORMAT);  //add date and time with appropriate format
            fName += FILE_EXTEN;    //inculding extention for log file 
            return fName;
        }
        /*================================================================================================
        *  Function    : DateAndTime
        *  Description : This function will get current time and date in appropriate format.
        *  Parameters  : Nothing
        *  Returns     : string time : as current time and date
        ================================================================================================*/
        static private string DateAndTime()
        {
            string time = DateTime.Now.ToString(FORMAT);    //get the date and time with proper formate
            return time;    //return date and time
        }
        /*================================================================================================
        *  Function    : CreateLogString
        *  Description : This function will genrate the log string with all details.
        *  Parameters  : Nothing
        *  Returns     : string time : as current time and date
        ================================================================================================*/
        static private string CreateLogString(string details)
        {
            string logString;     
            logString = DateAndTime();  //add date and time 
            logString += DELEMETER + details + Environment.NewLine;    //add log details and with delemeter 
            return logString;
        }
        /*================================================================================================
        *  Function    : CreateLog
        *  Description : This function will log down the details in the log file 
        *  Parameters  : Nothing
        *  Returns     : string time : as current time and date
        ================================================================================================*/
        static public void CreateLog(string details)
        {
            string logevent = CreateLogString(details);  //create log string to log down on the log file 
            string fName = FileNameGenerate();          //generate name for log file according to the current time and date
            var location = Directory.GetCurrentDirectory() + @"\log";  //loaction for tht log file
            try
            {   
                if (!Directory.Exists(location))  // check if the directory is exist or not ?
                {
                    Directory.CreateDirectory(location);    //create the directory if not found there
                }
                File.AppendAllText(Path.Combine(location, fName), logevent);
            }
            catch (Exception e)
            {             
                // haven't done anything with this exception as it is in logger class
            }
        }
    }
}
