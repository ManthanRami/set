﻿/*
*  FILE          : Program.cs
*  PROJECT       : Assignment #3  PROG-2121 Windows and Mobile Programming
*  PROGRAMMER    : Manthan Rami
*  FIRST VERSION : 2019-10-04
*  DESCRIPTION   : In this assignment we test the speed of data structure available in c# for example List, Dictionary and Hash Table
*                  There are two types of test has been done in this assignment 1) Searching data and 2) Retriving data from the data
*                  structure and observing the result. In this project we used a Text file for the string data reference is given below
*                  words_alpha.txt https://raw.githubusercontent.com/dwyl/english-words/master/words_alpha.txt 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Assignment_03
{
    class Program
    {

        static void Main(string[] args)
        {
            int loop = 0;       //used as counter
            bool found = false;     //checking if the value is found or not 
            int loopTest = 2000;    //number of times looping the test
            long maxInteger = 9000000;  //maximum number of integer datat to tbe store
            int half = (int)maxInteger / 2; //middle data of the integer data
            int last = 8999999;     //last digit of the integer data
            string inputLine = String.Empty;    //string used to get line from the file to store 
            string retriveString = String.Empty;    //string used to retrive data from the contianer 
            int retriveInt = 0;         //used for retriveing integer data

            //string data structure
            Hashtable hashTableList = new Hashtable();
            List<string> myStringList = new List<string>();
            Dictionary<string, string> myStringDictionary = new Dictionary<string, string>();

            //int data structure
            Dictionary<int, int> myIntDictionary = new Dictionary<int, int>();
            List<int> myIntList = new List<int>();
            Hashtable hashTableStringList = new Hashtable();

            //file open for reading
            FileStream textFile = new FileStream("words_alpha.txt", FileMode.Open, FileAccess.Read);
            FileStream textFile2 = new FileStream("words_alpha.txt", FileMode.Open, FileAccess.Read);
            FileStream textFile3 = new FileStream("words_alpha.txt", FileMode.Open, FileAccess.Read);

            //Stopwatch for testing time record
            Stopwatch listWatch = new Stopwatch();   //used for Integer 
            Stopwatch DictionaryWatch = new Stopwatch();   //used for Integer 
            Stopwatch hashWatch = new Stopwatch();   //used for Integer  

            Console.WriteLine("Loading data Integer data to Container.......\n");
            //                                   Loading data to List
            //=================================================================================================
            using (StreamReader textList = new StreamReader(textFile, Encoding.UTF8))
            {
                for (int i = 0; (inputLine = textList.ReadLine()) != null; i++)
                {
                    myStringList.Add(inputLine);
                }
            }
            //                                  Loading data to Dictionary
            //=================================================================================================
            using (StreamReader textDictionary = new StreamReader(textFile2, Encoding.UTF8))
            {
                for (int value = 0; (inputLine = textDictionary.ReadLine()) != null; value++)
                {
                    myStringDictionary.Add(inputLine, inputLine);
                }
            }
            //                                  Loading data to HashTable
            //=================================================================================================
            using (StreamReader textHashtable = new StreamReader(textFile3, Encoding.UTF8))
            {
                for (int key = 0; (inputLine = textHashtable.ReadLine()) != null; key++)
                {
                    hashTableStringList.Add(inputLine, inputLine);
                }
            }
            Console.WriteLine("Loading String data to Container...........\n");
            //                                   Loading data to List
            //=================================================================================================
            for (int i = 0; i <= maxInteger; i++)
            {
                myIntList.Add(i);
            }
            //                                  Loading data to Dictionary
            //=================================================================================================
            for (int data = 0; data <= maxInteger; data++)
            {
                myIntDictionary.Add(data, data);
            }
            //                                  Loading data to HashTable
            //=================================================================================================
            for (int data = 0; data <= maxInteger; data++)
            {
                hashTableList.Add(data, data);
            }
            //=============================================================================================================================================================            
            Console.WriteLine("Testing Searching Performance\n\n");

            Console.WriteLine("  \nTest 1: Searching \"1\" in list\n");
            listWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                found = myIntList.Contains(1);        //searching for 1 in List                                                 
                loop++;
            }
            listWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By list to search \"1\" :" + listWatch.Elapsed);
            listWatch.Reset();

            Console.WriteLine("  \nTest 2: Searching \"1\" in dictionary\n");
            DictionaryWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                found = myIntDictionary.ContainsKey(1);      //searching for 89919999 in List                                                 
                loop++;
            }
            DictionaryWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By Dictionary to search \"1\" :" + DictionaryWatch.Elapsed);
            DictionaryWatch.Reset();

            Console.WriteLine("  \nTest 3: Searching \"1\" in hash table\n");
            hashWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                found = hashTableList.ContainsKey(1);      //searching for 1 in List                                                 
                loop++;
            }
            hashWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By  hash table to search \"1\" :" + hashWatch.Elapsed);
            hashWatch.Reset();
            //=============================================================================================================================================================
            Console.WriteLine("  \nTest 4: Searching for \"4,500,000\" in List\n");
            listWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                found = myIntList.Contains(half);        //searching for 4,500,000 in List                                                 
                loop++;
            }
            listWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By list to search \"4500000\" :" + listWatch.Elapsed);
            listWatch.Reset();

            Console.WriteLine("  \nTest 5: Searching \"4500000\" in dictionary\n");
            DictionaryWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                found = myIntDictionary.ContainsKey(half);      //searching for 4,500,000 in List                                                 
                loop++;
            }
            DictionaryWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By Dictionary to search \"4500000\" :" + DictionaryWatch.Elapsed);
            DictionaryWatch.Reset();

            Console.WriteLine("  \nTest 6: Searching \"4500000\" in hash table\n");
            hashWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                found = hashTableList.ContainsKey(half);      //searching for 4500000 in List                                                 
                loop++;
            }
            hashWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By Hash Table to search \"4,500,000\" :" + hashWatch.Elapsed);
            hashWatch.Reset();

            //=============================================================================================================================================================

            Console.WriteLine("  \nTest 7: Searching for \"8999999\" in List\n");
            listWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                found = myIntList.Contains(last);        //searching for 1 in List                                                 
                loop++;
            }
            listWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By list to search \"8999999\" :" + listWatch.Elapsed);
            listWatch.Reset();

            Console.WriteLine("  \nTest 8: Searching \"8999999\" in dictionary\n");
            DictionaryWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                found = myIntDictionary.ContainsKey(last);      //searching for 8999999 in List                                                 
                loop++;
            }
            DictionaryWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By Dictionary to search \"8999999\" :" + DictionaryWatch.Elapsed);
            DictionaryWatch.Reset();

            Console.WriteLine("  \nTest 9: Searching \"8999999\" in hash table\n");
            hashWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                found = hashTableList.ContainsKey(last);      //searching for 8999999 in List                                                 
                loop++;
            }
            hashWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By Hash Table to search \"8999999\" :" + hashWatch.Elapsed);
            hashWatch.Reset();
            //=============================================================================================================================================================
            Console.WriteLine("\nTesting Retriving Performance\n\n");

            Console.WriteLine("  \nTest 10: Retriveing \"1\" from List\n");
            listWatch.Start();
            while (loop < loopTest)          //searching for 20000 time for increaseing the milliseconds 
            {

                retriveInt = myIntList.Find(i => i == 1);
                loop++;
            }
            listWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By list to retrive \"1\" :" + listWatch.Elapsed);
            listWatch.Reset();

            Console.WriteLine("  \nTest 11: Retriveing \"1\" from Dictionary\n");
            DictionaryWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                retriveString = myIntDictionary[1].ToString();
                loop++;
            }
            DictionaryWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By Dictionary  \"1\" :" + DictionaryWatch.Elapsed);
            DictionaryWatch.Reset();

            Console.WriteLine("  \nTest 12: Searching for \"1\" in Hash Table");
            hashWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                retriveString = hashTableList[1].ToString();
                loop++;
            }
            hashWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By Hash Table to retrive \"1\" :" + hashWatch.Elapsed);
            hashWatch.Reset();
            //=============================================================================================================================================================
            Console.WriteLine("  \nTest 13: Retriveing \"4500000\" from List\n");
            listWatch.Start();
            while (loop < loopTest)          //searching for 20000 time for increaseing the milliseconds 
            {
                retriveInt = myIntList.Find(i => i == half);
                loop++;
            }
            listWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By list to retrive \"4500000\" :" + listWatch.Elapsed);
            listWatch.Reset();

            Console.WriteLine("  \nTest 14: Retriveing \"4500000\" from Dictionary\n");
            DictionaryWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                retriveString = myIntDictionary[half].ToString();
                loop++;
            }
            DictionaryWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By Dictionary to retrive \"4500000\" :" + DictionaryWatch.Elapsed);
            DictionaryWatch.Reset();

            Console.WriteLine("  \nTest 15: Searching for \"4500000\" in Hash Table");
            hashWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                retriveString = hashTableList[half].ToString();
                loop++;
            }
            hashWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By Hash Table to retrive \"4500000\" :" + hashWatch.Elapsed);
            hashWatch.Reset();
            //=============================================================================================================================================================
            Console.WriteLine("  \nTest 16: Retriveing \"8999999\" from List\n");
            listWatch.Start();
            while (loop < loopTest)          //searching for 20000 time for increaseing the milliseconds 
            {
                retriveInt = myIntList.Find(i => i == last);
                loop++;
            }
            listWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By list to retrive \"8999999\" :" + listWatch.Elapsed);
            listWatch.Reset();

            Console.WriteLine("  \nTest 17: Retriveing \"8999999\" from Dictionary\n");
            DictionaryWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                retriveString = myIntDictionary[last].ToString();
                loop++;
            }
            DictionaryWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By Dictionary to retrive \"8999999\" :" + DictionaryWatch.Elapsed);
            DictionaryWatch.Reset();

            Console.WriteLine("  \nTest 18: Searching for \"8999999\" in Hash Table");
            hashWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                retriveString = hashTableList[last].ToString();
                loop++;
            }
            loop = 0;
            hashWatch.Stop();
            Console.WriteLine("      Time taken By Hash Table to retrive \"8999999\" :" + hashWatch.Elapsed);
            hashWatch.Reset();
            //=============================================================================================================================================================

            Console.WriteLine("Testing Searching Performance : String data type\n\n");
            Console.WriteLine("  \nTest 19: Searching \"aa\" in list\n");
            listWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                found = myStringList.Contains("aa");        //searching for 1 in List                                                 
                loop++;
            }
            listWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By list to search \"aa\" :" + listWatch.Elapsed);
            listWatch.Reset();

            Console.WriteLine("  \nTest 20: Searching \"aa\" in dictionary\n");
            DictionaryWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                found = myStringDictionary.ContainsKey("aa");      //searching for 89919999 in List                                                 
                loop++;
            }
            DictionaryWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By Dictionary to search \"aa\" :" + DictionaryWatch.Elapsed);
            DictionaryWatch.Reset();


            Console.WriteLine("  \nTest 21: Searching \"aa\" in hash table\n");
            hashWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                found = hashTableStringList.ContainsKey("aa");      //searching for 1 in List                                                 
                loop++;
            }
            hashWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By  hash table to search \"aa\" :" + hashWatch.Elapsed);
            hashWatch.Reset();
            //=============================================================================================================================================================

            Console.WriteLine("Testing Searching Performance : String data type\n\n");
            Console.WriteLine("  \nTest 22: Searching \"metroliner\" in list\n");
            listWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                found = myStringList.Contains("metroliner");        //searching for 1 in List                                                 
                loop++;
            }
            listWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By list to search \"metroliner\" :" + listWatch.Elapsed);
            listWatch.Reset();

            Console.WriteLine("  \nTest 23: Searching \"metroliner\" in dictionary\n");
            DictionaryWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                found = myStringDictionary.ContainsKey("metroliner");      //searching for 89919999 in List                                                 
                loop++;
            }
            DictionaryWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By Dictionary to search \"metroliner\" :" + DictionaryWatch.Elapsed);
            DictionaryWatch.Reset();


            Console.WriteLine("  \nTest 24: Searching \"metroliner\" in hash table\n");
            hashWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                found = hashTableStringList.ContainsKey("metroliner");      //searching for 1 in List                                                 
                loop++;
            }
            hashWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By  hash table to search \"metroliner\" :" + hashWatch.Elapsed);
            hashWatch.Reset();
            //=============================================================================================================================================================

            Console.WriteLine("  \nTest 25: Searching \"zwitterion\" in list\n");
            listWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                found = myStringList.Contains("zwitterion");        //searching for 1 in List                                                 
                loop++;
            }
            listWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By list to search \"aa\" :" + listWatch.Elapsed);
            listWatch.Reset();

            Console.WriteLine("  \nTest 26: Searching \"zwitterion\" in dictionary\n");
            DictionaryWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                found = myStringDictionary.ContainsKey("zwitterion");      //searching for 89919999 in List                                                 
                loop++;
            }
            DictionaryWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By Dictionary to search \"zwitterion\" :" + DictionaryWatch.Elapsed);
            DictionaryWatch.Reset();


            Console.WriteLine("  \nTest 27: Searching \"zwitterion\" in hash table\n");
            hashWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                found = hashTableStringList.ContainsKey("zwitterion");      //searching for 1 in List                                                 
                loop++;
            }
            hashWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By  hash table to search \"zwitterion\" :" + hashWatch.Elapsed);
            hashWatch.Reset();
            //=============================================================================================================================================================


            Console.WriteLine("Testing Retriving Performance : String data type\n\n");

            Console.WriteLine("  \nTest 28: Retriveing \"aa\" from List\n");
            listWatch.Start();
            while (loop < loopTest)          //searching for 20000 time for increaseing the milliseconds 
            {

                retriveString = myStringList.Find(i => i == "aa");
                loop++;
            }
            listWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By list to retrive \"aa\" :" + listWatch.Elapsed);
            listWatch.Reset();

            Console.WriteLine("  \nTest 29: Retriveing \"aa\" from Dictionary\n");
            DictionaryWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                retriveString = myStringDictionary["aa"];
                loop++;
            }
            DictionaryWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By Dictionary  \"aa\" :" + DictionaryWatch.Elapsed);
            DictionaryWatch.Reset();

            Console.WriteLine("  \nTest 30: Searching for \"aa\" in Hash Table");
            hashWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                retriveString = hashTableStringList["aa"].ToString();
                loop++;
            }
            hashWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By Hash Table to retrive \"aa\" :" + hashWatch.Elapsed);
            hashWatch.Reset();
            //=============================================================================================================================================================
            Console.WriteLine("  \nTest 31: Retriveing \"metroliner\" from List\n");
            listWatch.Start();
            while (loop < loopTest)          //searching for 20000 time for increaseing the milliseconds 
            {
                retriveString = myStringList.Find(i => i == "metroliner");
                loop++;
            }
            listWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By list to retrive \"metroliner\" :" + listWatch.Elapsed);
            listWatch.Reset();

            Console.WriteLine("  \nTest 32: Retriveing \"metroliner\" from Dictionary\n");
            DictionaryWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                retriveString = myStringDictionary["metroliner"];
                loop++;
            }
            DictionaryWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By Dictionary to retrive \"metroliner\" :" + DictionaryWatch.Elapsed);
            DictionaryWatch.Reset();

            Console.WriteLine("  \nTest 33: Searching for \"metroliner\" in Hash Table");
            hashWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                retriveString = hashTableStringList["metroliner"].ToString();
                loop++;
            }
            hashWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By Hash Table to retrive \"metroliner\" :" + hashWatch.Elapsed);
            hashWatch.Reset();
            //=============================================================================================================================================================
            Console.WriteLine("  \nTest 34: Retriveing \"zwitterion\" from List\n");
            listWatch.Start();
            while (loop < loopTest)          //searching for 20000 time for increaseing the milliseconds 
            {
                retriveString = myStringList.Find(i => i == "zwitterion");
                loop++;
            }
            listWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By list to retrive \"zwitterion\" :" + listWatch.Elapsed);
            listWatch.Reset();

            Console.WriteLine(" \nTest 35: Retriveing \"zwitterion\" from Dictionary\n");
            DictionaryWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                retriveString = myStringDictionary["zwitterion"];
                loop++;
            }
            DictionaryWatch.Stop();
            loop = 0;
            Console.WriteLine("      Time taken By Dictionary to retrive \"zwitterion\" :" + DictionaryWatch.Elapsed);
            DictionaryWatch.Reset();

            Console.WriteLine("  \nTest 36: Searching for \"zwitterion\" in Hash Table");
            hashWatch.Start();
            while (loop < loopTest)    //searching for 20000 time for increaseing the milliseconds 
            {
                retriveString = hashTableStringList["zwitterion"].ToString();
                loop++;
            }
            loop = 0;
            hashWatch.Stop();
            Console.WriteLine("      Time taken By Hash Table to retrive \"zwitterion\" :" + hashWatch.Elapsed);
            hashWatch.Reset();
            //=============================================================================================================================================================


        }
    }
}
