﻿/*
*   FILE : MainWindow.xaml.cs
*   PROJECT : PROG2121 - Assignment #2
*   PROGRAMMER : Manthan Rami
*   FIRST VERSION : 2019-09-27
*   DESCRIPTION :
*   In this file containt of back_End logic of My Notepad application bulit in WPF using C# 
*   language is has basic functionality such as "New", Open, SaveAs and Close.
*/
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyTextEditor
{
    /*
    * NAME : Notepad
    * PURPOSE : The role of this class is to create an text editor layout for the user.
    */
    public partial class Notepad : Window
    {
        public bool AnyChange = false;      //for checking if there is any change in text file
        public bool FileSave = false;       //For checking if the current file is saved or not?
        public string filename = "";        //for saving file name
        public const string defaultName = "Document";   //default name of the file during save
        public string WHichFunction = "";           //identifier which function is calling savePrompt function
        public bool retcode = false;                //signal to check if user press NO while opening new file without saving current file

        /*====================================================================================================================================
        *  Function    : Notepad
        *  Description : This is CONSTRUCTION which initialize new Notepad window for the user.
        *  Parameters  : Nothing
        *  Returns     : Nothing
        =======================================================================================================================================*/
        public Notepad()
        {
            InitializeComponent();
        }
        /*====================================================================================================================================
        *  Function    : SaveAs
        *  Description : This function will save the file which has been created in myNotepad.
        *  Parameters  : object     sender: 
        *                RoutedEventArgs    e: 
        *  Returns     : Nothing
        *  Refrence     : Stackoverflow(https://stackoverflow.com/questions/12618180/how-to-save-the-content-of-textbox-into-a-textfile)
        =======================================================================================================================================*/
        public void SaveAs(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.FileName = filename;
            dlg.DefaultExt = ".txt"; 
            dlg.Filter = "Text documents (.txt)|*.txt"; 
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)     //check if user has save file successfully or  not
            {
                // SaveAs document               
                filename = dlg.SafeFileName;
                FileSave = true;
                File.WriteAllText(dlg.FileName, textEditor.Text);
                 MyNotepad.Title = filename+"- MyNotepad";
            }
        }
        /*==========================================================================================================
        *  Function    : Open
        *  Description : This function will decide when to open a dialog box and it works depends on condition
        *  Parameters  : object     sender: 
        *                RoutedEventArgs    e: 
        *  Returns     : Nothing
        ============================================================================================================*/
        public void Open(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            while (true)
            {
                if (FileSave == true)   //file is  not saved
                {
                    OpenFile(dlg);
                    break;
                }
                if (textEditor.Text == "")  //text editor is empty
                {
                    OpenFile(dlg);
                    break;
                }
                if (filename == "" && AnyChange) //new file
                {
                    //ask for save
                    WHichFunction = "Open";
                    PromptUser(sender, e, dlg);
                    break;
                }
                if (filename != "" && !AnyChange)    //just open file
                {
                    //open dialog
                    OpenFile(dlg);
                    break;
                }
                if (filename != "" && AnyChange)    //if file is not saved and has any changes
                {
                    //ask for save
                    if (retcode)
                    {
                        OpenFile(dlg);
                        break;
                    }
                    WHichFunction = "Open";
                    PromptUser(sender, e, dlg);
                    break;
                }
            }
        }
        /*==========================================================================================================
        *  Function    : New
        *  Description : This function will create a new blank text file to write on it .
        *  Parameters  : object     sender: 
        *                RoutedEventArgs    e: 
        *  Returns     : Nothing
        ============================================================================================================*/
        public void New(object sender, RoutedEventArgs e)
        {
            while (true)
            {
                WHichFunction = "New";
                if (textEditor.Text == "")  //text editor is empty
                {
                    //new file
                    UpdateNewFile();
                    break;
                }
                if (FileSave == true)      //file is   saved
                {
                    //new file
                    UpdateNewFile();
                    break;
                }
                if (FileSave == false)      //file is  not saved
                {
                    SaveFileDialog dlg = new SaveFileDialog();
                    PromptUser(sender, e, dlg);
                    if (FileSave == true)
                    {
                        UpdateNewFile();
                        break;
                    }
                    break;
                }
            }
        }
        /*==========================================================================================================
        *  Function    : Close
        *  Description : This function will close the current file and ask for saving file file is not saved.
        *  Parameters  : object     sender: 
        *                RoutedEventArgs    e: 
        *  Returns     : Nothing
        ============================================================================================================*/
        public void Close(object sender, RoutedEventArgs e)
        {
            if (FileSave == false)      //file is  not saved
            {
                SaveFileDialog dlg = new SaveFileDialog();
                PromptUser(sender, e, dlg);
            }
            if (FileSave == true)       //file is saved
            {
                Close();
            }
        }
        /*==========================================================================================================
        *  Function    : textBox_TextChanged
        *  Description : This function will change character counts and change value of variable which check for is 
        *                file is saved or not or changed or not.
        *  Parameters  : object     sender: 
        *                EventArgs    e: 
        *  Returns     : Nothing
        ============================================================================================================*/
        private void textBox_TextChanged(object sender, EventArgs e)
        {
            AnyChange = true;
            FileSave = false;
            count.Text = "Character " + textEditor.Text.Length; //update count of character
        }
        /*==========================================================================================================
        *  Function    : PromptUser
        *  Description : This function will Open a prompt user for saving file and works on some condition.
        *                with it.
        *  Parameters  : object     sender: 
        *                RoutedEventArgs    e: 
        *                FileDialog     dlg:
        *  Returns     : Nothing
        ============================================================================================================*/
        public void PromptUser(object sender, RoutedEventArgs e, FileDialog dlg)
        {
            MessageBoxResult result = MessageBox.Show("Do you want to save file ?", "MyNotepad", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    SaveAs(sender, e);
                    break;
                case MessageBoxResult.No:       //call  function upon which function promptuser is called 
                    if (WHichFunction == "Open")   //called from Open function
                    {
                        retcode = true;
                        Open(sender, e);
                    }
                    if (WHichFunction == "Close")   //called from Close function
                    {
                        Close();
                    }
                    if (WHichFunction == "New")   //called from New function
                    {
                        textEditor.Text = "";
                        dlg.FileName = defaultName;
                    }
                    break;
                case MessageBoxResult.Cancel:
                    break;
            }
        }
        /*==========================================================================================================
        *  Function    : OpenFile
        *  Description : This function will Open a opendialog box from which user can open a text file and can play 
        *                with it.
        *  Parameters  : object     sender: 
        *                RoutedEventArgs    e: 
        *  Returns     : Nothing
        ============================================================================================================*/
        public void OpenFile(OpenFileDialog dlg)
        {
            dlg.DefaultExt = ".txt";
            dlg.Filter = "Text documents| *.txt|All files (*.*)|*.*";
            if (dlg.ShowDialog() == true)
            {
                textEditor.Text = File.ReadAllText(dlg.FileName);
                filename = dlg.SafeFileName;
                FileSave = true;
                AnyChange = false;
                MyNotepad.Title = filename+"-MyNotepad";
            }
        }
        /*==========================================================================================================
        *  Function    : UpdateNewFile
        *  Description : This function will update information of the file 
        *  Parameters  : object     sender: 
        *                RoutedEventArgs    e: 
        *  Returns     : Nothing
        ============================================================================================================*/
        public void UpdateNewFile()
        {
            textEditor.Text = "";   //set texteditor empty if new window is opened
            FileSave = false;       //set filesave to false
            filename = defaultName; //set default name 
        }
        /*==========================================================================================================
        *  Function    : About()
        *  Description : This function will display a about window containing information of the application
        *  Parameters  : object     sender: 
        *                RoutedEventArgs    e: 
        *  Returns     : Nothing
        ============================================================================================================*/
        public void About(object sender, RoutedEventArgs e)
        {
            AboutBox1 info = new AboutBox1();   //initialize new window
            info.ShowDialog();      //display About window
        }        
    }
}
