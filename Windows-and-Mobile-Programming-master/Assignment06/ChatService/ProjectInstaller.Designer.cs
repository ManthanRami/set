﻿namespace ChatService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ChatServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.ChatServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // ChatServiceProcessInstaller
            // 
            this.ChatServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.ChatServiceProcessInstaller.Password = null;
            this.ChatServiceProcessInstaller.Username = null;
            // 
            // ChatServiceInstaller
            // 
            this.ChatServiceInstaller.ServiceName = "MyServer";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.ChatServiceProcessInstaller,
            this.ChatServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller ChatServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller ChatServiceInstaller;
    }
}