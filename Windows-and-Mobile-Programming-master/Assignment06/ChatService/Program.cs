﻿/*
* FILE : Program.cs
* PROJECT : server
* PROGRAMMER : Manthan Rami, Jayson Ovishek Biswas
* UPDATED VERSION : 2019-11-08
* DESCRIPTION : This program is main function for this project to run this service .
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace ChatService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new ChatService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
