﻿/*
* FILE : Server.cs
* PROJECT : server
* PROGRAMMER : Manthan Rami, Jayson Ovishek Biswas
* UPDATED VERSION : 2019-11-08
* DESCRIPTION : This program is a server for clients to connect. It parses an ip address and waits for the cilents to connect.
*               When a client is connected the program shows that the client is connected in the console. The job of this server is to
*               connect the clients successfully and recieve message from a client, convert them into bytes and send the message back to 
*               all the clients that are connected. This server supports more than 2 clients.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace ChatService
{
    public class Server
    {
        public static List<NetworkStream> clients = new List<NetworkStream>();
        TcpListener server = null;
        public static volatile bool pause = false;
        public void ConnectIt()
        {

            try
            {
                // Set the TcpListener on port 13000.
                Int32 port = 13000;
                IPAddress localAddr = IPAddress.Parse("127.0.0.1");

                // TcpListener server = new TcpListener(port);
                server = new TcpListener(localAddr, port);

                // Start listening for client requests.
                server.Start();


                // Enter the listening loop.
                while (true)
                {

                    // Perform a blocking call to accept requests.
                    // You could also user server.AcceptSocket() here.
                    TcpClient client = server.AcceptTcpClient();
                    ParameterizedThreadStart ts = new ParameterizedThreadStart(Worker);
                    Thread clientThread = new Thread(ts);
                    clientThread.Start(client);
                    clients.Add(client.GetStream());

                }
            }
            catch (SocketException e)
            {
                using (StreamWriter w = File.AppendText(AppDomain.CurrentDomain.BaseDirectory + "log.txt"))
                {
                    w.WriteLine($" Socket Exception : {e} : {DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()}");
                }
            }
            finally
            {
                // Stop listening for new clients.
                server.Stop();
            }
        }
        /*==================================================================================================================================== 
        *  Function    : Worker 
        *  Description : This is WORKER THREAD WHICH CONTINUESLY LISTEN TO THE CLIENT IF CLIENTS SENDS ANY MSG OR NOT
        *  Parameters  : OBJECT O
        *  Returns     : Nothing
        =======================================================================================================================================*/
        public static void Worker(Object o)
        {
            TcpClient client = (TcpClient)o;


            if (pause == false)
            {
                // Buffer for reading data
                Byte[] bytes = new Byte[256];
                String data = null;

                data = null;

                // Get a stream object for reading and writing
                NetworkStream stream = client.GetStream();


                int i;

                // Loop to receive all the data sent by the client.
                while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                {
                    // Translate data bytes to a ASCII string.
                    data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
                    Console.WriteLine("Received: {0}", data);

                    // Process the data sent by the client.
                    //data = data.ToUpper();

                    //for disonnect
                    string output = data.Substring(data.IndexOf(':'));
                    string name = data.Substring(0, data.IndexOf(':'));
                    name = name.Trim();

                    byte[] msg = System.Text.Encoding.ASCII.GetBytes(data);

                    foreach (NetworkStream s in clients)
                    {
                        // Send back a response.
                        s.Write(msg, 0, msg.Length);

                        Console.WriteLine("Sent: {0}", data);
                    }
                    if (output == ": Just Joined Chat !!") //if disconnect message is received close client
                    {
                        using (StreamWriter w = File.AppendText(AppDomain.CurrentDomain.BaseDirectory + "log.txt"))
                        {
                            w.WriteLine($"Connected : {name} : {DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()}");
                        }
                    }


                    else if (output == ": Just Left Chat !!") //if disconnect message is received close client
                    {
                        using (StreamWriter w = File.AppendText(AppDomain.CurrentDomain.BaseDirectory + "log.txt"))
                        {
                            w.WriteLine($"Disconnected : {name} : {DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()}");
                        }
                        clients.Remove(stream);
                        break;
                    }

                    else
                    {
                        using (StreamWriter w = File.AppendText(AppDomain.CurrentDomain.BaseDirectory + "log.txt"))
                        {
                            w.WriteLine($"A message was Sent From : {name} : {DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()}");
                        }
                    }


                }
            }
            // Shutdown and end connection
            client.Close();
        }
        /*====================================================================
        *  Function    : pause
        *  Description : Purpose of this method is to pause Myserver service
        *  Parameters  : Nothing
        *  Returns     : Nothing
        ======================================================================*/
        public void Pause()
        {
            pause = true;
        }
        /*====================================================================
        *  Function    : Unpause
        *  Description : Purpose of this method is to resume Myserver service
        *  Parameters  : Nothing
        *  Returns     : Nothing
        ======================================================================*/
        public void Unpause()
        {
            pause = false;
        }
        /*====================================================================
        *  Function    : Closer
        *  Description : Purpose of this method is to Stop MyServer service
        *  Parameters  : Nothing
        *  Returns     : Nothing
        ======================================================================*/
        public void Closer()
        {
            clients.Clear();
            try
            {
                server.Stop();
            }
            catch (Exception)
            {
                using (StreamWriter w = File.AppendText(AppDomain.CurrentDomain.BaseDirectory + "log.txt"))
                {
                    w.WriteLine("There was a problem stopping");
                }
            }

            return;
        }
    }

}
