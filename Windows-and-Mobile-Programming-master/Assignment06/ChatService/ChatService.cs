﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;

namespace ChatService
{
    public partial class ChatService : ServiceBase
    {
        Thread server;
        Server chatService;
        string str = AppDomain.CurrentDomain.BaseDirectory + "log.txt";

        public ChatService()
        {
            CanPauseAndContinue = true;
            InitializeComponent();
        }

        protected override void OnStart(string[] args)

        {

            chatService = new Server();
            server = new Thread(new ThreadStart(chatService.ConnectIt));
            server.Start();
            using (StreamWriter w = File.AppendText(str))
            {
                w.WriteLine($"Server Connected : {DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()}");
            }
        }

        protected override void OnContinue()
        {
            chatService.Unpause();
            using (StreamWriter w = File.AppendText(str))
            {
                w.WriteLine($"Server Paused : {DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()}");
            }
        }

        protected override void OnPause()
        {
            chatService.Pause();
            using (StreamWriter w = File.AppendText(str))
            {
                w.WriteLine($"Server Resumed : {DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()}");
            }
        }
        protected override void OnStop()
        {

            chatService.Closer();

            using (StreamWriter w = File.AppendText(str))
            {
                w.WriteLine($"Server Disconnected : {DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()}");
            }
        }
    }
}
