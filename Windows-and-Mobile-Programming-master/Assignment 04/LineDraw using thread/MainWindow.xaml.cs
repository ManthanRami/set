﻿/*
*   FILE : MainWindow.xaml.cs
*   PROJECT : PROG2121 - Assignment #4
*   PROGRAMMER : Manthan Rami
*   FIRST VERSION : 2019-10-18
*   DESCRIPTION :
*   In this file containt of back_End logic Line which are working on the UI of this application It cotaint all the function defination 
    which is used to complete all the function of this application. 
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LineDraw_using_thread
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window
    {
        /*
        * NAME : MainWindow
        * PURPOSE : The create a new window for user to interact with the application.
        */
        private static List<Thread> threadList = new List<Thread>();
        private static List<Line> lineList = new List<Line>();
        private static List<Line> MovelineList = new List<Line>();
        private int counter = 0;
        volatile bool linesMove;
        volatile bool runState;
        /*==================================================================================================================================== 
        *  Function    : MainWindow
        *  Description : This is CONSTRUCTION which initialize new application window for the user.
        *  Parameters  : Nothing
        *  Returns     : Nothing
        =======================================================================================================================================*/
        public MainWindow()
        {
            InitializeComponent();
        }
        /*====================================================================================================================================
         *  Function    : BtnStart_Click
         *  Description : This function will create a new line with tral every time it is called.
         *  Parameters  : object     sender: 
         *                RoutedEventArgs    e: 
         *  Returns     : Nothing
         =======================================================================================================================================*/
        private void BtnStart_Click(object sender, RoutedEventArgs e)
        {
            linesMove = true;
            runState = true;

            // New Lines can only be added if the program is not paused
            if (runState)
            {
                Line newLine = drawLine();
                Thread trailStart = new Thread(new ParameterizedThreadStart(CreatTail));
                trailStart.Start(newLine);
                //Thread move = new Thread(new ParameterizedThreadStart(LineMover));
                //move.Start(newLine);
                lineList.Add(newLine);
                //threadList.Add(move);
            }
        }
        /*====================================================================================================================================
        *  Function    : LineMover
        *  Description : This function will make all the line move between the canvas.
        *  Parameters  : object  l: 
        *  Returns     : Nothing
        =======================================================================================================================================*/
        public void LineMover(object l)
        {
            bool one = false;
            bool two = false;
            bool three = false;
            bool four = false;
            Line newLine = (Line)l;

            while (linesMove)
            {
                if (!runState)
                {
                    try
                    {
                        Thread.Sleep(Timeout.Infinite);
                    }
                    catch (ThreadInterruptedException e)
                    {
                        // handling this exception is still WIP
                        Console.WriteLine("{0}", e);
                    }
                }
                this.Dispatcher.Invoke(() =>
                {

                    output.Text = FunCanvas.ActualWidth.ToString() + " " + FunCanvas.ActualHeight.ToString();
                    
                    if (!one)
                    {
                        newLine.X1 += 1;
                        if (newLine.X1 == FunCanvas.ActualWidth)
                        {
                            one = true;
                        }
                    }
                    else
                    {
                        newLine.X1 -= 1;
                        if (newLine.X1 == 0)
                        {
                            one = false;
                        }
                    }
                    if (!two)
                    {
                        newLine.Y1 += 1;
                        if (newLine.Y1 == FunCanvas.ActualHeight)
                        {
                            two = true;
                        }
                    }
                    else
                    {
                        newLine.Y1 -= 1;
                        if (newLine.Y1 == 0)
                        {
                            two = false;
                        }
                    }

                    if (!three)
                    {
                        newLine.X2 += 1;
                        if (newLine.X2 == FunCanvas.ActualWidth)
                        {
                            three = true;
                        }
                    }
                    else
                    {
                        newLine.X2 -= 1;
                        if (newLine.X2 == 0)
                        {
                            three = false;
                        }
                    }
                    if (!four)
                    {
                        newLine.Y2 += 1;
                        if (newLine.Y2 == FunCanvas.ActualHeight)
                        {
                            four = true;
                        }
                    }
                    else
                    {
                        newLine.Y2 -= 1;
                        if (newLine.Y2 == 0)
                        {
                            four = false;
                        }
                    }
                });
                Thread.Sleep(1);
            }
        }
        /*====================================================================================================================================
        *  Function    : drawLine
        *  Description : This function will called in start button fuction and create new line on canvas 
        *  Parameters  : Nothing 
        *  Returns     : Line Object
        =======================================================================================================================================*/
        public Line drawLine()
        {            
            Stick newLine = new Stick(FunCanvas.ActualWidth, FunCanvas.ActualHeight);
            Line temp = newLine.CreateLine();
            this.Dispatcher.Invoke(() =>
            {
                this.FunCanvas.Children.Add(temp);
            });
            return temp;
        }
        /*====================================================================================================================================
          *  Function    : drawLine
          *  Description : This function will called in creat tail function which create line with appropriate value of co-ordinates and color
          *  Parameters  : x1 :   Co-ordinates of the line
          *                y1 :   Co-ordinates of the line
          *                x2 :   Co-ordinates of the line
          *                y2 :   Co-ordinates of the line
          *             color :   Color of the line to be created for tail
          *  Returns     : Line Object
          =======================================================================================================================================*/
        public Line DrawLine(double x1, double y1, double x2, double y2,Brush color)
        {

            this.Dispatcher.Invoke(() =>
            {
                Stick newLine = new Stick(FunCanvas.ActualWidth, FunCanvas.ActualHeight);
                Line temp = new Line();
                temp = newLine.CreateLine(x1, y1, x2, y2,color);
                this.FunCanvas.Children.Add(temp);
                MovelineList.Add(temp);

            });
            return MovelineList[counter];
        }


        /*====================================================================================================================================
      *  Function    : BtnStop_Click
      *  Description : This function will deleteall the line with tails from the canvas
      *  Parameters  : object     sender: 
      *                RoutedEventArgs    e: 
      *  Returns     : Nothing
      =======================================================================================================================================*/
        private void BtnStop_Click(object sender, RoutedEventArgs e)
        {
            linesMove = false;
            for (int i = 0; i < lineList.Count; i++)
            {
                FunCanvas.Children.Remove(lineList[i]);
            }
            for (int i = 0; i < MovelineList.Count; i++)
            {
                FunCanvas.Children.Remove(MovelineList[i]);
            }
        }
        /*====================================================================================================================================
        *  Function    : BtnPause_Click
        *  Description : This function will pause all the moving line with tail on th canvas
        *  Parameters  : object     sender: 
        *                RoutedEventArgs    e: 
        *  Returns     : Nothing
        =======================================================================================================================================*/
        private void BtnPause_Click(object sender, RoutedEventArgs e)
        {
            runState = false;
        }
        /*====================================================================================================================================
        *  Function    : BtnResume_Click
        *  Description : This function will resume all the moving line  with tail which are paused on th canvas
        *  Parameters  : object     sender: 
        *                RoutedEventArgs    e: 
        *  Returns     : Nothing
        =======================================================================================================================================*/
        private void BtnResume_Click(object sender, RoutedEventArgs e)
        {
            if (!runState)
            {
                foreach (Thread t in threadList)
                {
                    t.Interrupt();
                }
            }
            runState = true;
        }

        /*====================================================================================================================================
        *  Function    : CreatTail
        *  Description : This function will take main line create tail on basis of main line co-ordinates
        *  Parameters  : object  l: 
        *  Returns     : Nothing
        =======================================================================================================================================*/
        public void CreatTail(object l)
        {
            int count = 0;
            Line TrailLine = (Line)l;
            List<Line> t = new List<Line>();
            double one = 0;
            double two = 0;
            double three = 0;
            double four = 0;
            this.Dispatcher.Invoke(() =>
            {
                one = Convert.ToInt32(TrailLine.X1);
                two = Convert.ToInt32(TrailLine.Y1);
                three = Convert.ToInt32(TrailLine.X2);
                four = Convert.ToInt32(TrailLine.Y2);
            });
            this.Dispatcher.Invoke(() =>
            {
                while (count < 5)
                {
                    Line temp = DrawLine(one, two, three, four,TrailLine.Stroke);
                    t.Add(temp);
                    count++;
                    one++;
                    two++;
                    three++;
                    four++;
                    one++;
                    two++;
                    three++;
                    four++;
                    counter++;
                }
            });
            Thread move = new Thread(new ParameterizedThreadStart(LineMover));
            move.Start(TrailLine);
            threadList.Add(move);
            for (int i = 0; i < 5; i++)
            {
                Thread moveShadow = new Thread(new ParameterizedThreadStart(LineMover));
                moveShadow.Start(t[i]);
                threadList.Add(moveShadow);
            }
        }
    }
}
