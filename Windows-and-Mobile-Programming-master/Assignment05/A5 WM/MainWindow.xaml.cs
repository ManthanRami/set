/*
* FILE : MainWindow.xaml.cs
* PROJECT : A5 WM
* PROGRAMMER : Manthan Rami, Jayson Ovishek Biswas
* UPDATED VERSION : 2019-11-01
* DESCRIPTION : This program is a client that connects to the server. The ip address that it tries to connect is 127.0.0.1(can be changed).
*               This client program can send messages to the server so that the server can process the message and send it to all other 
*               connected clients. This program requests a connection to the server. If there is any problem with the connection, it 
*               handles exception. This client program can also receive response from the server and shows that in the chat box. It can 
*               disconnect each clients from the server. After disconnecting a client can't send message anymore
*             
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace A5_WM
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {
        bool serverStart = false;
        private string username = "";       
        private string ip = "127.0.0.1";    //target ip
        bool iniConnect = false;
        private TcpClient client = new TcpClient();
        private static NetworkStream cltStream;


        public MainWindow()
        {
            InitializeComponent();
        }

        private void ConTbuttn_Click(object sender, RoutedEventArgs e)
        {        
            // if the disconnect button is clicked
            if (ConTbuttn.Content == "Disconnect")
            {
                iniConnect = true;
                msgSender(username, "Just Left Chat !!");
                ConTbuttn.Content = "Connect";
                signal.Fill = Brushes.Red;
               

            }
            else //if the connect button is clicked
            {
                Int32 port = 13000;
                try
                {
                    client.Connect(ip, port);   //request connection
                    
                    username = NameBox.Text;
                }
                catch(Exception s)
                {
                    
                    MessageBox.Show("Close the Application and try again.");
                }
               
                if (client.Connected)
                {
                    iniConnect = true;
                    msgSender(NameBox.Text, "Just Joined Chat !!");
                    ParameterizedThreadStart clientListen = new ParameterizedThreadStart(Worker);
                    Thread listen = new Thread(clientListen);
                    listen.Start();
                    SolidColorBrush mySolidColorBrush = new SolidColorBrush();
                    mySolidColorBrush.Color = Color.FromRgb(0, 255, 0);
                    signal.Fill = mySolidColorBrush;
                    ConTbuttn.Content = "Disconnect";
                }

            }
        }


        ///Worker Method: Receives response from the server, 
        //                converts it to bytes. Prints the response data got from the server
        public void Worker(Object o)
        {
            NetworkStream clt = (NetworkStream)o;

            while (true)
            {
                Byte[] data1 = new Byte[256];

                // String to store the response ASCII representation.
                String responseData = String.Empty;
                // Read the first batch of the TcpServer response bytes.
                Int32 bytes = cltStream.Read(data1, 0, data1.Length);

                responseData = System.Text.Encoding.ASCII.GetString(data1, 0, bytes);
                try
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        display.Items.Add(responseData);
                    });
                }

               catch (Exception s)
                {
                    MessageBox.Show("Done");
                    break;
                }
                    
                
            }
        }

        private void Sentbuttn_Click(object sender, RoutedEventArgs e)
        {
            msgSender(NameBox.Text, MsgBox.Text);
        }


        //Name: msgSender
        //Parameters : string name   -> username
        //             string msg    -> message that has to be sent
        //Description:
        //             connects the username and the message in a string. and sends the message to the server
        private void msgSender(string name, string msg)
        {
            if (MsgBox.Text != "" || iniConnect)
            {
                iniConnect = false;
                Byte[] data = System.Text.Encoding.ASCII.GetBytes(name + " : " + msg); //convert message string into bytes
                NetworkStream stream = client.GetStream();
                username = name;
                cltStream = stream;
                stream.Write(data, 0, data.Length); //send message and clear box
                MsgBox.Clear();
            }
        }

    }
}