﻿/*
* FILE : Program.cs
* PROJECT : server
* PROGRAMMER : Manthan Rami, Jayson Ovishek Biswas
* UPDATED VERSION : 2019-11-01
* DESCRIPTION : This program is a server for clients to connect. It parses an ip address and waits for the cilents to connect.
*               When a client is connected the program shows that the client is connected in the console. The job of this server is to
*               connect the clients successfully and recieve message from a client, convert them into bytes and send the message back to 
*               all the clients that are connected. This server supports more than 2 clients.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.ComponentModel;
using System.Data;
// The following code is extracted from the MSDN site:
// https://msdn.microsoft.com/en-us/library/system.net.sockets.tcplistener(v=vs.110).aspx
//
namespace server
{
    class Program
    {

        public static List<NetworkStream> clientStream = new List<NetworkStream>(); //list of clients
        public static bool connected = true;
        static void Main(string[] args)
        {
            TcpListener server = null;
            try
            {
                // Set the TcpListener on port 13000.
                Int32 port = 13000;
                IPAddress localAddr = IPAddress.Parse("127.0.0.1"); //parsing local IP address for server
                                                                    //But we can also parse IPv4 address and the clients can connect using 
                                                                    //different computers

                // TcpListener server = new TcpListener(port);
                server = new TcpListener(localAddr, port);
                // Start listening for client requests.
                server.Start();


                // Enter the listening loop.
                while (true)
                {
                    Console.Write("Waiting for a connection... ");

                    // Perform a blocking call to accept requests.
                    // You could also user server.AcceptSocket() here.
                    TcpClient client = server.AcceptTcpClient();
                    Console.WriteLine("Connected!");
                    ParameterizedThreadStart ts = new ParameterizedThreadStart(Worker);
                    Thread clientThread = new Thread(ts);
                    clientThread.Start(client);                 
                    clientStream.Add(client.GetStream());   
                    

                }
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
            finally
            {
                // Stop listening for new clients.
                server.Stop();
            }


            Console.WriteLine("\nHit enter to continue...");
            Console.Read();
        }

        //Worker Method: receives message, converts it to byte. Send back the message to all the clients 
        public static void Worker(Object o)
        {
            TcpClient client = (TcpClient)o;
            // Buffer for reading data
            Byte[] bytes = new Byte[256];
            String data = null;

            data = null;
            

            // Get a stream object for reading and writing
            NetworkStream stream = client.GetStream();

            int i;

            while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
            {
                // Translate data bytes to a ASCII string.
                data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);

                byte[] msg = System.Text.Encoding.ASCII.GetBytes(data);

                //for disonnect
                string output = data.Substring(data.IndexOf(':'));

                foreach (NetworkStream ns in clientStream)
                {
                    ns.Write(msg, 0, msg.Length);
                    if(ns == stream)
                    {
                        if (output == ": Just Left Chat !!") //if disconnect message is received close client
                        {
                            clientStream.Remove(ns);

                            connected = false;
                            break;
                        }
                        
                    }
                }

               if(connected == false)
                {
                    break;
                }

            }


            

        }

    }
}