﻿/*
* FILE : Vehicle.cs
* PROJECT : PROG2121 - Assignment #1
* PROGRAMMER : Manthan Rami
* FIRST VERSION : 2019-09-17
* DESCRIPTION :
 This file contains class declaration of Vehicle which is inherited from the parent class called vehicel.
* This file is used in project A1 for testing diffrent function.
*/
using System;

namespace Assignment01
{
    public delegate void OnOdometerChanged(object sender, EventArgs args);
    /*
    * NAME : Vehicle
    * PURPOSE : The main purpose of this class is that it is going to be a parent class of car, truck and motorcycle on which test are going to be done.
    * This file is used in project A-1 
    */
    class Vehicle
    {

        //Properties
        public string Color;
        public string Type;
        public int Odometer;
        public string EnergySource;
        public delegate void OnOdometerChanged(object soure, EventArgs args);

        /*****************************************************************************************************************
        Name	:	Car -- CONSTRUCTOR 
        Purpose :	To instantiate a new Car object - given a set of attribute values
        Inputs	:	color2	        String		Colour of the object
                    type1		    String		Type of the tire is used in object
                    odometer1       int         number of kilometer travelled by the vehicel
                    sourceType1     string      source of the energy used in vehicel
        Outputs :   NONE
        Returns :	Nothing
        *******************************************************************************************************************/
        public Vehicle(string color1 = "Unknown", string type1 = "Unknown", int odometer1=0,string sourceType1 = "Gas")
        {
            Color = color1;
            Type = type1;
            Odometer = odometer1;
            EnergySource = sourceType1;
        }
        /*******************************************************************************************************************
        Name	:	Show
        Purpose :	To display out object information
        Inputs	:	Nothing
        Outputs :   display Car object inforamtion
        Returns :	Nothing
        *********************************************************************************************************************/
        public virtual void Show()
        {
            Console.WriteLine("Please Create A vehicle first !!");
        }
        /*********************************************************************************************************************
        Name	:	ChangeColoToRed
        Purpose :	To set the object's colour to red
        Inputs	:	Nothing
        Outputs :   Nothing
        Returns :	Nothing
        **********************************************************************************************************************/
        public void ChangeColoToRed()
        {
            Color = "RED";
        }
        /*********************************************************************************************************************
        Name	:	IncreaseOdoMeter
        Purpose :	To increase the value of the odometer of the object
        Inputs	:	Nothing
        Outputs :   display Car object inforamtion
        Returns :	Nothing
        **********************************************************************************************************************/
        public void IncreaseOdoMeter()
        {
            Odometer++;
            if(odometerChanged!=null)
            {
                odometerChanged(this, new EventArgs());
            }
        }
        public event OnOdometerChanged odometerChanged; 


    }
   
}

