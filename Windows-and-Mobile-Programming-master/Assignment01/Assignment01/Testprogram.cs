﻿/*
* FILE : TestProgram.cs
* PROJECT : PROG2121 - Assignment #1
* PROGRAMMER : Manthan Rami
* FIRST VERSION : 2019-09-17
* DESCRIPTION :
* The program will create diffrent object of Car, Truck and Motorcycle using Vehicel as a Parent Class. This program is unit test without any kind of framework. Moreover it will perform diffrent kinds of test mention below and show ouput result .
    b. Execution of the inherited methods
    c. Execution of the overridden method(s).
    d. Execution of the unique method(s).
    e. The use of an event handler to respond to the event created as per 7. 
*/
using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment01
{
 /*
 * NAME : TestProgram
 * PURPOSE : The main purpose of this class is to test diffrent class with their functionallity.
 * This class is a simple test script it did not use any kind of framework to test. 
 */
    class TestProgram
    {
        public static void Main()
        {                                                        // Testing with car Objects
            Console.WriteLine("======================================================================================================\n");
            Console.WriteLine("Test For Car Class !!");
            Console.WriteLine("New Car object has been created \"carObject\"\n");
            Car carObject = new Car("White", "Summer Tire");        // car object has been created 
            
            Console.WriteLine("  ==> Test A : Access to the inherited variables\n");         
            Console.WriteLine("         Trying to access \"Colour\" varaible using carObject");
            Console.WriteLine("           Colour" + string.Format("{0,18}", ": " + carObject.Color+"\n"));
            Console.WriteLine("         Trying to access \"TierType\" varaible using carObject");
            Console.WriteLine("            TireType" + string.Format("{0,21}", ": " + carObject.TireType));

            Console.WriteLine("\n  ==> Test B : Execution of the inherited methods( ChangeColorToRed() )\n");
            carObject.ChangeColoToRed();
            Console.WriteLine("           Colour" + string.Format("{0,15}", ": " + carObject.Color));

            Console.WriteLine("\n  ==> Test C : Execution of the overridden method( Show() )");
            carObject.Show();

            Console.WriteLine("\n  ==> Test D : Execution of the unique method( SwitchTire() )\n");
            carObject.SwitchTire();
            Console.WriteLine("         TireType" + string.Format("{0,21}", ": " + carObject.TireType));

            Console.WriteLine("\n  ==> Test E : The use of an event handler to respond to the event created usng (IncreaseOdoMeter())\n");
            carObject.odometerChanged += OnOdometerChanged;     //use of event handler
            carObject.IncreaseOdoMeter();
            Console.WriteLine("         Odometer value" + string.Format("{0,7}", ": " + carObject.Odometer.ToString() + "km"));

            Console.WriteLine("\n  ==> Final Value of \"carObject\" are :");
            carObject.Show();
            carObject = null;       //freeing the memory allocated with the carObject

                                                                  // Testing with Truck Objects
            Console.WriteLine("======================================================================================================");

            Console.WriteLine("\nTest For Truck Class !!");
            Console.WriteLine("New Car object has been created \"truckObject\"\n");
            Truck truckObject = new Truck("Orange", 570);   //truck object has been created
           
            Console.WriteLine("  ==> Test A : Access to the inherited variables\n");
            Console.WriteLine("         Trying to access \"Colour\" varaible using carObject");
            Console.WriteLine("           Colour" + string.Format("{0,18}", ": " + truckObject.Color + "\n"));
            Console.WriteLine("         Trying to access \"weight\" varaible using carObject");
            Console.WriteLine("            Weight" + string.Format("{0,16}", ": " +truckObject.weight.ToString()+" kg"));

            Console.WriteLine("\n  ==> Test B : Execution of the inherited methods( ChangeColoToRed() )\n");
            truckObject.ChangeColoToRed();
            Console.WriteLine("          Colour" + string.Format("{0,15}", ": " + truckObject.Color));

            Console.WriteLine("\n  ==> Test C : Execution of the overridden method( Show() )");
            truckObject.Show();

            Console.WriteLine("\n  ==> Test D : Execution of the unique method( IncreaseWeight() )\n");
            truckObject.IncreaseWeight();
            Console.WriteLine("         Weight" + string.Format("{0,14}", ": " + truckObject.weight.ToString()));

            Console.WriteLine("\n  ==> Test E : The use of an event handler to respond to the event created usng (IncreaseOdoMeter())\n");
            truckObject.odometerChanged += OnOdometerChanged;       //use of event handler
            truckObject.IncreaseOdoMeter();
            Console.WriteLine("         Odometer value" + string.Format("{0,7}", ": " + truckObject.Odometer.ToString() + "km"));

            Console.WriteLine("\n  ==> Final Value of \"carObject\" are :");
            truckObject.Show();
            truckObject = null;     //freeing the memory allocated to the TruckObject 

                                                                //Testing with MotorCycle Object
            Console.WriteLine("======================================================================================================");
            
            Console.WriteLine("\nTest For Motocycle Class !!");
            Console.WriteLine("New Car object has been created \"motorcycleObject\"\n");

            Motorcycle motorcycleObject = new Motorcycle("Black", 110);     //MotorCycle object has been created
           
            Console.WriteLine("  ==> Test A : Access to the inherited variables\n");
            Console.WriteLine("         Trying to access \"Colour\" varaible using motorcycleObject");
            Console.WriteLine("           Colour" + string.Format("{0,18}", ": " + motorcycleObject.Color + "\n"));
            Console.WriteLine("         Trying to access \"weight\" varaible using motorcycleObject");
            Console.WriteLine("            Speed" + string.Format("{0,19}", ": " + motorcycleObject.speed.ToString() + " speed"));

            Console.WriteLine("\n  ==> Test B : Execution of the inherited methods( ChangeColoToRed() )\n");
            motorcycleObject.ChangeColoToRed();
            Console.WriteLine("          Colour" + string.Format("{0,15}", ": " + motorcycleObject.Color));

            Console.WriteLine("\n  ==> Test C : Execution of the overridden method( Show() )");
            motorcycleObject.Show();

            Console.WriteLine("\n  ==> Test D : Execution of the unique method( IncreaseWeight() )\n");
            motorcycleObject.IncreaseRPM();
            Console.WriteLine("         Speed" + string.Format("{0,14}", ": " + motorcycleObject.speed.ToString()+"rpm"));

            Console.WriteLine("\n  ==> Test E : The use of an event handler to respond to the event created usng (IncreaseOdoMeter())\n");
            motorcycleObject.odometerChanged += OnOdometerChanged;          //user of event handler
            motorcycleObject.IncreaseOdoMeter();
            Console.WriteLine("         Odometer value" + string.Format("{0,7}", ": " + motorcycleObject.Odometer.ToString() + "km"));

            Console.WriteLine("\n  ==> Final Value of \"motorcycleObject\" are :");
            motorcycleObject.Show();
            motorcycleObject = null;       //freeing the memory allocated to the motorcycleObject
            Console.WriteLine("======================================================================================================");
        }
        /*****************************************************************************************************************
        Name	:	OnOdometerChanged -- Event Handler 
        Purpose :	To fire a message when event is created.
        Inputs	:	sender		    object		
                    e	            EventArgs		
        Outputs :   Print out simple message that event has been created or done.
        Returns :	Nothing
        *******************************************************************************************************************/
        static void OnOdometerChanged(object sender, EventArgs e)   //event handler 
        {
            Console.WriteLine("         Message: ");
            Console.WriteLine("         =================================");
            Console.WriteLine("         |  Odometer Has been Increased  | ");
            Console.WriteLine("         =================================\n");
        }

    }
}