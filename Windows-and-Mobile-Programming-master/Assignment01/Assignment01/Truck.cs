﻿/*
* FILE : Truck.cs
* PROJECT : PROG2121 - Assignment #1
* PROGRAMMER : Manthan Rami
* FIRST VERSION : 2019-09-17
* DESCRIPTION :
  This file contains class declaration of Truck which is inherited from the parent class called vehicel.
* This file is used in project A1 for testing diffrent function.
*/
using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment01
{
    /*
    * NAME : Truck
    * PURPOSE : The Truck class has been created to accurately model the behavior of a standard
    * Truck. It have data member called Weight which is used to mention what is the size of the truck.
    */
    class Truck : Vehicle
    {
        public int weight = 0;  //used to mention what is the weight of the truck
        /*****************************************************************************************************************
        Name	:	Car -- CONSTRUCTOR 
        Purpose :	To instantiate a new Car object - given a set of attribute values
        Inputs	:	color2		    String		Colour of the object
                    weight_input	String		Weight of the object
        Outputs :   NONE
        Returns :	Nothing
        *******************************************************************************************************************/
        public Truck(string color2 = "Unknown",int weight_input = 0) : base(color2, "Truck", 0, "GAS")
        {
            weight = weight_input;
        }

        /*****************************************************************************************************************************************************************************************************
        Name	:	Show
        Purpose :	To display out object information
        Inputs	:	Nothing
        Outputs :   display truck object inforamtion
        Returns :	Nothing
        *******************************************************************************************************************************************************************************************************/
        public override void Show()
        {
            Console.WriteLine("\n         The Truck Information:");
            Console.WriteLine("         Colour" + string.Format("{0,15}", ": " + Color));
            Console.WriteLine("         Type" + string.Format("{0,19}", ": " + Type));
            Console.WriteLine("         Weight" + string.Format("{0,18}", ": " + weight+" kg"));
            Console.WriteLine("         Odometer value" + string.Format("{0,7}", ": " + Odometer.ToString() + "km"));
            Console.WriteLine("         Energy Source" + string.Format("{0,8}", ": " + EnergySource.ToString()));

        }
        /*****************************************************************************************************************************************************************************************************
        Name	:	IncreaseWeight
        Purpose :	To increase the weight of the truck object
        Inputs	:	Nothing
        Outputs :   Nothing
        Returns :	Nothing
        *******************************************************************************************************************************************************************************************************/
        public void IncreaseWeight()
        {
            weight++;      //increase weight by 1kg
        }
    }
}
