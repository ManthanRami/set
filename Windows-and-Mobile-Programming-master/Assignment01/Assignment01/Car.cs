﻿/*
* FILE : Car.cs
* PROJECT : PROG2121 - Assignment #1
* PROGRAMMER : Manthan Rami
* FIRST VERSION : 2019-09-17
* DESCRIPTION :
* This file contains class declaration of Cars which is inherited from the parent class called vehicel.
* This file is used in project A1 for testing diffrent function.
*/
using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment01
{ 
   
    /*
   * NAME : Car
   * PURPOSE : The Car class has been created to accurately model the behavior of a standard
   * car. It have data member called TireType which is used to mention which type od tire is used in it.
   */
    class Car : Vehicle
    {
        public string TireType;    //used to save what kind of tire is used in this car 

        /*****************************************************************************************************************
        Name	:	Car -- CONSTRUCTOR 
        Purpose :	To instantiate a new Car object - given a set of attribute values
        Inputs	:	color2		    String		Colour of the object
                    tire_input		String		Type of the tire is used in object
        Outputs :   NONE
        Returns :	Nothing
        *******************************************************************************************************************/
        public Car(string color2 = "Unknown", string tire_input="Winter Tire") : base(color2, "Car", 0, "GAS")
        {
            TireType = tire_input;
        }

        /*****************************************************************************************************************************************************************************************************
        Name	:	Show
        Purpose :	To display out object information
        Inputs	:	Nothing
        Outputs :   display Car object inforamtion
        Returns :	Nothing
        *******************************************************************************************************************************************************************************************************/
        public override void Show()
        {
            Console.WriteLine("\n         The Car Information:");
            Console.WriteLine("         Colour" + string.Format("{0,15}", ": " + Color));
            Console.WriteLine("         Type" + string.Format("{0,17}", ": " + Type));
            Console.WriteLine("         Tire" + string.Format("{0,25}", ": " + TireType));
            Console.WriteLine("         Odometer value" + string.Format("{0,7}", ": " + Odometer.ToString()+"km"));
            Console.WriteLine("         Energy Source" + string.Format("{0,8}", ": " + EnergySource.ToString()));

        }

        /*****************************************************************************************************************************************************************************************************
        Name	:	SwitchTire
        Purpose :	To switch the typr of tiers of the car.
        Inputs	:	Nothing
        Outputs :   Nothing
        Returns :	Nothing
        *******************************************************************************************************************************************************************************************************/
        public virtual void  SwitchTire()
        {
            if(TireType == "Winter Tier")
            {
                TireType = "Summer Tier";
            }
            else if (TireType == "Summer Tire")
            {
                TireType = "Winter Tier";
            }
        }
    }
}
