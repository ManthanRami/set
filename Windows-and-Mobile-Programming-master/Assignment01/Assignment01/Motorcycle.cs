﻿/*
* FILE : Motorcycle.cs
* PROJECT : PROG2121 - Assignment #1
* PROGRAMMER : Manthan Rami
* FIRST VERSION : 2019-09-17
* DESCRIPTION :
  This file contains class declaration of Motorcycle which is inherited from the parent class called vehicle.
* This file is used in project A1 for testing diffrent function.
*/

using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment01
{
    /*
    * NAME : Motorcycle
    * PURPOSE : The Motorcycle class has been created to accurately model the behavior of a standard
    * Motorcycle. It have data member called speed which is used to mention what is the maximum speed of the motorcycle.
    */
    class Motorcycle : Vehicle
    {
        public int speed;   //used to mention what is the speed of the motorcycle
        public Motorcycle(string color2 = "Unknown",int rpm_input = 0) : base(color2, "Motorcycle", 0, "GAS")
        {
            speed = rpm_input;
        }
        /*****************************************************************************************************************************************************************************************************
        Name	:	IncreaseRPM
        Purpose :	To increase the speedby rpm  of the motorcycle object
        Inputs	:	Nothing
        Outputs :   Nothing
        Returns :	Nothing
        *******************************************************************************************************************************************************************************************************/
        public void IncreaseRPM()
        {
            speed++;        //increaseing speed by 1rpm
        }

        /*****************************************************************************************************************************************************************************************************
        Name	:	Show
        Purpose :	To display out object information
        Inputs	:	Nothing
        Outputs :   display truck object inforamtion
        Returns :	Nothing
        *******************************************************************************************************************************************************************************************************/
        public override void Show()
        {
            Console.WriteLine("\n         The Motorcycle Information:");
            Console.WriteLine("         Colour" + string.Format("{0,15}", ": " + Color));
            Console.WriteLine("         Type" + string.Format("{0,24}", ": " + Type));
            Console.WriteLine("         Speed" + string.Format("{0,19}", ": " + speed.ToString()+"rpm"));
            Console.WriteLine("         Odometer value" + string.Format("{0,7}", ": " + Odometer.ToString() + "km"));
            Console.WriteLine("         Energy Source" + string.Format("{0,8}", ": " + EnergySource.ToString()));

        }
    }
}
