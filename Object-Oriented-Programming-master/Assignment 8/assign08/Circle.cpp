/*
*  FILE          : Circle.cpp
*  PROJECT       : assign08
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 08-10-2019
*  DESCRIPTION   : This file containts defination of cunstructor, mutator, accessor, Overloaded operators, destructor and other function of Circle object which are used in project assign08.
*/
#include"Circle.h"
#include"Shape.h"
/*****************************************************************************************************************************************************************************************************
	Name	:		Circle -- CONSTRUCTOR
	Purpose :	To instantiate a new Shape object - given a set of attribute values
	Inputs	:	buff	String		Name of the colour of the object.
				r		float		radius of the object
	Outputs :   NONE
	Returns :	Nothing
*****************************************************************************************************************************************************************************************************/
Circle::Circle(string buff, float r)
{
	SetRadius(r);
	SetName("Circle");
	SetColour(buff);
}
/*****************************************************************************************************************************************************************************************************
	Name	:		~Circle -- DESTRUCTOR
	Purpose :	To destroy created object object
	Inputs  :	Nothing
	Outputs :   simply printout the a message.
	Returns :	Nothing
*******************************************************************************************************************************************************************************************************/
Circle::~Circle()
{
	cout << "\nThe circle is destroyed..." << endl;
}
/*****************************************************************************************************************************************************************************************************
	Name	:		GetRadius -- Accessor
	Purpose :	Called to get the value of radius data member of the object
	Inputs  :	Nothing
	Outputs :   NONE
	Returns :	radius	float	radius of the circle 	
*******************************************************************************************************************************************************************************************************/
float Circle::GetRadius(void)
{
	return radius;
}
/*****************************************************************************************************************************************************************************************************
	Name	:		SetRadius -- Mutator
	Purpose :	To instantiate radius data member of shape class to given a set of attribute values
	Inputs  :	r	float	radius of the object.
	Outputs :   NONE
	Returns :	Nothing
*******************************************************************************************************************************************************************************************************/
void Circle::SetRadius(float r)
{
	if (r >= MIN_RADIUS)
	{
		radius = r;
	}
	else
	{
		radius = MIN_RADIUS;
	}
}
/*****************************************************************************************************************************************************************************************************
	Name	:	Perimeter
	Purpose :	To calculate the perimeter of the square
	Inputs	:	Nothing
	Outputs :   NONE
	Returns :	Total calculated perimeter.
*******************************************************************************************************************************************************************************************************/
float Circle::Perimeter()
{
	return 2.0f * 3.141592f*radius;
}
/*****************************************************************************************************************************************************************************************************
	Name	:	Area
	Purpose :	To calculate the Area of the square
	Inputs	:	Nothing
	Outputs :   NONE
	Returns :	Total calculated Area.
*******************************************************************************************************************************************************************************************************/
float Circle::Area()
{
	return (float)3.141592*radius*radius;
}
/*****************************************************************************************************************************************************************************************************
	Name	:	OverallDimension
	Purpose :	To calculate the OverallDimension of the square
	Inputs	:	Nothing
	Outputs :   NONE
	Returns :	Total OverallDimension Area.
*******************************************************************************************************************************************************************************************************/
float Circle::OverallDimension()
{
	return (float)2 * radius;
}
/*****************************************************************************************************************************************************************************************************
	Name	:	Show
	Purpose :	To display out object information
	Inputs	:	Nothing
	Outputs :   display Circle object inforamtion
	Returns :	Nothing
*******************************************************************************************************************************************************************************************************/
void Circle::Show()
{
	cout << "		*************************************************" << endl;
	cout << "		Shape Information" << endl;
	cout << "		Name		  :" << GetName() << endl;
	cout << "		Colour		  :" << GetColour() << endl;
	cout.setf(ios::fixed);
	cout << "		Radius		  :" << setprecision(DECIMAL_PRECISION) << GetRadius() << "cm" << endl;
	cout << "		Circumfrence	  :" << setprecision(DECIMAL_PRECISION) << Perimeter() << "cm" << endl;
	cout << "		Area		  :" << setprecision(DECIMAL_PRECISION) << Area() << "square cm" << endl;
	cout << "		*************************************************" << endl;
}
/********************************************************************************************************
 * Name			: Circle operator+
 * DESCRIPTION  :  in the case of the + and * operators - both are binary operators 
				   we *DO NOT* want to modify the RHS or LHS operand                                                  
				   so we need to create a 3rd (tempoary) variable to hold the result                                  
				   and we pass this temporary result back by value (not that efficient - but what can you do?)      
				   so the result doesn't lose scope before the calling side of this operator has time to            
				   do something with it ... 
 * PARAMETERS   : Circle& obj2: Circle object
 * RETURNS		: Circle temp: a temp object with new value in it
/*******************************************************************************************************/
 Circle Circle::operator+(const Circle& obj2)  
{
	Circle temp;
	temp.radius = radius + obj2.radius;
	temp.SetColour(this->GetColour());
	return temp;
}
 /********************************************************************************************************
  * Name		: Circle operator=(const Circle& obj2)
  * DESCRIPTION : in the case of the = assignment operator - which is a binary operator                              
				  we *DO* want to modify the LHS operand (but not the RHS)                                           
				  so this is why we directly modify the LHS's 3D data members and return a dereferenced 
				  this back  to the LHS    
  * PARAMETERS  : Circle& obj2: Circle object
  * RETURNS		: *this: A pointer to the current object
 /*******************************************************************************************************/
Circle Circle::operator=(const Circle& obj2)
{
	this->radius = obj2.radius;
	this->SetColour(obj2.GetColour());
	return *this;
}
/********************************************************************************************************
 * Name			: Circle operator==
 * DESCRIPTION  : In the case of the == operator - which is a binary operator   
				  we are simply comparing the LHS's value(s) to the RHS's values and returning a boolean 
				  indicate if the LHS==RHS
 * PARAMETERS	: Circle& obj2: Circle object
 * RETURNS		: Circle temp: a temp object with new value in it
/*******************************************************************************************************/
bool Circle::operator==(const Circle& obj2)
{
	if (this->radius == obj2.radius && this->GetColour() == obj2.GetColour()) 
	{
		return true;
	}
	else
	{
		return false;
	}
}
/********************************************************************************************************
 * Name			: operator*(const Circle& obj2)
 * DESCRIPTION  : Here in this function we are multipyling radius of the object passed and current object 
				  and sttting the lhs object's colour t0 temporary objectr and return it
 * PARAMETERS	: Circle& obj2: Circle object
 * RETURNS		: Circle temp: a temp object with new value in it
/*******************************************************************************************************/
Circle Circle::operator*(const Circle& obj2)
{
	Circle temp;
	temp.radius = radius * obj2.radius;
	temp.SetColour(obj2.GetColour());
	return temp;
}