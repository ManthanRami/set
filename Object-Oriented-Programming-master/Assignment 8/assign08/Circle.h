/*
*  FILE          : Circle.h
*  PROJECT       : assign08
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 08-10-2019
*  DESCRIPTION   :
*	This file contains all the prototypes, class defination and constant declaration of circle which is used in project assign08.
*/
#include"Shape.h"
static const float MIN_RADIUS = 0.00;	///<used to assign minimum radius of the circle
///
/// \class Circle
///
/// \brief The purpose of this class is to realistically model the physical attributes and functionallity of a Circle.
///
/// This class will create a Circle object with some initial value of data member of name and colour having some functionallity.
///Also it going to use shape as parent class for Circle object with having some additional functionality
///
/// \author A <i>Manthan Rami</i>
///
class Circle :public Shape
{
private:
	float radius;	//radius of circle
public:
	//constructor
	//Circle(void);
	Circle(string buff="undefined", float r= MIN_RADIUS);
	//destructor
	~Circle();
	static const int DECIMAL_PRECISION= 2; ///<used to set decimal precision in output format of circle information
	//mutator
	void SetRadius(float);
	//accessor
	float GetRadius(void);
	//other methods
	void Show(void);
	float Perimeter(void);
	float Area(void);
	float OverallDimension(void);
	//overloaded operator
	Circle operator+(const Circle& obj2);
	Circle operator=(const Circle& obj2);
	bool  operator==(const Circle& op2);
	Circle operator*(const Circle& obj2);
};

