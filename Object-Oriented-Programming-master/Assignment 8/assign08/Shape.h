/*
*  FILE          : Shape.h
*  PROJECT       : assign08
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 08-10-2019
*  DESCRIPTION   :
*	This file contains all the prototypes, class defination and constant declaration of shape which is used in project assign08.
*/
#pragma once
#include<stdio.h>
#include<string.h>
#include<string>
#include<iostream>
#include<new.h>
#include<iomanip>

using namespace std;
#define STOP  -1	///<used to throw exception in numrical signal
template <class data>data CombineShape(data& a, data& b)
{
	data temp;
	temp = a + b;
	return temp;
}
/// 
/// \class Shape
///
/// \brief The purpose of this class is to realistically model the physical attributes and functionallity of a Shape.
///
/// This class will create a shape object with some initial value of data member of name and colour having some functionallity.
///Also it going to be as parent class for square and circle class.
///
/// \author  <i>Manthan Rami</i>
///
class Shape
{
private:
	string name;	///<used to save name of the shape
	string colour;	///<used to save colour of the shape

public:
	//constructor
	Shape(string name="Unknown", string colour="undefined");
	//destructor
	~Shape();
	//accesssor
	const string GetName()const;
	const string GetColour()const;
	//mutator
	void SetName(string); 
	void SetColour(string);
	//other methods(Pure virtual function)
	virtual float Perimeter(void) = 0;
	virtual float Area(void) = 0;
	virtual float OverallDimension(void) = 0;
};