/*
*  FILE          : myShape.cpp
*  PROJECT       : assign08
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 08-03-2019
*  DESCRIPTION   : This program is a modified version of assignment-04. It will create two ciecle with value and one with default cunstructor
				   and play ith object with overloaded function "+ + *" and store to playAround object and print out information. same thing is
				   the squre object after all it will print out shape information.
*/
#include"Circle.h"
#include"Square.h"
#include"Shape.h"

int main()
{
	Circle round1("red", 5.5);
	Circle round2("blue", 10.5);
	Circle playAround;

	Square square1(5, "orange");
	Square square2(12, "purple");
	Square playASquare;

	//printing round1 round2 information
	cout << "\nround1 Info:" << endl;
	round1.Show();
	cout << "\nround2 Info:" << endl;
	round2.Show();

	//printing playAround  information
	cout << "\nplayAround Info:" << endl;
	playAround.Show();


	//printing square1 square2 information
	cout << "\nsquare1 Info:" << endl;
	square1.Show();
	cout << "\nsquare2 Info:" << endl;
	square2.Show();
	
	//printing playASquare  information
	cout << "\nplayASquare Info:" << endl;
	playASquare.Show();

	//Adding both circle object to playAround
	playAround = CombineShape(round1, round2);

	//Adding both square object to playAround
	playASquare = CombineShape(square1,square2);

	//printing playAround information after combining round 1 and round 2
	cout << "\nplayAround Info:" << endl;
	playAround.Show();

	//printing playASquare information after combining square 1 and square 2
	cout << "\nplayAsquare Info:" << endl;
	playASquare.Show();

	try
	{
		cout << "\ndifferenceSquareGood Info:" << endl;
		Square differenceSquareGood = (square2 - square1);
		differenceSquareGood.Show();
		cout << "\ndifferenceSquareBad Info:" << endl;
		Square differenceSquareBad = (square1 - square2);
		cout << "\ndifferenceSquareBad Info:" << endl;
		differenceSquareBad.Show();
	}
	catch (int error)
	{
		if (error = STOP)
		{
			cout << "		=================================================" << endl;
			cout << "		Invalid square substraction\n" << endl;
			cout << "		=================================================" << endl;		
		}
	}
	return 0;
}