/// \mainpage assign08 Project   
/// 
/// \image html image.jpg
///
///
/// \section intro Program Introduction
///
///This program is a modified version of assignment-0.7 also it uses templeate 
///function and throw and exception moreover any addtional overloaded operator
///is added "-".It will create two circle with value and two square object with 
///value and circle one with default constructor square one with default constructor
///and by using template it add both circle object and assign to playAround object with
///object with overloaded function "+ ,+ * and print out information. Also it will create
///an square object and assign it by substracting two object useing overloaded operator.
///and print out sqaure object after all it will print out shape information.
///
///
///
///
///
///
///
/// <hr>
/// \section version Current version of the Shape Project :
/// <ul>
/// <li>\authors   <b><i>Manthan Rami</i></b></li>
/// <li>\version   1.00.00</li>
/// <li>\date      08-09-2019</li>
/// <li>\pre       In order to use the <i>Shape Project</i> - you must first start with a circle or square</li>
/// <li>\copyright Shape-Expert developer</li>
/// </ul>
///