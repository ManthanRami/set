/*
*  FILE          : Square.cpp
*  PROJECT       : assign08
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 08-03-2019
*  DESCRIPTION   : This file containts defination of cunstructor, mutator, accessor, overloaded operator, destructor and other function of square object which are used in project assign08.
*/ 
#include"Square.h"
#include"Shape.h"
/*****************************************************************************************************************************************************************************************************
	Name	:		Square -- CONSTRUCTOR
	Purpose :	To instantiate a new Shape object - given a set of attribute values
	Inputs	:	slen		float		length of side of the object
				colour		String		Name of the colour of the object.
	Outputs :   NONE
	Returns :	Nothing
*******************************************************************************************************************************************************************************************************/
/**
	* \brief To instantiate a new Square object - given a set of attribute values
	* \details <b>Details</b>
	*
	* This is a CONSTRUCTOR it will create and assign initial value to the object 
	* \param slen  - <b>float</b> - representation of the square's side length
	* \param buff - <b>string</b> - representation of the square's colour
	*
	* \return As this is a <i>constructor</i> for the square class, nothing is returned
	*
	* \see ~square()
	*/
Square::Square(float slen, string buff)
{
	SetLength(slen);
	SetName("Square");
	SetColour(buff);
}
/*****************************************************************************************************************************************************************************************************
	Name	:	~Square -- DESTRUCTOR
	Purpose :	To destroy created object object 
	Inputs	:	Nothing
	Outputs :   simply printout the a message.
	Returns :	Nothing
*******************************************************************************************************************************************************************************************************/
///
/// \brief Called upon to <i>destroy</i> a Square object - once it loses <b>scope</b>
/// \details <b>Details</b>
///
/// Here is where I can provide a better, more detailed description of what this method does...
///
/// \param NOTTA - <b>Nothing</b> - it's a destructor 
///
/// \return As this is a <i>destructor</i> for the Square class, nothing is returned
///
/// \see Square()
Square::~Square()
{
	cout << "\nThe square is destroyed..." << endl;
}
/*****************************************************************************************************************************************************************************************************
	Name	:	GetLength -- Accessor
	Purpose :	Called to get the value of sideLength data member of the object
	Inputs	:	Nothing
	Outputs :   NONE
	Returns :	sideLength		float		length of side of the object
*******************************************************************************************************************************************************************************************************/
///
/// \brief Called to get the value of sideLength data member of the object
/// \details <b>Details</b>
///
/// This method will invoke sidelength data member of square object and get the value of it.
///
/// \param NOTTA - <b>void</b> 
///
/// \return This method is returning value of  <i>sideLength</i> data memberof the square object
///
/// \see square()
///
float Square::GetLength(void)
{
	return sideLength;
}
/*****************************************************************************************************************************************************************************************************
	Name	:	SetLength -- Mutator
	Purpose :	To instantiate colour data member of shape class to given a set of attribute values
	Inputs	:	slen	float	length of side of the object.
	Outputs :   NONE
	Returns :	Nothing
*******************************************************************************************************************************************************************************************************/
///
/// \brief To instantiate colour data member of shape class to given a set of attribute values
/// \details <b>Details</b>
///
/// This method will do the validification of the value to be set to the sideLength and set it if it is equal or more than 0.00.
///
/// \param NOTTA - <b>float slen</b> 
///
/// \return This method is returning <i>Nothing</i>
///
/// \see square()
///
void Square::SetLength(float slen)
{
	if (slen >= MIN_LENGTH)
	{
		sideLength = slen;
	}
	else
	{
		sideLength = MIN_LENGTH;
	}
}
/*****************************************************************************************************************************************************************************************************
	Name	:	Perimeter 
	Purpose :	To calculate the perimeter of the square
	Inputs	:	Nothing
	Outputs :   NONE
	Returns :	Total calculated perimeter.
*******************************************************************************************************************************************************************************************************/
///
/// \brief Called To calculate the perimeter of the square
/// \details <b>Details</b>
///
/// This method will get the value of sidelength and calculates perimeter of square and gives value back
///
/// \param NOTTA - <b>void</b> 
///
/// \return This method is returning <i>Calculated Perimeter</i>
///
/// \see square()
///
float Square::Perimeter(void)
{
	return 4 * sideLength;
}
/*****************************************************************************************************************************************************************************************************
	Name	:	Area
	Purpose :	To calculate the Area of the square
	Inputs	:	Nothing
	Outputs :   NONE
	Returns :	Total calculated Area.
*******************************************************************************************************************************************************************************************************/
///
/// \brief Called To calculate the Area of the square
/// \details <b>Details</b>
///
/// This method will get  the value of <b>sideLength </b>and calculate the area of the square and gives value back.
///
/// \param NOTTA - <b>void</b> 
///
/// \return This method is returning <i>Calculated Area</i>
///
/// \see square()
///
float Square::Area(void)
{
	return sideLength * sideLength;
}
/*****************************************************************************************************************************************************************************************************
	Name	:	OverallDimension
	Purpose :	To calculate the OverallDimension of the square
	Inputs	:	Nothing
	Outputs :   NONE
	Returns :	Total OverallDimension Area.
*******************************************************************************************************************************************************************************************************/
///
/// \brief Called To calculate the OverallDimension of the square
/// \details <b>Details</b>
///
/// This method will get  the value of <b>sideLength </b>and calculate the Overall Dimension of the square and gives value back.
///
/// \param NOTTA - <b>void</b> 
///
/// \return This method is returning <i>Calculated Overall Dimension</i>
///
/// \see square()
///
float Square::OverallDimension(void)
{
	return sideLength;
}
/*****************************************************************************************************************************************************************************************************
	Name	:	Show
	Purpose :	To display out object information
	Inputs	:	Nothing
	Outputs :   display square object inforamtion
	Returns :	Nothing
*******************************************************************************************************************************************************************************************************/
///
/// \brief Called To display out object information
/// \details <b>Details</b>
///
/// This method will get  the value of <b>All the Data member </b>and print out all the information of square object into appropriate formate.
///
/// \param NOTTA - <b>void</b> 
///
/// \return This method is returning <i>void</i>
///
/// \see square()
///
void Square::Show(void)
{
	cout << "		*************************************************" << endl;
	cout << "		Shape Information" << endl;
	cout << "		Name		 :" << GetName() << endl;
	cout << "		Colour		 :" << GetColour() << endl;
	cout.setf(ios::fixed);
	cout << "		Side-Length	 :" << setprecision(DECIMAL_PRECISION) << GetLength() << "cm" << endl;
	cout << "		Perimeter	 :" << setprecision(DECIMAL_PRECISION) << Perimeter() << "cm" << endl;
	cout << "		Area		 :" << setprecision(DECIMAL_PRECISION) << Area() << "square cm" << endl;
	cout << "		*************************************************" << endl;
}
/********************************************************************************************************
 * Name			: Square operator+
 * DESCRIPTION  :  in the case of the + and * operators - both are binary operators
				   we *DO NOT* want to modify the RHS or LHS operand
				   so we need to create a 3rd (tempoary) variable to hold the result
				   and we pass this temporary result back by value (not that efficient - but what can you do?)
				   so the result doesn't lose scope before the calling side of this operator 
 * PARAMETERS   : Square& obj2: Square object
 * RETURNS		: Square temp: a temp object with new value in it
/*******************************************************************************************************/
///
/// \brief Called to add two object of type Square
/// \details <b>Details</b>
///
///In the case of the + and * operators - both are binary operators we *DO NOT* want to modify the RHS or LHS operand so we
///need to create a 3rd(tempoary) variable to hold the result and we pass this temporary result back by value(not that efficient - but what
///can you do so the result doesn't lose scope before the calling side of this operator 
///
/// \param NOTTA - <b>const Square& obj2</b> 
///
/// \return This method is returning <i>temparory sqaure object with new values</i>
///
/// \see square()
///
Square Square::operator+(const Square& obj2)
{
	Square temp;
	temp.sideLength = sideLength + obj2.sideLength;
	temp.SetColour(obj2.GetColour());
	return temp;
}
/********************************************************************************************************
  * Name		: Square operator=(const Square& obj2)
  * DESCRIPTION : in the case of the = assignment operator - which is a binary operator
				  we *DO* want to modify the LHS operand (but not the RHS)
				  so this is why we directly modify the LHS's 3D data members and return a dereferenced
				  this back  to the LHS
  * PARAMETERS  : Square& obj2: Square object
  * RETURNS		: *this: A pointer to the current object
 /*******************************************************************************************************/
 ///
 /// \brief Called to assign value of one object to another
 /// \details <b>Details</b>
 ///
 ///in the case of the = assignment operator - which is a binary operator
 ///we *DO* want to modify the LHS operand(but not the RHS)
 ///so this is why we directly modify the LHS's 3D data members and return a dereferenced
 ///this back  to the LHS
 ///
 /// \param NOTTA - <b>const Square& obj2</b> 
 ///
 /// \return This method is returning <i>current object with new value</i>
 ///
 /// \see square()
 ///
Square Square::operator=(const Square& obj2)
{
	this->sideLength = obj2.sideLength;
	this->SetColour(obj2.GetColour());
	return *this;
}
/********************************************************************************************************
 * Name			: Square operator==
 * DESCRIPTION  : In the case of the == operator - which is a binary operator
				  we are simply comparing the LHS's value(s) to the RHS's values and returning a boolean
				  indicate if the LHS==RHS
 * PARAMETERS	: Square& obj2: Square object
 * RETURNS		: Square temp: a temp object with new value in it
/*******************************************************************************************************/
///
/// \brief Called to check if both the sqaure object have the same value 
/// \details <b>Details</b>
///
///In the case of the == operator - which is a binary operator
///we are simply comparing the LHS's value(s) to the RHS's values and returning a boolean
///indicate if the LHS == RHS
///
/// \param NOTTA - <b>const Square& obj2</b> 
///
/// \return This method is returning <i>boolean expression</i>
///
/// \see square()
///
bool Square::operator==(const Square& obj2)
{
	if (this->sideLength == obj2.sideLength && this->GetColour() == obj2.GetColour())
	{
		return true;
	}
	else
	{
		return false;
	}
}
/********************************************************************************************************
 * Name			: operator*(const Square& obj2)
 * DESCRIPTION  : Here in this function we are multipyling radius of the object passed and current object
				  and sttting the lhs object's colour t0 temporary objectr and return it
 * PARAMETERS	: Square& obj2: Square object
 * RETURNS		: Square temp: a temp object with new value in it
/*******************************************************************************************************/
///
/// \brief Called to multiply two square object and return temp object
/// \details <b>Details</b>
///
///Here in this function we are multipyling radius of the object passed and current object
///and sttting the lhs object's colour t0 temporary objectr and return it
///
/// \param NOTTA - <b>const Square& obj2</b> 
///
/// \return This method is returning <i>current object with new value</i>
///
/// \see square()
///
Square Square::operator*(const Square& obj2)
{
	Square temp;
	temp.sideLength = this->sideLength*obj2.sideLength;
	temp.SetColour(obj2.GetColour());
	return temp;
}
/********************************************************************************************************
 * Name			: operator-(const Square& obj2)
 * DESCRIPTION  : Here in this function we are substracting two square object and if we get the result of 
				  sidelength less than or equal to 0 then it will throw an error.
 * PARAMETERS	: Square& obj2: Square object
 * RETURNS		: Square temp: a temp object with new value in it
/*******************************************************************************************************/
///
/// \brief Called to substract value of two square ibject 
/// \details <b>Details</b>
///
/// Here in this function we are substracting two square object and if we get the result of 
///sidelength less than or equal to 0 then it will throw an error.
///
/// \param NOTTA - <b>const Square& obj2</b> 
///
/// \return This method is returning <i>temp object with new value</i>
///
/// \see square()
///
Square Square::operator-(const Square& obj2)
{
	Square temp;
	temp.sideLength = sideLength - obj2.sideLength;
	if (temp.sideLength <= ZERO)
	{
		throw STOP;
	}
	temp.SetColour(this->GetColour());
	return temp;
}