/*
*  FILE          : Square.h
*  PROJECT       : assign08
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 08-10-2019
*  DESCRIPTION   :
*	This file contains all the prototypes, class defination and constant declaration of Square which is used in project assign08.
*/
#include"Shape.h"
static const float MIN_LENGTH = 0.00;	///<used to assign default value of side length 
/// 
/// \class Square
///
/// \brief The purpose of this class is to realistically model the physical attributes and functionallity of a Square.
///
/// This class will create a Square object with some initial value of data member of name and colour having some functionallity.
///Also it going to use shape as parent class for squar object with having some additional functionality
///
/// \author  <i>Manthan Rami</i>
///
class Square :public Shape
{
private:
	float sideLength;	//side lenght of the square
public:

	//constant declaration 
	static const int ZERO = 0;	///< used to for comaprison range of resultant side length value
	static const int DECIMAL_PRECISION = 2; ///<used to set decimal precision in output format of square information
	//constructor
	Square(float slen = MIN_LENGTH, string buff = "undefined");

	//destructor
	~Square();


	//mutator
	void SetLength(float);	

	//accessor
	float GetLength(void);

	//other methods
	void Show(void);
	float Perimeter(void);
	float Area(void);
	float OverallDimension(void);

	// overloaded operator 
	Square operator+(const Square& obj2);
	Square operator-(const Square& obj2);
	Square operator=(const Square& obj2);
	bool  operator==(const Square& obj2);
	Square operator*(const Square& obj2);
};	