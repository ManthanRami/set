var class_shape =
[
    [ "Shape", "class_shape.html#af6d3ad0472c93d4ea96f44637143745f", null ],
    [ "~Shape", "class_shape.html#a935afc9e576015f967d90de56977167d", null ],
    [ "Area", "class_shape.html#aa665a1f4b62734e963a83eb92dcc8a84", null ],
    [ "GetColour", "class_shape.html#aaff1535de5a43f489d49dcb5d5180e71", null ],
    [ "GetName", "class_shape.html#a94f4bd85be585fc93d63a7d01a5c9ec6", null ],
    [ "OverallDimension", "class_shape.html#a2b0cbb9ef1f25d4451be4d37329ebe66", null ],
    [ "Perimeter", "class_shape.html#adab98c87926d812d02f8d53eb65a625e", null ],
    [ "SetColour", "class_shape.html#ac431d623dd3ae3a0531854ee7909b43b", null ],
    [ "SetName", "class_shape.html#ad91a621c8cb31c1acd1992ce1722219f", null ]
];