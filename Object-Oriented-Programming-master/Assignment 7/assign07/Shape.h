/*
*  FILE          : Shape.h
*  PROJECT       : assign07
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 08-03-2019
*  DESCRIPTION   :
*	This file contains all the prototypes, class defination and constant declaration of shape which is used in project assign07.
*/
#pragma once
#include<stdio.h>
#include<string.h>
#include<string>
#include<iostream>
#include<new.h>
#include<iomanip>

using namespace std;

class Shape
{
private:
	string name;	//name of the shape
	string colour;	//colour of the shape

public:
	//constructor
	Shape(string name="Unknown", string colour="undefined");
	//destructor
	~Shape(void);
	//accesssor
	const string GetName()const;
	const string GetColour()const;
	//mutator
	void SetName(string);
	void SetColour(string);
	//other methods(Pure virtual function)
	virtual float Perimeter(void) = 0;
	virtual float Area(void) = 0;
	virtual float OverallDimension(void) = 0;
};