/*
*  FILE          : Circle.h
*  PROJECT       : assign04
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 08-03-2019
*  DESCRIPTION   :
*	This file contains all the prototypes, class defination and constant declaration of circle which is used in project assign07.
*/
#include"Shape.h"

class Circle :public Shape
{
private:
	float radius;	//radius of circle
public:
	//constructor
	//Circle(void);
	Circle(string buff="undefined", float r=0.0);
	//destructor
	~Circle(void);
	//mutator
	void SetRadius(float);
	//accessor
	float GetRadius(void);
	//other methods
	void Show(void);
	float Perimeter(void);
	float Area(void);
	float OverallDimension(void);
	//overloaded operator
	Circle operator+(const Circle& obj2);
	Circle operator=(const Circle& obj2);
	bool  operator==(const Circle& op2);
	Circle operator*(const Circle& obj2);
};

