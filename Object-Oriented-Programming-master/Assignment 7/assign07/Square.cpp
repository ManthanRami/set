/*
*  FILE          : Square.cpp
*  PROJECT       : assign07
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 08-03-2019
*  DESCRIPTION   : This file containts defination of cunstructor, mutator, accessor, overloaded operator, destructor and other function of square object which are used in project assign07.
*/ 
#include"Square.h"
#include"Shape.h"
/*****************************************************************************************************************************************************************************************************
	Name	:		Square -- CONSTRUCTOR
	Purpose :	To instantiate a new Shape object - given a set of attribute values
	Inputs	:	slen		float		length of side of the object
				colour		String		Name of the colour of the object.
	Outputs :   NONE
	Returns :	Nothing
*******************************************************************************************************************************************************************************************************/
Square::Square(float slen, string buff)
{
	SetLength(slen);
	SetName("Square");
	SetColour(buff);
}
/*****************************************************************************************************************************************************************************************************
	Name	:	~Square -- DESTRUCTOR
	Purpose :	To destroy created object object 
	Inputs	:	Nothing
	Outputs :   simply printout the a message.
	Returns :	Nothing
*******************************************************************************************************************************************************************************************************/
Square::~Square()
{
	cout << "\nThe square is destroyed..." << endl;
}
/*****************************************************************************************************************************************************************************************************
	Name	:	GetLength -- Accessor
	Purpose :	Called to get the value of sideLength data member of the object
	Inputs	:	Nothing
	Outputs :   NONE
	Returns :	sideLength		float		length of side of the object
*******************************************************************************************************************************************************************************************************/
float Square::GetLength()
{
	return sideLength;
}
/*****************************************************************************************************************************************************************************************************
	Name	:	SetLength -- Mutator
	Purpose :	To instantiate colour data member of shape class to given a set of attribute values
	Inputs	:	slen	float	length of side of the object.
	Outputs :   NONE
	Returns :	Nothing
*******************************************************************************************************************************************************************************************************/
void Square::SetLength(float slen)
{
	if (slen >= 0.00)
	{
		sideLength = slen;
	}
	else
	{
		sideLength = 0.00;
	}
}
/*****************************************************************************************************************************************************************************************************
	Name	:	Perimeter 
	Purpose :	To calculate the perimeter of the square
	Inputs	:	Nothing
	Outputs :   NONE
	Returns :	Total calculated perimeter.
*******************************************************************************************************************************************************************************************************/
float Square::Perimeter(void)
{
	return 4 * sideLength;
}
/*****************************************************************************************************************************************************************************************************
	Name	:	Area
	Purpose :	To calculate the Area of the square
	Inputs	:	Nothing
	Outputs :   NONE
	Returns :	Total calculated Area.
*******************************************************************************************************************************************************************************************************/
float Square::Area(void)
{
	return sideLength * sideLength;
}
/*****************************************************************************************************************************************************************************************************
	Name	:	OverallDimension
	Purpose :	To calculate the OverallDimension of the square
	Inputs	:	Nothing
	Outputs :   NONE
	Returns :	Total OverallDimension Area.
*******************************************************************************************************************************************************************************************************/
float Square::OverallDimension(void)
{
	return sideLength;
}
/*****************************************************************************************************************************************************************************************************
	Name	:	Show
	Purpose :	To display out object information
	Inputs	:	Nothing
	Outputs :   display square object inforamtion
	Returns :	Nothing
*******************************************************************************************************************************************************************************************************/
void Square::Show()
{
	cout << "\nShape Information" << endl;
	cout << "Name		 :" << GetName() << endl;
	cout << "Colour		 :" << GetColour() << endl;
	cout.setf(ios::fixed);
	cout << "Side-Length	 :" << setprecision(2) << GetLength() << "cm" << endl;
	cout << "Perimeter	 :" << setprecision(2) << Perimeter() << "cm" << endl;
	cout << "Area		 :" << setprecision(2) << Area() << "square cm" << endl;
}
/********************************************************************************************************
 * Name			: Square operator+
 * DESCRIPTION  :  in the case of the + and * operators - both are binary operators
				   we *DO NOT* want to modify the RHS or LHS operand
				   so we need to create a 3rd (tempoary) variable to hold the result
				   and we pass this temporary result back by value (not that efficient - but what can you do?)
				   so the result doesn't lose scope before the calling side of this operator has time to
				   do something with it ...
 * PARAMETERS   : Square& obj2: Square object
 * RETURNS		: Square temp: a temp object with new value in it
/*******************************************************************************************************/
Square Square::operator+(const Square& obj2)
{
	Square temp;
	temp.sideLength = sideLength + obj2.sideLength;
	temp.SetColour(obj2.GetColour());
	return temp;
}
/********************************************************************************************************
  * Name		: Square operator=(const Square& obj2)
  * DESCRIPTION : in the case of the = assignment operator - which is a binary operator
				  we *DO* want to modify the LHS operand (but not the RHS)
				  so this is why we directly modify the LHS's 3D data members and return a dereferenced
				  this back  to the LHS
  * PARAMETERS  : Square& obj2: Square object
  * RETURNS		: *this: A pointer to the current object
 /*******************************************************************************************************/
Square Square::operator=(const Square& obj2)
{
	this->sideLength = obj2.sideLength;
	this->SetColour(obj2.GetColour());
	return *this;
}
/********************************************************************************************************
 * Name			: Square operator==
 * DESCRIPTION  : In the case of the == operator - which is a binary operator
				  we are simply comparing the LHS's value(s) to the RHS's values and returning a boolean
				  indicate if the LHS==RHS
 * PARAMETERS	: Square& obj2: Square object
 * RETURNS		: Square temp: a temp object with new value in it
/*******************************************************************************************************/
bool Square::operator==(const Square& obj2)
{
	if (this->sideLength == obj2.sideLength && this->GetColour() == obj2.GetColour())
	{
		return true;
	}
	else
	{
		return false;
	}
}
/********************************************************************************************************
 * Name			: operator*(const Square& obj2)
 * DESCRIPTION  : Here in this function we are multipyling radius of the object passed and current object
				  and sttting the lhs object's colour t0 temporary objectr and return it
 * PARAMETERS	: Square& obj2: Square object
 * RETURNS		: Square temp: a temp object with new value in it
/*******************************************************************************************************/
Square Square::operator*(const Square& obj2)
{
	Square temp;
	temp.sideLength = this->sideLength*obj2.sideLength;
	temp.SetColour(obj2.GetColour());
	return temp;
}