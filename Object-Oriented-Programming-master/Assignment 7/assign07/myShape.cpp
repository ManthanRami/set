/*
*  FILE          : myShape.cpp
*  PROJECT       : assign07
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 08-03-2019
*  DESCRIPTION   : This program is a modified version of assignment-04. It will create two ciecle with value and one with default cunstructor 
				   and play ith object with overloaded function "+ + *" and store to playAround object and print out information. same thing is  
				   the squre object after all it will print out shape information.
*/
#include"Circle.h"
#include"Square.h"
#include"Shape.h"
  
int main()
{
	Circle round1("red",5.5);
	Circle round2("blue",10.5);
	Circle playAround;

	Square square1(5, "orange");
	Square square2(12, "purple");
	Square playASquare;

	//printing round1 round2 information
	cout << "\nround1 Info:" << endl << "\t";
	round1.Show();
	cout << "\nround2 Info:" << endl << "\t";
	round2.Show();

	//printing playAround playASquare information
	cout << "\nplayAround Info:" << endl << "\t";
	playAround.Show();
	cout << "\nplayASquare Info:" << endl << "\t";
	playASquare.Show();
	
	//printing square1 square2 information
	cout << "\nsquare1 Info:" << endl << "\t";
	square1.Show();
	cout << "\nsquare2 Info:" << endl << "\t";
	square2.Show();
	
	//Adding both circle object to playAround
	playAround = round1 + round2;
	
	//Adding both square object to playAround
	playASquare = square1 + square2;
	
	//printing playAround information
	cout << "\nplayAround Info:" << endl << "\t";
	playAround.Show();

	//printing playASquare information
	cout << "\nplayAsquare Info:" << endl << "\t";
	playASquare.Show();
	
	//Multiplying both circle object to playAround
	playAround = round1 * round2;
	
	//Multiplying both square object to playAround
	playASquare = square2 * square1;
	
	//printing playAround information
	cout << "\nplayAround Info:" << endl << "\t";
	playAround.Show();
	
	//printing playASquare information
	cout << "\nplayAsquare Info:" << endl << "\t";
	playASquare.Show();
	
	//assigning playAround to round1
	playAround = round1;
	cout << "\nAfter checking if playAround is equal to round one or not" << endl;
	if (playAround == round1)	//checking if the both object are same or not
	{
		cout << "Hurray !!" << endl;
	}
	else
	{
		cout << "Awww !!" << endl;
	}
	return 0;
}