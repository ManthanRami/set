/*
*  Projectname:	assign1
 * Filename:	assign1.cpp
 * Author:		Manthan Rami
 * Date:		05/25/2019
 *Description:	This program will take input from the user in any form of grade.
				after some calculation it display whether the student is pass or not or having any special situation.
 */

#include "assessGrade.h"

int main()
{
	double studentMark = 0;
	int studentScore[5] = { 0 };
	char userInput[100];
	int whichCase = 0;
	while (1)
	{
		printf("\n Please specify the student's final grade or set of assignment marks: ");
		fgets(userInput, 100, stdin);
		whichCase = checking(userInput);
		switch (whichCase)
		{
		case 1:
			printf("Exiting program\n");
			return 0;
			break;
		case 2:
			assessGrade(userInput);
			break;
		case 3:
			if ((sscanf(userInput, "%lf", &studentMark)) == 1)
			{
				assessGrade(studentMark);
			}
			break;
		case 4:
			if ((sscanf(userInput, "%d %d %d %d %d", &studentScore[0], &studentScore[1], &studentScore[2], &studentScore[3], &studentScore[4])) == 5);
			{
				assessGrade(studentScore);
			}
			break;
		default:
			printf("   >> User specified a INVALID \"String\" type grade \n");
		}
	}
}