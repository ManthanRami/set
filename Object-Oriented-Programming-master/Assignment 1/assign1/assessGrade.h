/*

* Description: This file contains the prototypes, constant and include files for the assign1 project.
			   These are all of the prototypes for the various functions found within the source files.
*/
#pragma once
#include<stdio.h>
#include<string.h>
#include<conio.h>
#include<ctype.h>

#pragma warning(disable: 4996)



// Prototype(required function)
void assessGrade(char* marks);
void assessGrade(double marks);
int assessGrade(int marks[5]);

// Prototype(additional function)
int checking(double marks);
int checking(int marks[5]);
int checking(char input[]);
int findingAverage(int marks);
void emptyArray(int mark[]);

//Constant variable declaration
#define MAX_CONDITION 26
#define MAX_MARK 100
#define MIN_MARK 0
#define PASS_MARK 54.50
#define SUCESS 1
#define FAIL 0
#define GRADE_INDEX 16
#define STOP_SEARCH 25

