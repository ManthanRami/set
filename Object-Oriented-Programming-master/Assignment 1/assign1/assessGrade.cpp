#include "assessGrade.h"
/*=======================================================================================================================================
  * Function: assessGrade()

 * Description:	This function will check for grade to their respected % and print out whether student is pass or fail.

 * Parameter:	char* marks: - It will contain Alphabetical characters given by the user in main function.

 * Return Value returns nothing as return type is void.

 */
void assessGrade(char* marks)																										   
{																																	   
	int indexNum = 0;																												   
	double studentMarks = 0;																										   
	int gradeIndex = 1;																												   
	int conditionIndex = 1;																											   
																																	   
	//																																   
	const char* studentGrade[MAX_CONDITION] = { "A+","95","A","85","B+","77","B","72","C+","67","C","62","D","57","F","50",			  
												"I","Incomplete","Q","Withdrawal after drop/refund date","AU","Audit","DNA",		   
												"Did Not Attend","I/P","In Progress" };												   
						
	if (strchr(marks, '\n') != NULL) //finding '\n' if it found or not
	{
		(*(strchr(marks, '\n')) = '\0');
	}																					  
	while (indexNum < MAX_CONDITION)																								  
	{						
	
		gradeIndex = strcmp(studentGrade[indexNum], marks); // searching for grade as user provided															  
		if (indexNum < GRADE_INDEX && gradeIndex == 0)																				  
		{																															  
			if ((sscanf(studentGrade[indexNum + 1], "%lf", &studentMarks)) == 1)													  
			{																														  
				assessGrade(studentMarks);	// calling assessGrade(double) verssion to check whether student is pass or not													  
				break;																												  
			}																														  
		}
		else
		{
			conditionIndex = (strcmp(studentGrade[indexNum], marks));
			if (indexNum >= GRADE_INDEX && indexNum < MAX_CONDITION && conditionIndex == 0)
			{
				printf(" Student has Special Situation: %s (%s)\n", studentGrade[indexNum], studentGrade[indexNum + 1]);
				break;
			}
			if (indexNum == STOP_SEARCH)
			{
				printf("   >> User specified a INVALID \"String\" type grade \n"); 
			}
		}
		indexNum++;
	}
}

/*=======================================================================================================================================
* Function: assessGrade()

 * Description:	This function will take student marks and check it and displays whether student is pass or fail.

 * Parameter:	double marks: - It contain final grades of student provided by the user in main function.

 * Return Value returns nothing as return type is void

 */

void assessGrade(double marks)
{
	int signal = 0;
	signal = checking(marks);// checking thst input is in range or not

	if (signal == SUCESS)
	{
		if (marks > PASS_MARK)
		{
			printf(" Student achieved %.2f%% which is a PASS condition.\n", marks);
		}
		else
		{
			printf(" Student achieved %.2f%% which is a FAIL condition.\n", marks);

		}
	}
	else
	{
		printf("   >> User specified a INVALID \"double\" type grade \n");
	}
}

/*=======================================================================================================================================
* Function: assessGrade()

 * Description:	This function will take student marks and check it and displays whether student is pass or fail.

 * Parameter:	int mark []: - It contain 5 integer marks of 5 different assignment of student provided by the user in main function.

 * Return Value returns 0 if the user input is out of range otherwise 0.

 */
int assessGrade(int marks[5])
{
	int totalSum = 0;
	int index = 0;
	double totalAvereage = 0;
	int signal = 0;

	signal = checking(marks);
	if (signal == 0)
	{
		printf("   >> User specified a INVALID \"array\" type grade \n");
		return 0;
	}
	while (signal == 1 && index < 5)
	{
		totalSum = totalSum + marks[index];
		index++;
	}
	totalAvereage = findingAverage(totalSum);
	assessGrade(totalAvereage);
	emptyArray(marks);
	return 1;
}
/*=======================================================================================================================================
* Function: findingAverage()

 * Description:	This function will take sum of each element of array from calculate total average.

 * Parameter:	int marks: - It contains total sum of elementís of array.

 * Return Value returns average: - Number containing total average value.

 */
int findingAverage(int marks)
{
	int average = 0;
	average = marks / 5;
	return average;
}

/*=======================================================================================================================================
* Function: checking ()

 * Description:	This function will take an array of marks and check each element if they are in range or not.

 * Parameter:	char* marks- It contain 5 integer marks of 5 different assignment of student provided by the user in main function.

 * Return Value returns 1 if all the elements are in range.

 */
int checking(int marks[5])
{
	int count = 0;

	while (count < 5)
	{
		if (marks[count]<0 || marks[count]>MAX_MARK)
		{
			return 0;
		}
		count++;
	}
	return 1;
}

/*=======================================================================================================================================
 * Function: checking ()

 * Description:	This function will take final grade of student and check if they are in range or not.

 * Parameter:	double marks: - It contain final grades of student provided by the user in main function.

 * Return Value returns success if the number is in range otherwise return Fail

 */
int checking(double marks)
{
	if (marks < MAX_MARK && marks > MIN_MARK)
	{
		return SUCESS;
	}
	else
	{
		return FAIL;
	}
}

/*=======================================================================================================================================
* Function: emptyArray()

 * Description:	This function will take an array of marks and fill elements of array with value 0.

 * Parameter:	int mark []: -  It contain 5 integer marks of 5 different assignment of student provided by the user in main function.

 * Return Value returns nothing

 */
void emptyArray(int mark[])
{
	int i = 0;
	do
	{
		mark[i] = 0;
		i++;
	} while (i < 5);
}

/*=======================================================================================================================================
 * Function: checking ()

 * Description:	This function will check user Input whether it is a floating point, alphabetical character or an integer

 * Parameter:	char input[]:- an array contaning user input

 * Return Value: returns 1 if user input special character for exit, return 2 if it an alphabetical character, 
				 return 3 if it is a floating point, return 4 if it is an integer.

 */
int checking(char input[])
{
	int exit = 1;
	exit = (strcmp(input, "X\n"));
	(*(strchr(input, '\n')) = '\0');

	if (exit == 0)
	{
		return 1;
	}
	else if (isalpha(input[0]) != 0 && strlen(input) <= 3)
	{
		return 2;

	}
	else if (strchr(input, '.') != NULL)
	{

		return 3;
	}
	else if (isdigit(input[0]) != 0)
	{
		return 4;
	}
}