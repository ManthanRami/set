/*
*  FILE          : Circle.cpp
*  PROJECT       : assign04
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 07-13-2019
*  DESCRIPTION   : This file containts defination of cunstructor, mutator, accessor, destructor and other function of Circle object which are used in project assign04.
*/
#include"Circle.h"
#include"Shape.h"
/*****************************************************************************************************************************************************************************************************
	Name :		Circle -- CONSTRUCTOR
	Purpose :	To instantiate a new Shape object - given a set of attribute values
	Inputs :	buff	String		Name of the colour of the object.
				r		float		radius of the object
	Outputs :   NONE
	Returns :	Nothing
*****************************************************************************************************************************************************************************************************/
Circle::Circle(string buff, float r)
{
	SetRadius(r);
	SetName("Circle");
	SetColour(buff);
}
/*****************************************************************************************************************************************************************************************************
	Name :		~Circle -- DESTRUCTOR
	Purpose :	To destroy created object object
	Inputs :	Nothing
	Outputs :   simply printout the a message.
	Returns :	Nothing
*******************************************************************************************************************************************************************************************************/
Circle::~Circle()
{
	cout << "The circle is destroyed..." << endl;
}
/*****************************************************************************************************************************************************************************************************
	Name :		GetRadius -- Accessor
	Purpose :	Called to get the value of radius data member of the object
	Inputs :	Nothing
	Outputs :   NONE
	Returns :	radius	float	radius of the circle 	
*******************************************************************************************************************************************************************************************************/
float Circle::GetRadius(void)
{
	return radius;
}
/*****************************************************************************************************************************************************************************************************
	Name :		SetRadius -- Mutator
	Purpose :	To instantiate radius data member of shape class to given a set of attribute values
	Inputs :	r	float	radius of the object.
	Outputs :   NONE
	Returns :	Nothing
*******************************************************************************************************************************************************************************************************/
void Circle::SetRadius(float r)
{
	if (r >= 0.00)
	{
		radius = r;
	}
	else
	{
		radius = 0.00;
	}
}
/*****************************************************************************************************************************************************************************************************
	Name	:	Perimeter
	Purpose :	To calculate the perimeter of the square
	Inputs	:	Nothing
	Outputs :   NONE
	Returns :	Total calculated perimeter.
*******************************************************************************************************************************************************************************************************/
float Circle::Perimeter()
{
	return 2.0f * 3.141592f*radius;
}
/*****************************************************************************************************************************************************************************************************
	Name	:	Area
	Purpose :	To calculate the Area of the square
	Inputs	:	Nothing
	Outputs :   NONE
	Returns :	Total calculated Area.
*******************************************************************************************************************************************************************************************************/
float Circle::Area()
{
	return (float)3.141592*radius*radius;
}
/*****************************************************************************************************************************************************************************************************
	Name	:	OverallDimension
	Purpose :	To calculate the OverallDimension of the square
	Inputs	:	Nothing
	Outputs :   NONE
	Returns :	Total OverallDimension Area.
*******************************************************************************************************************************************************************************************************/
float Circle::OverallDimension()
{
	return (float)2 * radius;
}
/*****************************************************************************************************************************************************************************************************
	Name	:	Show
	Purpose :	To display out object information
	Inputs	:	Nothing
	Outputs :   display Circle object inforamtion
	Returns :	Nothing
*******************************************************************************************************************************************************************************************************/
void Circle::Show()
{
	cout << "\nShape Information" << endl;
	cout << "Name		  :" << GetName() << endl;
	cout << "Colour		  :" << GetColour() << endl;
	cout << "Radius		  :" << GetRadius() << "cm" << endl;
	cout << "Circumfrence	  :" << Perimeter() << "cm" << endl;
	cout << "Area		  :" << Area() << "square cm" << endl;
}