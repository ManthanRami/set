/*
*  FILE          : Shape.h
*  PROJECT       : assign04
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 07-13-2019
*  DESCRIPTION   :
*	This file contains all the prototypes, class defination and constant declaration of shape which is used in project assign04.
*/
#pragma once
#include<stdio.h>
#include<string.h>
#include<string>
#include<iostream>
#include<new.h>

using namespace std;

class Shape
{
private:
	string name;	//name of the shape
	string colour;	//colour of the shape

public:
	//constructor
	Shape(string name="Unknown", string colour="undefined");
	//destructor
	~Shape(void);
	//accesssor
	const string GetName();
	const string GetColour();
	//mutator
	void SetName(string);
	void SetColour(string);
	//other methods(Pure virtual function)
	virtual float Perimeter(void) = 0;
	virtual float Area(void) = 0;
	virtual float OverallDimension(void) = 0;
};