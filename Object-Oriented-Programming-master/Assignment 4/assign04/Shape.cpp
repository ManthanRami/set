/*
*  FILE          : Circle.cpp
*  PROJECT       : assign04
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 07-13-2019
*  DESCRIPTION   : This file containts defination of cunstructor, mutator, accessor, destructor and other function of shape object which are used in project assign04.
*/
#include"Shape.h"

/*****************************************************************************************************************************************************************************************************
	Name :		Shape -- CONSTRUCTOR
	Purpose :	To instantiate a new Shape object - given a set of attribute values
	Inputs :	name		String		name of the object
				colour		String		Name of the colour of the object.
	Outputs :   NONE
	Returns :	Nothing
*******************************************************************************************************************************************************************************************************/
Shape::Shape(string name, string colour)
{
	SetName(name);
	SetColour(colour);
}
/*****************************************************************************************************************************************************************************************************
	Name :		SetName -- Mutator
	Purpose :	To instantiate name data member of shape class to given a set of attribute values
	Inputs :	name		String		name of the object.
	Outputs :   NONE
	Returns :	Nothing
*******************************************************************************************************************************************************************************************************/
void Shape::SetName(string buff)
{
	if (buff == "Circle" || buff == "Square" || buff == "Unknown")
	{
		name = buff;
	}
	
}
/*****************************************************************************************************************************************************************************************************
	Name :		SetColour -- Mutator
	Purpose :	To instantiate colour data member of shape class to given a set of attribute values
	Inputs :	buffer		String		colour of the object.
	Outputs :   NONE
	Returns :	Nothing
*******************************************************************************************************************************************************************************************************/
void Shape::SetColour(string buffer)
{
	if (buffer == "red" || buffer == "green"
		|| buffer == "blue" || buffer == "yellow"
		|| buffer == "purple" || buffer == "pink"
		|| buffer == "orange" || buffer == "undefined")
	{
		colour = buffer;
	}
}
/*****************************************************************************************************************************************************************************************************
	Name :		GetName -- Accessor
	Purpose :	Called to get the value of name data member of the object
	Inputs :	Nothing
	Outputs :   NONE
	Returns :	name	const string	name of the shape
*******************************************************************************************************************************************************************************************************/
const string Shape::GetName()
{
	return name;
}
/*****************************************************************************************************************************************************************************************************
	Name :		GetColour -- Accessor
	Purpose :	Called to get the value of colour data member of the object
	Inputs :	Nothing
	Outputs :   NONE
	Returns :	coloue	const string	colour of the shape
*******************************************************************************************************************************************************************************************************/
const string Shape::GetColour()
{
	return colour;
}
/*****************************************************************************************************************************************************************************************************
	Name :		~Shape -- Destructor
	Purpose :	Does Nothing
	Inputs :	Nothing
	Outputs :   NONE
	Returns :	Nothing
*******************************************************************************************************************************************************************************************************/
Shape::~Shape()
{
	
}
