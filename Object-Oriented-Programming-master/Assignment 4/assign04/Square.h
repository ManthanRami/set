/*
*  FILE          : Square.h
*  PROJECT       : assign04
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 07-13-2019
*  DESCRIPTION   :
*	This file contains all the prototypes, class defination and constant declaration of Square which is used in project assign04.
*/
#include"Shape.h"
class Square :public Shape
{
private:
	float sideLength;	//side lenght of the square
public:
	//constructor
	Square(float slen = 0.00, string buff = "undefined");
	//destructor
	~Square(void);
	//mutator
	void SetLength(float);
	//accessor
	float GetLength(void);
	//other methods
	void Show(void);
	float Perimeter(void);
	float Area(void);
	float OverallDimension(void);
};	