/*
*  FILE          : myShape.cpp
*  PROJECT       : assign04
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 07-13-2019
*  DESCRIPTION   : This program will do a test harness on dianamically created object. 
				   it take input from the user and create dunamaically an object.
*/
#include"Circle.h"
#include"Square.h"
#include"Shape.h"
  
int main()
{
	float circleRadius = 0;
	float squareLength = 0;
	string buffer_s = "";
	string buffer_c ="";
	Circle *ptr1 = NULL;
	Square *ptr2 = NULL;
	
	cout << "Please Enter detail for circle\n " << endl;
	cout << "Please Enter radius for circle: ";
	cin >> circleRadius;
	cout << "Please Enter colour for circle: ";
	cin >> buffer_c;

	ptr1 = new  Circle(buffer_c, circleRadius);
	if (ptr1 == NULL)							//checking for memory error
	{
		cout << "Out of memory" << endl;
		return 0;
	}
	ptr1->Show();
	cout << "\n";
	cout << "Please Enter detail for square\n " << endl;
	cout << "Please Enter length for square: ";
	cin >> squareLength;
	cout << "Please Enter colour for circle: ";
	cin >> buffer_s;

	ptr2 = new Square(squareLength, buffer_s);
	if (ptr2 == NULL)							//checking for memory error
	{
		cout << "Out of memory" << endl;
		return 0;
	}
	ptr2->Show();
	printf("\n\n");
	delete ptr1;
	printf("\n");
	delete ptr2;

	return 0;
}