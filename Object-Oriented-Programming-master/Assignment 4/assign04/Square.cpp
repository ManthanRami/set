/*
*  FILE          : Square.cpp
*  PROJECT       : assign04
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 07-13-2019
*  DESCRIPTION   : This file containts defination of cunstructor, mutator, accessor, destructor and other function of square object which are used in project assign04.
*/ 
#include"Square.h"
#include"Shape.h"
/*****************************************************************************************************************************************************************************************************
	Name :		Square -- CONSTRUCTOR
	Purpose :	To instantiate a new Shape object - given a set of attribute values
	Inputs :	slen		float		length of side of the object
				colour		String		Name of the colour of the object.
	Outputs :   NONE
	Returns :	Nothing
*******************************************************************************************************************************************************************************************************/
Square::Square(float slen, string buff)
{
	SetLength(slen);
	SetName("Square");
	SetColour(buff);
}
/*****************************************************************************************************************************************************************************************************
	Name :		~Square -- DESTRUCTOR
	Purpose :	To destroy created object object 
	Inputs :	Nothing
	Outputs :   simply printout the a message.
	Returns :	Nothing
*******************************************************************************************************************************************************************************************************/
Square::~Square()
{
	cout << "The square is destroyed..." << endl;
}
/*****************************************************************************************************************************************************************************************************
	Name :		GetLength -- Accessor
	Purpose :	Called to get the value of sideLength data member of the object
	Inputs :	Nothing
	Outputs :   NONE
	Returns :	sideLength		float		length of side of the object
*******************************************************************************************************************************************************************************************************/
float Square::GetLength()
{
	return sideLength;
}
/*****************************************************************************************************************************************************************************************************
	Name :		SetLength -- Mutator
	Purpose :	To instantiate colour data member of shape class to given a set of attribute values
	Inputs :	slen	float	length of side of the object.
	Outputs :   NONE
	Returns :	Nothing
*******************************************************************************************************************************************************************************************************/
void Square::SetLength(float slen)
{
	if (slen >= 0.00)
	{
		sideLength = slen;
	}
	else
	{
		sideLength = 0.00;
	}
}
/*****************************************************************************************************************************************************************************************************
	Name	:	Perimeter 
	Purpose :	To calculate the perimeter of the square
	Inputs	:	Nothing
	Outputs :   NONE
	Returns :	Total calculated perimeter.
*******************************************************************************************************************************************************************************************************/
float Square::Perimeter(void)
{
	return 4 * sideLength;
}
/*****************************************************************************************************************************************************************************************************
	Name	:	Area
	Purpose :	To calculate the Area of the square
	Inputs	:	Nothing
	Outputs :   NONE
	Returns :	Total calculated Area.
*******************************************************************************************************************************************************************************************************/
float Square::Area(void)
{
	return sideLength * sideLength;
}
/*****************************************************************************************************************************************************************************************************
	Name	:	OverallDimension
	Purpose :	To calculate the OverallDimension of the square
	Inputs	:	Nothing
	Outputs :   NONE
	Returns :	Total OverallDimension Area.
*******************************************************************************************************************************************************************************************************/
float Square::OverallDimension(void)
{
	return sideLength;
}
/*****************************************************************************************************************************************************************************************************
	Name	:	Show
	Purpose :	To display out object information
	Inputs	:	Nothing
	Outputs :   display square object inforamtion
	Returns :	Nothing
*******************************************************************************************************************************************************************************************************/
void Square::Show()
{
	cout << "\nShape Information" << endl;
	cout << "Name		 :" << GetName() << endl;
	cout << "Colour		 :" << GetColour() << endl;
	cout << "Side-Length	 :" << GetLength() << "cm" << endl;
	cout << "Perimeter	 :" << Perimeter() << "cm" << endl;
	cout << "Area		 :" << Area() << "square cm" << endl;
}