/*
*  FILE          : testDisneyCharacter.cpp
*  PROJECT       : DisneyCharacter
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 06-21-2019
*  DESCRIPTION   :
*	This project will create two characters named "Mickey" and "Minnie" and assign their value and play with it by copy data of each other.
*/
#include"DisneyCharacter.h"

int main(void)
{
	//creating object named mickey
	DisneyCharacter mickey("Mickey", "1929-01-01", 100, 'M');
	//creating object named minnie
	DisneyCharacter minnie("Minnie", "1930-01-01");
	//dumping out mickey
	mickey.ShowInfo();
	//dumping out minnie
	minnie.ShowInfo();
	//try setting the number of Minnie�s movies equal to Mickey�s by calling the SameMovies() method 
	minnie.SameMovies(mickey);
	//dumping out minnie
	printf("\n\n Dumping out Minnie after setting the number of Minnie�s movies equal to Mickey�s \n ");
	minnie.ShowInfo();
	//placing minnie in Epcot Park
	minnie.PlaceCharacter('E');
	printf("\n\n Dumping out Minnie after placing minnie in Epcot Park\n");
	//dumping out minnie after
	minnie.ShowInfo();
	return 0;
}