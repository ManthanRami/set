/*
*  FILE          : DisneyCharacter.h
*  PROJECT       : DisneyCharacter
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 06-21-2019
*  DESCRIPTION   :
*	This file contains all the prototypes, class defination and constant declaration which is used in project DisneyCharacter.
*/
#pragma once
#include<iostream>
#include<stdio.h>
#include<string>
#pragma warning(disable : 4996)

using namespace std;

//Constant declaration
#define EPCOT 'E'
#define NOT_PLACED 'N'
#define DISNEY_STUDIO 'S'
#define MAGIC_KINGDOM 'M'
#define ANIMAL_KINGDOM 'A'
#define CALIFORNIA_ADVENTURE 'C'

#define BLANK_LEAVE ""
#define DATE_FORMAT '-'
#define FORMAT_CHECK1 4
#define FORMAT_CHECK2 7
#define DEFAULT_VALUE 0
#define MAX_SIZE 50
#define ZERO 0


class DisneyCharacter
{
private:
	char name[MAX_SIZE];
	string creationDate;
	int numMovies;
	string whichPark;


public:

	// mutator -modify/change the data member values for the object
	void SetName(const char*);
	bool SetNumMovie(int);
	bool SetParkName(char);
	bool SetDate(string);

	//accessors
	char* getName();
	string getDate();
	int getNumMovies();
	string getPark();

	//Constructor prototype;
	DisneyCharacter(const char *Cname, const char * date, int Mnumber, char Pname);

	//Constructor prototype;
	DisneyCharacter(const char*, const char *);

	//Destructor
	~DisneyCharacter();

	//Other methods
	void ShowInfo(void);
	void PlaceCharacter(char);
	void SameMovies(DisneyCharacter& anothercahracter);
};
