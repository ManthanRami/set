/*
*  FILE          : DisneyCharacter.cpp
*  PROJECT       : DisneyCharacter
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 06-21-2019
*  DESCRIPTION   :
*	This file contains all the defination of construction, destruction, mutators,accessor and other methods which is used in project DisneyCharacter.
*/
#include"DisneyCharacter.h"
/*****************************************************************************************************************************************************************************************************
	Name :		DisneyCharacter -- CONSTRUCTOR
	Purpose :	To instantiate a new DisneyCharacter object - given a set of attribute values
	Inputs :	name		String		name of the Character
				date		String		data of creation of character
				m_number	int			Number of movies done by the character
				pname		String		Name of the park character was
	Outputs :   NONE
	Returns :	Nothing
*******************************************************************************************************************************************************************************************************/
DisneyCharacter::DisneyCharacter(const char* name, const char * date, int m_number, char pname)
{
	//setting name
	SetName(name);
	bool retCode = false;
	//setting park name
	retCode = SetParkName(pname);
	if (retCode == false)
	{
		SetParkName(NOT_PLACED);	
	}
	//setting date
	retCode = SetDate(date);
	if (retCode == false)
	{
		creationDate = BLANK_LEAVE;
	}
	// setting number of movies
	retCode = SetNumMovie(m_number);
	if (retCode == false)
	{
		SetNumMovie(DEFAULT_VALUE);
	}
}
/*****************************************************************************************************************************************************************************************************
	Name :		DisneyCharacter -- CONSTRUCTOR
	Purpose :	To instantiate a new DisneyCharacter object - given a set of attribute values
	Inputs :	name		String		name of the Character
				date		String		data of creation of character
	Outputs :	NONE
	Returns :	Nothing
*******************************************************************************************************************************************************************************************************/
// Constructor defination
DisneyCharacter::DisneyCharacter(const char* name, const char * date)
{
	bool retcode = false;
	SetNumMovie(DEFAULT_VALUE);
	SetParkName(NOT_PLACED);
	SetName(name);
	retcode = SetDate(date);
	if (retcode == false)
	{
		creationDate = BLANK_LEAVE;
	}
}
/*****************************************************************************************************************************************************************************************************
	Name	:	SetName
	Purpose :	This mutator method is used to set the name data member of the character
	Inputs	:	buffer	string	the name of the character that is trying to be set
	Outputs	:	NONE
	Returns	:	Nothing
*******************************************************************************************************************************************************************************************************/
void DisneyCharacter::SetName(const char* buffer)
{
	unsigned index = ZERO;
	size_t len = strlen(buffer);
	for (index = ZERO; index < len; index++)
	{
		name[index] = buffer[index];
	}
	name[index] = '\0';
}
/*****************************************************************************************************************************************************************************************************
	Name	:	SetNumMovie
	Purpose :	This mutator method is used to set the numMovies data member of the character
	Inputs	:	number	int	the number of movies the character played that is trying to be set
	Outputs	:	NONE
	Returns	:	retCode		bool	indicates whether the number is in range or not
*******************************************************************************************************************************************************************************************************/
bool DisneyCharacter::SetNumMovie(int number)
{
	bool retCode = false;
	if (number >= ZERO)
	{
		numMovies =number ;
		retCode = true;
		return retCode;
	}
	else
	{
		return retCode;
	}
}
/*****************************************************************************************************************************************************************************************************
	Name	:	SetParkName
	Purpose :	This mutator method is used to set the whichPark data member of the character
	Inputs	:	buf	string	the name of park the character played in that is trying to be set
	Outputs	:	NONE
	Returns	:	retCode		bool	indicates whether the given input is valid or not
*******************************************************************************************************************************************************************************************************/
bool DisneyCharacter::SetParkName(char buf)
{
	bool retCode = true;
	if (buf == MAGIC_KINGDOM)
	{
		whichPark = "Magic Kingdom";
		return retCode;
	}
	else if (buf == DISNEY_STUDIO)
	{
		whichPark = "Disney Studios";
		return retCode;
	}
	else if (buf == ANIMAL_KINGDOM)
	{
		whichPark = "Animal Kingdom";
		return retCode;
	}
	else if (buf == EPCOT)
	{
		whichPark = "Epcot";
		return retCode;
	}
	else if (buf == CALIFORNIA_ADVENTURE)
	{
		whichPark = "California Adventure";
		return retCode;

	}
	else if (buf == NOT_PLACED)
	{
		whichPark = "Not Placed";
		return retCode;
	}
	else
	{
		retCode = false;
		return retCode;
	}
}
/*****************************************************************************************************************************************************************************************************
	Name	:	SetDate
	Purpose :	this mutator method is used to set the creationDate data member of the character
	Inputs	:	date	string	the date of creation of the character that is trying to be set
	Outputs	:	NONE
	Returns	:	retCode		bool	indicates whether the given date is in format or not
*******************************************************************************************************************************************************************************************************/
bool DisneyCharacter::SetDate(string date)
{
	bool retCode = false;
	if (date[FORMAT_CHECK1] == DATE_FORMAT && date[FORMAT_CHECK2] == DATE_FORMAT)
	{
		creationDate = date;
		retCode = true;
	}
	else
	{
		return retCode;
	}
	return retCode;
}
/*****************************************************************************************************************************************************************************************************
	Name	:	getName
	Purpose :	Called to get the value of name data member of the character
	Inputs	:	NONE
	Outputs	:	NONE
	Returns	:	the name of charater as a string
*******************************************************************************************************************************************************************************************************/
char* DisneyCharacter::getName(void)
{
	return name;
}
/*****************************************************************************************************************************************************************************************************
	Name	:	getDate
	Purpose :	Called to get the value of creationDate data member of the character
	Inputs	:	NONE
	Outputs	:	NONE
	Returns	:	the date of creation of the charater as a string
*******************************************************************************************************************************************************************************************************/
string DisneyCharacter::getDate(void)
{
	return creationDate;
}
/*****************************************************************************************************************************************************************************************************
	Name	:	getNumMovies
	Purpose :	Called to get the value of numMovie data member of the character
	Inputs	:	NONE
	Outputs	:	NONE
	Returns	:	the number of movie done by the charater as an integer
*******************************************************************************************************************************************************************************************************/
int DisneyCharacter::getNumMovies(void)
{
	return numMovies;
}
/*****************************************************************************************************************************************************************************************************
	Name	:	getPark
	Purpose :	Called to get the value of whichPark data member of the character
	Inputs	:	NONE
	Outputs	:	NONE
	Returns	:	the name of the park charater is placed as a string
*******************************************************************************************************************************************************************************************************/
string DisneyCharacter::getPark(void)
{
	return whichPark;
}
/*****************************************************************************************************************************************************************************************************
	Name	:	ShowInfo
	Purpose :	Called printout athe value of all data member of the character
	Inputs	:	NONE
	Outputs	:	NONE
	Returns	:	Nothing
*******************************************************************************************************************************************************************************************************/
void DisneyCharacter::ShowInfo(void)
{
	printf("\n\n Name of the character is:               %s \n ", getName());
	printf("Date of creation of character is :      %s\n", getDate().c_str());
	printf(" Number of movies done by character are: %d\n", getNumMovies());
	printf(" Park name of character is:              %s\n", getPark().c_str());
}
/*****************************************************************************************************************************************************************************************************
	Name	:	PlaceCharacter
	Purpose :	Called to set the value of whichPark data member of the character to particular park
	Inputs	:	park	string		name of the park character is palced to
	Outputs	:	NONE
	Returns	:	Nothing
*******************************************************************************************************************************************************************************************************/
void DisneyCharacter::PlaceCharacter(char park)
{
	SetParkName(park);
}
/*****************************************************************************************************************************************************************************************************
	Name	:	SameMovies
	Purpose :	Called copy the value of numMovie data member of one character to other
	Inputs	:	anothercahracter name of the object or particular object is passed.
	Outputs	:	NONE
	Returns	:	Nothing
*******************************************************************************************************************************************************************************************************/
void DisneyCharacter::SameMovies(DisneyCharacter& anothercahracter)
{
	SetNumMovie(anothercahracter.getNumMovies());
}
/*****************************************************************************************************************************************************************************************************
Name	: DisneyCharacter -- DESTRUCTOR
	Purpose : to destroy the DisneyCharacter object - free up the memory associated with the object
	Inputs	:	NONE
	Outputs	:	outputs a final message from the object before being destroyed
	Returns	:	Nothing
*******************************************************************************************************************************************************************************************************/
DisneyCharacter::~DisneyCharacter()
{
	printf("\n\n%s destroyed\n", getName());
}