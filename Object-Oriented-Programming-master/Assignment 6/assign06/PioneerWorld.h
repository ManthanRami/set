/*
*  FILE          : PioneerWorld.h
*  PROJECT       : assign06
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 07-27-2019
*  DESCRIPTION   :
*	This file contains all the prototypes, class defination of PioneerWorld and  all defination of mutators in this file  which is used in project assign06.
*/
#pragma once
#include"PioneerAm.h"
#define P_WRLD_MAX_AM 1602
#define P_WRLD_DIF_AM 9
#define P_WRLD_MIN_AM 531

class PioneerWorld : public PioneerAM
{

private:

public:

	/************************************************************************************************************
	Name	:	PioneerWorld()---constructor
	Purpose :	This block of code create a object of type PioneerWorld and set it curent station and preset to minimum value.
	Inputs	:	NONE
	Outputs	:	NONE
	Returns	:	Nothing
	/************************************************************************************************************/
	PioneerWorld() :PioneerAM()
	{
		SetStation(P_WRLD_MIN_AM);
		SetPreset(P_WRLD_MIN_AM);
	}
	/**********************************************************************************************************************
	Name :		~PioneerAM -- DESTRUCTOR
	Purpose :	to destroy the PioneerAM object - free up the memory associated with the object
	Inputs :	NONE
	Outputs :   outputs a final message from the object before being destroyed
	Returns :	Nothing
	**********************************************************************************************************************/
	virtual ~PioneerWorld(void)
	{
		cout << "Destroying Pioneer XS440-WRLD Radio Classs\n" << endl;
	}
	/*************************************************************************************************************
	Name	:	ScanUp()---Mutator
	Purpose :	This mutator will scan up the frequency up in case of AM it will increase by 9.
	Inputs	:	NONE
	Outputs	:	NONE
	Returns	:	NOTING
	/*************************************************************************************************************/
	void ScanUp()
	{
		if (strcmp("AM", GetFrequency()) == 0)
		{
			//if current_station is 1602, the current_station becomes 531
			if (GetCurrentStation() == P_WRLD_MAX_AM)
			{
				SetStation(P_WRLD_MIN_AM);
			}
			else
			{
				SetStation(GetCurrentStation() + P_WRLD_DIF_AM);
			}
			SetLastStation(GetFrequency(), (int)GetCurrentStation());
		}

	}
	/*************************************************************************************************************
	Name	:	ShowCurrentSettings()---Mutator
	Purpose :	This mutator will print out all the current setting state of the radio
	Inputs	:	NONE
	Outputs	:	SHow radio current setting
	Returns	:	NOTING
	/*************************************************************************************************************/
	virtual void ShowCurrentSettings(void)
	{
		cout << "*********************************************************" << endl;
		cout << "\n" << endl;
		cout << "Pioneer XS440-WRLD" << endl;
		if (IsRadioOn())
		{
			cout << "Radio is on" << endl;
			cout << "Volume:" << GetCurrentVolume() << endl;
			cout << "Current Station:   " << GetCurrentStation() << " " << GetFrequency() << endl;
			cout << "AM Buttons:" << endl;
			cout << "1:  " << setw(5) << GetButton()[0].AMFreqs << ",  ";
			cout << "2:  " << setw(5) << GetButton()[1].AMFreqs << ",  ";
			cout << "3:  " << setw(5) << GetButton()[2].AMFreqs << ",  ";
			cout << "4:  " << setw(5) << GetButton()[3].AMFreqs << ",  ";
			cout << "5:  " << setw(5) << GetButton()[4].AMFreqs << endl;
			cout << "\n\n\n" << endl;
		}
		else
		{
			cout << "Radio is off" << endl;
			cout << "\n\n\n" << endl;
		}

	}
	/*************************************************************************************************************
	Name	:	ScanDown()---Mutator
	Purpose :	This mutator will scan up the frequency up in case of AM it will decrease by 9.
	Inputs	:	NONE
	Outputs	:	NONE
	Returns	:	NOTING
	/*************************************************************************************************************/
	virtual void ScanDown()
	{
		if (strcmp("AM", GetFrequency()) == 0)
		{
			//if current_station is 531, the current_station becomes 1602
			if (GetCurrentStation() == P_WRLD_MIN_AM)
			{
				SetStation(P_WRLD_MAX_AM);
			}
			else
			{
				SetStation(GetCurrentStation() - P_WRLD_DIF_AM);
			}
			SetLastStation(GetFrequency(), (int)GetCurrentStation());
		}
	}
};