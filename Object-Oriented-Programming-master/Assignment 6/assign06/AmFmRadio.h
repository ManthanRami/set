/*
*  FILE          : AmFmRadio.h
*  PROJECT       : assign06
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 07-27-2019
*  DESCRIPTION   :
*	This file contains all the prototypes, class defination of AmFmRadio and constant declaration which is used in project assign06.
*/
#include<iostream>
#include<iomanip>
#include<conio.h>
//AmFmRadio.h
#pragma warning(disable: 4996)

using namespace std;

#define LOW_VOL 0
#define NUM_FREQ 5

#define MIN_AM 530
#define MAX_AM 1700

#define MIN_FM 87.9
#define MAX_FM 107.9

#define MIN_VOL 0
#define MAX_VOL 100

#define DIF_AM 10
#define DIF_FM 0.2

#define VOL_LEN 20




#ifndef _AmFmRadio_H
#define _AmFmRadio_H

struct Freqs
{
	int AMFreqs;
	double FMFreqs;
};

class AmFmRadio
{
private:

	Freqs	button[NUM_FREQ];	//used to store value of AM & FM buttons value
	double	current_station;	//used to save current station
	char	frequency[3];		//used to store current band AM or FM
	int		volume;				//used to save current volume
	Freqs	lastStation;		//used to save last station value before turning off radio
	int		prev_volume;		//used to save current volume into this data member before turning off radio.
	bool	on;					//ussed to save  current radio state ON or OFF by boolean value
	bool displayOutput;			//used to save dispaly output condition 


public:


	//constructor
	AmFmRadio(bool);
	AmFmRadio(bool radio_cur_state, struct Freqs f_Array[NUM_FREQ]);

	//destructor
	virtual ~AmFmRadio(void);

	// Settor
	void SetStation(double);
	//sets button with current station by being passed a button number
	int SetButton(int);
	//sets volume
	int SetVolume(void);
	int SetVolume(int);

	//Methods
	void SetdisplayOutput(bool);
	//shows volume, button settings, current station, AM or FM
	void ShowCurrentSettings(void);
	void PowerToggle(void);
	//changes frequency up in increments of .2 for FM, 10 for AM
	virtual void ScanUp(void);
	//changes frequency up in decrements of .2 for FM, 10 for AM
	virtual void ScanDown(void);
	void SetLastStation(char* freq, int station);
	//returns a true if the radio is currently powered on, and false if the radio is in
	//the off position
	bool IsRadioOn(void);
	//toggles frequency between AM and FM and sets current station
	void ToggleFrequency(void);
	//toggles frequency between AM and FM and sets current station 
	virtual void ToggleBand(void);
	//sets current station by being passed a button number
	int SelectCurrentStation(int Freqs_num);
	void SetFrequence(bool);
	void SetPreset(int station_AM = MIN_AM, float station_FM = MIN_FM);
	//accessor
	double GetCurrentStation(void);
	int GetCurrentVolume(void);
	int Getprev_Volume(void);
	char* GetFrequency(void);
	Freqs* GetButton(void);

};
#endif