/*
*  FILE          : UltimateRadio.cpp
*  PROJECT       : assign06
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 07-27-2019
*  DESCRIPTION   :
*	This program will create a dynamically created pRadio object by using Grand parent class
	AmFmRadio by concept of inheritence and also take a character as a user input and play with
	setting of myRadio.
*/
#include"AmFmRadio.h"
#include"PioneerAm.h"
#include"PioneerCarRadio.h"
#include"PioneerWorld.h"

#define NO_RADIO_CONDITION 1
#define EXIT_CONDITION 2
#define MEMORY_ERROR_CONDITION 3
//prototype
PioneerCarRadio* createRadio(char buffer);

int main()
{
	PioneerCarRadio *pRadio = NULL;
	char buf = NULL;

	while (true)
	{
		buf = getch();
		try
		{
			pRadio = createRadio(buf);
		}
		catch (int error)
		{
			if (error == EXIT_CONDITION)
			{
				cout << "Exiting Program" << endl;
				return 0;
			}
			else if(error== NO_RADIO_CONDITION)
			{
				cout << "No radio selected" << endl;
				return 0;
			}
			else if (error == MEMORY_ERROR_CONDITION)
			{
				return 0;
			}
			
		}
		while (true)
		{
			pRadio->ShowCurrentSettings();
			buf = getch();
			if (buf == 'x')
			{
				break;
			}
			pRadio->UserInterface(buf);
		}
		delete pRadio;
	}
	return 0;
}
/************************************************************************************************************
Name	:	PioneerCarRadio* createRadio
Purpose :	This block of code returns appropriate pointer to the object depends upon user selection
Inputs	:	buffer char: User selection for object
Outputs	:	NONE
Returns	:	Pointer to the object or NULL.
/************************************************************************************************************/
PioneerCarRadio* createRadio(char buffer)
{
	PioneerCarRadio* temp = NULL;
	while (true)
	{
		if (buffer == 'c')
		{
			try 
			{
				temp = new PioneerCarRadio();
			}
			catch (bad_alloc& b)
			{
				cout << "Error in Memory Allocation:  " << b.what() << endl;
			}
			break;
		}
		else if (buffer == 'w')
		{
			try 
			{
				temp = new PioneerWorld;
			}
			catch (bad_alloc& b)
			{
				cout << "Error in Memory Allocation:  " << b.what() << endl;
			}
			break;
		}
		else if (buffer == 'a')
		{
			try
			{
				temp = new PioneerAM();

			}
			catch (bad_alloc& b)
			{
				cout << "Error in Memory Allocation:  " << b.what() << endl;
			}
			break;
		}
		else if (buffer == 'x')
		{
			throw EXIT_CONDITION;
			return NULL;
		}
		if (temp != NULL)
		{
			return temp;
		}
		else
		{
			throw NO_RADIO_CONDITION;
		}

	}

}