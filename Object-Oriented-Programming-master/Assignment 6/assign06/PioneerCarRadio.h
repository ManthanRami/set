#pragma once
/*
*  FILE          : PioneerCarRadio.h
*  PROJECT       : assign05
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 07-20-2019
*  DESCRIPTION   :
*	This file contains all the declaration construction, destruction,,constants, mutators,accessor and other methods which is used in project assign06.
*/
#include"AmFmRadio.h"
//constant declaration
#define MAX_BUTTON 16

#define ON_OFF		0
#define INC_VOL		1
#define DEC_VOL		2
#define SCAN_UP		3
#define SCAN_DOWN	4
#define AM_FM		5

#define C_BUTTON1	6
#define C_BUTTON2	7
#define C_BUTTON3	8
#define C_BUTTON4	9
#define C_BUTTON5	10

#define S_BUTTON1	11
#define S_BUTTON2	12
#define S_BUTTON3	13
#define S_BUTTON4	14
#define S_BUTTON5	15

#define BUTTON1		0
#define BUTTON2		1
#define BUTTON3		2
#define BUTTON4		3
#define BUTTON5		4
#define EXIT	   -1

const char interfaceButton[MAX_BUTTON] = { 'o','+','_' ,'=' ,'-' ,'b' ,
											'1','2','3','4','5','!' ,
											'@' ,'#' ,'$' ,'%' };

//class defination
class PioneerCarRadio :public AmFmRadio
{

private:
	int	ExitCondition; // use to store exit condition if user want to exit program 
public:

	//constructor
	PioneerCarRadio();
	virtual ~PioneerCarRadio();
	//acessor
	int GetExitCondition(void);
	//mutators
	void SetExitCondition(int);
	//methode
	virtual int UserInterface(char);
	void IncreaseVolume(void);
	void DecreaseVolume(void);
	virtual void ShowCurrentSettings(void);

};