/*
*  FILE          : PioneerAM.h
*  PROJECT       : assign06
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 07-27-2019
*  DESCRIPTION   :
*	This file contains all the prototypes, class defination of PioneerAM and  all defination of mutators in this file  which is used in project assign06.
*/
#pragma once
#include"PioneerCarRadio.h"
#include"AmFmRadio.h"

class PioneerAM : public PioneerCarRadio
{

private:


public:

	/************************************************************************************************************
	Name	:	PioneerAM()---constructor
	Purpose :	This block of code create a object of type PioneerAM.
	Inputs	:	NONE
	Outputs	:	NONE
	Returns	:	Nothing
	/************************************************************************************************************/
	PioneerAM() :PioneerCarRadio()
	{

	}
	/**********************************************************************************************************************
	Name :		~PioneerAM -- DESTRUCTOR
	Purpose :	to destroy the PioneerAM object - free up the memory associated with the object
	Inputs :	NONE
	Outputs :   outputs a final message from the object before being destroyed
	Returns :	Nothing
	**********************************************************************************************************************/
	virtual ~PioneerAM(void)
	{
		cout << "Destroying Pioneer XS440-AM Radio Classs\n" << endl;
	}
	/************************************************************************************************************
	Name	:	ToggleBand()---Mutator
	Purpose :	This block of code will set the current band of radio to AM as this radio doesnot support FM it
				just set it to AM
	Inputs	:	NONE
	Outputs	:	NONE
	Returns	:	Nothing
	/************************************************************************************************************/
	virtual void ToggleBand(void)
	{
		SetFrequence(true);
	}
	/*************************************************************************************************************
	Name	:	ShowCurrentSettings()---Mutator
	Purpose :	This mutator will print out all the current setting state of the radio
	Inputs	:	NONE
	Outputs	:	SHow radio current setting
	Returns	:	NOTING
	/*************************************************************************************************************/
	virtual void ShowCurrentSettings(void)
	{
		cout << "*********************************************************" << endl;
		cout << "\n" << endl;
		cout << "Pioneer XS440-AM" << endl;
		if (IsRadioOn())
		{
			cout << "Radio is on" << endl;
			cout << "Volume:" << GetCurrentVolume() << endl;
			cout << "Current Station:   " << GetCurrentStation() << " " << GetFrequency() << endl;
			cout << "AM Buttons:" << endl;
			cout << "1:  " << setw(5) << GetButton()[0].AMFreqs << ",  ";
			cout << "2:  " << setw(5) << GetButton()[1].AMFreqs << ",  ";
			cout << "3:  " << setw(5) << GetButton()[2].AMFreqs << ",  ";
			cout << "4:  " << setw(5) << GetButton()[3].AMFreqs << ",  ";
			cout << "5:  " << setw(5) << GetButton()[4].AMFreqs << endl;
			cout << "\n\n\n" << endl;
		}
		else
		{
			cout << "Radio is off" << endl;
			cout << "\n\n\n" << endl;
		}

	}
};
