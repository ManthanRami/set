/*
*  FILE          : AmFmRadio.h
*  PROJECT       : AmFmRadio
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 06-22-2019
*  DESCRIPTION   :
*	This file contains all the prototypes, class defination and constant declaration which is used in project AmFmRadio.
*/
#pragma once
//AmFmRadio.h
#pragma warning(disable: 4996)

#define LOW_VOL 0
#define NUM_FREQ 5

#define MIN_AM 530
#define MAX_AM 1700

#define MIN_FM 87.9
#define MAX_FM 107.9

#define MIN_VOL 0
#define MAX_VOL 100

#define DIF_AM 10
#define DIF_FM 0.2

#define VOL_LEN 20




#ifndef _AmFmRadio_H
#define _AmFmRadio_H

struct Freqs
{
	int AMFreqs;
	double FMFreqs;
};

class AmFmRadio
{
private:

	Freqs	button[NUM_FREQ];
	double	current_station;
	char	frequency[3];
	int		volume;
	Freqs	lastStation;
	int		prev_volume;
	int		current_volume;
	bool	on;
	bool displayOutput;
	

public:


	//constructor
	AmFmRadio(bool);
	AmFmRadio(bool radio_cur_state, struct Freqs f_Array[NUM_FREQ]);

	//destructor
	~AmFmRadio();

	// Settora
	int SetFreqs(int Freqs_num);
	void SetStation(double);
	int SetButton(int);
	int SetVolume();
	int SetVolume(int);

	//Methods
	void SetdisplayOutput(bool);
	void ShowCurrentSettings();
	void PowerToggle();
	void ScanUp();
	void ScanDown();
	bool IsRadioOn();
	void ToggleFrequency();
	void ToggleBand();
	int SelectCurrentStation(int Freqs_num);

	//accessor
	double getCurrentStation();
	int getCurrentVolume();
	int getprev_Volume();
	char* getFrequency();

};
#endif