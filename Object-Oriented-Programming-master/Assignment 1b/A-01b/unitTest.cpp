/*
*  FILE          : unitTest.cpp
*  PROJECT       : A-01b
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 07-02-2019
*  DESCRIPTION   :This program will do a test harness on three overloaded function of assessGrade and print out result weather the test is passed or not,
				  Als0 containt defination of functional exceptional and boundary test defination.
*/
#include"assessGrade.h"
int main()
{
	int count = 0;
	int i = 0;
	unitTest test[MAX_TEST];
	fillStruct(test);
	void(*fTest)(unitTest) = &functionTest;
	void(*eTest)(unitTest) = &exceptionTest;
	void(*bTest)(unitTest) = &boundaryTest;
	void(*funcArray[])(unitTest test) = { fTest,eTest,bTest };
	while (count < MAX_TEST)
	{
		if (count < S_INDEX)
		{
			if (count < S_F_INDEX)
			{
				funcArray[FIRST_ELE](test[count]);
			}
			else
			{
				funcArray[SECOND_ELE](test[count]);
			}
		}
		if (count >= S_INDEX && count < D_INDEX)
		{
			if (count < D_F_INDEX)
			{
				funcArray[FIRST_ELE](test[count]);
			}
			else if (count >= D_F_INDEX && count < D_E_INDEX)
			{
				funcArray[SECOND_ELE](test[count]);
			}
			else
			{
				funcArray[THIRD_ELE](test[count]);
			}
		}
		if (count >= D_INDEX && count <= A_INDEX)
		{

			if (count < A_F_INDEX)
			{
				funcArray[FIRST_ELE](test[count]);
			}
			else
			{
				funcArray[SECOND_ELE](test[count]);
			}
		}
		count++;
	}
	return 0;
}
/*=======================================================================================================================================
* Function: functionTest()

 * Description:	This function will take a block containing test harness hardcoded values and do functional test on assessGrade

 * Parameter:	(unitTest block): - It contain test harness hardcoded values to be tested

 * Return Value returns nothing as return type is void

=======================================================================================================================================*/
void functionTest(unitTest block)
{
	int code = FAIL;
	if (block.grade != NULL)
	{
		cout << "Test# " << block.testNumber << " Function test of  " << block.functionName << endl;
		cout << "\t>> Submitting \"" << block.grade << "\" as the student's mark" << endl;
		cout << "\t\t>> Expect Result: " << block.expectedOutput << endl;
		cout << "\t\t>> Actual Result:";
		code = assessGrade((char*)block.grade);
		if (code == block.expectedResult)
		{
			cout << "\t**Test Passed**\n\n" << endl;
		}
		else
		{
			cout << "\t**Test failed**\n\n" << endl;
		}
	}
	else if (block.finalmark != NULL)
	{
		cout << "Test# " << block.testNumber << " Function test of  " << block.functionName << endl;
		cout << "\t>> Submitting \"" << block.finalmark << "\" as the student's mark" << endl;
		cout << "\t\t>> Expect Result: " << block.expectedOutput << endl;
		cout << "\t\t>> Actual Result:";
		code = assessGrade(block.finalmark);
		if (code == block.expectedResult)
		{
			cout << "\t**Test Passed**\n\n" << endl;
		}
		else
		{
			cout << "\t**Test failed**\n\n" << endl;
		}

	}
	else
	{
		cout << "Test# " << block.testNumber << " Function test of  " << block.functionName << endl;
		cout << "\t>> Submitting \"" << block.assignMarks[FIRST_ELE] << ","
			<< block.assignMarks[SECOND_ELE] << ","
			<< block.assignMarks[THIRD_ELE] << ","
			<< block.assignMarks[FOURTH_ELE] << ","
			<< block.assignMarks[FIFTH_ELE]
			<< "\" as the student's mark" << endl;
		cout << "\t\t>> Expect Result: " << block.expectedOutput << endl;
		cout << "\t\t>> Actual Result:";
		code = assessGrade(block.assignMarks);
		if (code == block.expectedResult)
		{
			cout << "\t**Test Passed**\n\n" << endl;
		}
		else
		{
			cout << "\t**Test failed**\n\n" << endl;
		}

	}
}
/*=======================================================================================================================================
* Function: exceptionTest()

 * Description:	This function will take a block containing test harness hardcoded values and do functional test on assessGrade

 * Parameter:	(unitTest block): - It contain test harness hardcoded values to be tested

 * Return Value returns nothing as return type is void

=======================================================================================================================================*/
void exceptionTest(unitTest block)
{
	int code = FAIL;
	if (block.grade != NULL)
	{
		cout << "Test# " << block.testNumber << " Exception test of  " << block.functionName << endl;
		cout << "\t>> Submitting \"" << block.grade << "\" as the student's mark" << endl;
		cout << "\t\t>> Expect Result: " << block.expectedOutput << endl;
		cout << "\t\t>> Actual Result:";
		code = assessGrade((char*)block.grade);
		if (code == block.expectedResult)
		{
			cout << "\t**Test Passed**\n\n" << endl;
		}
		else
		{
			cout << "\t**Test failed**\n\n" << endl;
		}
	}
	else if (block.finalmark != NULL)
	{
		cout << "Test# " << block.testNumber << " Exception test of  " << block.functionName << endl;
		cout << "\t>> Submitting \"" << block.finalmark << "\" as the student's mark" << endl;
		cout << "\t\t>> Expect Result: " << block.expectedOutput << endl;
		cout << "\t\t>> Actual Result:";
		code = assessGrade(block.finalmark);
		if (code == block.expectedResult)
		{
			cout << "\t**Test Passed**\n\n" << endl;
		}
		else
		{
			cout << "\t**Test failed**\n\n" << endl;
		}

	}
	else
	{
		cout << "Test# " << block.testNumber << " Exception test of  " << block.functionName << endl;
		cout << "\t>> Submitting \"" << block.assignMarks[FIRST_ELE] << ","
			<< block.assignMarks[SECOND_ELE] << ","
			<< block.assignMarks[THIRD_ELE] << ","
			<< block.assignMarks[FOURTH_ELE] << ","
			<< block.assignMarks[FIFTH_ELE]
			<< "\" as the student's mark" << endl;
		cout << "\t\t>> Expect Result: " << block.expectedOutput << endl;
		cout << "\t\t>> Actual Result:";
		code=assessGrade(block.assignMarks);
		if (code == block.expectedResult)
		{
			cout << "\t**Test Passed**\n\n" << endl;
		}
		else
		{
			cout << "\t**Test failed**\n\n" << endl;
		}

	}
}
/*=======================================================================================================================================
* Function: boundaryTest()

 * Description:	This function will take a block containing test harness hardcoded values and do functional test on assessGrade

 * Parameter:	(unitTest block): - It contain test harness hardcoded values to be tested

 * Return Value returns nothing as return type is void

=======================================================================================================================================*/
void boundaryTest(unitTest block)
{
	int code = 0;
	cout << "Test# " << block.testNumber << " Boundary test of  " << block.functionName << endl;
	printf("\t>> Submitting \"%.2f% \" as the student's mark\n", block.finalmark);
	cout << "\t\t>> Expect Result: " << block.expectedOutput << endl;
	cout << "\t\t>> Actual Result:";
	code=assessGrade(block.finalmark);
	if (code == block.expectedResult)
	{
		cout << "\t**Test Passed**\n\n" << endl;
	}
	else
	{
		cout << "\t**Test failed**\n\n" << endl;
	}

}
/*=======================================================================================================================================
* Function: fillStruct()

 * Description:	This function will fill the array of the struct with hardcoded value which are going to be tested.

 * Parameter:	(unitTest *test): - An address of an array of struct type

 * Return Value returns nothing as return type is void
=======================================================================================================================================*/
void fillStruct(unitTest *test)
{
	int i = 0;//counter
	int j = 0;//counter
	int index = 0;
	int testNumber = 1;
	int stringIndex = 0;  //counter
	int outputIndex = 0;  //counter
	int funcIndex = 0;	  //counter INCREASING INDEX OF ARRAY funcName 
	int markIndex = 0;	  //counter
	int fGradeIndex = 0;  //counter
	int assignmentMark[MAX_ARRAY][MAX_ELEMENT] = { {75,90,80,70,60},{56,65,34,87,98},{80,20},{34,54,67,23,42},{20,-10,90,80},{234,45,-656},{34,-65,466,76,-88},{-87,-98,-99,-35,99} };
	const char* studentGrade[MAX_GRADE] = { "A+","B+","DNA","AU", "A-", "Hello","Abcd","Z-" };
	double finalGrade[MAX_FINAL_MARK] = {80.0,79.20, 42.37,53.25, -23.5,234.1, -45.67,-345.2, 0.00,100.00 };
	const string exOutputs[MAX_OUTPUT] = { "Student achieved 95.00% which is a PASS condition",
											"Student achieved 77.00% which is a PASS condition",
											" Student has Special Situation: Did Not Attend",
											" Student has Special Situation: Audit",
											" INVALID \"String\" type grade",
											" INVALID \"String\" type grade",
											" INVALID \"String\" type grade",
											" INVALID \"String\" type grade",
											"Student achieved 80.0% which is a PASS condition",
											"Student achieved 79.20% which is a PASS condition",
											"Student achieved 42.3% which is a Fail condition",
											"Student achieved 53.25% which is a Fail condition",
											" INVALID \"double\" type grade",
											" INVALID \"double\" type grade",
											" INVALID \"double\" type grade",
											" INVALID \"double\" type grade",
											"Student achieved 0.00% which is a FAIL condition",
											"Student achieved 100.00% which is a PASS condition",
											"Student achieved 75.00% which is a PASS condition",
											"Student achieved 68.00% which is a PASS condition",
											"Student achieved 20.00% which is a FAIL condition",
											"Student achieved 44.00% which is a FAIL condition",
											" INVALID \"array\" type grade",
											" INVALID \"array\" type grade",
											" INVALID \"array\" type grade",
											" INVALID \"array\" type grade" };

	const string funcName[MAX_OUTPUT] = { "assessGrade(char*)",
											"assessGrade(char*)",
											"assessGrade(char*)",
											"assessGrade(char*)",
											"assessGrade(char*)",
											"assessGrade(char*)",
											"assessGrade(char*)",
											"assessGrade(char*)",
											"assessGrade(double)",
											"assessGrade(double)",
											"assessGrade(double)",
											"assessGrade(double)",
											"assessGrade(double)",
											"assessGrade(double)",
											"assessGrade(double)",
											"assessGrade(double)",
											"assessGrade(double)",
											"assessGrade(double)",
											"assessGrade(int[])",
											"assessGrade(int[])",
											"assessGrade(int[])",
											"assessGrade(int[])",
											"assessGrade(int[])",
											"assessGrade(int[])",
											"assessGrade(int[])",
											"assessGrade(int[])" };

	while (index < MAX_TEST)
	{
		if (index < S_INDEX)
		{
			test[index].finalmark = NULL;
			test[index].assignMarks[5] = NULL;
			test[index].dataType = STRING_VER;
			test[index].expectedResult = PASS;
			test[index].testNumber = testNumber;
			test[index].functionName = funcName[funcIndex];
			test[index].grade = studentGrade[stringIndex];
			test[index].expectedOutput = exOutputs[outputIndex];
			if (index < THIRD_ELE)
			{
				test[index].expectedResult = PASS;
			}
			else if(index<FIFTH_ELE)
			{
				test[index].expectedResult = SPECIAL_CONDITION;
			}
			else
			{
				test[index].expectedResult = INVALID_CONDITION;
			}
			stringIndex++;
			funcIndex++;
			outputIndex++;
			testNumber++;
		}
		else if (index >= S_INDEX && index < D_INDEX)
		{
			test[index].dataType = DOUBLE_VER;
			test[index].testNumber = testNumber;
			test[index].finalmark = finalGrade[fGradeIndex];
			test[index].functionName = funcName[funcIndex];
			test[index].expectedOutput = exOutputs[outputIndex];
			test[index].grade = NULL;
			test[index].assignMarks[5] = NULL;
			if (index < TENTH_ELE || index == SEVENTEEN_ELE)
			{
				test[index].expectedResult = PASS;
			}
			else if (index < TWELVETH_ELE ||index == SIXTEEN_ELE)
			{
				test[index].expectedResult = FAIL;
			}
			else
			{
				test[index].expectedResult = INVALID_CONDITION;
			}
			fGradeIndex++;
			testNumber++;
			funcIndex++;
			outputIndex++;
		}
		else if (index >= D_INDEX && index < A_INDEX)
		{
			test[index].dataType = ASSIGNMENT_VER;
			test[index].testNumber = testNumber;
			test[index].functionName = funcName[funcIndex];
			test[index].expectedOutput = exOutputs[outputIndex];
			test[index].finalmark = NULL;
			test[index].grade = NULL;
			if (index < TWENTIENTH_ELE)
			{
				test[index].expectedResult = PASS;
			}
			else if (index < TWENTYSECOND_ELE)
			{
				test[index].expectedResult = FAIL;
			}
			else
			{
				test[index].expectedResult = INVALID_CONDITION;
			}
			while (markIndex < LOOP_TIME)
			{
				test[index].assignMarks[markIndex] = assignmentMark[i][j];
				j++;//counter
				markIndex++;
			}
			i++;//counter
			j = 0;//counter
			outputIndex++;
			funcIndex++;
			testNumber++;
			markIndex = 0;
		}
		index++;
	}
}