/*
*  FILE          : assessGrade.h
*  PROJECT       : A-01b
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 07-02-2019
*  DESCRIPTION   :This file contains the prototypes, constant and include files for the A-01b project.
				  These are all of the prototypes for the various functions found within the source files.
*/
#pragma once
#include<stdio.h>
#include<string.h>
#include<string>
#include<iostream>
#include<conio.h>
#include<ctype.h>
#include <iomanip>

using namespace std;
#pragma warning(disable: 4996)
#pragma warning(disable: 4476)
//Constant variable declaration
#define ASSIGNMENT_NUMBER 5
#define MAX_CONDITION 26
#define MAX_MARK 100
#define MIN_MARK 0
#define PASS_MARK 54.50

#define LOOP_TIME 5
#define PASS 1
#define FAIL 2
#define SPECIAL_CONDITION 3
#define INVALID_CONDITION 4


#define GRADE_INDEX 16
#define STOP_SEARCH 25

#define MAX_FINAL_MARK 10
#define MAX_GRADE 8
#define MAX_ARRAY 8
#define	MAX_ELEMENT 5

#define MAX_LEN 100
#define MAX_SIZE 5
#define MAX_TEST 26
#define MAX_OUTPUT 26

#define FIRST_ELE 0
#define SECOND_ELE 1
#define THIRD_ELE 2
#define FOURTH_ELE 3
#define FIFTH_ELE 4
#define TENTH_ELE 10
#define SEVENTEEN_ELE 17
#define SIXTEEN_ELE 17
#define TWELVETH_ELE 12
#define TWENTIENTH_ELE 20
#define TWENTYSECOND_ELE 22

#define S_INDEX 8	//STRING  INDEX
#define D_INDEX 18	//DOUBLE  INDEX
#define A_INDEX 26	//ARRAY   INDEX

#define S_F_INDEX 4		//STRING FUNCTIONAL  INDEX
#define D_F_INDEX 12	//DOUBLE FUNCTIONAL  INDEX
#define D_E_INDEX 16	//DOUBLE EXCEPTIONAL INDEX
#define A_F_INDEX 22	//ARRAY  FUNCTIONAL  INDEX

#define STRING_VER 1	//STRING	 VERSION OF FUNCTION TO BE CALLED
#define DOUBLE_VER 2	//DOUBLE	 VERSION OF FUNCTION TO BE CALLED
#define ASSIGNMENT_VER 3//ASSIGNMENT VERSION OF FUNCTION TO BE CALLED

typedef struct unitTest
{
	int dataType;			 //contain int value which function to call
	int testNumber;			 //which number of test is going
	string functionName;	 //which function we are calling 
	string expectedOutput;	//our expected result
	int expectedResult;	//our expected result
	int assignMarks[MAX_SIZE]; //array contain assignment marks 
	double finalmark;//final marks
	const char* grade;//final grade

}unitTest;

// Prototype(required function)
int assessGrade(char* marks);
int assessGrade(double marks);
int assessGrade(int marks[5]);

// Prototype(additional function)
int checking(double marks);
int checking(int[] );
int checking(char input[]);
int findingAverage(int marks);
void emptyArray(int mark[]);

// Prototype(Unit Test function)
void fillStruct(unitTest *test);
void functionTest(unitTest block);
void exceptionTest(unitTest block);
void boundaryTest(unitTest block);