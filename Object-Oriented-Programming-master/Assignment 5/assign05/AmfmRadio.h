/*
*  FILE          : AmFmRadio.h
*  PROJECT       : assign05
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 07-20-2019
*  DESCRIPTION   :
*	This file contains all the prototypes, class defination and constant declaration which is used in project assign05.
*/
#include<iostream>
#include<iomanip>
#include<conio.h>
//AmFmRadio.h
#pragma warning(disable: 4996)
using namespace std;

#define LOW_VOL 0
#define NUM_FREQ 5

#define MIN_AM 530
#define MAX_AM 1700

#define MIN_FM 87.9
#define MAX_FM 107.9

#define MIN_VOL 0
#define MAX_VOL 100

#define DIF_AM 10
#define DIF_FM 0.2

#define VOL_LEN 20




#ifndef _AmFmRadio_H
#define _AmFmRadio_H

struct Freqs
{
	int AMFreqs;
	double FMFreqs;
};

class AmFmRadio
{
private:

	Freqs	button[NUM_FREQ];	//used to store value of AM & FM buttons value
	double	current_station;	//used to save current station
	char	frequency[3];		//used to store current band AM or FM
	int		volume;				//used to save current volume
	Freqs	lastStation;		//used to save last station value before turning off radio
	int		prev_volume;		//used to save current volume into this data member before turning off radio.
	bool	on;					//ussed to save  current radio state ON or OFF by boolean value
	bool displayOutput;			//used to save dispaly output condition 


public:


	//constructor
	AmFmRadio(bool);
	AmFmRadio(bool radio_cur_state, struct Freqs f_Array[NUM_FREQ]);

	//destructor
	 ~AmFmRadio();

	// Settor
	void SetStation(double);
	//sets button with current station by being passed a button number
	int SetButton(int);
	//sets volume
	int SetVolume();
	int SetVolume(int);

	//Methods
	void SetdisplayOutput(bool);
	//shows volume, button settings, current station, AM or FM
	void ShowCurrentSettings();
	void PowerToggle();
	//changes frequency up in increments of .2 for FM, 10 for AM
	void ScanUp();
	//changes frequency up in decrements of .2 for FM, 10 for AM
	void ScanDown();

	//returns a true if the radio is currently powered on, and false if the radio is in
	//the off position
	bool IsRadioOn();
	//toggles frequency between AM and FM and sets current station
	void ToggleFrequency();
	//toggles frequency between AM and FM and sets current station 
	void ToggleBand();
	//sets current station by being passed a button number
	int SelectCurrentStation(int Freqs_num);

	//accessor
	double GetCurrentStation();
	int GetCurrentVolume();
	int Getprev_Volume();
	char* GetFrequency();
	Freqs* GetButton(void);

};
#endif