/*
*  FILE          : AmFmRadio.cpp
*  PROJECT       : assign05
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 07-20-2019
*  DESCRIPTION   :
*	This file contains all the defination of construction, destruction, mutators,accessor and other methods which is used in project assign05.
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "AmFmRadio.h"

/**********************************************************************************************************************
	Name :		AmFmRadio -- CONSTRUCTOR
	Purpose :	To instantiate a new Radio object - given a set of attribute values
	Inputs :	radioCondition		bool		radio status
	Outputs :   NONE
	Returns :	Nothing
**********************************************************************************************************************/
AmFmRadio::AmFmRadio(bool radioCondition = false)
{
	int count = 0;
	on = radioCondition;
	do
	{
		button[count].AMFreqs = MIN_AM;
		button[count].FMFreqs = MIN_FM;
		count++;
	} while (count < NUM_FREQ);
	lastStation.AMFreqs = MIN_AM;
	lastStation.FMFreqs = MIN_FM;
	strcpy(frequency, "FM");
	volume = MIN_VOL;
	prev_volume = volume;
	current_station = MIN_FM;
}

/**********************************************************************************************************************
	Name :		AmFmRadio -- CONSTRUCTOR
	Purpose :	To instantiate a new AmFmRadio object - given a set of attribute values
	Inputs :	radio_cur_state		bool			radio status
				fq_Array			struct Freqs	initial radio preset value
	Outputs :   NONE
	Returns :	Nothing
**********************************************************************************************************************/
AmFmRadio::AmFmRadio(bool radio_cur_state, struct Freqs fq_Array[NUM_FREQ])
{
	on = radio_cur_state;
	int count = 0;

	do
	{
		button[count].AMFreqs = fq_Array[count].AMFreqs;
		button[count].FMFreqs = fq_Array[count].FMFreqs;
		count++;
	} while (count < NUM_FREQ);

	current_station = MIN_AM;
	volume = MIN_VOL;
	strcpy(frequency, "AM");
	prev_volume = volume;
}
/**********************************************************************************************************************
	Name :		AmFmRadio -- DESTRUCTOR
	Purpose :	to destroy the AmFmRadio object - free up the memory associated with the object
	Inputs :	NONE
	Outputs :   outputs a final message from the object before being destroyed
	Returns :	Nothing
**********************************************************************************************************************/
AmFmRadio:: ~AmFmRadio()
{
	printf("Destroying AmFmRadio");
}
/*************************************************************************************************************
	Name	:	PowerToggle()---Mutator
	Purpose :	This mutator will toggle between on and off radio state.
	Inputs	:	NONE
	Outputs	:	NONE
	Returns	:	NOTING
/*************************************************************************************************************/
void AmFmRadio::PowerToggle()
{
	if (on == false)
	{
		on = true;
		lastStation.FMFreqs = GetCurrentStation();
		volume = prev_volume;
	}
	else
	{
		on = false;
		prev_volume = volume;
		volume = LOW_VOL;
	}
}
/*************************************************************************************************************
	Name	:	IsRadioOn()---Mutator
	Purpose :	This mutator will send a single back indicating radio is on
	Inputs	:	NONE
	Outputs	:	NONE
	Returns	:	on		bool	radion state
/*************************************************************************************************************/
bool AmFmRadio::IsRadioOn()
{
	return on;
}
/*************************************************************************************************************
	Name	:	SetButton()---Mutator
	Purpose :	This mutator will set frequency
	Inputs	:	Freqs_num	int		value of freq
	Outputs	:	NONE
	Returns	:	1 or 0
/*************************************************************************************************************/
int AmFmRadio::SetButton(int Freqs_num)
{
	if ((Freqs_num >= 0) && (Freqs_num <= 4))
	{
		if (strcmp("AM", frequency) == 0)
		{
			button[Freqs_num].AMFreqs = (int)current_station;
		}
		else
		{
			button[Freqs_num].FMFreqs = current_station;
		}
		return 1;
	}
	return 0;
}
/*************************************************************************************************************
	Name	:	ToggleBand()---Mutator
	Purpose :	This mutator will switch radio frequency
	Inputs	:	NONE
	Outputs	:	NONE
	Returns	:	NOTHING
/*************************************************************************************************************/
void AmFmRadio::ToggleBand()
{
	if (strcmp(frequency, "AM") == 0)
	{
		lastStation.AMFreqs = (int)GetCurrentStation();
		strcpy(frequency, "FM");
		SetStation(lastStation.FMFreqs);
	}
	else
	{
		lastStation.FMFreqs = GetCurrentStation();
		strcpy(frequency, "AM");
		SetStation(lastStation.AMFreqs);
	}
}
/*************************************************************************************************************
	Name	:	SetVolume()---Mutator
	Purpose :	This mutator will prompt user for setting volume of the radio.
	Inputs	:	NONE
	Outputs	:	statement prompting user for entering volume
	Returns	:	int		1 if volume is in range, 2 if volume is set to maximum, 0 if the volume is set to min.
/*************************************************************************************************************/
int AmFmRadio::SetVolume()
{
	char buf[VOL_LEN] = "";

	printf("\nEnter the volume level (0 - 100). ");
	fgets(buf, sizeof buf, stdin);
	volume = atoi(buf);
	prev_volume = volume;

	if (volume < MIN_VOL) //if user enters volume less than 0, volume = 0
	{
		volume = MIN_VOL;
		return 0;
	}
	if (volume > MAX_VOL) //if user enters volume greater than 100, volume = 100
	{
		volume = MAX_VOL;
		return 2;
	}
	return 1;
}
/*************************************************************************************************************
	Name	:	SetVolume()---Mutator
	Purpose :	This mutator will prompt user for setting volume of the radio.
	Inputs	:	vol		int		value to be set  for volume
	Outputs	:	NONE
	Returns	:	int		1 if volume is in range, 2 if volume is set to maximum, 0 if the volume is set to min.
/**************************************************************************************************************/
int AmFmRadio::SetVolume(int vol)
{
	if (vol < MIN_VOL)
	{
		volume = MIN_VOL;
		return 0;
	}
	else if (vol >= MAX_VOL)
	{
		volume = MAX_VOL;
		return 2;
	}
	volume = vol;
	return 1;
}
/*************************************************************************************************************
	Name	:	ToggleFrequency()---Mutator
	Purpose :	This mutator will switch radio frequency
	Inputs	:	NONE
	Outputs	:	NONE
	Returns	:	NOTHING
/*************************************************************************************************************/
void AmFmRadio::ToggleFrequency()
{
	if (strcmp(frequency, "AM") == 0)
	{
		lastStation.AMFreqs = (int)current_station;
		strcpy(frequency, "FM");
		current_station = lastStation.AMFreqs;
	}
	else
	{
		lastStation.FMFreqs = current_station;
		strcpy(frequency, "AM");
		current_station = lastStation.FMFreqs;
	}
}
/*************************************************************************************************************
	Name	:	SelectCurrentStation()---Mutator
	Purpose :	This mutator will set the current_station data member of the object
	Inputs	:	Freqs_num		int		Number of the button holding value
	Outputs	:	NONE
	Returns	:	NOTING
/*************************************************************************************************************/
int AmFmRadio::SelectCurrentStation(int Freqs_num)
{
	if ((Freqs_num >= 0) && (Freqs_num <= 4))
	{
		if (strcmp("AM", frequency) == 0)
		{
			current_station = button[Freqs_num].AMFreqs;
		}
		else
		{
			current_station = button[Freqs_num].FMFreqs;
		}
		return 1;
	}
	return 0;
}
/*************************************************************************************************************
	Name	:	ShowCurrentSettings()---Mutator
	Purpose :	This mutator will print out all the current setting state of the radio
	Inputs	:	NONE
	Outputs	:	SHow radio current setting
	Returns	:	NOTING
/*************************************************************************************************************/
void AmFmRadio::ShowCurrentSettings()
{
	if (on == true)
	{
		printf("\n\nRadio is on. \n");
	}
	else
	{
		printf("\n\nRadio is off. \n");
	}
	printf("\nFrequency: %s\n", frequency);
	printf("Volume: %d\n", volume);
	printf("Current Station: %.1f %s\n", current_station, frequency);
	printf("AM Freqs Settings: ");
	for (int i = 0; i < 5; ++i)
	{
		printf("%d) %6d ", i + 1, button[i].AMFreqs);
	}

	printf("\nFM Freqs Settings: ");
	for (int j = 0; j < 5; ++j)
	{
		printf("%d) %6.1f ", j + 1, button[j].FMFreqs);
	}
}
/*************************************************************************************************************
	Name	:	ScanDown()---Mutator
	Purpose :	This mutator will scan up the frequency up in case of AM it will increase by 10 and
				incase of FM it will increase by 0.2
	Inputs	:	NONE
	Outputs	:	NONE
	Returns	:	NOTING
/*************************************************************************************************************/
void AmFmRadio::ScanUp()
{
	if (strcmp("AM", frequency) == 0)
	{
		//if current_station is 1700, the current_station becomes 530
		if (current_station == MAX_AM)
		{
			current_station = MIN_AM;
		}
		else
		{
			current_station = current_station + DIF_AM;

		}
		lastStation.AMFreqs = (int)GetCurrentStation();
	}
	else
	{
		//if the current_station is 107.9, the current_station becomes 87.9
		//Note: car radios jump .2 for the FM. That's how it's modeled here.
		if (current_station >= MAX_FM)
		{
			current_station = MIN_FM;
		}
		else
		{
			current_station = current_station + DIF_FM;
		}
		lastStation.FMFreqs = GetCurrentStation();
	}
	if (displayOutput)
	{
		printf("\nCurrent station: %f %s\n", current_station, frequency);
	}
}
/*************************************************************************************************************
	Name	:	ScanDown()---Mutator
	Purpose :	This mutator will scan up the frequency up in case of AM it will decrease by 10 and
				incase of FM it will decrease by 0.2
	Inputs	:	NONE
	Outputs	:	NONE
	Returns	:	NOTING
/*************************************************************************************************************/
void AmFmRadio::ScanDown()
{
	if (strcmp("AM", frequency) == 0)
	{
		//if current_station is 1700, the current_station becomes 530
		if (current_station == MIN_AM)
		{
			current_station = MAX_AM;
		}
		else
		{
			current_station = current_station - DIF_AM;
		}
		lastStation.AMFreqs = (int)GetCurrentStation();
	}
	else
	{
		//if the current_station is 107.9, the current_station becomes 87.9
		//Note: car radios jump .2 for the FM. That's how it's modeled here.
	if (current_station <= MIN_FM)
		{
			current_station = MAX_FM;
		}
		else
		{
			current_station = current_station - DIF_FM;
		}
		lastStation.FMFreqs = GetCurrentStation();
	}
	if (displayOutput)
	{
		printf("\nCurrent station: %f %s\n", current_station, frequency);
	}
}
/*************************************************************************************************************
	Name	:	SetdisplayOutput()---Mutator
	Purpose :	This mutator will set the dispayOutput data member of the object.
	Inputs	:	state		bool	state of radio in form of true and false
	Outputs	:	NONE
	Returns	:	NOTING
/*************************************************************************************************************/
void AmFmRadio::SetdisplayOutput(bool state)
{
	displayOutput = state;
}
/*************************************************************************************************************
	Name	:	SetStation()---Mutator
	Purpose :	This block of code set the current_station data member of the class
	Inputs	:	station	double	station value
	Outputs	:	NONE
	Returns	:	NOTING
/*************************************************************************************************************/
void AmFmRadio::SetStation(double station)
{
	current_station = station;
}
/*************************************************************************************************************
	Name	:	GetCurrentStation()---Accessor
	Purpose :	This block of code returns current station of thw radio
	Inputs	:	NONE
	Outputs	:	NONE
	Returns	:	current_station	double	current station of the radio
/*************************************************************************************************************/
double AmFmRadio::GetCurrentStation()
{
	return current_station;
}
/*************************************************************************************************************
	Name	:	GetCurrentVolume()---Accessor
	Purpose :	This block of code returns current volume of thw radio
	Inputs	:	NONE
	Outputs	:	NONE
	Returns	:	current_volume	int	current volume of the radio
/*************************************************************************************************************/
int AmFmRadio::GetCurrentVolume()
{
	return volume;
}
/************************************************************************************************************
	Name	:	GetFrequency()---Accessor
	Purpose :	This block of code returns current Frequency of thw radio
	Inputs	:	NONE
	Outputs	:	NONE
	Returns	:	Frequency	char*	Frequency of the radio
/************************************************************************************************************/
char* AmFmRadio::GetFrequency()
{
	return frequency;
}
/************************************************************************************************************
	Name	:	Getprev_Volume()---Accessor
	Purpose :	This block of code returns current volume of thw radio
	Inputs	:	NONE
	Outputs	:	NONE
	Returns	:	volume	int	volume of the radio
/************************************************************************************************************/
int AmFmRadio::Getprev_Volume()
{
	return prev_volume;
}
/************************************************************************************************************
	Name	:	GetButton()---Accessor
	Purpose :	This block of code returns current button value  of the radio
	Inputs	:	NONE
	Outputs	:	NONE
	Returns	:	button	current	value of the radio
/************************************************************************************************************/
 Freqs* AmFmRadio::GetButton(void)
 {
	return button;
 }