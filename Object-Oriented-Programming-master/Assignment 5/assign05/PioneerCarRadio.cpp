/*
*  FILE          : PioneerCarRadio.cpp
*  PROJECT       : assign05
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 07-20-2019
*  DESCRIPTION   :
*	This file contains all the defination of construction, destruction, mutators,accessor and other methods which is used in project assign05.
*/
#include"AmfmRadio.h"
#include"PioneerCarRadio.h"

/**********************************************************************************************************************
	Name :		PioneerCarRadio -- CONSTRUCTOR
	Purpose :	To instantiate a new PioneerCarRadio object - given a set of attribute values
	Inputs :	radioCondition		bool		radio status
	Outputs :   NONE
	Returns :	Nothing
**********************************************************************************************************************/
PioneerCarRadio::PioneerCarRadio() :AmFmRadio(false)
{
	SetdisplayOutput(false);
}
/*************************************************************************************************************
	Name	:	UserInterface()---Mutator
	Purpose :	This mutator will call diffrent methode of radio setting and change radio  state
	Inputs	:	buffer char: input provided by the user in main function.
	Outputs	:	NONE
	Returns	:	0 Or EXIT as -1 if user want to quit program
/*************************************************************************************************************/
int PioneerCarRadio::UserInterface(char buffer)
{
	if (buffer == interfaceButton[ON_OFF])
	{
		PowerToggle();
	}
	else if (buffer == interfaceButton[INC_VOL])
	{
		IncreaseVolume();
	}
	else if (buffer == interfaceButton[DEC_VOL])
	{
		DecreaseVolume();
	}
	else if (buffer == interfaceButton[SCAN_UP])
	{
		ScanUp();
	}
	else if (buffer == interfaceButton[SCAN_DOWN])
	{
		ScanDown();
	}
	else if (buffer == interfaceButton[AM_FM])
	{
		ToggleBand();
	}
	else if (buffer == interfaceButton[C_BUTTON1])
	{
		SelectCurrentStation(BUTTON1);
	}
	else if (buffer == interfaceButton[C_BUTTON2])
	{
		SelectCurrentStation(BUTTON2);
	}
	else if (buffer == interfaceButton[C_BUTTON3])
	{
		SelectCurrentStation(BUTTON3);
	}
	else if (buffer == interfaceButton[C_BUTTON4])
	{
		SelectCurrentStation(BUTTON4);
	}
	else if (buffer == interfaceButton[C_BUTTON5])
	{
		SelectCurrentStation(BUTTON5);
	}
	else if (buffer == interfaceButton[S_BUTTON1])
	{
		SetButton(BUTTON1);
	}
	else if (buffer == interfaceButton[S_BUTTON2])
	{
		SetButton(BUTTON2);
	}
	else if (buffer == interfaceButton[S_BUTTON3])
	{
		SetButton(BUTTON3);
	}
	else if (buffer == interfaceButton[S_BUTTON4])
	{
		SetButton(BUTTON4);
	}
	else if (buffer == interfaceButton[S_BUTTON5])
	{
		SetButton(BUTTON5);
	}
	else if (buffer == 'x')
	{
		return EXIT;
	}
	return 0;
}
/*************************************************************************************************************
	Name	:	IncreaseVolume()---Method
	Purpose :	This mutator will increase the current volume of the radio by 1.
	Inputs	:	NONE
	Outputs	:	NONE
	Returns	:	NOTHING
/*************************************************************************************************************/
void PioneerCarRadio::IncreaseVolume(void)
{
	int temp_vol = 0;
	temp_vol = GetCurrentVolume();
	++temp_vol;
	SetVolume(temp_vol);
}
/*************************************************************************************************************
	Name	:	DecreaseVolume()---Method
	Purpose :	This mutator will decrease the current volume of the radio by 1.
	Inputs	:	NONE
	Outputs	:	NONE
	Returns	:	NOTHING
/*************************************************************************************************************/
void PioneerCarRadio::DecreaseVolume(void)
{
	int temp = 0;
	temp = GetCurrentVolume();
	--temp;
	SetVolume(temp);
}
/*************************************************************************************************************
	Name	:	SetExitCondition()---Mutator
	Purpose :	This mutator will set the value of "ExitCondition" data member.
	Inputs	:	int exit: Exit condition returns from interface function
	Outputs	:	NONE
	Returns	:	NOTHING
/*************************************************************************************************************/
void PioneerCarRadio::SetExitCondition(int exit)
{
	ExitCondition = exit;
}
/*************************************************************************************************************
	Name	:	SetExitCondition()---Acessor
	Purpose :	This mutator will return value of "ExitCondition" data member
	Inputs	:	NONE
	Outputs	:	NONE
	Returns	:	ExitCondition value
/*************************************************************************************************************/
int PioneerCarRadio::GetExitCondition(void)
{
	return ExitCondition;
}
/*************************************************************************************************************
	Name	:	ShowRadioSetting()---Mutator
	Purpose :	This mutator will print out all the current setting state of the radio
	Inputs	:	NONE
	Outputs	:	SHow radio current setting
	Returns	:	NOTING
/*************************************************************************************************************/
void PioneerCarRadio::ShowRadioSetting(void)
{
	cout << "*********************************************************" << endl;
	cout << "\n"<<endl;
	cout << "Pioneer XS440" << endl;
	if (IsRadioOn())
	{
		cout << "Radio is on"<<endl;
		cout << "Volume:" <<GetCurrentVolume() << endl;
		cout << "Current Station:   " << GetCurrentStation()<<" "<<GetFrequency() << endl;
		cout << "AM Buttons:" << endl;
		cout << "1:  " << setw(5) << GetButton()[0].AMFreqs<<",  ";
		cout << "2:  " << setw(5) << GetButton()[1].AMFreqs<<",  ";
		cout << "3:  " << setw(5) << GetButton()[2].AMFreqs<<",  ";
		cout << "4:  " << setw(5) << GetButton()[3].AMFreqs<<",  ";
		cout << "5:  " << setw(5) << GetButton()[4].AMFreqs << endl;
		cout << "FM Buttons:" << endl;
		cout << "1:  " << setw(5) << GetButton()[0].FMFreqs << ",  ";
		cout << "2:  " << setw(5) << GetButton()[1].FMFreqs << ",  ";
		cout << "3:  " << setw(5) << GetButton()[2].FMFreqs << ",  ";
		cout << "4:  " << setw(5) << GetButton()[3].FMFreqs << ",  ";
		cout << "5:  " << setw(5) << GetButton()[4].FMFreqs << endl;
		cout << "\n\n\n" << endl;
	}
	else
	{
		cout << "Radio is off" << endl;
		cout << "\n\n\n" << endl;
	}
	
}
/**********************************************************************************************************************
	Name :		PioneerCarRadio -- DESTRUCTOR
	Purpose :	to destroy the PioneerCarRadio object - free up the memory associated with the object
	Inputs :	NONE
	Outputs :   outputs a final message from the object before being destroyed
	Returns :	Nothing
**********************************************************************************************************************/
PioneerCarRadio:: ~PioneerCarRadio()
{
	printf("Destroying PioneerCarRadio\n");
}