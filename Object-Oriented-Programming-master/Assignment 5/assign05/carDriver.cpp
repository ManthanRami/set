/*
*  FILE          : carDriver.cpp
*  PROJECT       : assign05
*  PROGRAMMER    : Manthan Rami
*  DATE			 : 07-20-2019
*  DESCRIPTION   :
*	This program will create a dynamically created  myRadio radio object by using parent class
	AmFmRadio by concept of inheritence and also take a character as a user input and play with
	setting of myRadio.
*/
#include"AmfmRadio.h"
#include"PioneerCarRadio.h"

int main(void)
{
	char buffer = NULL;
	PioneerCarRadio*myRadio = NULL;

	myRadio = new PioneerCarRadio();	//dinamically created PioneerCarRadio object 
	if (myRadio == NULL)	//checking for memory
	{
		cout << "Not Enough Memory to inistance objecr" << endl;
	}
	myRadio->ShowRadioSetting();	//displaying current settings
	while (true)		//loop
	{
		buffer=	getch();
		myRadio->SetExitCondition(myRadio->UserInterface(buffer));
		if (myRadio->GetExitCondition() == EXIT)					//checking for exit conditon
		{
			delete myRadio;	//delete dynamically created myRadio object
			break;
		}
		myRadio->ShowRadioSetting(); //displaying current settings
	}
	return 0;
}